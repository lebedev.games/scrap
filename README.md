# Waste Collectors Management Service

![](readme/stage-1.png)
![](readme/stage-2.png)
![](readme/stage-3.png)

### Preparation

1. Install Rust
    ```shell script
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
    ```
2. Install Node
    ```shell script
    brew install node
    ```
3. Install Yarn
    ```shell script
    brew install yarn
    ```

### Debug Rust service only (manually)

1. Set up database. Data changes will be persisted
2. Open `scrap/service` folder in CLion
3. Run debug/release configuration with default environment variables
4. Send to service any http request from `examples` folder (or in another preferred way: postman, curl, etc)
5. Connect to database with any client to analyze data

### Debug Rust service via BDD tests

1. Set up database. In this case no data changes will be persisted 
   because of database `scrap` recreation from template `scrap_template` for each test scenario.
2. Open `scrap/service` folder in CLion
3. Run debug/release configuration with default environment variables
4. Open `scrap` folder in PyCharm
5. Mark `scrap/bdd` folder as sources root
6. Build a "scrap-bdd-dev" Docker image to use the inside Python interpreter
    ```shell script
    docker build -t scrap-bdd-dev -f ./bdd/Dockerfile ./bdd
    ```
7. Run any test from `scrap/bdd/features` with default environment variables
8. Configure your database client to auto close session to prevent database recreation in BDD environment

### Debug web (manually)

1. Set up database. Data changes will be persisted
2. Open `scrap/service` folder in CLion
3. Run debug/release configuration with default environment variables
4. Open `scrap` folder in PyCharm   
5. Open `scrap/web/project.json` and start from scripts
6. Otherwise you can run following command in `scrap/web`:
    ```shell script
    yarn start
    ```
 
### Reproduce CI testing

Run following commands in `scrap` folder:

```shell script
docker-compose down --remove-orphans --volumes
docker-compose up -d postgres
docker-compose up --build database
docker-compose up --build -d service
docker-compose up --build bdd
```

### Reproduce CI deploy

Run following commands in `scrap` folder:

```shell script
export REACT_APP_SERVICE_URL="http://localhost/api"
docker-compose down --remove-orphans --volumes
docker-compose up -d postgres
docker-compose up --build database
docker-compose up --build -d service web router
``` 
 
### Database 

Run postgres

```shell script
docker run -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=scrap -p 65432:5432 -d postgres:12.4
```

Build a "scrap-database-dev" Docker image to use alembic versioning of database:

```shell script
docker build -t scrap-database-dev -f ./database/Dockerfile ./database
```

Generate new migration:

```shell script
docker run -v $(pwd)/database:/opt/database --rm -it scrap-database-dev revision -m "name of changes"
```

Write SQL in generated migration file:

```shell script
./database/alembic/versions/2020-09-17_468b569df431_name_of_changes.py
```

Migrate to new database version:

```shell script
docker run --network="host" -v $(pwd)/database:/opt/database --rm -it -e DATABASE_URL=postgres://postgres:postgres@127.0.0.1:65432/scrap scrap-database-dev upgrade head
```

**NOTE:** additional database `scrap_template` will be created via migration to use in testing process as database template 

 