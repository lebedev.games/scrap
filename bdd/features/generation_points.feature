# Created by artem at 12.10.2020
Feature: Waste generation points
  # Enter feature description here
#
#  Scenario: Create generation point
#    Given generation point internal_system_number "GP"
#    Given generation point external_registry_number "GP-1"
#    Given generation point waste_type "ТКО"
#    Given generation point organization_legal_type "ТСЖ"
#    Given generation point operational_status "активная"
#    Given generation point full_address "St. Petersburg"
#    Given generation point coordinates "100, 100"
#    Given generation point area_m2 "50.05"
#    Given generation point covering_type "грунт"
#    Given generation point fence_material "бетон"
#    Given generation point service_company_name "Test Company"
#    Given generation point generator "00000000-00000000-00000000-00000000"
#    Given generation point description "Test description"
#    Given generation point containers
#      | container_type   | number_of_containers |
#      | "0.75м3 пластик" | 1                    |
#      | "6м3 металл"     | 2                    |
#    Given generation point transporter
#      | property                  | value                                 |
#      | transporter               | "00000000-00000000-00000000-00000003" |
#      | frequency                 | often                                 |
#      | start_transportation_date | "2018-06-29T00:00:00.00Z"            |
#      | end_transportation_date   | "2018-06-29T00:00:00.00Z"            |
#    Given generation point photos
#      | photo                                 |
#      | "00000000-00000000-00000000-00000004" |
#      | "00000000-00000000-00000000-00000005" |
#      | "00000000-00000000-00000000-00000006" |
#    Given generation point accrual_method "test"
#    Given generation point measurement_unit "м3"
#
#    When I create generation point
#    When I get generation point
#
#    Then generation point internal_system_number "GP"
#    Then generation point external_registry_number "GP-1"
#    Then generation point waste_type "ТКО"
#    Then generation point organization_legal_type "ТСЖ"
#    Then generation point operational_status "активная"
#    Then generation point full_address "St. Petersburg"
#    Then generation point coordinates "100, 100"
#    Then generation point area_m2 "50.05"
#    Then generation point covering_type "грунт"
#    Then generation point fence_material "бетон"
#    Then generation point service_company_name "Test Company"
#    Then generation point generator "00000000-00000000-00000000-00000000"
#    Then generation point description "Test description"
#    Then generation point containers
#      | container_type   | number_of_containers |
#      | "0.75м3 пластик" | 1                    |
#      | "6м3 металл"     | 2                    |
#    Then generation point transporter
#      | property                  | value                                 |
#      | transporter               | "00000000-00000000-00000000-00000003" |
#      | frequency                 | often                                 |
#      | start_transportation_date | "2018-06-29T00:00:00.00Z"            |
#      | end_transportation_date   | "2018-06-29T00:00:00.00Z"            |
#    Then generation point photos
#      | photo                                 |
#      | "00000000-00000000-00000000-00000004" |
#      | "00000000-00000000-00000000-00000005" |
#      | "00000000-00000000-00000000-00000006" |
#    Then generation point accrual_method "test"
#    Then generation point measurement_unit "м3"