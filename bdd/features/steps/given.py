from dataclasses import dataclass
from datetime import datetime
from decimal import Decimal
from uuid import UUID

import dateutil.parser
from behave import given
from behave import register_type

from features.client import BankDetails
from features.client import BusinessEntityModel
from features.client import EntrepreneurModel
from features.client import GenerationPointFormContainer
from features.client import GenerationPointFormTransporter
from features.client import RegisterClientForm
from features.client import RegisterTransportationForm
from features.client import ResponsiblePerson
from features.client import TransportationDestination
from features.client import WasteCollection
from features.environment import StepContext


def parse_datetime(representation: str) -> datetime:
    return dateutil.parser.isoparse(representation)


WASTE_TYPE_ID_TO_NAME = dict([
    (UUID('529a07b6-fa73-4536-ac94-fabf95b9ff63'), 'ТКО'),
    (UUID('a05ed0f4-dca0-4e25-a374-047d2870eddc'), 'КГО')
])


@dataclass
class WasteType:
    id: UUID
    name: str


def parse_waste_type(name: str) -> WasteType:
    name_to_waste_type_id = dict(map(reversed, WASTE_TYPE_ID_TO_NAME.items()))
    if name not in name_to_waste_type_id:
        raise ValueError(f"Invalid name, should be one of {', '.join(name_to_waste_type_id)}")

    return WasteType(name_to_waste_type_id[name], name)


ORGANIZATION_LEGAL_TYPE_ID_TO_NAME = dict([
    (UUID('2d20f39e-e332-49e3-b45f-a49fcf0006d6'), 'ТСЖ'),
    (UUID('c3cd0010-3610-4263-bafa-8f6ba1e9bb5f'), 'ЮЛ'),
    (UUID('0b86ec05-ecc4-496f-a88a-67fb42a98b07'), 'СНТ'),
    (UUID('148dcc15-13e3-4677-a267-07ddceb15b30'), 'СТН'),
    (UUID('f21f4bf1-acbb-408d-9efb-94e67084e864'), 'ТСН'),
    (UUID('abb76d46-3120-43fc-b3d0-89b80d39ab5b'), 'ИЖС'),
    (UUID('9f94f519-1911-4a7e-815c-9ed40f75f217'), 'МКД/ТСН'),
    (UUID('87af06a0-5b5b-4ae9-b3b4-b1545c85e0cf'), 'ИП'),
    (UUID('1ebe2a79-b674-4336-a21b-40af600a1e2a'), 'ДНП'),
    (UUID('dbd024d7-87dd-443e-8b94-fdd487423330'), 'ЖКС'),
    (UUID('dc2c73e0-58bf-43cb-b75c-e25741eb4a57'), 'МКД/ТСЖ'),
    (UUID('7b7024b7-3e22-4383-97c0-752eefd7a95b'), 'ИЖС/ТСН')
])


@dataclass
class OrganizationLegalType:
    id: UUID
    name: str


def parse_organization_legal_type(name: str) -> OrganizationLegalType:
    name_to_organization_legal_type_id = dict(map(reversed, ORGANIZATION_LEGAL_TYPE_ID_TO_NAME.items()))
    if name not in name_to_organization_legal_type_id:
        raise ValueError(f"Invalid name, should be one of {', '.join(name_to_organization_legal_type_id)}")

    return OrganizationLegalType(name_to_organization_legal_type_id[name], name)


OPERATIONAL_STATUS_ID_TO_NAME = dict([
    (UUID('d6e6e2a6-989c-4446-9c64-da534fbda03b'), 'активная'),
    (UUID('1efb4e58-89c7-498c-8087-95126bbe7a51'), 'неактивная'),
    (UUID('d3ee93a3-d37e-49ac-ad51-195101c4c516'), 'проектируемая')
])


@dataclass
class OperationalStatus:
    id: UUID
    name: str


def parse_operational_status(name: str) -> OperationalStatus:
    name_to_operational_status_id = dict(map(reversed, OPERATIONAL_STATUS_ID_TO_NAME.items()))
    if name not in name_to_operational_status_id:
        raise ValueError(f"Invalid name, should be one of {', '.join(name_to_operational_status_id)}")

    return OperationalStatus(name_to_operational_status_id[name], name)


COVERING_TYPE_ID_TO_NAME = dict([
    (UUID('2f9e0dea-50e4-40b1-a182-216594d76024'), 'грунт'),
    (UUID('33668d6c-ddf1-41cd-9bf8-de3907a8d728'), 'бетон')
])


@dataclass
class CoveringType:
    id: UUID
    name: str


def parse_covering_type(name: str) -> CoveringType:
    name_to_covering_type_id = dict(map(reversed, COVERING_TYPE_ID_TO_NAME.items()))
    if name not in name_to_covering_type_id:
        raise ValueError(f"Invalid name, should be one of {', '.join(name_to_covering_type_id)}")

    return CoveringType(name_to_covering_type_id[name], name)


FENCE_MATERIAL_ID_TO_NAME = dict([
    (UUID('2c42e6b8-cdef-4b10-90ee-1d1fe3a6f222'), 'проф. лист'),
    (UUID('d12f57ea-e687-4323-b6b1-2d3b0a54fc3a'), 'металл'),
    (UUID('737ff861-8e77-46df-b42a-ca8206f5e7f7'), 'бетон')
])


@dataclass
class FenceMaterial:
    id: UUID
    name: str


def parse_fence_material(name: str) -> FenceMaterial:
    name_to_fence_material_id = dict(map(reversed, FENCE_MATERIAL_ID_TO_NAME.items()))
    if name not in name_to_fence_material_id:
        raise ValueError(f"Invalid name, should be one of {', '.join(name_to_fence_material_id)}")

    return FenceMaterial(name_to_fence_material_id[name], name)


ACCRUAL_METHOD_ID_TO_NAME = dict([
    (UUID('12ff4c0f-4897-4f3f-af55-37edcbf60803'), 'test')
])


@dataclass
class AccrualMethod:
    id: UUID
    name: str


def parse_accrual_method(name: str) -> AccrualMethod:
    name_to_accrual_method_id = dict(map(reversed, ACCRUAL_METHOD_ID_TO_NAME.items()))
    if name not in name_to_accrual_method_id:
        raise ValueError(f"Invalid name, should be one of {', '.join(name_to_accrual_method_id)}")

    return AccrualMethod(name_to_accrual_method_id[name], name)


CONTAINER_TYPE_ID_TO_NAME = dict([
    (UUID('5f701da0-afc9-4d6d-809f-db5531cef21e'), '0.75м3 пластик'),
    (UUID('b4ab2b45-e647-471b-90e4-7768ff0f4831'), '6м3 металл'),
    (UUID('aba815af-dc2e-4265-b42c-6ecc87dd89c8'), 'КГО')
])


@dataclass
class ContainerType:
    id: UUID
    name: str


def parse_container_type(name: str) -> ContainerType:
    name_to_container_type_id = dict(map(reversed, CONTAINER_TYPE_ID_TO_NAME.items()))
    if name not in name_to_container_type_id:
        raise ValueError(f"Invalid name, should be one of {', '.join(name_to_container_type_id)}")

    return ContainerType(name_to_container_type_id[name], name)


MEASUREMENT_UNIT_ID_TO_NAME = dict([
    (UUID('167d3dba-91a7-4a85-94ad-0aa9443e0732'), 'м3')
])


@dataclass
class MeasurementUnit:
    id: UUID
    name: str


def parse_measurement_unit(name: str) -> MeasurementUnit:
    name_to_measurement_unit_id = dict(map(reversed, MEASUREMENT_UNIT_ID_TO_NAME.items()))
    if name not in name_to_measurement_unit_id:
        raise ValueError(f"Invalid name, should be one of {', '.join(name_to_measurement_unit_id)}")

    return MeasurementUnit(name_to_measurement_unit_id[name], name)


register_type(UUID=UUID)
register_type(datetime=parse_datetime)
register_type(WasteType=parse_waste_type)
register_type(OrganizationLegalType=parse_organization_legal_type)
register_type(OperationalStatus=parse_operational_status)
register_type(Decimal=Decimal)
register_type(CoveringType=parse_covering_type)
register_type(FenceMaterial=parse_fence_material)
register_type(AccrualMethod=parse_accrual_method)
register_type(MeasurementUnit=parse_measurement_unit)


@given("something data")
def step_impl(context: StepContext):
    pass


@given("entrepreneur bank details input")
def step_impl(context: StepContext):
    properties = {}
    for row in context.table:
        key, value = row['property'], row['value']
        properties[key] = value
    context.user.input_entrepreneur_bank_details = BankDetails(**properties)


@given('entrepreneur actual address "{actual_address}"')
def step_impl(context: StepContext, actual_address: str):
    context.user.input_entrepreneur_actual_address = actual_address


@given('entrepreneur address "{address}"')
def step_impl(context: StepContext, address: str):
    context.user.input_entrepreneur_address = address


@given('entrepreneur email "{email}"')
def step_impl(context: StepContext, email: str):
    context.user.input_entrepreneur_email_contact = email


@given('entrepreneur phone "{phone}"')
def step_impl(context: StepContext, phone: str):
    context.user.input_entrepreneur_phone = phone


@given('entrepreneur ogrnip "{ogrnip}"')
def step_impl(context: StepContext, ogrnip: str):
    context.user.input_entrepreneur_ogrnip = ogrnip


@given('entrepreneur inn "{inn}"')
def step_impl(context: StepContext, inn: str):
    context.user.input_entrepreneur_inn = inn


@given('entrepreneur patronymic "{patronymic}"')
def step_impl(context: StepContext, patronymic: str):
    context.user.input_entrepreneur_patronymic = patronymic


@given('entrepreneur last name "{last_name}"')
def step_impl(context: StepContext, last_name: str):
    context.user.input_entrepreneur_last_name = last_name


@given('entrepreneur first name "{first_name}"')
def step_impl(context: StepContext, first_name: str):
    context.user.input_entrepreneur_first_name = first_name


@given('business entity full name "{full_name}"')
def step_impl(context: StepContext, full_name: str):
    context.user.input_business_entity_full_name = full_name


@given('business entity short name "{short_name}"')
def step_impl(context: StepContext, short_name: str):
    context.user.input_business_entity_short_name = short_name


@given('business entity inn "{inn}"')
def step_impl(context: StepContext, inn: str):
    context.user.input_business_entity_inn = inn


@given('business entity kpp "{kpp}"')
def step_impl(context: StepContext, kpp: str):
    context.user.input_business_entity_kpp = kpp


@given('business entity ogrn "{ogrn}"')
def step_impl(context: StepContext, ogrn: str):
    context.user.input_business_entity_ogrn = ogrn


@given('business entity phone "{phone}"')
def step_impl(context: StepContext, phone: str):
    context.user.input_business_entity_phone = phone


@given('business entity email "{email}"')
def step_impl(context: StepContext, email: str):
    context.user.input_business_entity_email = email


@given('business entity address "{address}"')
def step_impl(context: StepContext, address: str):
    context.user.input_business_entity_address = address


@given('business entity actual address "{actual_address}"')
def step_impl(context: StepContext, actual_address: str):
    context.user.input_business_entity_actual_address = actual_address


@given('business entity postal address "{postal_address}"')
def step_impl(context: StepContext, postal_address: str):
    context.user.input_business_entity_postal_address = postal_address


@given("business entity responsible persons")
def step_impl(context: StepContext):
    responsible_persons = []
    for row in context.table:
        full_name, phone = row['full_name'], row['phone']
        responsible_persons.append(ResponsiblePerson(full_name, phone))
    context.user.input_business_entity_responsible_persons = responsible_persons


@given("business entity bank details input")
def step_impl(context: StepContext):
    properties = {}
    for row in context.table:
        key, value = row['property'], row['value']
        properties[key] = value
    context.user.input_business_entity_bank_details = BankDetails(**properties)


@given('well known business entity client "{short_name}"')
def step_impl(context: StepContext, short_name: str):
    legal_entity = BusinessEntityModel(
        f'OOO {short_name}',
        short_name,
        '100485636000',
        '783901000',
        '1177847045000',
        '+79811648000',
        f'{short_name}@mail',
        f'{short_name} address',
        f'{short_name} postal address',
        f'{short_name} actual address',
        [
            ResponsiblePerson('Person 1', '+79811648001'),
            ResponsiblePerson('Person 2', '+79811648002')
        ],
        BankDetails(
            '40802810832130000000',
            '30101810600000000000',
            'Bank',
            '044030000'
        ),
    )
    form = RegisterClientForm(
        None,
        legal_entity
    )
    context.user.client.register_generator(form)


@given('well known entrepreneur client "{full_name}"')
def step_impl(context: StepContext, full_name: str):
    last_name, first_name, patronymic = full_name.split(' ')
    legal_entity = EntrepreneurModel(
        first_name,
        last_name,
        patronymic,
        '100485636000',
        '316100100060000',
        '+79811648000',
        f'{last_name}@mail',
        f'{last_name} address',
        f'{last_name} actual address',
        BankDetails(
            '40802810832130000000',
            '30101810600000000000',
            'Bank',
            '044030000'
        )
    )
    form = RegisterClientForm(
        None,
        legal_entity
    )
    context.user.client.register_generator(form)


@given('well known transportation with "{truck:UUID}" truck at "{execution_date:datetime}"')
def step_impl(context: StepContext, truck: UUID, execution_date: datetime):
    form = RegisterTransportationForm(
        truck=truck,
        execution_date=execution_date,
        destinations=[
            TransportationDestination(
                id=UUID('69f736f3-8fb4-4825-aa84-2dfa54eb1f5a'),
                collection_start_time=parse_datetime('2018-06-29T08:15:27.243860Z'),
                waste_generation_point=UUID('090c5a8b-b707-4b61-a916-0d62c939c9dd'),
                collections=[
                    WasteCollection(
                        id=UUID('dc8aa752-011b-4bfe-8e42-4321280940cf'),
                        destination=UUID('69f736f3-8fb4-4825-aa84-2dfa54eb1f5a'),
                        container_type=UUID('5a06c0cb-3cef-4b30-8ef2-11c7f8797d83'),
                        container_volume=Decimal('1.5'),
                        container_utilization=Decimal('1.0'),
                        waste_volume_m3=Decimal('1.5'),
                        photos=[
                            UUID('826d6505-2085-4879-8f5b-40da81ebe091'),
                            UUID('aef35430-9b09-4af1-af44-622e45bc1b69'),
                        ]
                    )
                ]
            )
        ]
    )
    context.user.client.register_transportation(form)


@given('generation point internal_system_number "{internal_system_number}"')
def step_impl(context: StepContext, internal_system_number: str):
    context.user.input_generation_point_internal_system_number = internal_system_number


@given('generation point external_registry_number "{external_registry_number}"')
def step_impl(context: StepContext, external_registry_number: str):
    context.user.input_generation_point_external_registry_number = external_registry_number


@given('generation point waste_type "{waste_type:WasteType}"')
def step_impl(context: StepContext, waste_type: WasteType):
    context.user.input_generation_point_waste_type = waste_type.id


@given('generation point organization_legal_type "{organization_legal_type:OrganizationLegalType}"')
def step_impl(context: StepContext, organization_legal_type: OrganizationLegalType):
    context.user.input_generation_point_organization_legal_type = organization_legal_type.id


@given('generation point operational_status "{operational_status:OperationalStatus}"')
def step_impl(context: StepContext, operational_status: OperationalStatus):
    context.user.input_generation_point_operational_status = operational_status.id


@given('generation point full_address "{full_address}"')
def step_impl(context: StepContext, full_address: str):
    context.user.input_generation_point_full_address = full_address


@given('generation point coordinates "{coordinates}"')
def step_impl(context: StepContext, coordinates: str):
    context.user.input_generation_point_coordinates = coordinates


@given('generation point area_m2 "{area_m2:Decimal}"')
def step_impl(context: StepContext, area_m2: Decimal):
    context.user.input_generation_point_area_m2 = area_m2


@given('generation point covering_type "{covering_type:CoveringType}"')
def step_impl(context: StepContext, covering_type: CoveringType):
    context.user.input_generation_point_covering_type = covering_type.id


@given('generation point fence_material "{fence_material:FenceMaterial}"')
def step_impl(context: StepContext, fence_material: FenceMaterial):
    context.user.input_generation_point_fence_material = fence_material.id


@given('generation point service_company_name "{service_company_name}"')
def step_impl(context: StepContext, service_company_name: str):
    context.user.input_generation_point_service_company_name = service_company_name


@given('generation point generator "{generator:UUID}"')
def step_impl(context: StepContext, generator: UUID):
    context.user.input_generation_point_generator = generator


@given('generation point description "{description}"')
def step_impl(context: StepContext, description: str):
    context.user.input_generation_point_description = description


@given("generation point containers")
def step_impl(context: StepContext):
    container_type_to_number = dict()
    for row in context.table:
        container_type, number = row['container_type'], row['number_of_containers']
        container_type_to_number[parse_container_type(container_type[1:-1]).id] = int(number)

    context.user.input_generation_point_containers = []
    for container_type, number in container_type_to_number.items():
        context.user.input_generation_point_containers.extend(
            [GenerationPointFormContainer(container_type) for _ in range(number)]
        )


@given("generation point transporter")
def step_impl(context: StepContext):
    properties = {}
    for row in context.table:
        key, value = row['property'], row['value']
        if key == 'transporter':
            value = UUID(value[1:-1])
        elif key == 'start_transportation_date':
            value = parse_datetime(value[1:-1])
        elif key == 'end_transportation_date':
            value = parse_datetime(value[1:-1])
        properties[key] = value
    context.user.input_generation_point_transporter = GenerationPointFormTransporter(**properties)


@given("generation point photos")
def step_impl(context: StepContext):
    context.user.input_generation_point_photos = []
    for row in context.table:
        photo = row['photo']
        context.user.input_generation_point_photos.append(UUID(photo[1:-1]))


@given('generation point accrual_method "{accrual_method:AccrualMethod}"')
def step_impl(context: StepContext, accrual_method: AccrualMethod):
    context.user.input_generation_point_accrual_method = accrual_method.id


@given('generation point measurement_unit "{measurement_unit:MeasurementUnit}"')
def step_impl(context: StepContext, measurement_unit: MeasurementUnit):
    context.user.input_generation_point_measurement_unit = measurement_unit.id