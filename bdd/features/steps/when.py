from typing import List
from uuid import UUID

from behave import register_type
from behave import when

from features.client import BusinessEntityModel, GenerationPointForm
from features.client import EntrepreneurModel
from features.client import RegisterClientForm
from features.environment import StepContext


def parse_string_list(value: str) -> List[str]:
    return value.split(',')


register_type(UUID=UUID)
register_type(StringList=parse_string_list)


@when('I create user "{user_id:UUID}" with permissions "{permissions:StringList}"')
def step_impl(context: StepContext, user_id: UUID, permissions: List[str]):
    context.user.client.register_user(user_id, permissions)


@when("I get users")
def step_impl(context: StepContext):
    context.user.users = context.user.client.get_users()


@when("I get client")
def step_impl(context: StepContext):
    context.user.current_client = context.user.client.get_generator(context.user.last_created_client_id)


@when("I create entrepreneur client")
def step_impl(context: StepContext):
    legal_entity = EntrepreneurModel(
        context.user.input_entrepreneur_first_name,
        context.user.input_entrepreneur_last_name,
        context.user.input_entrepreneur_patronymic,
        context.user.input_entrepreneur_inn,
        context.user.input_entrepreneur_ogrnip,
        context.user.input_entrepreneur_phone,
        context.user.input_entrepreneur_email_contact,
        context.user.input_entrepreneur_address,
        context.user.input_entrepreneur_actual_address,
        context.user.input_entrepreneur_bank_details
    )
    form = RegisterClientForm(
        None,
        legal_entity
    )
    client_id = context.user.client.register_generator(form)
    context.user.last_created_client_id = client_id


@when("I create business entity client")
def step_impl(context: StepContext):
    legal_entity = BusinessEntityModel(
        context.user.input_business_entity_full_name,
        context.user.input_business_entity_short_name,
        context.user.input_business_entity_inn,
        context.user.input_business_entity_kpp,
        context.user.input_business_entity_ogrn,
        context.user.input_business_entity_phone,
        context.user.input_business_entity_email,
        context.user.input_business_entity_address,
        context.user.input_business_entity_postal_address,
        context.user.input_business_entity_actual_address,
        context.user.input_business_entity_responsible_persons,
        context.user.input_business_entity_bank_details,
    )
    form = RegisterClientForm(
        None,
        legal_entity
    )
    client_id = context.user.client.register_generator(form)
    context.user.last_created_client_id = client_id


@when("I create generation point")
def step_impl(context: StepContext):
    generation_point = GenerationPointForm(
        context.user.input_generation_point_internal_system_number,
        context.user.input_generation_point_external_registry_number,
        context.user.input_generation_point_waste_type,
        context.user.input_generation_point_organization_legal_type,
        context.user.input_generation_point_operational_status,
        context.user.input_generation_point_full_address,
        context.user.input_generation_point_coordinates,
        context.user.input_generation_point_area_m2,
        context.user.input_generation_point_covering_type,
        context.user.input_generation_point_fence_material,
        context.user.input_generation_point_service_company_name,
        context.user.input_generation_point_generator,
        context.user.input_generation_point_description,
        context.user.input_generation_point_containers,
        context.user.input_generation_point_transporter,
        context.user.input_generation_point_photos,
        context.user.input_generation_point_accrual_method,
        context.user.input_generation_point_measurement_unit
    )

    resource = context.user.client.register_generation_point(generation_point)
    context.user.last_created_generation_point_id = resource.id


@when("I get generators")
def step_impl(context: StepContext):
    context.user.current_clients = context.user.client.get_generators()


@when("I get parental clients")
def step_impl(context: StepContext):
    context.user.current_parental_clients = context.user.client.get_parental_generators()


@when("I get transportations")
def step_impl(context: StepContext):
    context.user.current_transportations = context.user.client.get_transportations()


@when('I get generation point')
def step_impl(context: StepContext):
    context.user.current_generation_point = context.user.client.get_generation_point(context.user.last_created_generation_point_id)