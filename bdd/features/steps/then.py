from collections import Counter
from decimal import Decimal
from typing import Set
from uuid import UUID

from behave import register_type
from behave import then
from hamcrest import assert_that
from hamcrest import equal_to, contains, is_in

from features.client import BankDetails, GenerationPointFormContainer, GenerationPointFormTransporter
from features.client import ResponsiblePerson
from features.environment import StepContext
from features.steps.given import WasteType, OrganizationLegalType, OperationalStatus, CoveringType, FenceMaterial, \
    parse_container_type, parse_datetime, AccrualMethod, MeasurementUnit


def parse_string_set(representation: str) -> Set[str]:
    return {value.strip() for value in representation.split(',')}


register_type(int=int)
register_type(StringSet=parse_string_set)


@then('users count should be {count:int}')
def step_impl(context: StepContext, count: int):
    assert_that(len(context.user.users), equal_to(count))


@then('entrepreneur first name should be "{first_name}"')
def step_impl(context: StepContext, first_name: str):
    assert_that(context.user.current_client.legal_entity.first_name, equal_to(first_name))


@then('entrepreneur last name should be "{last_name}"')
def step_impl(context: StepContext, last_name: str):
    assert_that(context.user.current_client.legal_entity.last_name, equal_to(last_name))


@then('entrepreneur patronymic should be "{patronymic}"')
def step_impl(context: StepContext, patronymic: str):
    assert_that(context.user.current_client.legal_entity.patronymic, equal_to(patronymic))


@then('entrepreneur inn should be "{inn}"')
def step_impl(context: StepContext, inn: str):
    assert_that(context.user.current_client.legal_entity.inn, equal_to(inn))


@then('entrepreneur ogrnip should be "{ogrnip}"')
def step_impl(context: StepContext, ogrnip: str):
    assert_that(context.user.current_client.legal_entity.ogrnip, equal_to(ogrnip))


@then('entrepreneur phone should be "{phone}"')
def step_impl(context: StepContext, phone: str):
    assert_that(context.user.current_client.legal_entity.phone, equal_to(phone))


@then('entrepreneur email should be "{email}"')
def step_impl(context: StepContext, email: str):
    assert_that(context.user.current_client.legal_entity.email, equal_to(email))


@then('entrepreneur address should be "{address}"')
def step_impl(context: StepContext, address: str):
    assert_that(context.user.current_client.legal_entity.address, equal_to(address))


@then('entrepreneur actual address should be "{actual_address}"')
def step_impl(context: StepContext, actual_address: str):
    assert_that(context.user.current_client.legal_entity.actual_address, equal_to(actual_address))


@then("entrepreneur bank details should be")
def step_impl(context: StepContext):
    actual_bank_details = context.user.current_client.legal_entity.bank_details

    properties = {}
    for row in context.table:
        key, value = row['property'], row['value']
        properties[key] = value
    expected_bank_details = BankDetails(**properties)

    assert_that(actual_bank_details, equal_to(expected_bank_details))


@then("clients count should be {count:int}")
def step_impl(context: StepContext, count: int):
    assert_that(len(context.user.current_clients), equal_to(count))


@then("parental clients count should be {count:int}")
def step_impl(context: StepContext, count: int):
    assert_that(len(context.user.current_parental_clients), equal_to(count))


@then('business entity full name should be "{full_name}"')
def step_impl(context: StepContext, full_name: str):
    assert_that(context.user.current_client.legal_entity.full_name, equal_to(full_name))


@then('business entity short name should be "{short_name}"')
def step_impl(context: StepContext, short_name: str):
    assert_that(context.user.current_client.legal_entity.short_name, equal_to(short_name))


@then('business entity inn should be "{inn}"')
def step_impl(context: StepContext, inn: str):
    assert_that(context.user.current_client.legal_entity.inn, equal_to(inn))


@then('business entity kpp should be "{kpp}"')
def step_impl(context: StepContext, kpp: str):
    assert_that(context.user.current_client.legal_entity.kpp, equal_to(kpp))


@then('business entity ogrn should be "{ogrn}"')
def step_impl(context: StepContext, ogrn: str):
    assert_that(context.user.current_client.legal_entity.ogrn, equal_to(ogrn))


@then('business entity phone should be "{phone}"')
def step_impl(context: StepContext, phone: str):
    assert_that(context.user.current_client.legal_entity.phone, equal_to(phone))


@then('business entity email should be "{email}"')
def step_impl(context: StepContext, email: str):
    assert_that(context.user.current_client.legal_entity.email, equal_to(email))


@then('business entity address should be "{address}"')
def step_impl(context: StepContext, address: str):
    assert_that(context.user.current_client.legal_entity.address, equal_to(address))


@then('business entity actual address should be "{actual_address}"')
def step_impl(context: StepContext, actual_address: str):
    assert_that(context.user.current_client.legal_entity.actual_address, equal_to(actual_address))


@then('business entity postal address should be "{postal_address}"')
def step_impl(context: StepContext, postal_address: str):
    assert_that(context.user.current_client.legal_entity.postal_address, equal_to(postal_address))


@then("business entity responsible persons should be")
def step_impl(context: StepContext):
    actual_responsible_persons = context.user.current_client.legal_entity.responsible_persons

    expected_responsible_persons = []
    for row in context.table:
        full_name, phone = row['full_name'], row['phone']
        expected_responsible_persons.append(ResponsiblePerson(full_name, phone))

    assert_that(actual_responsible_persons, equal_to(expected_responsible_persons))


@then("business entity bank details input should be")
def step_impl(context: StepContext):
    actual_bank_details = context.user.current_client.legal_entity.bank_details

    properties = {}
    for row in context.table:
        key, value = row['property'], row['value']
        properties[key] = value
    expected_bank_details = BankDetails(**properties)

    assert_that(actual_bank_details, equal_to(expected_bank_details))


@then('parental clients names should be "{names:StringSet}"')
def step_impl(context: StepContext, names: Set[str]):
    parental_client_names = {option.name for option in context.user.current_parental_clients}
    assert_that(parental_client_names, equal_to(names))


@then('clients names should be "{names:StringSet}"')
def step_impl(context: StepContext, names: Set[str]):
    client_names = {client.name for client in context.user.current_clients}
    assert_that(client_names, equal_to(names))


@then("transportations count should be {count:int}")
def step_impl(context: StepContext, count: int):
    assert_that(len(context.user.current_transportations), equal_to(count))
    

@then('generation point internal_system_number "{internal_system_number}"')
def step_impl(context: StepContext, internal_system_number: str):
    assert_that(context.user.current_generation_point.internal_system_number, equal_to(
        internal_system_number
    ))


@then('generation point external_registry_number "{external_registry_number}"')
def step_impl(context: StepContext, external_registry_number: str):
    assert_that(context.user.current_generation_point.external_registry_number, equal_to(
        external_registry_number
    ))


@then('generation point waste_type "{waste_type:WasteType}"')
def step_impl(context: StepContext, waste_type: WasteType):
    assert_that(context.user.current_generation_point.waste_type, equal_to(
        waste_type.id
    ))


@then('generation point organization_legal_type "{organization_legal_type:OrganizationLegalType}"')
def step_impl(context: StepContext, organization_legal_type: OrganizationLegalType):
    assert_that(context.user.current_generation_point.organization_legal_type, equal_to(
        organization_legal_type.id
    ))


@then('generation point operational_status "{operational_status:OperationalStatus}"')
def step_impl(context: StepContext, operational_status: OperationalStatus):
    assert_that(context.user.current_generation_point.operational_status, equal_to(
        operational_status.id
    ))


@then('generation point full_address "{full_address}"')
def step_impl(context: StepContext, full_address: str):
    assert_that(context.user.current_generation_point.full_address, equal_to(full_address))


@then('generation point coordinates "{coordinates}"')
def step_impl(context: StepContext, coordinates: str):
    assert_that(context.user.current_generation_point.coordinates, equal_to(coordinates))


@then('generation point area_m2 "{area_m2:Decimal}"')
def step_impl(context: StepContext, area_m2: Decimal):
    assert_that(context.user.current_generation_point.area_m2, equal_to(area_m2))


@then('generation point covering_type "{covering_type:CoveringType}"')
def step_impl(context: StepContext, covering_type: CoveringType):
    assert_that(context.user.current_generation_point.covering_type, equal_to(covering_type.id))


@then('generation point fence_material "{fence_material:FenceMaterial}"')
def step_impl(context: StepContext, fence_material: FenceMaterial):
    assert_that(context.user.current_generation_point.fence_material, equal_to(fence_material.id))


@then('generation point service_company_name "{service_company_name}"')
def step_impl(context: StepContext, service_company_name: str):
    assert_that(context.user.current_generation_point.service_company_name, equal_to(service_company_name))


@then('generation point generator "{generator:UUID}"')
def step_impl(context: StepContext, generator: UUID):
    assert_that(context.user.current_generation_point.generator, equal_to(generator))


@then('generation point description "{description}"')
def step_impl(context: StepContext, description: str):
    assert_that(context.user.current_generation_point.description, equal_to(description))


@then("generation point containers")
def step_impl(context: StepContext):
    expected_container_type_to_number = dict()
    for row in context.table:
        container_type, number = row['container_type'], row['number_of_containers']
        expected_container_type_to_number[parse_container_type(container_type[1:-1]).id] = int(number)

    container_type_to_number = Counter(
        [container.container_type for container in context.user.current_generation_point.containers]
    )

    assert_that(set(container_type_to_number), equal_to(set(expected_container_type_to_number)))
    for container_type in container_type_to_number:
        assert_that(container_type_to_number[container_type],
                    equal_to(expected_container_type_to_number[container_type]))


@then("generation point transporter")
def step_impl(context: StepContext):
    expected_properties = {}
    for row in context.table:
        key, value = row['property'], row['value']
        if key == 'transporter':
            value = UUID(value[1:-1])
        elif key == 'start_transportation_date':
            value = parse_datetime(value[1:-1])
        elif key == 'end_transportation_date':
            value = parse_datetime(value[1:-1])
        expected_properties[key] = value

    for key, value in vars(context.user.current_generation_point.transporter).items():
        assert_that(expected_properties[key], equal_to(value))


@then("generation point photos")
def step_impl(context: StepContext):
    expected_generation_point_photos = []
    for row in context.table:
        photo = row['photo']
        expected_generation_point_photos.append(UUID(photo[1:-1]))

    assert_that(len(expected_generation_point_photos), equal_to(len(
        context.user.current_generation_point.photos
    )))
    for photo in expected_generation_point_photos:
        assert_that(photo, is_in(context.user.current_generation_point.photos))


@then('generation point accrual_method "{accrual_method:AccrualMethod}"')
def step_impl(context: StepContext, accrual_method: AccrualMethod):
    assert_that(context.user.current_generation_point.accrual_method, equal_to(
        accrual_method.id
    ))


@then('generation point measurement_unit "{measurement_unit:MeasurementUnit}"')
def step_impl(context: StepContext, measurement_unit: MeasurementUnit):
    assert_that(context.user.current_generation_point.measurement_unit, equal_to(measurement_unit.id))
