Feature: Something

  Scenario: Regular Task 1
    When I create user "afc2b4a5-7395-4b8e-a701-0623195d8fcb" with permissions "read,write"
    When I get users
    Then users count should be 1

  Scenario: Regular Task 2
    When I create user "afc2b4a5-7395-4b8e-a701-0623195d8fcb" with permissions "read,write"
    When I create user "750fd08e-70aa-40bf-85d2-e9506e250ff8" with permissions "read"
    When I get users
    Then users count should be 2