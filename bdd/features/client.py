import json
from dataclasses import asdict
from dataclasses import dataclass
from dataclasses import field
from dataclasses import is_dataclass
from datetime import datetime
from decimal import Decimal
from re import sub
from typing import Any
from typing import List
from typing import Optional
from typing import Union
from uuid import UUID

import dateutil.parser
import jwt
import requests
from dataclasses_json import LetterCase
from dataclasses_json import config
from dataclasses_json import dataclass_json
from marshmallow import fields


def datetime_field():
    return field(
        metadata=config(
            encoder=lambda value: value.strftime('%Y-%m-%dT%H:%M:%S.%fZ'),
            decoder=lambda value: dateutil.parser.isoparse(value),
            mm_field=fields.DateTime(format='iso')
        ))


@dataclass_json(letter_case=LetterCase.CAMEL)
@dataclass
class UserListItem:
    id: UUID
    permissions: List[str]


@dataclass_json(letter_case=LetterCase.CAMEL)
@dataclass
class BankDetails:
    account: str
    correspondent_account: str
    bank_name: str
    rcbic: str


@dataclass_json(letter_case=LetterCase.CAMEL)
@dataclass
class EntrepreneurModel:
    first_name: str
    last_name: str
    patronymic: str
    inn: str
    ogrnip: str
    phone: str
    email: Optional[str]
    address: str
    actual_address: str
    bank_details: BankDetails
    type: str = 'Entrepreneur'


@dataclass_json(letter_case=LetterCase.CAMEL)
@dataclass
class ResponsiblePerson:
    full_name: str
    phone: str


@dataclass_json(letter_case=LetterCase.CAMEL)
@dataclass
class BusinessEntityModel:
    full_name: str
    short_name: str
    inn: str
    kpp: str
    ogrn: str
    phone: str
    email: Optional[str]
    address: str
    postal_address: str
    actual_address: str
    responsible_persons: List[ResponsiblePerson]
    bank_details: BankDetails
    type: str = 'BusinessEntity'


@dataclass_json(letter_case=LetterCase.CAMEL)
@dataclass
class ClientModel:
    parent: Optional[UUID]
    legal_entity: Union[BusinessEntityModel, EntrepreneurModel]

    def __post_init__(self):
        if self.legal_entity['type'] == 'BusinessEntity':
            self.legal_entity = BusinessEntityModel.from_dict(self.legal_entity)
        elif self.legal_entity['type'] == 'Entrepreneur':
            self.legal_entity = EntrepreneurModel.from_dict(self.legal_entity)


@dataclass_json(letter_case=LetterCase.CAMEL)
@dataclass
class GenerationPointFormContainer:
    container_type: UUID


@dataclass_json(letter_case=LetterCase.CAMEL)
@dataclass
class GenerationPointFormTransporter:
    transporter: UUID
    frequency: str
    start_transportation_date: datetime = datetime_field()
    end_transportation_date: datetime = datetime_field()


@dataclass_json(letter_case=LetterCase.CAMEL)
@dataclass
class GenerationPointForm:
    internal_system_number: str
    external_registry_number: str
    waste_type: UUID
    organization_legal_type: UUID
    operational_status: UUID
    full_address: str
    coordinates: str
    area_m2: Decimal
    covering_type: UUID
    fence_material: UUID
    service_company_name: Optional[str]
    generator: UUID
    description: str
    containers: List[GenerationPointFormContainer]
    transporter: GenerationPointFormTransporter
    photos: List[UUID]
    accrual_method: UUID
    measurement_unit: UUID


@dataclass_json(letter_case=LetterCase.CAMEL)
@dataclass
class GenerationPointModelContainer:
    container_type: UUID


@dataclass_json(letter_case=LetterCase.CAMEL)
@dataclass
class GenerationPointModelTransporter:
    transporter: UUID
    start_transportation_date: datetime = datetime_field()
    end_transportation_date: datetime = datetime_field()


@dataclass_json(letter_case=LetterCase.CAMEL)
@dataclass
class GenerationPointModel:
    id: UUID
    internal_system_number: str
    external_registry_number: str
    waste_type: UUID
    organization_legal_type: UUID
    operational_status: UUID
    full_address: str
    coordinates: str
    area_m2: Decimal
    covering_type: UUID
    fence_material: UUID
    service_company_name: Optional[str]
    generator: UUID
    description: str
    containers: List[GenerationPointModelContainer]
    transporter: GenerationPointModelTransporter
    photos: List[UUID]
    accrual_method: UUID
    measurement_unit: UUID
    transportation_frequency: str

    def __post_init__(self):
        self.photos = list(map(UUID, self.photos))


@dataclass_json(letter_case=LetterCase.CAMEL)
@dataclass
class RegisterClientForm:
    parent: Optional[UUID]
    legal_entity: Union[EntrepreneurModel, BusinessEntityModel]


@dataclass_json(letter_case=LetterCase.CAMEL)
@dataclass
class Affiliate:
    id: UUID
    name: str
    inn: str
    legal_entity_type: str


@dataclass_json(letter_case=LetterCase.CAMEL)
@dataclass
class ClientListItem:
    id: UUID
    name: str
    inn: str
    legal_entity_type: str
    affiliates: List[Affiliate]


@dataclass_json(letter_case=LetterCase.CAMEL)
@dataclass
class ClientOption:
    id: UUID
    name: str


def format_in_camel_case(key: str) -> str:
    key = sub(r"(_|-)+", " ", key).title().replace(" ", "")
    return key[0].lower() + key[1:]


def format_in_snake_case(name: str) -> str:
    name = sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return sub('([a-z0-9])([A-Z])', r'\1_\2', name).lower()


def convert_json(d, convert):
    new_d = {}
    for k, v in d.items():
        new_d[convert(k)] = convert_json(v, convert) if isinstance(v, dict) else v
    return new_d


@dataclass_json(letter_case=LetterCase.CAMEL)
@dataclass
class WasteCollection:
    id: UUID
    destination: UUID
    container_type: UUID
    container_volume: Decimal
    container_utilization: Decimal
    waste_volume_m3: Decimal
    photos: List[UUID]


@dataclass_json(letter_case=LetterCase.CAMEL)
@dataclass
class TransportationDestination:
    id: UUID
    waste_generation_point: UUID
    collections: List[WasteCollection]
    collection_start_time: datetime = datetime_field()


@dataclass_json(letter_case=LetterCase.CAMEL)
@dataclass
class RegisterTransportationForm:
    truck: UUID
    destinations: List[TransportationDestination]
    execution_date: datetime = datetime_field()


@dataclass_json(letter_case=LetterCase.CAMEL)
@dataclass
class TransportationForm:
    truck: UUID
    destinations: List[TransportationDestination]
    execution_date: datetime = datetime_field()


@dataclass_json(letter_case=LetterCase.CAMEL)
@dataclass
class TransportationListItemModel:
    id: UUID
    district: str
    truck_registration_number: str
    status: str
    execution_date: str


@dataclass_json(letter_case=LetterCase.CAMEL)
@dataclass
class Resource:
    id: UUID


class ScrapClientEncoder(json.JSONEncoder):

    def default(self, o: Any) -> Any:
        if is_dataclass(o):
            return convert_json(asdict(o), format_in_camel_case)
        if isinstance(o, UUID):
            return str(o)
        return super().default(o)


class ScrapClient:

    def __init__(self, base_url: str, user_id: UUID):
        self.base_url = base_url
        self.token = jwt.encode({'sub': str(user_id)}, 'scrap', algorithm='HS256').decode("utf-8")

    def register_user(self, user_id: UUID, permissions: List[str]):
        url = self._get_url('/automation/users')
        data = {
            'user_id': str(user_id),
            "role": "contractor",
            "name": "name",
            "login": "login",
            "password": "password",
            'permissions': permissions
        }
        requests.post(url, json=data, headers=self._get_headers())

    def grant_user_permission(self, user_id: UUID, permission: str):
        pass

    def get_users(self) -> List[UserListItem]:
        url = self._get_url('/automation/users')
        response = requests.get(url, headers=self._get_headers())
        dogs = []
        for entry in response.json():
            dogs.append(UserListItem(
                entry['id'],
                entry['permissions'],
            ))
        return dogs

    def delete_user(self, user_id: UUID):
        pass

    def register_generator(self, form: RegisterClientForm) -> UUID:
        url = self._get_url('/generators')
        data = form.to_json()
        response = requests.post(url, data=data, headers=self._get_headers())
        data = convert_json(response.json(), format_in_snake_case)
        return UUID(data['id'])

    def get_generator(self, client_id: UUID) -> ClientModel:
        url = self._get_url(f'/generators/{client_id}')
        response = requests.get(url, headers=self._get_headers())
        json = response.json()
        return ClientModel.from_dict(json)

    def get_generators(self) -> List[ClientListItem]:
        url = self._get_url('/generators')
        response = requests.get(url, headers=self._get_headers())
        json = response.json()
        return ClientListItem.schema().load(json, many=True)

    def get_parental_generators(self) -> List[ClientOption]:
        url = self._get_url('/generators/parental')
        response = requests.get(url, headers=self._get_headers())
        json = response.json()
        return ClientOption.schema().load(json, many=True)

    def register_transportation(self, form: RegisterTransportationForm) -> Resource:
        url = self._get_url('/transportations')
        data = form.to_json()
        response = requests.post(url, data=data, headers=self._get_headers())
        json = response.json()
        return Resource.from_dict(json)

    def register_generation_point(self, form: GenerationPointForm) -> Resource:
        url = self._get_url('/generation_points')
        data = form.to_json()
        response = requests.post(url, data=data, headers=self._get_headers())
        json = response.json()
        return Resource.from_dict(json)

    def get_transportation(self, id: UUID) -> TransportationForm:
        url = self._get_url(f'/transportations/{id}')
        response = requests.get(url, headers=self._get_headers())
        json = response.json()
        return TransportationForm.from_dict(json)

    def get_transportations(self) -> List[TransportationListItemModel]:
        url = self._get_url('/transportations')
        response = requests.get(url, headers=self._get_headers())
        json = response.json()
        return TransportationListItemModel.schema().load(json, many=True)

    def get_generation_point(self, id: UUID) -> GenerationPointModel:
        url = self._get_url(f'/generation_points/{id}')
        response = requests.get(url, headers=self._get_headers())
        json = response.json()
        return GenerationPointModel.from_dict(json)

    def _get_url(self, path: str) -> str:
        return f'{self.base_url}{path}'

    def _serialize_data(self, data: Any) -> str:
        return json.dumps(data, cls=ScrapClientEncoder)

    def _get_headers(self):
        return {
            'Content-Type': 'application/json',
            'Authorization': f'Bearer {self.token}'
        }
