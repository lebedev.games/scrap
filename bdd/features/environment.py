import os
from decimal import Decimal
from typing import List
from typing import Optional
from typing import Protocol
from uuid import UUID

import psycopg2
from behave.model import Scenario
from behave.model import Table
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

from features.client import BankDetails, GenerationPointModel, GenerationPointFormContainer, \
    GenerationPointFormTransporter
from features.client import ClientListItem
from features.client import ClientModel
from features.client import ClientOption
from features.client import ResponsiblePerson
from features.client import ScrapClient
from features.client import TransportationForm
from features.client import TransportationListItemModel
from features.client import UserListItem


class UserContextObject(dict):
    def __getattr__(self, name):
        if name in self:
            return self[name]
        else:
            return None

    def __setattr__(self, name, value):
        self[name] = value

    def __delattr__(self, name):
        if name in self:
            del self[name]


class UserContext(Protocol):
    client: ScrapClient

    users: List[UserListItem]

    last_created_client_id: UUID
    last_created_generation_point_id: UUID

    current_client: ClientModel
    current_clients: List[ClientListItem]
    current_parental_clients: List[ClientOption]
    current_transportations: List[TransportationListItemModel]
    current_transportation: TransportationForm
    current_generation_point: GenerationPointModel

    input_entrepreneur_first_name: str
    input_entrepreneur_last_name: str
    input_entrepreneur_patronymic: str
    input_entrepreneur_inn: str
    input_entrepreneur_ogrnip: str
    input_entrepreneur_phone: str
    input_entrepreneur_email_contact: Optional[str]
    input_entrepreneur_address: str
    input_entrepreneur_actual_address: str
    input_entrepreneur_bank_details: BankDetails

    input_business_entity_full_name: str
    input_business_entity_short_name: str
    input_business_entity_inn: str
    input_business_entity_kpp: str
    input_business_entity_ogrn: str
    input_business_entity_phone: str
    input_business_entity_email: Optional[str]
    input_business_entity_address: str
    input_business_entity_postal_address: str
    input_business_entity_actual_address: str
    input_business_entity_responsible_persons: List[ResponsiblePerson]
    input_business_entity_bank_details: BankDetails

    input_generation_point_internal_system_number: str
    input_generation_point_external_registry_number: str
    input_generation_point_waste_type: UUID
    input_generation_point_organization_legal_type: UUID
    input_generation_point_operational_status: UUID
    input_generation_point_full_address: str
    input_generation_point_coordinates: str
    input_generation_point_area_m2: Decimal
    input_generation_point_covering_type: UUID
    input_generation_point_fence_material: UUID
    input_generation_point_service_company_name: Optional[str]
    input_generation_point_generator: UUID
    input_generation_point_description: str
    input_generation_point_containers: List[GenerationPointFormContainer]
    input_generation_point_transporter: GenerationPointFormTransporter
    input_generation_point_photos: List[UUID]
    input_generation_point_accrual_method: UUID
    input_generation_point_measurement_unit: UUID


class StepContext(Protocol):
    table: Table
    user: UserContext


def backup_database():
    database_host = os.environ.get('DATABASE_HOST', 'postgres://postgres:postgres@host.docker.internal:65432')
    database_name = os.environ.get('DATABASE_NAME', 'scrap')
    database_back = 'scrap_backup'
    database_temp = 'scrap_template'
    database_url = f'{database_host}/postgres'

    connection = psycopg2.connect(database_url)
    connection.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    connection.cursor().execute(
        f"SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE datname='{database_name}';"
    )
    connection.cursor().execute(
        f"ALTER DATABASE {database_name} RENAME TO {database_back};"
    )
    connection.cursor().execute(
        f"CREATE DATABASE {database_name} TEMPLATE {database_temp};"
    )
    connection.close()


def restore_database():
    database_host = os.environ.get('DATABASE_HOST', 'postgres://postgres:postgres@host.docker.internal:65432')
    database_name = os.environ.get('DATABASE_NAME', 'scrap')
    database_back = 'scrap_backup'
    database_temp = 'scrap_template'
    database_url = f'{database_host}/postgres'

    connection = psycopg2.connect(database_url)
    connection.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    connection.cursor().execute(
        f"SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE datname='{database_name}';"
    )
    connection.cursor().execute(
        f'DROP DATABASE {database_name};'
    )
    connection.cursor().execute(
        f"ALTER DATABASE {database_back} RENAME TO {database_name};"
    )
    connection.close()


def before_scenario(context: StepContext, scenario: Scenario):
    backup_database()

    service_url = os.environ.get('SERVICE_URL', 'http://host.docker.internal:8004')
    context.user = UserContextObject()
    context.user.client = ScrapClient(service_url, UUID('7036ba92-3c56-4405-8fdc-be3863f3cc70'))


def after_scenario(context: StepContext, scenario: Scenario):
    restore_database()
