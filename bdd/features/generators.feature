Feature: Waste Generators

#  Scenario: Empty Client Registry
#    When I get generators
#    Then clients count should be 0
#
#  Scenario: Client Registry
#    Given well known business entity client "Mega"
#    Given well known entrepreneur client "Ivanov Ivan Ivanovich"
#    When I get generators
#    Then clients count should be 2
#    Then clients names should be "Mega, Ivanov Ivan Ivanovich"
#
#  Scenario: Parental Client Options
#    Given well known business entity client "Mega"
#    Given well known business entity client "Alpha"
#    Given well known entrepreneur client "Ivanov Ivan Ivanovich"
#    When I get parental clients
#    Then parental clients count should be 2
#    Then parental clients names should be "Mega,Alpha"
#
#  Scenario: Regular Entrepreneur Client Registration
#    Given entrepreneur first name "Ilya"
#    Given entrepreneur last name "Lebedev"
#    Given entrepreneur patronymic "Nikolaevich"
#    Given entrepreneur inn "100485636000"
#    Given entrepreneur ogrnip "316100100060000"
#    Given entrepreneur phone "+79811648000"
#    Given entrepreneur email "lebedev@mail"
#    Given entrepreneur address "Karelia"
#    Given entrepreneur actual address "SPb"
#    Given entrepreneur bank details input
#      | property              | value                |
#      | account               | 40802810832130000000 |
#      | correspondent_account | 30101810600000000000 |
#      | bank_name             | AO 'Betabank'        |
#      | rcbic                 | 044030000            |
#
#    When I create entrepreneur client
#    When I get client
#
#    Then entrepreneur first name should be "Ilya"
#    Then entrepreneur last name should be "Lebedev"
#    Then entrepreneur patronymic should be "Nikolaevich"
#    Then entrepreneur inn should be "100485636000"
#    Then entrepreneur ogrnip should be "316100100060000"
#    Then entrepreneur phone should be "+79811648000"
#    Then entrepreneur email should be "lebedev@mail"
#    Then entrepreneur address should be "Karelia"
#    Then entrepreneur actual address should be "SPb"
#    Then entrepreneur bank details should be
#      | property              | value                |
#      | account               | 40802810832130000000 |
#      | correspondent_account | 30101810600000000000 |
#      | bank_name             | AO 'Betabank'        |
#      | rcbic                 | 044030000            |
#
#
#  Scenario: Regular Business Entity Client Registration
#    Given business entity full name "OOO Mega Company"
#    Given business entity short name "Mega"
#    Given business entity inn "100485636000"
#    Given business entity kpp "783901000"
#    Given business entity ogrn "1177847045000"
#    Given business entity phone "+79811648000"
#    Given business entity email "mega@mail"
#    Given business entity address "Moscow"
#    Given business entity actual address "SPb"
#    Given business entity postal address "Moscow"
#    Given business entity responsible persons
#      | full_name                    | phone        |
#      | Lebedev Ilya Nikolaevich     | +79811648000 |
#      | Golanov Maxim Aleksandrovich | +79811648001 |
#    Given business entity bank details input
#      | property              | value                |
#      | account               | 40802810832130000000 |
#      | correspondent_account | 30101810600000000000 |
#      | bank_name             | AO 'Betabank'        |
#      | rcbic                 | 044030000            |
#
#    When I create business entity client
#    When I get client
#
#    Then business entity full name should be "OOO Mega Company"
#    Then business entity short name should be "Mega"
#    Then business entity inn should be "100485636000"
#    Then business entity kpp should be "783901000"
#    Then business entity ogrn should be "1177847045000"
#    Then business entity phone should be "+79811648000"
#    Then business entity email should be "mega@mail"
#    Then business entity address should be "Moscow"
#    Then business entity actual address should be "SPb"
#    Then business entity postal address should be "Moscow"
#    Then business entity responsible persons should be
#      | full_name                    | phone        |
#      | Lebedev Ilya Nikolaevich     | +79811648000 |
#      | Golanov Maxim Aleksandrovich | +79811648001 |
#    Then business entity bank details input should be
#      | property              | value                |
#      | account               | 40802810832130000000 |
#      | correspondent_account | 30101810600000000000 |
#      | bank_name             | AO 'Betabank'        |
#      | rcbic                 | 044030000            |

