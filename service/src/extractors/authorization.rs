use actix_web::error::ErrorUnauthorized;
use actix_web::{dev, Error, FromRequest, HttpRequest};
use futures::future::{err, ok, Ready};
use jsonwebtoken::{decode, Algorithm, DecodingKey, Validation};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Debug, Serialize, Deserialize)]
pub struct Claims {
   pub sub: String,
}

#[derive(Clone)]
pub struct AccessToken {
    pub user: Uuid
}

impl FromRequest for AccessToken {
    type Error = Error;
    type Future = Ready<Result<AccessToken, Error>>;
    type Config = ();

    fn from_request(_req: &HttpRequest, _payload: &mut dev::Payload) -> Self::Future {
        let header = _req.headers().get("Authorization");
        match header {
            Some(value) => {
                let value = value.to_str().unwrap_or("");
                let token = value.split("Bearer").collect::<Vec<&str>>()[1].trim();
                let key = DecodingKey::from_secret("scrap".as_bytes());
                let validation = Validation {
                    leeway: 0,
                    validate_exp: false,
                    validate_nbf: false,
                    iss: None,
                    sub: None,
                    aud: None,
                    algorithms: vec![Algorithm::HS256],
                };
                match decode::<Claims>(token, &key, &validation) {
                    Ok(token) => match Uuid::parse_str(&token.claims.sub) {
                        Ok(user) => ok(AccessToken {user}),
                        Err(error) => {
                            warn!("Unable to authorize user because of id parsing, {}", error);
                            err(ErrorUnauthorized("Unauthorized"))
                        }
                    },
                    Err(error) => {
                        warn!("Unable to authorize user because of token decode {}", error);
                        err(ErrorUnauthorized("Unauthorized"))
                    }
                }
            }
            None => {
                warn!("Unable to authorize user, authorization header not specified");
                err(ErrorUnauthorized("Unauthorized"))
            },
        }
    }
}