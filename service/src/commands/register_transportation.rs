use actix_web::HttpResponse;
use actix_web::web::{Data, Json};
use chrono::Utc;
use uuid::Uuid;

use crate::application::{EventStore, Version};
use crate::application::aggregates::{AccessUser, ensure_access, Permission, ReplayError, UserAccessError};
use crate::application::projections::{TransportationProjection, NegotiationProjection};
use crate::domain::{Transportation, Negotiation};
use crate::model::{RegisterTransportationForm, Resource};
use crate::extractors::AccessToken;

pub async fn handle_register_transportation(
    form: Json<RegisterTransportationForm>,
    store: Data<EventStore>,
    projection: Data<TransportationProjection>,
    negotiation_projection: Data<NegotiationProjection>,
    access_token: AccessToken
) -> HttpResponse {
    let form = form.into_inner();
    let transporter = access_token.user;

    match register_transportation(&store, &projection, &negotiation_projection, transporter, form).await {
        Ok(id) => HttpResponse::Created().json(Resource { id }),
        Err(error) => match error {
            RegisterTransportationError::TransporterReplayError(error) => {
                error!("Unable to register transportation because of transporter replay, {:?}", error);
                HttpResponse::Unauthorized().finish()
            }
            RegisterTransportationError::TransporterAccessDenied(error) => {
                warn!("Unable to register transportation, transporter access denied, {:?}", error);
                HttpResponse::Forbidden().finish()
            }
            RegisterTransportationError::TransportationPublishError(error) => {
                error!("Unable to register transportation because of event store, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
            RegisterTransportationError::TransportationProjectionError(error) => {
                error!("Unable to register transportation because of projection, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
            RegisterTransportationError::NegotiationPublishError(error) => {
                error!("Unable to register transportation because of negotiation event store, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
            RegisterTransportationError::NegotiationProjectionError(error) => {
                error!("Unable to register transportation because of negotiation projection, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
        }
    }
}

#[derive(Debug)]
pub enum RegisterTransportationError {
    TransporterReplayError(ReplayError),
    TransporterAccessDenied(UserAccessError),
    TransportationPublishError(sqlx::Error),
    TransportationProjectionError(sqlx::Error),
    NegotiationPublishError(sqlx::Error),
    NegotiationProjectionError(sqlx::Error),
}


pub async fn register_transportation(
    store: &EventStore,
    projection: &TransportationProjection,
    negotiation_projection: &NegotiationProjection,
    user: Uuid,
    form: RegisterTransportationForm,
) -> Result<Uuid, RegisterTransportationError> {
    let now = Utc::now();
    let transportation = Uuid::new_v4();

    let user = AccessUser::replay(&user, store)
        .await
        .map_err(RegisterTransportationError::TransporterReplayError)?;

    let transporter = match user.role_entity.as_ref() {
        Some(entity_id) => entity_id.clone(),
        None => return Err(RegisterTransportationError::TransporterAccessDenied(UserAccessError::RoleEntityUnassigned))
    };

    ensure_access(user, Permission::TransportationDraftChange)
        .map_err(RegisterTransportationError::TransporterAccessDenied)?;

    let mut events = vec![
        Transportation::Registered {
            transporter,
            date: now,
        },
        Transportation::TruckAssigned {
            transporter,
            date: now,
            truck: form.truck,
            execution_date: form.execution_date,
        },
    ];

    for destination in form.destinations {
        events.push(Transportation::DestinationAdded {
            id: destination.id,
            transporter,
            date: now,
            collection_start_time: destination.collection_start_time,
            waste_generation_point: destination.waste_generation_point,
        });

        for collection in destination.collections {
            events.push(Transportation::WasteCollected {
                id: collection.id,
                transporter,
                date: now,
                destination: destination.id,
                container_type: collection.container_type,
                container_volume: collection.container_volume,
                container_utilization: collection.container_utilization,
                waste_volume_m3: collection.waste_volume_m3,
            });

            events.push(Transportation::WasteCollectionPhotographed {
                id: collection.id,
                transporter,
                date: now,
                photos: collection.photos,
            });
        }
    }

    store.store(&transporter, &transportation, &events, &Version::Initial)
        .await
        .map_err(RegisterTransportationError::TransportationPublishError)?;

    projection.apply_transportation_events(transportation, &events)
        .await
        .map_err(RegisterTransportationError::TransportationProjectionError)?;

    let negotiation = Uuid::new_v4();
    let events = vec![
        Negotiation::Started {
            transporter,
            transportation,
            transportation_version: 0,
            date: now
        }
    ];

    store.store(&transporter, &negotiation, &events, &Version::Initial)
        .await
        .map_err(RegisterTransportationError::NegotiationPublishError)?;

    negotiation_projection.apply_negotiation_events(negotiation, &events)
        .await
        .map_err(RegisterTransportationError::NegotiationProjectionError)?;

    Ok(transportation)
}