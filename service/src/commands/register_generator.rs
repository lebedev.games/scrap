use actix_web::HttpResponse;
use actix_web::web::{Data, Json};
use chrono::Utc;
use uuid::Uuid;

use crate::application::{EventStore, Version};
use crate::application::aggregates::{AccessUser, ensure_access, Permission, ReplayError, UserAccessError};
use crate::application::projections::GeneratorProjection;
use crate::domain::{BusinessEntity, Entrepreneur, Generator};
use crate::extractors::AccessToken;
use crate::model::{GeneratorLegalEntity, RegisterGeneratorForm, RegisterGeneratorResponse};

pub async fn handle_register_generator(
    form: Json<RegisterGeneratorForm>,
    store: Data<EventStore>,
    projection: Data<GeneratorProjection>,
    access_token: AccessToken
) -> HttpResponse {
    let registrar = access_token.user;
    let now = Utc::now();

    let result = match AccessUser::replay(&registrar, &store).await {
        Ok(registrar) => {
            match ensure_access(registrar, Permission::GeneratorRegister) {
                Ok(registrar) => {
                    let form = form.into_inner();
                    let registrar = registrar.id;
                    match form.legal_entity {
                        GeneratorLegalEntity::BusinessEntity(input) => {
                            let mut business_entity_events = vec![
                                BusinessEntity::Registered {
                                    registrar,
                                    date: now,
                                },
                                BusinessEntity::AdministrativeNameUpdated {
                                    registrar,
                                    full_name: input.full_name,
                                    short_name: input.short_name,
                                },
                                BusinessEntity::TaxIdentificationUpdated {
                                    registrar,
                                    inn: input.inn,
                                },
                                BusinessEntity::TaxRegistrationUpdated {
                                    registrar,
                                    kpp: input.kpp,
                                },
                                BusinessEntity::PrimaryRegistrationUpdated {
                                    registrar,
                                    ogrn: input.ogrn,
                                },
                                BusinessEntity::PhoneContactUpdated {
                                    registrar,
                                    phone: input.phone,
                                },
                                BusinessEntity::AddressUpdated {
                                    registrar,
                                    full_address: input.address,
                                },
                                BusinessEntity::PostalAddressUpdated {
                                    registrar,
                                    full_address: input.postal_address,
                                },
                                BusinessEntity::ActualAddressUpdated {
                                    registrar,
                                    full_address: input.actual_address,
                                },
                                BusinessEntity::BankDetailsUpdated {
                                    registrar,
                                    account: input.bank_details.account,
                                    correspondent_account: input.bank_details.correspondent_account,
                                    bank_name: input.bank_details.bank_name,
                                    rcbic: input.bank_details.rcbic,
                                }
                            ];

                            for responsible in input.responsible_persons {
                                business_entity_events.push(BusinessEntity::ResponsiblePersonAdded {
                                    registrar,
                                    full_name: responsible.full_name,
                                    phone: responsible.phone,
                                });
                            }

                            if let Some(email) = input.email {
                                business_entity_events.push(BusinessEntity::EmailContactUpdated {
                                    registrar,
                                    email,
                                });
                            }

                            let business_entity = Uuid::new_v4();

                            match store.store(&registrar, &business_entity, &business_entity_events, &Version::Initial).await {
                                Ok(_) => {
                                    let mut generator_events = vec![
                                        Generator::Registered {
                                            registrar,
                                            date: now,
                                        },
                                        Generator::BusinessEntityAssigned {
                                            registrar,
                                            business_entity,
                                        }
                                    ];

                                    if let Some(parent) = form.parent {
                                        generator_events.push(Generator::ParentUpdated {
                                            registrar,
                                            parent,
                                        });
                                    }

                                    let generator = Uuid::new_v4();

                                    match store.store(&registrar, &generator, &generator_events, &Version::Initial).await {
                                        Err(error) => Err(Fail::GeneratorEventStoreError(error)),
                                        _ => {
                                            projection.apply_generator_events(generator, &generator_events).await;
                                            projection.apply_business_entity(business_entity, &business_entity_events).await;
                                            Ok(generator)
                                        }
                                    }
                                }
                                Err(error) => Err(Fail::BusinessEntityEventStoreError(error))
                            }
                        }
                        GeneratorLegalEntity::Entrepreneur(input) => {
                            let mut entrepreneur_events = vec![
                                Entrepreneur::Registered {
                                    registrar,
                                    date: now,
                                },
                                Entrepreneur::PersonalNameUpdated {
                                    registrar,
                                    first_name: input.first_name,
                                    last_name: input.last_name,
                                    patronymic: input.patronymic,
                                },
                                Entrepreneur::TaxIdentificationUpdated {
                                    registrar,
                                    inn: input.inn,
                                },
                                Entrepreneur::PrimaryRegistrationUpdated {
                                    registrar,
                                    ogrnip: input.ogrnip,
                                },
                                Entrepreneur::PhoneContactUpdated {
                                    registrar,
                                    phone: input.phone,
                                },
                                Entrepreneur::AddressUpdated {
                                    registrar,
                                    full_address: input.address,
                                },
                                Entrepreneur::ActualAddressUpdated {
                                    registrar,
                                    full_address: input.actual_address,
                                },
                                Entrepreneur::BankDetailsUpdated {
                                    registrar,
                                    account: input.bank_details.account,
                                    correspondent_account: input.bank_details.correspondent_account,
                                    bank_name: input.bank_details.bank_name,
                                    rcbic: input.bank_details.rcbic,
                                }
                            ];

                            if let Some(email) = input.email {
                                entrepreneur_events.push(Entrepreneur::EmailContactUpdated {
                                    registrar,
                                    email,
                                })
                            }

                            let entrepreneur = Uuid::new_v4();

                            match store.store(&registrar, &entrepreneur, &entrepreneur_events, &Version::Initial).await {
                                Ok(_) => {
                                    let mut generator_events = vec![
                                        Generator::Registered {
                                            registrar,
                                            date: now,
                                        },
                                        Generator::EntrepreneurAssigned {
                                            registrar,
                                            entrepreneur,
                                        }
                                    ];

                                    if let Some(parent) = form.parent {
                                        generator_events.push(Generator::ParentUpdated {
                                            registrar,
                                            parent,
                                        });
                                    }

                                    let generator = Uuid::new_v4();

                                    match store.store(&registrar, &generator, &generator_events, &Version::Initial).await {
                                        Err(error) => Err(Fail::GeneratorEventStoreError(error)),
                                        _ => {
                                            projection.apply_generator_events(generator, &generator_events).await;
                                            projection.apply_entrepreneur(entrepreneur, &entrepreneur_events).await;
                                            Ok(generator)
                                        }
                                    }
                                }
                                Err(error) => Err(Fail::EntrepreneurEventStoreError(error))
                            }
                        }
                    }
                },
                Err(error) => Err(Fail::ActionForbidden(error))
            }
        }
        Err(error) => Err(Fail::RegistrarReplayError(error))
    };


    match result {
        Ok(id) => {
            HttpResponse::Created().json(RegisterGeneratorResponse { id })
        }
        Err(error) => match error {
            Fail::RegistrarReplayError(error) => {
                error!("Unable to register generator because of registrar replay, {:?}", error);
                HttpResponse::UnprocessableEntity().finish()
            }
            Fail::ActionForbidden(error) => {
                info!("Unable to register generator, action forbidden, {:?}", error);
                HttpResponse::Forbidden().finish()
            },
            Fail::BusinessEntityEventStoreError(error) => {
                error!("Unable to register generator because of business entity event store, {}", error);
                return HttpResponse::UnprocessableEntity().finish()
            }
            Fail::EntrepreneurEventStoreError(error) => {
                error!("Unable to register generator because of entrepreneur event store, {}", error);
                return HttpResponse::UnprocessableEntity().finish()
            }
            Fail::GeneratorEventStoreError(error) => {
                error!("Unable to register generator because of event store, {:?}", error);
                return HttpResponse::UnprocessableEntity().finish()
            }
        }
    }
}

enum Fail {
    RegistrarReplayError(ReplayError),
    ActionForbidden(UserAccessError),
    BusinessEntityEventStoreError(sqlx::Error),
    EntrepreneurEventStoreError(sqlx::Error),
    GeneratorEventStoreError(sqlx::Error),
}