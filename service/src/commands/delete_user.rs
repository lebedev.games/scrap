use actix_web::HttpResponse;
use actix_web::web::{Data, Path};
use chrono::Utc;
use uuid::Uuid;

use crate::application::aggregates::{AccessUser, ensure_access, Permission, PublishError, ReplayError, UserAccessError};
use crate::application::EventStore;
use crate::application::projections::UserProjection;
use crate::domain::User::Deleted;
use crate::extractors::AccessToken;
use crate::model::{Resource};

pub async fn handle_delete_user(
    resource: Path<Resource>,
    store: Data<EventStore>,
    projection: Data<UserProjection>,
    access_token: AccessToken
) -> HttpResponse {
    let user = resource.id;
    let registrar = access_token.user;

    let result = match AccessUser::replay(&registrar, &store).await {
        Ok(registrar) => {
            match ensure_access(registrar, Permission::UserDelete) {
                Ok(registrar) => match AccessUser::replay(&user, &store).await {
                    Ok(user) => {
                        if !user.is_deleted {
                            let events = vec![
                                Deleted {
                                    registrar: registrar.id,
                                    date: Utc::now(),
                                }
                            ];
                            match user.publish(&registrar.id, &store, &events).await {
                                Err(error) => Err(Fail::EventStoreError(error)),
                                _ => Ok(projection.apply(&user.id, &events).await)
                            }
                        } else {
                            Err(Fail::UserAlreadyDeleted)
                        }
                    }
                    Err(error) => match error {
                        ReplayError::EntityNotFound(id) => Err(Fail::UserNotFound(id)),
                        ReplayError::ParsingError(error) => Err(Fail::UserReplayError(error))
                    }
                },
                Err(error) => Err(Fail::ActionForbidden(error))
            }
        }
        Err(error) => Err(Fail::RegistrarReplayError(error))
    };

    match result {
        Err(error) => match error {
            Fail::RegistrarReplayError(error) => {
                error!("Unable to delete user because of grantor replay, {:?}", error);
                return HttpResponse::UnprocessableEntity().finish();
            }
            Fail::UserNotFound(id) => {
                info!("Unable to delete user, user id={} not found", id);
                return HttpResponse::NotFound().finish();
            }
            Fail::UserReplayError(error) => {
                error!("Unable to delete user because of user replay, {}", error);
                return HttpResponse::UnprocessableEntity().finish();
            }
            Fail::UserAlreadyDeleted => {
                info!("Unable to delete user, already deleted");
                return HttpResponse::NoContent().finish();
            }
            Fail::ActionForbidden(error) => {
                info!("Unable to delete user, action forbidden, {:?}", error);
                return HttpResponse::Forbidden().finish();
            }
            Fail::EventStoreError(error) => {
                error!("Unable to delete user because of event store, {:?}", error);
                return HttpResponse::UnprocessableEntity().finish();
            }
        },
        _ => HttpResponse::NoContent().finish()
    }
}

enum Fail {
    RegistrarReplayError(ReplayError),
    UserNotFound(Uuid),
    UserReplayError(sqlx::Error),
    UserAlreadyDeleted,
    ActionForbidden(UserAccessError),
    EventStoreError(PublishError),
}