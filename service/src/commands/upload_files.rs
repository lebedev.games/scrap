use actix_multipart::Multipart;
use actix_web::{Error, HttpResponse};
use async_std::fs::File;
use futures::{AsyncWriteExt, StreamExt, TryStreamExt};
use uuid::Uuid;

use crate::model::FileUpload;

pub async fn handle_upload_files(mut payload: Multipart) -> Result<HttpResponse, Error> {
    let mut upload = FileUpload {
        files: vec![]
    };

    while let Ok(Some(mut field)) = payload.try_next().await {
        // let content_type = field.content_disposition().unwrap();
        let id = Uuid::new_v4();
        let filename = id.to_string();
        let filepath = format!("./upload/{}", &filename);
        let mut file = File::create(filepath).await?;

        while let Some(chunk) = field.next().await {
            let data = chunk.unwrap();
            file.write_all(&data).await?;
        }

        upload.files.push(id);
    }

    Ok(HttpResponse::Ok().json(upload))
}