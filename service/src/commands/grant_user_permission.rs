use actix_web::HttpResponse;
use actix_web::web::{Data, Json, Path};
use chrono::Utc;
use uuid::Uuid;

use crate::application::aggregates::{AccessUser, ensure_access, is_user_has_permission, Permission, PublishError, ReplayError, UserAccessError};
use crate::application::EventStore;
use crate::application::projections::UserProjection;
use crate::domain::User::PermissionGranted;
use crate::extractors::AccessToken;
use crate::model::{GrantUserPermissionBody, Resource};

pub async fn handle_grant_user_permission(
    resource: Path<Resource>,
    body: Json<GrantUserPermissionBody>,
    store: Data<EventStore>,
    projection: Data<UserProjection>,
    access_token: AccessToken
) -> HttpResponse {
    let user = resource.id;
    let permission = &body.permission;
    let grantor = access_token.user;

    let result = match AccessUser::replay(&grantor, &store).await {
        Ok(grantor) => {
            match ensure_access(grantor, Permission::UserGrantPermission) {
                Ok(grantor) => match AccessUser::replay(&user, &store).await {
                    Ok(user) => {
                        let permission = Permission::parse(permission.clone());
                        if !is_user_has_permission(&user, &permission) {
                            let events = vec![
                                PermissionGranted {
                                    grantor: grantor.id,
                                    permission: permission.to_string(),
                                    date: Utc::now(),
                                }
                            ];
                            match user.publish(&grantor.id, &store, &events).await {
                                Err(error) => Err(Fail::EventStoreError(error)),
                                _ => Ok(projection.apply(&user.id, &events).await)
                            }
                        } else {
                            Err(Fail::UserAlreadyHasPermission)
                        }
                    }
                    Err(error) => match error {
                        ReplayError::EntityNotFound(id) => Err(Fail::UserNotFound(id)),
                        ReplayError::ParsingError(error) => Err(Fail::UserReplayError(error))
                    }
                }
                Err(error) => Err(Fail::ActionForbidden(error))
            }
        }
        Err(error) => Err(Fail::GrantorReplayError(error))
    };

    match result {
        Err(error) => match error {
            Fail::GrantorReplayError(error) => {
                error!("Unable to grant user permission because of grantor replay, {:?}", error);
                return HttpResponse::UnprocessableEntity().finish();
            }
            Fail::UserNotFound(id) => {
                info!("Unable to grant user permission, user id={} not found", id);
                return HttpResponse::NotFound().finish();
            }
            Fail::UserReplayError(error) => {
                error!("Unable to grant user permission because of user replay, {}", error);
                return HttpResponse::UnprocessableEntity().finish();
            }
            Fail::UserAlreadyHasPermission => {
                info!("Unable to grant user permission, already granted");
                return HttpResponse::NoContent().finish();
            }
            Fail::ActionForbidden(error) => {
                info!("Unable to grant user permission, action forbidden, {:?}", error);
                return HttpResponse::Forbidden().finish();
            }
            Fail::EventStoreError(error) => {
                error!("Unable to grant user permission because of event store, {:?}", error);
                return HttpResponse::UnprocessableEntity().finish();
            }
        },
        _ => HttpResponse::NoContent().finish()
    }
}

enum Fail {
    GrantorReplayError(ReplayError),
    UserNotFound(Uuid),
    UserReplayError(sqlx::Error),
    UserAlreadyHasPermission,
    ActionForbidden(UserAccessError),
    EventStoreError(PublishError),
}
