use actix_web::HttpResponse;
use actix_web::web::{Data, Json};
use uuid::Uuid;

use crate::application::aggregates::{Permission, Role};
use crate::application::EventStore;
use crate::application::projections::{GenerationPointProjection, TransporterProjection, TruckProjection, UserProjection, GeneratorProjection};
use crate::commands;
use crate::domain::SYSTEM_USER;
use crate::extractors::AccessToken;
use crate::model::{BankDetails, BusinessEntityModel, DemoSetUp, EntrepreneurModel, RegisterTransporterForm, RegisterUserForm, ResponsiblePerson, TransporterLegalEntityModel, Resource, RegisterTruckForm, RegisterGeneratorForm, GeneratorLegalEntity, RegisterGenerationPointForm, RegisterGenerationPointFormTransporter, RegisterGenerationPointFormContainer};
use actix_web::body::{ResponseBody, Body};
use serde::Deserialize;
use sqlx::types::Decimal;
use chrono::Utc;

pub async fn handle_set_up_demo(
    store: Data<EventStore>,
    user_projection: Data<UserProjection>,
    transporter_projection: Data<TransporterProjection>,
    generator_projection: Data<GeneratorProjection>,
    generation_point_projection: Data<GenerationPointProjection>,
    truck_projection: Data<TruckProjection>,
) -> HttpResponse {
    let mut demo = DemoSetUp { errors: vec![] };
    let token = AccessToken { user: SYSTEM_USER };

    // Register Contractor
    let form = RegisterUserForm {
        user_id: Uuid::parse_str("a12e313f-c4a5-4366-b69f-400b9319849e").unwrap(),
        role: Role::Contractor.to_string(),
        name: "Ген. подрядчик".to_string(),
        login: "contractor".to_string(),
        password: "contractor".to_string(),
        permissions: vec![
            Permission::FileUpload.to_string(),
            Permission::GeneratorRegister.to_string(),
            Permission::GeneratorList.to_string(),
            Permission::GeneratorGet.to_string(),
            Permission::GeneratorGetParental.to_string(),
            Permission::GenerationPointRegister.to_string(),
            Permission::GenerationPointGet.to_string(),
            Permission::GenerationPointList.to_string(),
            Permission::TransporterRegister.to_string(),
            Permission::TransporterGet.to_string(),
            Permission::TransporterList.to_string(),
            Permission::TransportationDraftGet.to_string(),
            Permission::NegotiationComplete.to_string(),
            Permission::NegotiationIssuesDeclare.to_string(),
            Permission::NegotiationGet.to_string(),
            Permission::NegotiationList.to_string(),
            Permission::TruckRegister.to_string(),
            Permission::TruckGet.to_string(),
            Permission::TruckList.to_string(),
            Permission::Reports.to_string(),
        ],
    };
    if let Some(error) = commands::handle_register_user(Json(form), store.clone(), user_projection.clone(), token.clone()).await.error() {
        demo.errors.push(format!("Unable to register contractor, {:?}", error))
    }

    // Register Supervisor
    let form = RegisterUserForm {
        user_id: Uuid::parse_str("6dac534f-2032-418f-8332-f3c9b7a99432").unwrap(),
        role: Role::Supervisor.to_string(),
        name: "Рег. оператор".to_string(),
        login: "supervisor".to_string(),
        password: "supervisor".to_string(),
        permissions: vec![
            Permission::GeneratorRegister.to_string(),
            Permission::GeneratorList.to_string(),
            Permission::GeneratorGet.to_string(),
            Permission::GeneratorGetParental.to_string(),
            Permission::GenerationPointRegister.to_string(),
            Permission::GenerationPointGet.to_string(),
            Permission::GenerationPointList.to_string(),
            Permission::NegotiationGet.to_string(),
            Permission::NegotiationList.to_string(),
            Permission::TransportationDraftGet.to_string(),
            Permission::TransporterGet.to_string(),
            Permission::TransporterList.to_string(),
            Permission::TruckGet.to_string(),
            Permission::TruckList.to_string(),
            Permission::Reports.to_string(),
        ],
    };
    if let Some(error) = commands::handle_register_user(Json(form), store.clone(), user_projection.clone(), token.clone()).await.error() {
        demo.errors.push(format!("Unable to register supervisor, {:?}", error))
    }

    // Register Transporters
    let make_business_entity_transporter = |inn: &str, name: &str, ogrn: &str, parent: Option<Uuid>| {
        RegisterTransporterForm {
            legal_entity: TransporterLegalEntityModel::BusinessEntity(BusinessEntityModel {
                full_name: format!("Открытое акционерное общество «{}»", name.clone()),
                short_name: name.to_string(),
                inn: inn.to_string(),
                kpp: "784001001".to_string(),
                ogrn: ogrn.to_string(),
                phone: "+74999998283".to_string(),
                email: Some("test@mail.ru".to_string()),
                address: "Российская Федерация, 191002, г. Санкт-Петербург, ул. Достоевского, д. 15".to_string(),
                postal_address: "115172, Российская Федерация, Москва, ул. Гончарная, д. 30, стр 1.".to_string(),
                actual_address: "Российская Федерация, 191002, г. Санкт-Петербург, ул. Достоевского, д. 15".to_string(),
                responsible_persons: vec![
                    ResponsiblePerson {
                        full_name: "Елизаветинская Оксана Олеговна".to_string(),
                        phone: "+74999758990".to_string(),
                    },
                    ResponsiblePerson {
                        full_name: "Петров Олег Юрьевич".to_string(),
                        phone: "+74999758990".to_string(),
                    },
                ],
                bank_details: BankDetails {
                    account: "40702810036580130000".to_string(),
                    correspondent_account: "37893810400000000000".to_string(),
                    bank_name: "Публичное акционерное общество Сбербанк России".to_string(),
                    rcbic: "047965225".to_string(),
                },
            }),
            fkko: "fkko".to_string(),
            parent,
            truck_fleet_types: vec![
                Uuid::parse_str("1964b722-2ce3-4a67-a948-26ebd2d0171b").unwrap()
            ],
            note: format!("Заметки о {}", name),
        }
    };

    let make_entrepreneur_transporter = |inn: &str, last_name: &str, first_name: &str, patronymic: &str, ogrnip: &str, parent: Option<Uuid>| {
        RegisterTransporterForm {
            legal_entity: TransporterLegalEntityModel::Entrepreneur(EntrepreneurModel {
                first_name: first_name.to_string(),
                last_name: last_name.to_string(),
                patronymic: patronymic.to_string(),
                inn: inn.to_string(),
                ogrnip: ogrnip.to_string(),
                phone: "+79443138990".to_string(),
                email: None,
                address: "197664, Российская Федерация, г. Барнаул, Александровская ул., дом 7, квартира 192".to_string(),
                actual_address: "197664, Российская Федерация, г. Барнаул, Александровская ул., дом 7, квартира 192".to_string(),
                bank_details: BankDetails {
                    account: "40702810036580130000".to_string(),
                    correspondent_account: "37893810400000000000".to_string(),
                    bank_name: "Публичное акционерное общество Сбербанк России".to_string(),
                    rcbic: "047965225".to_string(),
                },
            }),
            fkko: "fkko".to_string(),
            parent,
            truck_fleet_types: vec![
                Uuid::parse_str("d11a6833-cb25-438e-8eab-05f15329491f").unwrap()
            ],
            note: format!("Заметки о {}", ogrnip.to_string()),
        }
    };

    // Main Demo Transporter
    let mut main_transporter = Uuid::new_v4();
    let registration = commands::handle_register_transporter(
        Json(make_business_entity_transporter("7767943778", "Перевозчикофф", "1027700198001", None)),
        store.clone(),
        transporter_projection.clone(),
        generation_point_projection.clone(),
        truck_projection.clone(),
        user_projection.clone(),
        token.clone(),
    );
    let response = registration.await;
    if let Some(error) = response.error() {
        demo.errors.push(format!("Unable to register transporter, {:?}", error))
    } else {
        let resource: Resource = extract_body(&response);
        main_transporter = resource.id;
    }
    //

    let transporter_registration_forms = vec![
        make_business_entity_transporter("7727563778", "Перевозчик Снег и Дождь", "1027700198002", None),
        make_entrepreneur_transporter("781633467974", "Игнатов", "Валентин", "Николаевич", "316100100060001", None),
        make_entrepreneur_transporter("781633333333", "Кондратьева", "Ольга", "Павловна", "316100100060002", None),
        make_entrepreneur_transporter("132808730606", "Смирнов", "Иван", "Алексеевич", "316100100060003", None),
        make_business_entity_transporter("7727563778", "Перевозчик Ландыши", "1027700198003", Some(main_transporter.clone())),
        make_entrepreneur_transporter("781633467974", "Петруйко", "Олег", "Игоревич", "316100100060004", None),
        make_business_entity_transporter("7727563778", "Перевозчик Кулебяки", "1027700198004", None),
        make_business_entity_transporter("7727563779", "Перевозчик Янаварь", "1027700198005", None),
        make_entrepreneur_transporter("781633468074", "Абдул-кызы", "Ибрагим", "Жоресович", "316100100060005", None),
        make_entrepreneur_transporter("781633460975", "Константинова", "Алина", "Игоревич", "316100100060006", None),
        make_business_entity_transporter("7727555777", "Перевозчик Супер Перевозчик", "1027700198006", None),
        make_entrepreneur_transporter("781633469076", "Степанов", "Артем", "Ильич", "316100100060007", Some(main_transporter.clone())),
        make_entrepreneur_transporter("781633460077", "Галанов", "Илья", "Юрьевич", "316100100060008", None),
        make_entrepreneur_transporter("781633460078", "Петрых", "Анастасия", "Валерьевна", "316100100060009", None),
    ];
    for form in transporter_registration_forms {
        let registration = commands::handle_register_transporter(
            Json(form),
            store.clone(),
            transporter_projection.clone(),
            generation_point_projection.clone(),
            truck_projection.clone(),
            user_projection.clone(),
            token.clone(),
        );
        let response = registration.await;
        if let Some(error) = response.error() {
            demo.errors.push(format!("Unable to register transporter, {:?}", error))
        }
    }

    let truck_kontainer = Uuid::parse_str("999dd103-83e8-417e-bd7a-b31f39344fd1").unwrap();
    let truck_kuzov = Uuid::parse_str("deac883d-57af-47e3-9a0d-465a0499151f").unwrap();
    let truck_samosval = Uuid::parse_str("b07ff2f9-eff5-4d88-b79e-9966f4164c0e").unwrap();
    let truck_arend = Uuid::parse_str("d11a6833-cb25-438e-8eab-05f15329491f").unwrap();
    let make_truck = |number: &str, cargo_body_type: &Uuid, transporter: &Uuid, truck_fleet_type: &Uuid| {
        RegisterTruckForm {
            owner: format!("Владелец {}", number),
            transporter: transporter.clone(),
            fleet_type: truck_fleet_type.clone(),
            vin: "4USBT53544LT26841".to_string(),
            registration_number: number.to_string(),
            garaging_number: "873YT76543F56432876T".to_string(),
            vehicle_model: "SCANIA P360 (XT) ZOELLER MEDIUM XXL 22,6".to_string(),
            waste_equipment_model: "ЭКО-МБ18К56".to_string(),
            cargo_body_type: cargo_body_type.clone(),
            required_driver_license_category: Uuid::parse_str("1a957e14-1658-452d-8071-ce41386e3a24").unwrap(),
            axle_configuration: Uuid::parse_str("1207d627-e764-467a-94a0-34d1122841fb").unwrap(),
            production_year: 1965,
            navigation_equipment_name: "ГЛОНАСС Carnet F900HD".to_string(),
            empty_weight_kg: Decimal::new(3000, 0),
            cargo_capacity_kg: Decimal::new(2000, 0),
        }
    };
    let truck_registration_forms = vec![
        make_truck("М765ТР78", &truck_samosval, &main_transporter, &truck_arend),
        make_truck("М654ТР78", &truck_kontainer, &main_transporter, &truck_arend),
        make_truck("М345ТР78", &truck_kuzov, &main_transporter, &truck_arend),
        make_truck("М747ТР78", &truck_samosval, &main_transporter, &truck_arend),
        make_truck("М734ТР78", &truck_kuzov, &main_transporter, &truck_arend),
        make_truck("C225ТР78", &truck_kontainer, &main_transporter, &truck_arend),
        make_truck("C117ТР78", &truck_samosval, &main_transporter, &truck_arend),
        make_truck("C558ТР78", &truck_kontainer, &main_transporter, &truck_arend),
        make_truck("O884ТР78", &truck_kuzov, &main_transporter, &truck_arend),
        make_truck("М443ТР78", &truck_kuzov, &main_transporter, &truck_arend),
        make_truck("М995ТР78", &truck_kuzov, &main_transporter, &truck_arend),
        make_truck("T033ТР78", &truck_kontainer, &main_transporter, &truck_arend),
        make_truck("T883ТР78", &truck_samosval, &main_transporter, &truck_arend),
        make_truck("М773ТР78", &truck_kontainer, &main_transporter, &truck_arend),
        make_truck("O335ТР78", &truck_samosval, &main_transporter, &truck_arend),
    ];
    for form in truck_registration_forms {
        let registration = commands::handle_register_truck(
            Json(form),
            store.clone(),
            truck_projection.clone(),
            token.clone(),
        );
        let response = registration.await;
        if let Some(error) = response.error() {
            demo.errors.push(format!("Unable to register truck, {:?}", error))
        }
    }

    let form = RegisterGeneratorForm {
        legal_entity: GeneratorLegalEntity::BusinessEntity(BusinessEntityModel {
            full_name: "Открытое акционерное общество «Мусорообразователь Компани»".to_string(),
            short_name: "Мусорообразователь Компани".to_string(),
            inn: "7707049388".to_string(),
            kpp: "784001001".to_string(),
            ogrn: "1027700198767".to_string(),
            phone: "+74999998283".to_string(),
            email: Some("test@mail.ru".to_string()),
            address: "Российская Федерация, 191002, г. Санкт-Петербург, ул. Достоевского, д. 15".to_string(),
            postal_address: "115172, Российская Федерация, Москва, ул. Гончарная, д. 30, стр 1.".to_string(),
            actual_address: "Российская Федерация, 191002, г. Санкт-Петербург, ул. Достоевского, д. 15".to_string(),
            responsible_persons: vec![
                ResponsiblePerson {
                    full_name: "Елизаветинская Оксана Олеговна".to_string(),
                    phone: "+74999758990".to_string(),
                },
                ResponsiblePerson {
                    full_name: "Петров Олег Юрьевич".to_string(),
                    phone: "+74999758990".to_string(),
                },
            ],
            bank_details: BankDetails {
                account: "40702810036580130000".to_string(),
                correspondent_account: "37893810400000000000".to_string(),
                bank_name: "Публичное акционерное общество Сбербанк России".to_string(),
                rcbic: "047965225".to_string(),
            },
        }),
        parent: None,
    };
    let registration = commands::handle_register_generator(
        Json(form),
        store.clone(),
        generator_projection.clone(),
        token.clone(),
    );
    let mut main_generator = Uuid::new_v4();
    let response = registration.await;
    if let Some(error) = response.error() {
        demo.errors.push(format!("Unable to register generator, {:?}", error))
    } else {
        let resource: Resource = extract_body(&response);
        main_generator = resource.id;
    }

    let make_generation_point = |system_number: &str, transporter: &Uuid, generator: &Uuid, address: &str, area: i64, containers: Vec<Uuid>| {
        RegisterGenerationPointForm {
            internal_system_number: system_number.to_string(),
            external_registry_number: "62517".to_string(),
            waste_type: Uuid::parse_str("529a07b6-fa73-4536-ac94-fabf95b9ff63").unwrap(), // ТКО
            organization_legal_type: Uuid::parse_str("2d20f39e-e332-49e3-b45f-a49fcf0006d6").unwrap(), // ТСЖ
            operational_status: Uuid::parse_str("d6e6e2a6-989c-4446-9c64-da534fbda03b").unwrap(), // Активная
            full_address: address.to_string(),
            coordinates: "59.871994, 30.441502".to_string(),
            area_m2: Decimal::new(area, 0),
            covering_type: Uuid::parse_str("2f9e0dea-50e4-40b1-a182-216594d76024").unwrap(), // Грунт
            fence_material: Uuid::parse_str("737ff861-8e77-46df-b42a-ca8206f5e7f7").unwrap(), // Бетон
            service_company_name: None,
            generator: generator.clone(),
            description: format!("Описание {}", system_number),
            containers: containers.into_iter().map(|container_type| RegisterGenerationPointFormContainer { container_type }).collect(),
            transporter: RegisterGenerationPointFormTransporter {
                transporter: transporter.clone(),
                start_transportation_date: Utc::now(),
                end_transportation_date: Utc::now(),
                frequency: "2 раза в неделю".to_string(),
            },
            photos: vec![],
            accrual_method: Uuid::parse_str("12ff4c0f-4897-4f3f-af55-37edcbf60803").unwrap(),
            measurement_unit: Uuid::parse_str("167d3dba-91a7-4a85-94ad-0aa9443e0732").unwrap(), // m3
        }
    };
    let container_plastic = Uuid::parse_str("5f701da0-afc9-4d6d-809f-db5531cef21e").unwrap();
    let container_metal = Uuid::parse_str("b4ab2b45-e647-471b-90e4-7768ff0f4831").unwrap();
    let container_kgo = Uuid::parse_str("aba815af-dc2e-4265-b42c-6ecc87dd89c8").unwrap();
    let forms = vec![
        make_generation_point("ТКО-543", &main_transporter, &main_generator, "Российская Федерация, 191002, г. Санкт-Петербург, ул. Маршала Новикова, д. 1", 15, vec![container_plastic, container_plastic, container_metal, container_kgo]),
        make_generation_point("ТКО-134", &main_transporter, &main_generator, "Российская Федерация,191025, г. Санкт-Петербург, Стремянная ул., 2", 20, vec![container_plastic]),
        make_generation_point("ТКО-148", &main_transporter, &main_generator, "Российская Федерация, 191002, г. Санкт-Петербург, просп. Непокоренных, д. 3", 20, vec![container_metal]),
        make_generation_point("ТКО-171", &main_transporter, &main_generator, "Российская Федерация, 191002, г. Санкт-Петербург, ул. Маршала Новикова, д. 4", 30, vec![container_kgo]),
        make_generation_point("ТКО-200", &main_transporter, &main_generator, "Российская Федерация, 191002, г. Санкт-Петербург, Ланское ш., д. 5", 30, vec![container_plastic, container_metal, container_kgo]),
        make_generation_point("ТКО-505", &main_transporter, &main_generator, "Российская Федерация,191025, г. Санкт-Петербург, Стремянная ул., 6", 20, vec![container_plastic, container_metal, container_kgo]),
        make_generation_point("ТКО-203", &main_transporter, &main_generator, "Российская Федерация, 191002, г. Санкт-Петербург, ул. Маршала Новикова, д. 7", 20, vec![container_plastic, container_metal, container_kgo]),
        make_generation_point("ТКО-227", &main_transporter, &main_generator, "Российская Федерация, 191002, г. Санкт-Петербург, просп. Непокоренных, д. 8", 40, vec![container_plastic, container_metal, container_kgo]),
        make_generation_point("ТКО-280", &main_transporter, &main_generator, "Российская Федерация, 191002, г. Санкт-Петербург, Ланское ш., д. 9", 30, vec![container_plastic, container_metal, container_kgo]),
        make_generation_point("ТКО-281", &main_transporter, &main_generator, "Российская Федерация,191025, г. Санкт-Петербург, Стремянная ул., 10", 20, vec![container_plastic, container_metal, container_kgo]),
        make_generation_point("ТКО-282", &main_transporter, &main_generator, "Российская Федерация, 191002, г. Санкт-Петербург, ул. Маршала Новикова, д. 11", 80, vec![container_plastic, container_metal, container_kgo]),
        make_generation_point("ТКО-309", &main_transporter, &main_generator, "Российская Федерация, 191002, г. Санкт-Петербург, Ланское ш., д. 12", 90, vec![container_plastic, container_metal, container_kgo]),
        make_generation_point("ТКО-388", &main_transporter, &main_generator, "Российская Федерация, 191002, г. Санкт-Петербург, просп. Непокоренных, д. 13", 20, vec![container_plastic, container_metal, container_kgo]),
        make_generation_point("ТКО-421", &main_transporter, &main_generator, "Российская Федерация, 191002, г. Санкт-Петербург, просп. Непокоренных, д. 14", 15, vec![container_plastic, container_metal, container_kgo]),
        make_generation_point("ТКО-432", &main_transporter, &main_generator, "Российская Федерация, 191002, г. Санкт-Петербург, Ланское ш., д. 15", 40, vec![container_plastic, container_metal, container_kgo]),
        make_generation_point("ТКО-481", &main_transporter, &main_generator, "Российская Федерация,191025, г. Санкт-Петербург, Стремянная ул., 16", 15, vec![container_plastic, container_metal, container_kgo]),
    ];
    for form in forms {
        let registration = commands::handle_register_generation_point(
            Json(form),
            store.clone(),
            generation_point_projection.clone(),
            token.clone(),
        );
        let response = registration.await;
        if let Some(error) = response.error() {
            demo.errors.push(format!("Unable to register generation point, {:?}", error))
        }
    }


    HttpResponse::Ok().json(demo)
}

fn extract_body<'a, T: Deserialize<'a>>(response: &'a HttpResponse) -> T {
    let body = match response.body() {
        ResponseBody::Body(body) => {
            match body {
                Body::None => b"",
                Body::Empty => b"",
                Body::Bytes(bytes) => &bytes[..],
                Body::Message(_) => b""
            }
        }
        ResponseBody::Other(_) => b""
    };
    serde_json::from_slice(&body).unwrap()
}
