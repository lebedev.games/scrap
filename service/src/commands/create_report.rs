use crate::extractors::AccessToken;
use actix_web::HttpResponse;
use uuid::Uuid;
use crate::application::aggregates::{AccessUser, ReplayError, UserAccessError, ensure_access, Permission};
use crate::domain::Report;
use crate::application::{EventStore, Version};
use chrono::Utc;
use crate::model::{CreateReportForm, Resource};
use actix_web::web::{Data, Json};
use crate::application::projections::{ReportProjection, NegotiationProjection};

pub async fn handle_create_report(
    access_token: AccessToken,
    form: Json<CreateReportForm>,
    store: Data<EventStore>,
    report_projection: Data<ReportProjection>,
    negotiation_projection: Data<NegotiationProjection>
) -> HttpResponse {
    let user = access_token.user;

    match create_report(&user, &store, &report_projection, &negotiation_projection, form.into_inner()).await {
        Ok(id) => HttpResponse::Ok().json(Resource {id}),
        Err(error) => match error {
            Fail::ContractorReplayError(error) => {
                error!("Unable to create report because of contractor replay, {:?}", error);
                HttpResponse::Unauthorized().finish()
            }
            Fail::ContractorAccessDenied(error) => {
                error!("Unable to create report, contractor access denied, {:?}", error);
                HttpResponse::Forbidden().finish()
            }
            Fail::ReportPublishError(error) => {
                error!("Unable to create report because of event store, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
            Fail::ReportProjectionError(error) => {
                error!("Unable to create report because of projection, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
            Fail::NegotiationProjectionError(error) => {
                error!("Unable to create report because of negotiation projection, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
        }
    }
}


#[derive(Debug)]
pub enum Fail {
    ContractorReplayError(ReplayError),
    ContractorAccessDenied(UserAccessError),
    ReportPublishError(sqlx::Error),
    ReportProjectionError(sqlx::Error),
    NegotiationProjectionError(sqlx::Error)
}

async fn create_report(
    user: &Uuid,
    store: &EventStore,
    report_projection: &ReportProjection,
    negotiation_projection: &NegotiationProjection,
    form: CreateReportForm,
) -> Result<Uuid, Fail> {
    let now = Utc::now();
    let report = Uuid::new_v4();

    let user = AccessUser::replay(&user, store)
        .await
        .map_err(Fail::ContractorReplayError)?;

    let contractor = user.id.clone();

    ensure_access(user, Permission::Reports)
        .map_err(Fail::ContractorAccessDenied)?;

    let events = vec![
        Report::Created {
            contractor,
            date: now,
        },
        Report::TargetSpecified {
            transporter: form.target_transporter,
            month: form.target_month
        },
        Report::TransportationCovered {
            transportations: form.transportations
        },
    ];

    store.store(&contractor, &report, &events, &Version::Initial)
        .await
        .map_err(Fail::ReportPublishError)?;

    report_projection.apply_report_events(report, &events)
        .await
        .map_err(Fail::ReportProjectionError)?;

    negotiation_projection.apply_report_events(&events)
        .await
        .map_err(Fail::NegotiationProjectionError)?;

    Ok(report.clone())
}