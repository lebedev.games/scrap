use actix_web::HttpResponse;
use actix_web::web::{Data, Json, Path};

use crate::application::EventStore;
use crate::extractors::AccessToken;
use crate::model::{SolveNegotiationIssuesRequest, Resource, ResolutionModel};
use crate::application::aggregates::{ReplayError, UserAccessError, AccessUser, ensure_access, Permission, NegotiationStage, TransportationDraft, Issue, PublishError};
use uuid::Uuid;
use chrono::Utc;
use crate::domain::{Negotiation, Transportation};
use crate::application::projections::{NegotiationProjection, TransportationProjection};

pub async fn handle_solve_negotiation_issues(
    resource: Path<Resource>,
    request: Json<SolveNegotiationIssuesRequest>,
    store: Data<EventStore>,
    negotiation_projection: Data<NegotiationProjection>,
    transportation_projection: Data<TransportationProjection>,
    access_token: AccessToken
) -> HttpResponse {
    let negotiation = resource.id;
    let transporter = access_token.user;

    let result = solve_negotiation_issues(
        &transporter,
        &negotiation,
        &store,
        request.into_inner(),
        &negotiation_projection,
        &transportation_projection
    ).await;

    match result {
        Ok(_) => HttpResponse::NoContent().finish(),
        Err(error) => match error {
            Fail::TransporterReplayError(error) => {
                error!("Unable to solve negotiation issues because of transporter replay, {:?}", error);
                HttpResponse::Unauthorized().finish()
            }
            Fail::TransporterAccessDenied(error) => {
                warn!("Unable to solve negotiation issues, transporter access denied, {:?}", error);
                HttpResponse::Forbidden().finish()
            }
            Fail::IssueNotFound(issue_id) => {
                error!("Unable to solve negotiation issues, issue id={} not found", issue_id);
                HttpResponse::BadRequest().finish()
            }
            Fail::NegotiationReplayError(error) => {
                error!("Unable to solve issues because of negotiation replay, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
            Fail::NegotiationPublishError(error) => {
                error!("Unable to solve issues because of negotiation event store, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
            Fail::NegotiationProjectionError(error) => {
                error!("Unable to solve issues because of negotiation projection, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
            Fail::TransportationReplayError(error) => {
                error!("Unable to solve issues because of transportation replay, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
            Fail::TransportationPublishError(error) => {
                error!("Unable to solve issues because of transportation event store, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
            Fail::TransportationProjectionError(error) => {
                error!("Unable to solve issues because of transportation event projection, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
        }
    }
}

#[derive(Debug)]
enum Fail {
    TransporterReplayError(ReplayError),
    TransporterAccessDenied(UserAccessError),
    IssueNotFound(Uuid),
    NegotiationReplayError(ReplayError),
    NegotiationPublishError(PublishError),
    NegotiationProjectionError(sqlx::Error),
    TransportationReplayError(ReplayError),
    TransportationPublishError(PublishError),
    TransportationProjectionError(sqlx::Error),
}


async fn solve_negotiation_issues(
    user: &Uuid,
    negotiation: &Uuid,
    store: &EventStore,
    request: SolveNegotiationIssuesRequest,
    negotiation_projection: &NegotiationProjection,
    transportation_projection: &TransportationProjection
) -> Result<(), Fail> {
    let now = Utc::now();

    let user = AccessUser::replay(&user, store)
        .await
        .map_err(Fail::TransporterReplayError)?;

    let transporter = match user.role_entity.as_ref() {
        Some(entity_id) => entity_id.clone(),
        None => return Err(Fail::TransporterAccessDenied(UserAccessError::RoleEntityUnassigned))
    };

    ensure_access(user, Permission::NegotiationIssuesSolve)
        .map_err(Fail::TransporterAccessDenied)?;

    let negotiation = NegotiationStage::replay(&negotiation, store)
        .await
        .map_err(Fail::NegotiationReplayError)?;

    let transportation = TransportationDraft::replay(&negotiation.transportation, store)
        .await
        .map_err(Fail::TransportationReplayError)?;

    let resolution = Uuid::new_v4();
    let mut negotiation_events = vec![
        Negotiation::IssuesResolutionStagePassed {
            id: resolution,
            transporter,
            date: now,
            // note: rework aggregate version update
            transportation_version: transportation.version + 1
        }];
    let mut transportation_events = vec![];

    let issues = negotiation.last_declaration_issues();

    for issue in request.resolutions {
        match issue {
            ResolutionModel::WasteVolumeIssueResolution { issue_id, waste_volume: waste_volume_m3 } => {
                let collection = match issues.get(&issue_id) {
                    Some(Issue::WasteVolumeIssue { collection, .. }) => {
                        collection.clone()
                    },
                    _ => return Err(Fail::IssueNotFound(issue_id))
                };
                negotiation_events.push(Negotiation::WasteVolumeIssueResolved {
                    id: issue_id,
                    resolution,
                    waste_volume_m3
                });
                transportation_events.push(Transportation::WasteVolumeChanged {
                    waste_collection: collection,
                    transporter,
                    date: now,
                    waste_volume_m3
                });
            }
            ResolutionModel::CollectionStartTimeIssueResolution { issue_id, start_time } => {
                let destination = match issues.get(&issue_id) {
                    Some(Issue::CollectionStartTimeIssue { destination, .. }) => {
                        destination.clone()
                    },
                    _ => return Err(Fail::IssueNotFound(issue_id))
                };
                negotiation_events.push(Negotiation::CollectionStartTimeIssueResolved {
                    id: issue_id,
                    resolution,
                    start_time
                });
                transportation_events.push(Transportation::CollectionStartTimeChanged {
                    destination,
                    transporter,
                    date: now,
                    start_time
                })
            }
            ResolutionModel::CollectionPhotoIssueResolution { issue_id, photos } => {
                let collection = match issues.get(&issue_id) {
                    Some(Issue::CollectionPhotoIssue { collection, .. }) => {
                        collection.clone()
                    },
                    _ => return Err(Fail::IssueNotFound(issue_id))
                };
                negotiation_events.push(Negotiation::CollectionPhotoIssueResolved {
                    id: issue_id,
                    resolution,
                    photos: photos.clone()
                });
                transportation_events.push(Transportation::WasteCollectionPhotographed {
                    id: collection,
                    transporter,
                    date: now,
                    photos
                })
            }
        }
    }

    negotiation.publish(&transporter, store, &negotiation_events)
        .await
        .map_err(Fail::NegotiationPublishError)?;

    transportation.publish(&transporter, store, &transportation_events)
        .await
        .map_err(Fail::TransportationPublishError)?;

    transportation_projection.apply_transportation_events(transportation.id, &transportation_events)
        .await
        .map_err(Fail::TransportationProjectionError)?;

    negotiation_projection.apply_negotiation_events(negotiation.id.clone(), &negotiation_events)
        .await
        .map_err(Fail::NegotiationProjectionError)?;

    Ok(())
}