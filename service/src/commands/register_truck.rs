use actix_web::HttpResponse;
use actix_web::web::{Data, Json};
use chrono::Utc;
use uuid::Uuid;

use crate::application::{EventStore, Version};
use crate::application::aggregates::{AccessUser, ensure_access, Permission, ReplayError, UserAccessError};
use crate::application::projections::{TruckProjection, TruckProjectionError};
use crate::domain::{Truck};
use crate::extractors::AccessToken;
use crate::model::{RegisterTruckForm, Resource};

pub async fn handle_register_truck(
    form: Json<RegisterTruckForm>,
    store: Data<EventStore>,
    projection: Data<TruckProjection>,
    access_token: AccessToken,
) -> HttpResponse {
    let form = form.into_inner();
    let contractor = access_token.user;

    match register_truck(&store, contractor, projection, form).await {
        Ok(id) => HttpResponse::Created().json(Resource { id }),
        Err(error) => match error {
            RegisterTruckError::TransporterReplayError(error) => {
                error!("Unable to register truck because of contractor replay, {:?}", error);
                HttpResponse::Unauthorized().finish()
            }
            RegisterTruckError::TransporterAccessDenied(error) => {
                warn!("Unable to register truck, contractor access denied, {:?}", error);
                HttpResponse::Forbidden().finish()
            }
            RegisterTruckError::TruckPublishError(error) => {
                error!("Unable to register truck because of event store, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
            RegisterTruckError::TruckProjectionError(error) => {
                error!("Unable to register truck because of projection, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
        }
    }
}

#[derive(Debug)]
pub enum RegisterTruckError {
    TransporterReplayError(ReplayError),
    TransporterAccessDenied(UserAccessError),
    TruckPublishError(sqlx::Error),
    TruckProjectionError(TruckProjectionError),
}


pub async fn register_truck(
    store: &EventStore,
    contractor: Uuid,
    projection: Data<TruckProjection>,
    form: RegisterTruckForm,
) -> Result<Uuid, RegisterTruckError> {
    let now = Utc::now();
    let entity_id = Uuid::new_v4();

    let user = AccessUser::replay(&contractor, store)
        .await
        .map_err(RegisterTruckError::TransporterReplayError)?;

    ensure_access(user, Permission::TruckRegister)
        .map_err(RegisterTruckError::TransporterAccessDenied)?;

    let events = vec![
        Truck::Registered {
            registrar: contractor,
            date: now,
        },
        Truck::TransporterAssigned {
            registrar: contractor,
            date: now,
            transporter: form.transporter,
        },
        Truck::OwnerChanged {
            registrar: contractor,
            date: now,
            owner: form.owner,
        },
        Truck::FleetTypeChanged {
            registrar: contractor,
            date: now,
            fleet_type: form.fleet_type,
        },
        Truck::VinChanged {
            registrar: contractor,
            date: now,
            vin: form.vin,
        },
        Truck::RegistrationNumberChanged {
            registrar: contractor,
            date: now,
            registration_number: form.registration_number,
        },
        Truck::GaragingNumberChanged {
            registrar: contractor,
            date: now,
            garaging_number: form.garaging_number,
        },
        Truck::VehicleModelChanged {
            registrar: contractor,
            date: now,
            vehicle_model: form.vehicle_model,
        },
        Truck::WasteEquipmentModelChanged {
            registrar: contractor,
            date: now,
            waste_equipment_model: form.waste_equipment_model,
        },
        Truck::CargoBodyTypeChanged {
            registrar: contractor,
            date: now,
            cargo_body_type: form.cargo_body_type,
        },
        Truck::RequiredDriverLicenseChanged {
            registrar: contractor,
            date: now,
            category: form.required_driver_license_category,
        },
        Truck::AxleConfigurationChanged {
            registrar: contractor,
            date: now,
            axle_configuration: form.axle_configuration,
        },
        Truck::ProductionYearChanged {
            registrar: contractor,
            date: now,
            production_year: form.production_year,
        },
        Truck::NavigationEquipmentNameChanged {
            registrar: contractor,
            date: now,
            navigation_equipment_name: form.navigation_equipment_name,
        },
        Truck::EmptyWeightChanged {
            registrar: contractor,
            date: now,
            empty_weight_kg: form.empty_weight_kg,
        },
        Truck::CargoCapacityChanged {
            registrar: contractor,
            date: now,
            cargo_capacity_kg: form.cargo_capacity_kg,
        },
    ];


    store.store(&contractor, &entity_id, &events, &Version::Initial)
        .await
        .map_err(RegisterTruckError::TruckPublishError)?;

    projection.apply_truck_events(entity_id, &events)
        .await
        .map_err(RegisterTruckError::TruckProjectionError)?;

    Ok(entity_id)
}