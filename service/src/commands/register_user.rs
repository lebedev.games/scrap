use actix_web::HttpResponse;
use actix_web::web::{Data, Json};
use chrono::Utc;
use uuid::Uuid;

use crate::application::{EventStore, Version};
use crate::application::aggregates::{AccessUser, ensure_access, Permission, ReplayError, UserAccessError};
use crate::application::projections::UserProjection;
use crate::domain::{User};
use crate::model::{RegisterUserForm, Resource};
use crate::extractors::AccessToken;

pub async fn handle_register_user(
    form: Json<RegisterUserForm>,
    store: Data<EventStore>,
    projection: Data<UserProjection>,
    access_token: AccessToken
) -> HttpResponse {
    let system = access_token.user;

    match register_user(&store, &projection, system, form.into_inner()).await {
        Ok(id) => {
            HttpResponse::Created().json(Resource {id})
        }
        Err(error) => match error {
            RegisterUserError::RegistrarReplayError(error) => {
                error!("Unable to register user because of registrar replay, {:?}", error);
                HttpResponse::Unauthorized().finish()
            }
            RegisterUserError::RegistrarAccessDenied(error) => {
                warn!("Unable to register user, registrar access denied, {:?}", error);
                HttpResponse::Forbidden().finish()
            }
            RegisterUserError::UserPublishError(error) => {
                error!("Unable to register user because of event store, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
        }
    }
}

#[derive(Debug)]
pub enum RegisterUserError {
    RegistrarReplayError(ReplayError),
    RegistrarAccessDenied(UserAccessError),
    UserPublishError(sqlx::Error)
}

pub async fn register_user(
    store: &EventStore,
    projection: &UserProjection,
    registrar: Uuid,
    form: RegisterUserForm,
) -> Result<Uuid, RegisterUserError> {
    let now = Utc::now();

    let user = AccessUser::replay(&registrar, store)
        .await
        .map_err(RegisterUserError::RegistrarReplayError)?;

    ensure_access(user, Permission::TransportationDraftChange)
        .map_err(RegisterUserError::RegistrarAccessDenied)?;

    let mut events = vec![
        User::Registered {
            registrar,
            date: now,
        },
        User::NameSpecified {
            registrar,
            name: form.name
        },
        User::RoleSpecified {
            manager: registrar,
            role: form.role
        },
        User::PasswordCredentialsSpecified {
            grantor: registrar,
            login: form.login,
            password: form.password
        }
    ];

    for permission in form.permissions {
        events.push(
            User::PermissionGranted {
                grantor: registrar,
                permission: permission.clone(),
                date: now,
            })
    }

    store.store(&registrar, &form.user_id, &events, &Version::Initial)
        .await
        .map_err(RegisterUserError::UserPublishError)?;

    projection.apply(&form.user_id, &events).await;

    Ok(form.user_id)
}