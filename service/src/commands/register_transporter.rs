use actix_web::HttpResponse;
use actix_web::web::{Data, Json};
use chrono::{DateTime, Utc};
use uuid::Uuid;

use crate::application::{EventStore, Version};
use crate::application::aggregates::{AccessUser, ensure_access, Permission, ReplayError, Role, UserAccessError};
use crate::application::projections::{TransporterProjection, UserProjection, TruckProjection, TruckProjectionError};
use crate::application::projections::{GenerationPointProjection, GenerationPointProjectionError};
use crate::domain::{BusinessEntity, Entrepreneur, Transporter, User};
use crate::extractors::AccessToken;
use crate::model::{BusinessEntityModel, EntrepreneurModel, RegisterTransporterForm, Resource, TransporterLegalEntityModel};

pub async fn handle_register_transporter(
    form: Json<RegisterTransporterForm>,
    store: Data<EventStore>,
    projection: Data<TransporterProjection>,
    generation_point_projection: Data<GenerationPointProjection>,
    truck_projection: Data<TruckProjection>,
    user_projection: Data<UserProjection>,
    access_token: AccessToken,
) -> HttpResponse {
    let form = form.into_inner();
    let registrar = access_token.user;

    match register_transporter(&store, &projection, &generation_point_projection, &truck_projection, &user_projection, registrar, form).await {
        Ok(id) => HttpResponse::Created().json(Resource { id }),
        Err(error) => match error {
            RegisterTransporterError::RegistrarReplayError(error) => {
                error!("Unable to register transporter because of registrar replay, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
            RegisterTransporterError::GenerationPointProjectionError(error) => {
                error!("Unable to register transporter because of generation point projection, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
            RegisterTransporterError::TruckProjectionError(error) => {
                error!("Unable to register transporter because of truck projection, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
            RegisterTransporterError::RegistrarAccessDenied(_) => {
                warn!("Unable to register transporter, registrar access denied");
                HttpResponse::Forbidden().finish()
            }
            RegisterTransporterError::UserPublishError(error) => {
                error!("Unable to register transporter because of user event store, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
            RegisterTransporterError::BusinessEntityPublishError(error) => {
                error!("Unable to register transporter because of business entity event store, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
            RegisterTransporterError::EntrepreneurPublishError(error) => {
                error!("Unable to register transporter because of entrepreneur event store, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
            RegisterTransporterError::TransporterPublishError(error) => {
                error!("Unable to register transporter because of event store, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
            RegisterTransporterError::TransporterProjectionError(error) => {
                error!("Unable to register transporter because of projection, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
            RegisterTransporterError::BusinessEntityProjectionError(error) => {
                error!("Unable to register transporter business entity because of projection, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
            RegisterTransporterError::EntrepreneurProjectionError(error) => {
                error!("Unable to register transporter entrepreneur because of projection, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
        }
    }
}

#[derive(Debug)]
pub enum RegisterTransporterError {
    RegistrarReplayError(ReplayError),
    RegistrarAccessDenied(UserAccessError),
    BusinessEntityPublishError(sqlx::Error),
    UserPublishError(sqlx::Error),
    EntrepreneurPublishError(sqlx::Error),
    TransporterPublishError(sqlx::Error),
    GenerationPointProjectionError(GenerationPointProjectionError),
    TransporterProjectionError(sqlx::Error),
    BusinessEntityProjectionError(sqlx::Error),
    EntrepreneurProjectionError(sqlx::Error),
    TruckProjectionError(TruckProjectionError),
}

pub async fn register_transporter(
    store: &EventStore,
    transporter_projection: &TransporterProjection,
    generation_point_projection: &GenerationPointProjection,
    truck_projection: &TruckProjection,
    user_projection: &UserProjection,
    registrar: Uuid,
    form: RegisterTransporterForm,
) -> Result<Uuid, RegisterTransporterError> {
    let now = Utc::now();
    let transporter_id = Uuid::new_v4();

    let user = AccessUser::replay(&registrar, store)
        .await
        .map_err(RegisterTransporterError::RegistrarReplayError)?;

    ensure_access(user, Permission::TransporterRegister)
        .map_err(RegisterTransporterError::RegistrarAccessDenied)?;

    let mut events = vec![
        Transporter::Registered {
            registrar,
            date: now,
        },
        Transporter::FederalClassificationUpdated {
            registrar,
            fkko: form.fkko,
        },
        Transporter::NoteUpdated {
            registrar,
            note: form.note,
        }
    ];


    let (name, login, password) = match form.legal_entity {
        TransporterLegalEntityModel::BusinessEntity(input) => {
            let (name, login, password) = (input.short_name.clone(), input.ogrn.clone(), input.ogrn.clone());
            let business_entity = register_transporter_business_entity(&store, &transporter_projection, &generation_point_projection, &truck_projection, input, registrar, now).await?;
            events.push(Transporter::BusinessEntityAssigned {
                registrar,
                business_entity,
            });
            (name, login, password)
        }
        TransporterLegalEntityModel::Entrepreneur(input) => {
            let (name, login, password) = (input.last_name.clone(), input.ogrnip.clone(), input.ogrnip.clone());
            let entrepreneur = register_transporter_entrepreneur(&store, &transporter_projection, &generation_point_projection, &truck_projection, input, registrar, now).await?;
            events.push(Transporter::EntrepreneurAssigned {
                registrar,
                entrepreneur,
            });
            (name, login, password)
        }
    };

    for truck_fleet_type in form.truck_fleet_types {
        events.push(Transporter::TruckFleetTypeSpecified {
            registrar,
            truck_fleet_type,
        });
    }

    if let Some(parent) = form.parent {
        events.push(Transporter::ParentUpdated {
            registrar,
            parent,
        });
    }

    register_transporter_user(&store, &user_projection, &transporter_id, name, login, password, registrar.clone(), now)
        .await?;

    store.store(&registrar, &transporter_id, &events, &Version::Initial)
        .await
        .map_err(RegisterTransporterError::TransporterPublishError)?;

    generation_point_projection.apply_transporter_events(&transporter_id, &events)
        .await
        .map_err(RegisterTransporterError::GenerationPointProjectionError)?;

    transporter_projection.apply_transporter_events(transporter_id, &events)
        .await
        .map_err(RegisterTransporterError::TransporterProjectionError)?;

    truck_projection.apply_transporter_events(&transporter_id, &events)
        .await
        .map_err(RegisterTransporterError::TruckProjectionError)?;

    Ok(transporter_id)
}


async fn register_transporter_user(
    store: &EventStore,
    projection: &UserProjection,
    transporter: &Uuid,
    name: String,
    login: String,
    password: String,
    registrar: Uuid,
    date: DateTime<Utc>,
) -> Result<Uuid, RegisterTransporterError> {
    let user = Uuid::new_v4();

    let events = vec![
        User::Registered {
            registrar,
            date,
        },
        User::PermissionGranted { grantor: registrar, permission: Permission::TransportationDraftRegister.to_string(), date },
        User::PermissionGranted { grantor: registrar, permission: Permission::TransportationDraftChange.to_string(), date },
        User::PermissionGranted { grantor: registrar, permission: Permission::TransportationDraftGet.to_string(), date },
        User::PermissionGranted { grantor: registrar, permission: Permission::TransportationDraftList.to_string(), date },
        User::PermissionGranted { grantor: registrar, permission: Permission::TargetGenerationPointList.to_string(), date },
        User::PermissionGranted { grantor: registrar, permission: Permission::NegotiationIssuesSolve.to_string(), date },
        User::PermissionGranted { grantor: registrar, permission: Permission::NegotiationGet.to_string(), date },
        User::PermissionGranted { grantor: registrar, permission: Permission::NegotiationList.to_string(), date },
        User::PermissionGranted { grantor: registrar, permission: Permission::TruckList.to_string(), date },
        User::RoleSpecified {
            manager: registrar,
            role: Role::Transporter.to_string(),
        },
        User::RoleEntityAssigned {
            manager: registrar,
            entity_id: transporter.clone()
        },
        User::NameSpecified {
            registrar,
            name,
        },
        User::PasswordCredentialsSpecified {
            grantor: registrar,
            login,
            password,
        }
    ];

    store.store(&registrar, &user, &events, &Version::Initial)
        .await
        .map_err(RegisterTransporterError::UserPublishError)?;

    projection.apply(&user, &events).await;

    Ok(user.clone())
}

async fn register_transporter_business_entity(
    store: &EventStore,
    projection: &TransporterProjection,
    generation_point_projection: &GenerationPointProjection,
    truck_projection: &TruckProjection,
    input: BusinessEntityModel,
    registrar: Uuid,
    date: DateTime<Utc>,
) -> Result<Uuid, RegisterTransporterError> {
    let mut business_entity_events = vec![
        BusinessEntity::Registered {
            registrar,
            date,
        },
        BusinessEntity::AdministrativeNameUpdated {
            registrar,
            full_name: input.full_name,
            short_name: input.short_name,
        },
        BusinessEntity::TaxIdentificationUpdated {
            registrar,
            inn: input.inn,
        },
        BusinessEntity::TaxRegistrationUpdated {
            registrar,
            kpp: input.kpp,
        },
        BusinessEntity::PrimaryRegistrationUpdated {
            registrar,
            ogrn: input.ogrn,
        },
        BusinessEntity::PhoneContactUpdated {
            registrar,
            phone: input.phone,
        },
        BusinessEntity::AddressUpdated {
            registrar,
            full_address: input.address,
        },
        BusinessEntity::PostalAddressUpdated {
            registrar,
            full_address: input.postal_address,
        },
        BusinessEntity::ActualAddressUpdated {
            registrar,
            full_address: input.actual_address,
        },
        BusinessEntity::BankDetailsUpdated {
            registrar,
            account: input.bank_details.account,
            correspondent_account: input.bank_details.correspondent_account,
            bank_name: input.bank_details.bank_name,
            rcbic: input.bank_details.rcbic,
        }
    ];

    if let Some(email) = input.email {
        business_entity_events.push(BusinessEntity::EmailContactUpdated {
            registrar,
            email,
        });
    }

    for responsible in input.responsible_persons {
        business_entity_events.push(BusinessEntity::ResponsiblePersonAdded {
            registrar,
            full_name: responsible.full_name,
            phone: responsible.phone,
        });
    }


    let business_entity = Uuid::new_v4();
    store.store(&registrar, &business_entity, &business_entity_events, &Version::Initial)
        .await
        .map_err(RegisterTransporterError::BusinessEntityPublishError)?;

    generation_point_projection.apply_business_entity_events(&business_entity, &business_entity_events)
        .await
        .map_err(RegisterTransporterError::GenerationPointProjectionError)?;

    projection.apply_business_entity_events(business_entity, &business_entity_events)
        .await
        .map_err(RegisterTransporterError::BusinessEntityProjectionError)?;

    truck_projection.apply_business_entity_events(&business_entity, &business_entity_events)
        .await
        .map_err(RegisterTransporterError::TruckProjectionError)?;

    Ok(business_entity)
}

async fn register_transporter_entrepreneur(
    store: &EventStore,
    projection: &TransporterProjection,
    generation_point_projection: &GenerationPointProjection,
    truck_projection: &TruckProjection,
    input: EntrepreneurModel,
    registrar: Uuid,
    date: DateTime<Utc>,
) -> Result<Uuid, RegisterTransporterError> {
    let mut entrepreneur_events = vec![
        Entrepreneur::Registered {
            registrar,
            date,
        },
        Entrepreneur::PersonalNameUpdated {
            registrar,
            first_name: input.first_name,
            last_name: input.last_name,
            patronymic: input.patronymic,
        },
        Entrepreneur::TaxIdentificationUpdated {
            registrar,
            inn: input.inn,
        },
        Entrepreneur::PrimaryRegistrationUpdated {
            registrar,
            ogrnip: input.ogrnip,
        },
        Entrepreneur::PhoneContactUpdated {
            registrar,
            phone: input.phone,
        },
        Entrepreneur::AddressUpdated {
            registrar,
            full_address: input.address,
        },
        Entrepreneur::ActualAddressUpdated {
            registrar,
            full_address: input.actual_address,
        },
        Entrepreneur::BankDetailsUpdated {
            registrar,
            account: input.bank_details.account,
            correspondent_account: input.bank_details.correspondent_account,
            bank_name: input.bank_details.bank_name,
            rcbic: input.bank_details.rcbic,
        }
    ];

    if let Some(email) = input.email {
        entrepreneur_events.push(Entrepreneur::EmailContactUpdated {
            registrar,
            email,
        });
    };

    let entrepreneur = Uuid::new_v4();
    store.store(&registrar, &entrepreneur, &entrepreneur_events, &Version::Initial)
        .await
        .map_err(RegisterTransporterError::EntrepreneurPublishError)?;

    generation_point_projection.apply_entrepreneur_events(&entrepreneur, &entrepreneur_events)
        .await
        .map_err(RegisterTransporterError::GenerationPointProjectionError)?;

    projection.apply_entrepreneur_events(entrepreneur, &entrepreneur_events)
        .await
        .map_err(RegisterTransporterError::EntrepreneurProjectionError)?;

    truck_projection.apply_entrepreneur_events(&entrepreneur, &entrepreneur_events)
        .await
        .map_err(RegisterTransporterError::TruckProjectionError)?;

    Ok(entrepreneur)
}