use actix_web::HttpResponse;
use actix_web::web::{Data, Json, Path};

use crate::application::EventStore;
use crate::extractors::AccessToken;
use crate::model::{CompleteNegotiationRequest, Resource};
use uuid::Uuid;
use crate::application::aggregates::{ReplayError, AccessUser, ensure_access, Permission, UserAccessError, NegotiationStage, TransportationDraft, PublishError};
use crate::domain::Negotiation;
use chrono::Utc;
use crate::application::projections::NegotiationProjection;

pub async fn handle_complete_negotiation(
    resource: Path<Resource>,
    request: Json<CompleteNegotiationRequest>,
    store: Data<EventStore>,
    access_token: AccessToken,
    projection: Data<NegotiationProjection>
) -> HttpResponse {
    let negotiation = resource.id;
    let user = access_token.user;

    match complete_negotiation(&user, &negotiation, &store, &projection, request.into_inner()).await {
        Ok(_) => HttpResponse::NoContent().finish(),
        Err(error) => match error {
            Fail::ContractorReplayError(error) => {
                error!("Unable to complete negotiation because of contractor replay, {:?}", error);
                HttpResponse::Unauthorized().finish()
            }
            Fail::ContractorAccessDenied(error) => {
                warn!("Unable to complete negotiation, contractor access denied, {:?}", error);
                HttpResponse::Forbidden().finish()
            }
            Fail::NegotiationReplayError(error) => {
                error!("Unable to complete negotiation because of negotiation replay, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
            Fail::NegotiationPublishError(error) => {
                error!("Unable to complete negotiation because of negotiation event store, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
            Fail::TransportationReplayError(error) => {
                error!("Unable to complete negotiation because of transportation replay, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
            Fail::NegotiationProjectionError(error) => {
                error!("Unable to complete negotiation because of negotiation projection, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
        }
    }
}

#[derive(Debug)]
enum Fail {
    ContractorReplayError(ReplayError),
    ContractorAccessDenied(UserAccessError),
    NegotiationReplayError(ReplayError),
    NegotiationPublishError(PublishError),
    NegotiationProjectionError(sqlx::Error),
    TransportationReplayError(ReplayError)
}


async fn complete_negotiation(
    user: &Uuid,
    negotiation: &Uuid,
    store: &EventStore,
    projection: &NegotiationProjection,
    _request: CompleteNegotiationRequest
) -> Result<(), Fail> {
    let now = Utc::now();

    let user = AccessUser::replay(&user, store)
        .await
        .map_err(Fail::ContractorReplayError)?;

    let contractor = user.id.clone();

    ensure_access(user, Permission::NegotiationComplete)
        .map_err(Fail::ContractorAccessDenied)?;

    let negotiation = NegotiationStage::replay(&negotiation, store)
        .await
        .map_err(Fail::NegotiationReplayError)?;

    let transportation = TransportationDraft::replay(&negotiation.transportation, store)
        .await
        .map_err(Fail::TransportationReplayError)?;

    let events = vec![
        Negotiation::Completed {
            contractor,
            date: now,
            transportation_version: transportation.version
        }
    ];

    negotiation.publish(&contractor, store, &events)
        .await
        .map_err(Fail::NegotiationPublishError)?;

    projection.apply_negotiation_events(negotiation.id.clone(), &events)
        .await
        .map_err(Fail::NegotiationProjectionError)?;

    Ok(())
}