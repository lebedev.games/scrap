use actix_web::HttpResponse;
use actix_web::web::{Data, Json};
use chrono::Utc;
use uuid::Uuid;

use crate::application::{EventStore, Version};
use crate::application::aggregates::{AccessUser, ensure_access, Permission, ReplayError, UserAccessError};
use crate::domain:: GenerationPoint;
use crate::extractors::AccessToken;
use crate::model::{RegisterGenerationPointForm, Resource};
use crate::application::projections::{GenerationPointProjection, GenerationPointProjectionError};

pub async fn handle_register_generation_point(
    form: Json<RegisterGenerationPointForm>,
    store: Data<EventStore>,
    projection: Data<GenerationPointProjection>,
    access_token: AccessToken
) -> HttpResponse {
    let form = form.into_inner();
    let registrar = access_token.user;

    match register_generation_point(&store, &registrar, &projection, &form).await {
        Ok(id) => HttpResponse::Created().json(Resource { id }),
        Err(error) => match error {
            RegisterGenerationPointError::GenerationPointReplayError(error) => {
                error!("Unable to register generation point because of generation point replay, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
            RegisterGenerationPointError::GenerationPointProjectionError(error) => {
                error!("Unable to register generation point because of projection, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
            RegisterGenerationPointError::RegistrarAccessDenied(_) => {
                warn!("Unable to register generation point, generation point access denied");
                HttpResponse::Forbidden().finish()
            }
            RegisterGenerationPointError::GenerationPointPublishError(error) => {
                error!("Unable to register generation point because of event store, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
        }
    }
}

#[derive(Debug)]
pub enum RegisterGenerationPointError {
    GenerationPointReplayError(ReplayError),
    RegistrarAccessDenied(UserAccessError),
    GenerationPointPublishError(sqlx::Error),
    GenerationPointProjectionError(GenerationPointProjectionError),
}

pub async fn register_generation_point(
    store: &EventStore,
    registrar: &Uuid,
    projection: &GenerationPointProjection,
    form: &RegisterGenerationPointForm,
) -> Result<Uuid, RegisterGenerationPointError> {
    let user = AccessUser::replay(&registrar, store)
        .await
        .map_err(RegisterGenerationPointError::GenerationPointReplayError)?;

    ensure_access(user, Permission::GenerationPointRegister)
        .map_err(RegisterGenerationPointError::RegistrarAccessDenied)?;

    let events = create_generation_point_events(form, registrar);

    let entity_id = Uuid::new_v4();

    store.store(&registrar, &entity_id, &events, &Version::Initial)
        .await
        .map_err(RegisterGenerationPointError::GenerationPointPublishError)?;

    projection.apply_generation_point_events(&entity_id, &events)
        .await
        .map_err(RegisterGenerationPointError::GenerationPointProjectionError)?;

    Ok(entity_id)
}


fn create_generation_point_events(form: &RegisterGenerationPointForm, registrar: &Uuid) -> Vec<GenerationPoint> {
    let now = Utc::now();
    let mut events = vec![
        GenerationPoint::Registered {
            registrar: *registrar,
            date: now
        },
        GenerationPoint::InternalIdentificationChanged {
            registrar: *registrar,
            date: now,
            system_number: form.internal_system_number.clone(),
        },
        GenerationPoint::ExternalRegistrationChanged {
            registrar: *registrar,
            date: now,
            registry_number: form.external_registry_number.clone()
        },
        GenerationPoint::WasteTypeChanged {
            registrar: *registrar,
            date: now,
            waste_type: form.waste_type
        },
        GenerationPoint::OrganizationLegalTypeChanged {
            registrar: *registrar,
            date: now,
            organization_legal_type: form.organization_legal_type.clone()
        },
        GenerationPoint::OperationalStatusChanged {
            registrar: *registrar,
            date: now,
            status: form.operational_status
        },
        GenerationPoint::AddressChanged {
            registrar: *registrar,
            date: now,
            full_address: form.full_address.clone()
        },
        GenerationPoint::CoordinatesChanged {
            registrar: *registrar,
            date: now,
            coordinates: form.coordinates.clone()
        },
        GenerationPoint::AreaChanged {
            registrar: *registrar,
            date: now,
            area_m2: form.area_m2
        },
        GenerationPoint::CoveringTypeChanged {
            registrar: *registrar,
            date: now,
            covering_type: form.covering_type,
        },
        GenerationPoint::FenceMaterialChanged {
            registrar: *registrar,
            date: now,
            fence_material: form.fence_material,
        },
        GenerationPoint::GeneratorAssigned {
            registrar: *registrar,
            date: now,
            generator: form.generator
        },
        GenerationPoint::DescriptionChanged {
            registrar: *registrar,
            date: now,
            description: form.description.clone()
        },
        GenerationPoint::TransporterAssigned {
            registrar: *registrar,
            date: now,
            transporter: form.transporter.transporter,
            start_transportation_date: form.transporter.start_transportation_date,
            end_transportation_date: form.transporter.end_transportation_date
        },
        GenerationPoint::TransportationFrequencyChanged {
            registrar: *registrar,
            date: now,
            frequency: form.transporter.frequency.clone()
        },
        GenerationPoint::PhotosChanged {
            registrar: *registrar,
            date: now,
            photos: form.photos.clone()
        },
        GenerationPoint::AccrualMethodChanged {
            registrar: *registrar,
            date: now,
            method: form.accrual_method
        },
        GenerationPoint::MeasurementUnitChanged {
            registrar: *registrar,
            date: now,
            unit: form.measurement_unit
        }
    ];

    if let Some(service_company_name) = form.service_company_name.clone() {
        events.push(GenerationPoint::ServiceCompanyChanged {
            registrar: *registrar,
            date: now,
            service_company_name
        })
    }

    for container in &form.containers{
        events.push(GenerationPoint::ContainerAdded {
            registrar: *registrar,
            date: now,
            container_type: container.container_type
        })
    }

    return events;
}