use actix_web::HttpResponse;
use actix_web::web::{Data, Json, Path};
use uuid::Uuid;

use crate::application::aggregates::{AccessUser, ensure_access, Permission, ReplayError, UserAccessError, NegotiationStage, TransportationDraft, PublishError};
use crate::application::EventStore;
use crate::extractors::AccessToken;
use crate::model::{DeclareNegotiationIssuesRequest, Resource, IssueModel};
use crate::domain::Negotiation;
use chrono::Utc;
use crate::application::projections::NegotiationProjection;

pub async fn handle_declare_negotiation_issues(
    resource: Path<Resource>,
    request: Json<DeclareNegotiationIssuesRequest>,
    projection: Data<NegotiationProjection>,
    store: Data<EventStore>,
    access_token: AccessToken
) -> HttpResponse {
    let transportation = resource.id;
    let user = access_token.user;

    match declare_negotiation_issues(&user, &transportation, &store, &projection, request.into_inner()).await {
        Ok(_) => HttpResponse::NoContent().finish(),
        Err(error) => match error {
            Fail::ContractorReplayError(error) => {
                error!("Unable to declare negotiation issues because of contractor replay, {:?}", error);
                HttpResponse::Unauthorized().finish()
            }
            Fail::ContractorAccessDenied(error) => {
                warn!("Unable to declare negotiation issues, contractor access denied, {:?}", error);
                HttpResponse::Forbidden().finish()
            }
            Fail::TransportationReplayError(error) => {
                error!("Unable to declare negotiation issues because of transportation replay, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
            Fail::NegotiationReplayError(error) => {
                error!("Unable to declare negotiation issues because of negotiation replay, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
            Fail::NegotiationPublishError(error) => {
                error!("Unable to declare negotiation issues because of negotiation event store, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
            Fail::NegotiationProjectionError(error) => {
                error!("Unable to declare negotiation issues because of negotiation projection, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
        }
    }
}


#[derive(Debug)]
enum Fail {
    ContractorReplayError(ReplayError),
    ContractorAccessDenied(UserAccessError),
    TransportationReplayError(ReplayError),
    NegotiationReplayError(ReplayError),
    NegotiationPublishError(PublishError),
    NegotiationProjectionError(sqlx::Error)
}


async fn declare_negotiation_issues(
    user: &Uuid,
    negotiation: &Uuid,
    store: &EventStore,
    projection: &NegotiationProjection,
    request: DeclareNegotiationIssuesRequest
) -> Result<(), Fail> {
    let now = Utc::now();

    let user = AccessUser::replay(&user, store)
        .await
        .map_err(Fail::ContractorReplayError)?;

    let contractor = user.id.clone();

    ensure_access(user, Permission::NegotiationIssuesDeclare)
        .map_err(Fail::ContractorAccessDenied)?;

    let negotiation = NegotiationStage::replay(&negotiation, store)
        .await
        .map_err(Fail::NegotiationReplayError)?;

    let transportation = TransportationDraft::replay(&negotiation.transportation, store)
        .await
        .map_err(Fail::TransportationReplayError)?;

    let declaration = Uuid::new_v4();
    let mut events = vec![
        Negotiation::IssuesDeclarationStagePassed {
            id: declaration,
            contractor,
            date: now,
            transportation_version: transportation.version
        }
    ];

    for issue in request.issues {
        match issue {
            IssueModel::WasteVolumeIssue { id, collection, comment, destination } => {
                events.push(Negotiation::WasteVolumeIssueOpened {
                    id,
                    declaration,
                    comment,
                    waste_collection: collection,
                    destination,
                })
            }
            IssueModel::CollectionStartTimeIssue { id, destination, comment } => {
                events.push(Negotiation::CollectionStartTimeIssueOpened {
                    id,
                    declaration,
                    comment,
                    destination
                })
            }
            IssueModel::CollectionPhotoIssue { id, collection, comment, destination } => {
                events.push(Negotiation::CollectionPhotoIssueOpened {
                    id,
                    declaration,
                    comment,
                    waste_collection: collection,
                    destination
                })
            }
        }
    }

    negotiation.publish(&contractor, store, &events)
        .await
        .map_err(Fail::NegotiationPublishError)?;

    projection.apply_negotiation_events(negotiation.id.clone(), &events)
        .await
        .map_err(Fail::NegotiationProjectionError)?;

    Ok(())
}