use actix_web::web::{Json, Data};
use actix_web::HttpResponse;
use crate::model::{AuthorizationRequest, AuthorizationForm, Authorization};
use crate::application::projections::{UserProjection};
use jsonwebtoken::{encode, EncodingKey, Header};
use crate::extractors::Claims;

pub async fn handle_authorize(
    request: Json<AuthorizationRequest>,
    projection: Data<UserProjection>
) -> HttpResponse {
    match request.into_inner().form {
        AuthorizationForm::PasswordCredentials { login, password } => {
            match projection.get_user_by_login(&login, &password).await {
                Ok(user) => {
                    let header = &Header::default();
                    let claims = &Claims {
                        sub: user.user_id.to_string()
                    };
                    let key = &EncodingKey::from_secret("scrap".as_ref());
                    match encode(header, claims, key) {
                        Ok(token) => {
                            HttpResponse::Ok().json(Authorization {
                                user_name: user.user_name,
                                user_id: user.user_id,
                                user_role: user.user_role,
                                access_token: token
                            })
                        }
                        Err(error) => {
                            error!("Unable to authorize user with login {} because of JWT, {:?}", login, error);
                            HttpResponse::Unauthorized().finish()
                        }
                    }
                }
                Err(error) => {
                    error!("Unable to authorize user with login {} because of projection, {:?}", login, error);
                    HttpResponse::Unauthorized().finish()
                }
            }
        }
    }
}