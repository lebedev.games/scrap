use actix_web::HttpResponse;
use actix_web::web::{Data, Json, Path};
use chrono::{DateTime, Utc};
use uuid::Uuid;

use crate::application::EventStore;
use crate::application::aggregates::{AccessUser, Destination, ensure_access, Permission, PublishError, ReplayError, TransportationDraft, UserAccessError, WasteCollection};
use crate::application::projections::TransportationProjection;
use crate::domain::Transportation;
use crate::extractors::AccessToken;
use crate::model::{EditTransportationForm, Resource, TransportationChange};

pub async fn handle_change_transportation(
    resource: Path<Resource>,
    form: Json<EditTransportationForm>,
    store: Data<EventStore>,
    projection: Data<TransportationProjection>,
    access_token: AccessToken,
) -> HttpResponse {
    let editor = access_token.user;

    match change_transportation(&resource.id, &store, &projection, editor, form.into_inner().changes).await {
        Ok(_) => HttpResponse::NoContent().finish(),
        Err(error) => match error {
            ChangeTransportationError::TransportationSubmitted => {
                error!("Unable to change transportation, already submitted");
                HttpResponse::Conflict().finish()
            }
            ChangeTransportationError::EditorReplayError(error) => {
                error!("Unable to change transportation because of editor replay, {:?}", error);
                HttpResponse::Unauthorized().finish()
            }
            ChangeTransportationError::TransportationReplayError(error) => {
                error!("Unable to change transportation because of replay, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
            ChangeTransportationError::EditorAccessDenied(error) => {
                warn!("Unable to change transportation, editor access denied, {:?}", error);
                HttpResponse::Forbidden().finish()
            }
            ChangeTransportationError::DestinationNotFound(id) => {
                error!("Unable to change transportation, destination id={} not found", id);
                HttpResponse::BadRequest().finish()
            }
            ChangeTransportationError::CollectionNotFound(id) => {
                error!("Unable to change transportation, collection id={} not found", id);
                HttpResponse::BadRequest().finish()
            }
            ChangeTransportationError::TransportationPublishError(error) => {
                error!("Unable to change transportation because of event store, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
            ChangeTransportationError::TransportationProjectionError(error) => {
                error!("Unable to change transportation because of projection, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
        }
    }
}


#[derive(Debug)]
pub enum ChangeTransportationError {
    EditorReplayError(ReplayError),
    TransportationReplayError(ReplayError),
    EditorAccessDenied(UserAccessError),
    TransportationSubmitted,
    DestinationNotFound(Uuid),
    CollectionNotFound(Uuid),
    TransportationPublishError(PublishError),
    TransportationProjectionError(sqlx::Error),
}


pub async fn change_transportation(
    id: &Uuid,
    store: &EventStore,
    projection: &TransportationProjection,
    editor: Uuid,
    changes: Vec<TransportationChange>,
) -> Result<(), ChangeTransportationError> {
    let now = Utc::now();

    let user = AccessUser::replay(&editor, store)
        .await
        .map_err(ChangeTransportationError::EditorReplayError)?;

    ensure_access(user, Permission::TransportationDraftChange)
        .map_err(ChangeTransportationError::EditorAccessDenied)?;

    let mut draft = TransportationDraft::replay(id, store)
        .await
        .map_err(ChangeTransportationError::TransportationReplayError)?;

    let events = apply_transportation_changes(now, editor, &mut draft, changes)?;

    draft.publish(&editor, store, &events)
        .await
        .map_err(ChangeTransportationError::TransportationPublishError)?;

    projection.apply_transportation_events(draft.id, &events)
        .await
        .map_err(ChangeTransportationError::TransportationProjectionError)?;

    Ok(())
}

fn apply_transportation_changes(
    date: DateTime<Utc>,
    transporter: Uuid,
    transportation: &mut TransportationDraft,
    changes: Vec<TransportationChange>,
) -> Result<Vec<Transportation>, ChangeTransportationError> {
    let mut events = vec![];

    for change in changes {
        ensure_change_available(transportation)?;

        match change {
            TransportationChange::AssignTruck { truck, execution_date } => {
                let event = Transportation::TruckAssigned {
                    transporter,
                    date,
                    truck,
                    execution_date,
                };
                transportation.apply(&event);
                events.push(event);
            }

            TransportationChange::AddDestination {
                id,
                collection_start_time,
                waste_generation_point
            } => {
                let event = Transportation::DestinationAdded {
                    id,
                    transporter,
                    date,
                    collection_start_time,
                    waste_generation_point,
                };
                transportation.apply(&event);
                events.push(event);
            }

            TransportationChange::ChangeCollectionStartTime { destination, start_time } => {
                ensure_destination(transportation, &destination)?;
                let event = Transportation::CollectionStartTimeChanged {
                    destination,
                    transporter,
                    date,
                    start_time,
                };
                transportation.apply(&event);
                events.push(event);
            }

            TransportationChange::ChangeWasteGenerationPoint { destination, generation_point } => {
                ensure_destination(transportation, &destination)?;
                let event = Transportation::WasteGenerationPointChanged {
                    destination,
                    transporter,
                    date,
                    generation_point,
                };
                transportation.apply(&event);
                events.push(event);
            }

            TransportationChange::RemoveDestination { id } => {
                ensure_destination(transportation, &id)?;
                let event = Transportation::DestinationRemoved {
                    id,
                    transporter,
                    date,
                };
                transportation.apply(&event);
                events.push(event);
            }

            TransportationChange::CollectWaste {
                id,
                destination,
                container_type,
                container_volume,
                container_utilization,
                waste_volume_m3
            } => {
                let event = Transportation::WasteCollected {
                    id,
                    transporter,
                    date,
                    destination,
                    container_type,
                    container_volume,
                    container_utilization,
                    waste_volume_m3,
                };
                transportation.apply(&event);
                events.push(event);
            }

            TransportationChange::ChangeWasteVolume { waste_collection, waste_volume_m3 } => {
                ensure_waste_collection(transportation, &waste_collection)?;
                let event = Transportation::WasteVolumeChanged {
                    waste_collection,
                    transporter,
                    date,
                    waste_volume_m3,
                };
                transportation.apply(&event);
                events.push(event);
            }

            TransportationChange::ChangeContainer {
                waste_collection,
                container_type,
                container_volume
            } => {
                ensure_waste_collection(transportation, &waste_collection)?;
                let event = Transportation::ContainerChanged {
                    waste_collection,
                    transporter,
                    date,
                    container_type,
                    container_volume,
                };
                transportation.apply(&event);
                events.push(event);
            }

            TransportationChange::ChangeContainerUtilization {
                waste_collection,
                container_utilization
            } => {
                ensure_waste_collection(transportation, &waste_collection)?;
                let event = Transportation::ContainerUtilizationChanged {
                    waste_collection,
                    transporter,
                    date,
                    container_utilization,
                };
                transportation.apply(&event);
                events.push(event);
            }

            TransportationChange::PhotographWasteCollection { id, photos } => {
                ensure_waste_collection(transportation, &id)?;
                let event = Transportation::WasteCollectionPhotographed {
                    id,
                    transporter,
                    date,
                    photos,
                };
                transportation.apply(&event);
                events.push(event);
            }

            TransportationChange::RevokeWasteCollection { id } => {
                ensure_waste_collection(transportation, &id)?;
                let event = Transportation::WasteCollectionRevoked {
                    id,
                    transporter,
                    date,
                };
                transportation.apply(&event);
                events.push(event);
            }
        }
    }

    Ok(events)
}


pub fn ensure_change_available(draft: &TransportationDraft) -> Result<(), ChangeTransportationError> {
    if draft.submitted {
        Err(ChangeTransportationError::TransportationSubmitted)
    } else {
        Ok(())
    }
}

pub fn ensure_destination<'a>(draft: &'a TransportationDraft, id: &Uuid) -> Result<&'a Destination, ChangeTransportationError> {
    for destination in draft.destinations.iter() {
        if &destination.id == id {
            return Ok(destination);
        }
    }
    Err(ChangeTransportationError::DestinationNotFound(id.clone()))
}

pub fn ensure_waste_collection<'a>(draft: &'a TransportationDraft, id: &Uuid) -> Result<&'a WasteCollection, ChangeTransportationError> {
    for collection in draft.waste_collections.iter() {
        if &collection.id == id {
            return Ok(collection);
        }
    }
    Err(ChangeTransportationError::CollectionNotFound(id.clone()))
}


