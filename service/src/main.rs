#![forbid(unsafe_code)]

#[macro_use]
extern crate log;
#[macro_use]
extern crate service_macro_derive;


use std::env;
use std::fs::create_dir;
use std::path::Path;
use std::time::Duration;

use actix_cors::Cors;
use actix_web::{App, HttpResponse, HttpServer};
use actix_web::error::{InternalError, JsonPayloadError};
use actix_web::middleware::Logger;
use actix_web::web::{delete, get, JsonConfig, post, ServiceConfig};
use env_logger::Env;
use sqlx::postgres::PgPoolOptions;

use crate::application::dictionary::Dictionary;
use crate::application::EventStore;
use crate::application::projections;
use crate::model::BadRequestError;
use crate::application::projections::GenerationPointProjection;

mod model;
mod application;
mod commands;
mod queries;
mod domain;
mod extractors;
mod serialization;


pub fn configure_web_api(config: &mut ServiceConfig) {
    config
        .route("/files/{file:.*}", get().to(queries::handle_get_upload_file))
        .route("/files/upload", post().to(commands::handle_upload_files))
        .route("/automation/users", post().to(commands::handle_register_user))
        .route("/automation/users", get().to(queries::handle_get_users))
        .route("/automation/users/{id}/permissions", post().to(commands::handle_grant_user_permission))
        .route("/automation/users/{id}", delete().to(commands::handle_delete_user))

        .route("/generators", post().to(commands::handle_register_generator))
        .route("/generators", get().to(queries::handle_get_generators))
        .route("/generators/parental", get().to(queries::handle_get_parental_generators))
        .route("/generators/options", get().to(queries::handle_get_generator_options))
        .route("/generators/{id}", get().to(queries::handle_get_generator))

        .route("/transportations", post().to(commands::handle_register_transportation))
        .route("/transportations", get().to(queries::handle_get_transportations))
        .route("/transportations/{id}/changes", post().to(commands::handle_change_transportation))
        .route("/transportations/{id}/versions/{version}", get().to(queries::handle_get_transportation))
        .route("/transportations/{id}", get().to(queries::handle_get_transportation))

        .route("/negotiations/{id}/issues/declaration", post().to(commands::handle_declare_negotiation_issues))
        .route("/negotiations/{id}/issues/resolution", post().to(commands::handle_solve_negotiation_issues))
        .route("/negotiations/{id}/completion", post().to(commands::handle_complete_negotiation))
        .route("/negotiations/{id}", get().to(queries::handle_get_negotiation))
        .route("/negotiations", get().to(queries::handle_get_negotiations))

        .route("/generation_points", post().to(commands::handle_register_generation_point))
        .route("/generation_points", get().to(queries::handle_get_generation_points))
        .route("/generation_points/{id}", get().to(queries::handle_get_generation_point))
        .route("/generation-points/options", get().to(queries::handle_get_generation_point_options))

        .route("/transporter/trucks/options", get().to(queries::handle_get_truck_options))
        .route("/transporter/generation-points/target", get().to(queries::handle_get_target_generation_points))
        .route("/transporters", post().to(commands::handle_register_transporter))
        .route("/transporters", get().to(queries::handle_get_transporters))
        .route("/transporters/parental", get().to(queries::handle_get_parental_transporters))
        .route("/transporters/options", get().to(queries::handle_get_transporter_options))
        .route("/transporters/{id}", get().to(queries::handle_get_transporter))

        .route("/trucks", post().to(commands::handle_register_truck))
        .route("/trucks", get().to(queries::handle_get_trucks))
        .route("/trucks/{id}", get().to(queries::handle_get_truck))

        .route("/dictionary/accrual-methods", get().to(queries::handle_get_accrual_method_options))
        .route("/dictionary/container-types", get().to(queries::handle_get_container_type_options))
        .route("/dictionary/covering-types", get().to(queries::handle_get_covering_type_options))
        .route("/dictionary/fence-materials", get().to(queries::handle_get_fence_material_options))
        .route("/dictionary/measurement-units", get().to(queries::handle_get_measurement_unit_options))
        .route("/dictionary/operational-statuses", get().to(queries::handle_get_operational_status_options))
        .route("/dictionary/organization-legal-types", get().to(queries::handle_get_organization_legal_type_options))
        .route("/dictionary/waste-types", get().to(queries::handle_get_waste_type_options))
        .route("/dictionary/truck-fleet-types", get().to(queries::handle_get_truck_fleet_type_options))
        .route("/dictionary/cargo-body-types", get().to(queries::handle_get_cargo_body_type_options))
        .route("/dictionary/driver-license-categories", get().to(queries::handle_get_driver_license_category_options))
        .route("/dictionary/axle-configurations", get().to(queries::handle_get_axle_configuration_options))

        .route("/reports", get().to(queries::handle_get_reports))
        .route("/reports", post().to(commands::handle_create_report))
        .route("/reports/{id}", get().to(queries::handle_download_report))

        .route("/authorization", post().to(commands::handle_authorize))
        .route("/demo", get().to(commands::handle_set_up_demo))
    ;
}

pub fn configure_json() -> JsonConfig {
    JsonConfig::default().error_handler(|error, request| {
        match error {
            JsonPayloadError::Deserialize(error) => {
                error!("Unable to handle request {} because of json {:?}", request.uri(), error);
                let payload = BadRequestError { error: error.to_string() };
                let response = HttpResponse::BadRequest().json(payload);
                InternalError::from_response(error, response).into()
            }
            _ => {
                error!("Unable to handle request {} because of json {:?}", request.uri(), error);
                let response = HttpResponse::BadRequest().finish();
                InternalError::from_response(error, response).into()
            }
        }
    })
}

#[actix_rt::main]
async fn main() {
    env_logger::from_env(Env::default().default_filter_or("info")).init();

    info!("Starting");

    let upload_path = Path::new("./upload");
    if !upload_path.exists() {
        if let Err(error) = create_dir(upload_path) {
            error!("Unable to start service, {}", error);
            return;
        }
    }

    let database_url = env::var("DATABASE_URL").unwrap_or("postgres://postgres:postgres@127.0.0.1:65432/scrap".into());

    let connection = PgPoolOptions::new()
        .max_connections(5)
        .connect_timeout(Duration::from_secs(1))
        .connect(&database_url)
        .await;

    let connection_pool = match connection {
        Ok(connection_pool) => connection_pool,
        Err(error) => {
            error!("Unable to start service, {}", error);
            return;
        }
    };

    let startup = move || {
        let storage = EventStore {
            connection_pool: connection_pool.clone()
        };

        let user_projection = projections::UserProjection {
            connection_pool: connection_pool.clone()
        };

        let generator_projection = projections::GeneratorProjection {
            connection_pool: connection_pool.clone()
        };

        let transportation_projection = projections::TransportationProjection {
            connection_pool: connection_pool.clone()
        };

        let generation_point_projection = GenerationPointProjection {
            connection_pool: connection_pool.clone()
        };

        let dictionary = Dictionary {
            connection_pool: connection_pool.clone()
        };

        let transporter_projection = projections::TransporterProjection {
            connection_pool: connection_pool.clone()
        };

        let truck_projection = projections::TruckProjection {
            connection_pool: connection_pool.clone()
        };

        let negotiation_projection = projections::NegotiationProjection {
            connection_pool: connection_pool.clone()
        };

        let report_projection = projections::ReportProjection {
            connection_pool: connection_pool.clone()
        };

        App::new()
            .app_data(configure_json())
            .data(storage)
            .data(user_projection)
            .data(generator_projection)
            .data(transportation_projection)
            .data(generation_point_projection)
            .data(dictionary)
            .data(transporter_projection)
            .data(truck_projection)
            .data(negotiation_projection)
            .data(report_projection)
            .wrap(Cors::new().finish())
            .wrap(Logger::default())
            .configure(configure_web_api)
    };

    match HttpServer::new(startup).bind("0.0.0.0:8004") {
        Ok(server) => server.run().await.unwrap(),
        Err(error) => error!("Unable to start service, {}", error)
    }
}
