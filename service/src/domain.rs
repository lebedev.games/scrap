use chrono::{DateTime, Utc};
use rust_decimal::Decimal;
use uuid::Uuid;

use service_macro::Persisted;

// 7036ba92-3c56-4405-8fdc-be3863f3cc70
pub const SYSTEM_USER: Uuid = Uuid::from_bytes(
    [112, 54, 186, 146, 60, 86, 68, 5, 143, 220, 190, 56, 99, 243, 204, 112]
);

#[derive(Debug, Persisted)]
pub enum User {
    Registered {
        registrar: Uuid,
        date: DateTime<Utc>,
    },
    PermissionGranted {
        grantor: Uuid,
        permission: String,
        date: DateTime<Utc>,
    },
    RoleSpecified {
        manager: Uuid,
        role: String,
    },
    RoleEntityAssigned {
        manager: Uuid,
        entity_id: Uuid
    },
    PasswordCredentialsSpecified {
        grantor: Uuid,
        login: String,
        password: String,
    },
    NameSpecified {
        registrar: Uuid,
        name: String,
    },
    Deleted {
        registrar: Uuid,
        date: DateTime<Utc>,
    },
}


#[derive(Debug, Persisted)]
pub enum Generator {
    Registered {
        registrar: Uuid,
        date: DateTime<Utc>,
    },
    EntrepreneurAssigned {
        registrar: Uuid,
        entrepreneur: Uuid,
    },
    BusinessEntityAssigned {
        registrar: Uuid,
        business_entity: Uuid,
    },
    ParentUpdated {
        registrar: Uuid,
        parent: Uuid,
    },
}

#[derive(Debug, Persisted)]
pub enum Transporter {
    Registered {
        registrar: Uuid,
        date: DateTime<Utc>,
    },
    EntrepreneurAssigned {
        registrar: Uuid,
        entrepreneur: Uuid,
    },
    BusinessEntityAssigned {
        registrar: Uuid,
        business_entity: Uuid,
    },
    FederalClassificationUpdated {
        registrar: Uuid,
        fkko: String,
    },
    ParentUpdated {
        registrar: Uuid,
        parent: Uuid,
    },
    TruckFleetTypeSpecified {
        registrar: Uuid,
        truck_fleet_type: Uuid,
    },
    TruckAdded {
        registrar: Uuid,
        truck: Uuid,
    },
    TargetGenerationPointAdded {
        registrar: Uuid,
        assignment_date: DateTime<Utc>,
        generation_point: Uuid,
    },
    NoteUpdated {
        registrar: Uuid,
        note: String,
    },
}

#[derive(Debug, Persisted)]
pub enum BusinessEntity {
    Registered {
        registrar: Uuid,
        date: DateTime<Utc>,
    },
    AdministrativeNameUpdated {
        registrar: Uuid,
        full_name: String,
        short_name: String,
    },
    TaxIdentificationUpdated {
        registrar: Uuid,
        inn: String,
    },
    TaxRegistrationUpdated {
        registrar: Uuid,
        kpp: String,
    },
    PrimaryRegistrationUpdated {
        registrar: Uuid,
        ogrn: String,
    },
    PhoneContactUpdated {
        registrar: Uuid,
        phone: String,
    },
    EmailContactUpdated {
        registrar: Uuid,
        email: String,
    },
    AddressUpdated {
        registrar: Uuid,
        full_address: String,
    },
    PostalAddressUpdated {
        registrar: Uuid,
        full_address: String,
    },
    ActualAddressUpdated {
        registrar: Uuid,
        full_address: String,
    },
    ResponsiblePersonAdded {
        registrar: Uuid,
        full_name: String,
        phone: String,
    },
    BankDetailsUpdated {
        registrar: Uuid,
        account: String,
        correspondent_account: String,
        bank_name: String,
        rcbic: String,
    },
}

#[derive(Debug, Persisted)]
pub enum Entrepreneur {
    Registered {
        registrar: Uuid,
        date: DateTime<Utc>,
    },
    PersonalNameUpdated {
        registrar: Uuid,
        first_name: String,
        last_name: String,
        patronymic: String,
    },
    TaxIdentificationUpdated {
        registrar: Uuid,
        inn: String,
    },
    PrimaryRegistrationUpdated {
        registrar: Uuid,
        ogrnip: String,
    },
    PhoneContactUpdated {
        registrar: Uuid,
        phone: String,
    },
    EmailContactUpdated {
        registrar: Uuid,
        email: String,
    },
    AddressUpdated {
        registrar: Uuid,
        full_address: String,
    },
    ActualAddressUpdated {
        registrar: Uuid,
        full_address: String,
    },
    BankDetailsUpdated {
        registrar: Uuid,
        account: String,
        correspondent_account: String,
        bank_name: String,
        rcbic: String,
    },
}

#[derive(Debug, Persisted)]
pub enum Transportation {
    Registered {
        transporter: Uuid,
        date: DateTime<Utc>,
    },
    Submitted {
        transporter: Uuid,
        date: DateTime<Utc>,
    },
    Deleted {
        transporter: Uuid,
        date: DateTime<Utc>,
    },
    TruckAssigned {
        transporter: Uuid,
        date: DateTime<Utc>,
        truck: Uuid,
        execution_date: DateTime<Utc>,
    },
    DestinationAdded {
        id: Uuid,
        transporter: Uuid,
        date: DateTime<Utc>,
        collection_start_time: DateTime<Utc>,
        waste_generation_point: Uuid,
    },
    CollectionStartTimeChanged {
        destination: Uuid,
        transporter: Uuid,
        date: DateTime<Utc>,
        start_time: DateTime<Utc>,
    },
    WasteGenerationPointChanged {
        destination: Uuid,
        transporter: Uuid,
        date: DateTime<Utc>,
        generation_point: Uuid,
    },
    DestinationRemoved {
        id: Uuid,
        transporter: Uuid,
        date: DateTime<Utc>,
    },
    WasteCollected {
        id: Uuid,
        transporter: Uuid,
        date: DateTime<Utc>,
        destination: Uuid,
        container_type: Uuid,
        container_volume: Decimal,
        container_utilization: Decimal,
        waste_volume_m3: Decimal,
    },
    WasteVolumeChanged {
        waste_collection: Uuid,
        transporter: Uuid,
        date: DateTime<Utc>,
        waste_volume_m3: Decimal,
    },
    ContainerChanged {
        waste_collection: Uuid,
        transporter: Uuid,
        date: DateTime<Utc>,
        container_type: Uuid,
        container_volume: Decimal,
    },
    ContainerUtilizationChanged {
        waste_collection: Uuid,
        transporter: Uuid,
        date: DateTime<Utc>,
        container_utilization: Decimal,
    },
    WasteCollectionPhotographed {
        id: Uuid,
        transporter: Uuid,
        date: DateTime<Utc>,
        photos: Vec<Uuid>,
    },
    WasteCollectionRevoked {
        id: Uuid,
        transporter: Uuid,
        date: DateTime<Utc>,
    },
}


#[derive(Debug, Persisted)]
pub enum GenerationPoint {
    Registered {
        registrar: Uuid,
        date: DateTime<Utc>,
    },
    InternalIdentificationChanged {
        registrar: Uuid,
        date: DateTime<Utc>,
        system_number: String,
    },
    ExternalRegistrationChanged {
        registrar: Uuid,
        date: DateTime<Utc>,
        registry_number: String,
    },
    WasteTypeChanged {
        registrar: Uuid,
        date: DateTime<Utc>,
        waste_type: Uuid,
    },
    OrganizationLegalTypeChanged {
        registrar: Uuid,
        date: DateTime<Utc>,
        organization_legal_type: Uuid,
    },
    OperationalStatusChanged {
        registrar: Uuid,
        date: DateTime<Utc>,
        status: Uuid,
    },
    AddressChanged {
        registrar: Uuid,
        date: DateTime<Utc>,
        full_address: String,
    },
    CoordinatesChanged {
        registrar: Uuid,
        date: DateTime<Utc>,
        coordinates: String,
    },
    AreaChanged {
        registrar: Uuid,
        date: DateTime<Utc>,
        area_m2: Decimal,
    },
    CoveringTypeChanged {
        registrar: Uuid,
        date: DateTime<Utc>,
        covering_type: Uuid,
    },
    FenceMaterialChanged {
        registrar: Uuid,
        date: DateTime<Utc>,
        fence_material: Uuid,
    },
    ServiceCompanyChanged {
        registrar: Uuid,
        date: DateTime<Utc>,
        service_company_name: String,
    },
    GeneratorAssigned {
        registrar: Uuid,
        date: DateTime<Utc>,
        generator: Uuid,
    },
    DescriptionChanged {
        registrar: Uuid,
        date: DateTime<Utc>,
        description: String,
    },
    ContainerAdded {
        registrar: Uuid,
        date: DateTime<Utc>,
        container_type: Uuid,
    },
    ContainerRemoved {
        registrar: Uuid,
        date: DateTime<Utc>,
        container_type: Uuid,
    },
    TransporterAssigned {
        registrar: Uuid,
        date: DateTime<Utc>,
        transporter: Uuid,
        start_transportation_date: DateTime<Utc>,
        end_transportation_date: DateTime<Utc>,
    },
    TransportationFrequencyChanged {
        registrar: Uuid,
        date: DateTime<Utc>,
        frequency: String,
    },
    PhotosChanged {
        registrar: Uuid,
        date: DateTime<Utc>,
        photos: Vec<Uuid>,
    },
    AccrualMethodChanged {
        registrar: Uuid,
        date: DateTime<Utc>,
        method: Uuid,
    },
    MeasurementUnitChanged {
        registrar: Uuid,
        date: DateTime<Utc>,
        unit: Uuid,
    },
}

#[derive(Debug, Persisted)]
pub enum Truck {
    Registered {
        registrar: Uuid,
        date: DateTime<Utc>,
    },
    OwnerChanged {
        registrar: Uuid,
        date: DateTime<Utc>,
        owner: String,
    },
    TransporterAssigned {
        registrar: Uuid,
        date: DateTime<Utc>,
        transporter: Uuid,
    },
    FleetTypeChanged {
        registrar: Uuid,
        date: DateTime<Utc>,
        fleet_type: Uuid,
    },
    VinChanged {
        registrar: Uuid,
        date: DateTime<Utc>,
        vin: String,
    },
    RegistrationNumberChanged {
        registrar: Uuid,
        date: DateTime<Utc>,
        registration_number: String
    },
    GaragingNumberChanged {
        registrar: Uuid,
        date: DateTime<Utc>,
        garaging_number: String,
    },
    VehicleModelChanged {
        registrar: Uuid,
        date: DateTime<Utc>,
        vehicle_model: String,
    },
    WasteEquipmentModelChanged {
        registrar: Uuid,
        date: DateTime<Utc>,
        waste_equipment_model: String,
    },
    CargoBodyTypeChanged {
        registrar: Uuid,
        date: DateTime<Utc>,
        cargo_body_type: Uuid,
    },
    RequiredDriverLicenseChanged {
        registrar: Uuid,
        date: DateTime<Utc>,
        category: Uuid
    },
    AxleConfigurationChanged {
        registrar: Uuid,
        date: DateTime<Utc>,
        axle_configuration: Uuid,
    },
    ProductionYearChanged {
        registrar: Uuid,
        date: DateTime<Utc>,
        production_year: i32,
    },
    NavigationEquipmentNameChanged {
        registrar: Uuid,
        date: DateTime<Utc>,
        navigation_equipment_name: String,
    },
    EmptyWeightChanged {
        registrar: Uuid,
        date: DateTime<Utc>,
        empty_weight_kg: Decimal,
    },
    CargoCapacityChanged {
        registrar: Uuid,
        date: DateTime<Utc>,
        cargo_capacity_kg: Decimal,
    }
}

#[derive(Debug, Persisted)]
pub enum Negotiation {
    Started {
        transporter: Uuid,
        transportation: Uuid,
        transportation_version: i32,
        date: DateTime<Utc>,
    },
    IssuesDeclarationStagePassed {
        id: Uuid,
        contractor: Uuid,
        date: DateTime<Utc>,
        transportation_version: i32,
    },
    TransportationIssueOpened {
        id: Uuid,
        declaration: Uuid,
        comment: String,
    },
    TransportationIssueReopened {
        id: Uuid,
        declaration: Uuid,
        comment: String,
    },
    WasteVolumeIssueOpened {
        id: Uuid,
        declaration: Uuid,
        comment: String,
        waste_collection: Uuid,
        destination: Uuid
    },
    WasteVolumeIssueReopened {
        id: Uuid,
        declaration: Uuid,
        comment: String,
    },
    CollectionStartTimeIssueOpened {
        id: Uuid,
        declaration: Uuid,
        comment: String,
        destination: Uuid,
    },
    CollectionStartTimeIssueReopened {
        id: Uuid,
        declaration: Uuid,
        comment: String,
    },
    CollectionPhotoIssueOpened {
        id: Uuid,
        declaration: Uuid,
        comment: String,
        waste_collection: Uuid,
        destination: Uuid
    },
    CollectionPhotoIssueReopened {
        id: Uuid,
        declaration: Uuid,
        comment: String,
    },
    IssuesResolutionStagePassed {
        id: Uuid,
        transporter: Uuid,
        date: DateTime<Utc>,
        transportation_version: i32,
    },
    TransportationIssueResolved {
        id: Uuid,
        resolution: Uuid,
        comment: String,
    },
    TransportationIssueRejected {
        id: Uuid,
        resolution: Uuid,
        comment: String,
    },
    WasteVolumeIssueResolved {
        id: Uuid,
        resolution: Uuid,
        waste_volume_m3: Decimal,
    },
    WasteVolumeIssueRejected {
        id: Uuid,
        resolution: Uuid,
        comment: String,
    },
    CollectionStartTimeIssueResolved {
        id: Uuid,
        resolution: Uuid,
        start_time: DateTime<Utc>,
    },
    CollectionStartTimeIssueRejected {
        id: Uuid,
        resolution: Uuid,
        comment: String,
    },
    CollectionPhotoIssueResolved {
        id: Uuid,
        resolution: Uuid,
        photos: Vec<Uuid>,
    },
    CollectionPhotoIssueRejected {
        id: Uuid,
        resolution: Uuid,
        comment: String,
    },
    Completed {
        contractor: Uuid,
        date: DateTime<Utc>,
        transportation_version: i32,
    },
}

#[derive(Debug, Persisted)]
pub enum Report {
    Created {
        contractor: Uuid,
        date: DateTime<Utc>,
    },
    TransportationCovered {
        transportations: Vec<Uuid>
    },
    TargetSpecified {
        transporter: Uuid,
        month: DateTime<Utc>
    }
}