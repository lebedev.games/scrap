use chrono::{DateTime, Utc};
use rust_decimal::Decimal;
use serde::{Deserialize, Serialize};
use uuid::Uuid;

use crate::serialization::date_format;

#[derive(Deserialize)]
pub struct RegisterUserForm {
    pub user_id: Uuid,
    pub role: String,
    pub name: String,
    pub login: String,
    pub password: String,
    pub permissions: Vec<String>,
}

#[derive(Serialize)]
pub struct UserListItem {
    pub id: Uuid,
    pub permissions: Vec<String>,
}

#[derive(Deserialize)]
pub struct GrantUserPermissionBody {
    pub permission: String
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct RegisterGeneratorForm {
    pub legal_entity: GeneratorLegalEntity,
    pub parent: Option<Uuid>,
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct GeneratorModel {
    pub legal_entity: GeneratorLegalEntity,
    pub parent: Option<Uuid>,
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct RegisterGeneratorResponse {
    pub id: Uuid
}

#[derive(Deserialize, Serialize)]
#[serde(tag = "type")]
pub enum GeneratorLegalEntity {
    BusinessEntity(BusinessEntityModel),
    Entrepreneur(EntrepreneurModel),
}

#[derive(Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct ResponsiblePerson {
    pub full_name: String,
    pub phone: String,
}

#[derive(Deserialize, Serialize, Default)]
#[serde(rename_all = "camelCase")]
pub struct BusinessEntityModel {
    pub full_name: String,
    pub short_name: String,
    pub inn: String,
    pub kpp: String,
    pub ogrn: String,
    pub phone: String,
    pub email: Option<String>,
    pub address: String,
    pub postal_address: String,
    pub actual_address: String,
    pub responsible_persons: Vec<ResponsiblePerson>,
    pub bank_details: BankDetails,
}

#[derive(Deserialize, Serialize, Default)]
#[serde(rename_all = "camelCase")]
pub struct EntrepreneurModel {
    pub first_name: String,
    pub last_name: String,
    pub patronymic: String,
    pub inn: String,
    pub ogrnip: String,
    pub phone: String,
    pub email: Option<String>,
    pub address: String,
    pub actual_address: String,
    pub bank_details: BankDetails,
}


#[derive(Deserialize, Serialize, Default)]
#[serde(rename_all = "camelCase")]
pub struct BankDetails {
    pub account: String,
    pub correspondent_account: String,
    pub bank_name: String,
    pub rcbic: String,
}


#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Affiliate {
    pub id: Uuid,
    pub name: String,
    pub inn: String,
    pub legal_entity_type: String,
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct GeneratorListItem {
    pub id: Uuid,
    pub name: String,
    pub inn: String,
    pub legal_entity_type: String,
    pub affiliates: Vec<Affiliate>,
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct SelectOption {
    pub id: Uuid,
    pub name: String,
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct RegisterTransportationForm {
    pub truck: Uuid,
    #[serde(with = "date_format")]
    pub execution_date: DateTime<Utc>,
    pub destinations: Vec<TransportationDestination>,
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct TransportationForm {
    pub truck: Uuid,
    #[serde(with = "date_format")]
    pub execution_date: DateTime<Utc>,
    pub destinations: Vec<TransportationDestination>,
}

#[derive(Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct TransportationDestination {
    pub id: Uuid,
    #[serde(with = "date_format")]
    pub collection_start_time: DateTime<Utc>,
    pub waste_generation_point: Uuid,
    pub collections: Vec<WasteCollection>,
}

#[derive(Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct WasteCollection {
    pub id: Uuid,
    pub destination: Uuid,
    pub container_type: Uuid,
    pub container_volume: Decimal,
    pub container_utilization: Decimal,
    pub waste_volume_m3: Decimal,
    pub photos: Vec<Uuid>,
}

#[derive(Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct EditTransportationForm {
    pub changes: Vec<TransportationChange>
}

#[derive(Deserialize, Serialize)]
#[serde(tag = "type", content = "data")]
pub enum TransportationChange {
    #[serde(rename_all = "camelCase")]
    AssignTruck {
        truck: Uuid,
        #[serde(with = "date_format")]
        execution_date: DateTime<Utc>,
    },
    #[serde(rename_all = "camelCase")]
    AddDestination {
        id: Uuid,
        #[serde(with = "date_format")]
        collection_start_time: DateTime<Utc>,
        waste_generation_point: Uuid,
    },
    #[serde(rename_all = "camelCase")]
    ChangeCollectionStartTime {
        destination: Uuid,
        #[serde(with = "date_format")]
        start_time: DateTime<Utc>,
    },
    #[serde(rename_all = "camelCase")]
    ChangeWasteGenerationPoint {
        destination: Uuid,
        generation_point: Uuid,
    },
    #[serde(rename_all = "camelCase")]
    RemoveDestination {
        id: Uuid,
    },
    #[serde(rename_all = "camelCase")]
    CollectWaste {
        id: Uuid,
        destination: Uuid,
        container_type: Uuid,
        container_volume: Decimal,
        container_utilization: Decimal,
        waste_volume_m3: Decimal,
    },
    #[serde(rename_all = "camelCase")]
    ChangeWasteVolume {
        waste_collection: Uuid,
        waste_volume_m3: Decimal,
    },
    #[serde(rename_all = "camelCase")]
    ChangeContainer {
        waste_collection: Uuid,
        container_type: Uuid,
        container_volume: Decimal,
    },
    #[serde(rename_all = "camelCase")]
    ChangeContainerUtilization {
        waste_collection: Uuid,
        container_utilization: Decimal,
    },
    #[serde(rename_all = "camelCase")]
    PhotographWasteCollection {
        id: Uuid,
        photos: Vec<Uuid>,
    },
    #[serde(rename_all = "camelCase")]
    RevokeWasteCollection {
        id: Uuid,
    },
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Resource {
    pub id: Uuid
}

#[derive(Debug, Deserialize)]
pub struct VersionedTransportationResource {
    pub id: Uuid,
    pub version: Option<i32>
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct TransportationListItemModel {
    pub id: Uuid,
    pub district: String,
    #[serde(with = "date_format")]
    pub execution_date: DateTime<Utc>,
    pub truck_registration_number: String,
    pub status: String,
}

#[derive(Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct RegisterGenerationPointFormContainer {
    pub container_type: Uuid,
}

#[derive(Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct RegisterGenerationPointFormTransporter {
    pub transporter: Uuid,
    #[serde(with = "date_format")]
    pub start_transportation_date: DateTime<Utc>,
    #[serde(with = "date_format")]
    pub end_transportation_date: DateTime<Utc>,
    pub frequency: String,
}

#[derive(Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct RegisterGenerationPointForm {
    pub internal_system_number: String,
    pub external_registry_number: String,
    pub waste_type: Uuid,
    pub organization_legal_type: Uuid,
    pub operational_status: Uuid,
    pub full_address: String,
    pub coordinates: String,
    pub area_m2: Decimal,
    pub covering_type: Uuid,
    pub fence_material: Uuid,
    pub service_company_name: Option<String>,
    pub generator: Uuid,
    pub description: String,
    pub containers: Vec<RegisterGenerationPointFormContainer>,
    pub transporter: RegisterGenerationPointFormTransporter,
    pub photos: Vec<Uuid>,
    pub accrual_method: Uuid,
    pub measurement_unit: Uuid,
}

#[derive(Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct GenerationPointModelContainer {
    pub container_type: Uuid,
}

#[derive(Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct GenerationPointModelTransporter {
    pub transporter: Uuid,
    #[serde(with = "date_format")]
    pub start_transportation_date: DateTime<Utc>,
    #[serde(with = "date_format")]
    pub end_transportation_date: DateTime<Utc>,
}

#[derive(Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct GenerationPointModel {
    pub id: Uuid,
    pub internal_system_number: String,
    pub external_registry_number: String,
    pub waste_type: Uuid,
    pub organization_legal_type: Uuid,
    pub operational_status: Uuid,
    pub full_address: String,
    pub coordinates: String,
    pub area_m2: Decimal,
    pub covering_type: Uuid,
    pub fence_material: Uuid,
    pub service_company_name: Option<String>,
    pub generator: Uuid,
    pub description: String,
    pub containers: Vec<GenerationPointModelContainer>,
    pub transporter: GenerationPointModelTransporter,
    pub photos: Vec<Uuid>,
    pub accrual_method: Uuid,
    pub measurement_unit: Uuid,
    pub transportation_frequency: String,
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct BadRequestError {
    pub error: String
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct FileUpload {
    pub files: Vec<Uuid>
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct TransporterModel {
    pub legal_entity: TransporterLegalEntityModel,
    pub fkko: String,
    pub parent: Option<Uuid>,
    pub truck_fleet_types: Vec<Uuid>,
    pub trucks: Vec<Uuid>,
    pub target_generation_points: Vec<Uuid>,
    pub note: String,
}

#[derive(Deserialize, Serialize)]
#[serde(tag = "type")]
pub enum TransporterLegalEntityModel {
    BusinessEntity(BusinessEntityModel),
    Entrepreneur(EntrepreneurModel),
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct RegisterTransporterForm {
    pub legal_entity: TransporterLegalEntityModel,
    pub fkko: String,
    pub parent: Option<Uuid>,
    pub truck_fleet_types: Vec<Uuid>,
    pub note: String,
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct TransporterSearchCriteria {
    pub inn: String,
    pub name: String,
    pub truck_fleet_type: Option<Uuid>,
    pub legal_entity_type: Option<String>,
    pub limit: Option<i32>,
    pub offset: Option<i32>,
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct TransporterListItemModel {
    pub id: Uuid,
    pub legal_entity_type: String,
    pub inn: String,
    pub name: String,
    pub truck_fleet_types: Vec<String>,
    pub districts: Vec<String>,
}


#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct TargetContainerType {
    pub id: Uuid,
    pub name: String,
    pub volume_limit_m3: Option<Decimal>,
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct TargetGenerationPoint {
    pub id: Uuid,
    pub address: String,
    pub container_types: Vec<TargetContainerType>,
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct GenerationPointListItemModel {
    pub id: Uuid,
    pub internal_system_number: String,
    pub full_address: String,
    pub transporter_name: String,
    pub area_m2: Decimal,
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct GenerationPointSearchCriteria {
    pub internal_system_number: String,
    pub transporter: String,
    pub address: String,
    pub limit: Option<i32>,
    pub offset: Option<i32>,
}

#[derive(Deserialize)]
pub enum AuthorizationForm {
    PasswordCredentials {
        login: String,
        password: String,
    }
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct AuthorizationRequest {
    pub form: AuthorizationForm
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Authorization {
    pub user_name: String,
    pub user_id: Uuid,
    pub user_role: String,
    pub access_token: String,
}


#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct DemoSetUp {
    pub errors: Vec<String>
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct RegisterTruckForm {
    pub owner: String,
    pub transporter: Uuid,
    pub fleet_type: Uuid,
    pub vin: String,
    pub registration_number: String,
    pub garaging_number: String,
    pub vehicle_model: String,
    pub waste_equipment_model: String,
    pub cargo_body_type: Uuid,
    pub required_driver_license_category: Uuid,
    pub axle_configuration: Uuid,
    pub production_year: i32,
    pub navigation_equipment_name: String,
    pub empty_weight_kg: Decimal,
    pub cargo_capacity_kg: Decimal,
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct TruckModel {
    pub owner: String,
    pub transporter: Uuid,
    pub fleet_type: Uuid,
    pub vin: String,
    pub registration_number: String,
    pub garaging_number: String,
    pub vehicle_model: String,
    pub waste_equipment_model: String,
    pub cargo_body_type: Uuid,
    pub required_driver_license_category: Uuid,
    pub axle_configuration: Uuid,
    pub production_year: i32,
    pub navigation_equipment_name: String,
    pub empty_weight_kg: Decimal,
    pub cargo_capacity_kg: Decimal,
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct TruckListItemModel {
    pub id: Uuid,
    pub registration_number: String,
    pub cargo_body_type: Uuid,
    pub transporter_name: String,
    pub truck_fleet_type: Uuid,
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct TruckSearchCriteria {
    pub registration_number: String,
    pub cargo_body_type: Option<Uuid>,
    pub transporter_name: String,
    pub truck_fleet_type: Option<Uuid>,
    pub limit: Option<i32>,
    pub offset: Option<i32>,
}


#[derive(Serialize, Deserialize)]
pub enum ResolutionModel {
    #[serde(rename_all = "camelCase")]
    WasteVolumeIssueResolution {
        issue_id: Uuid,
        waste_volume: Decimal,
    },
    #[serde(rename_all = "camelCase")]
    CollectionStartTimeIssueResolution {
        issue_id: Uuid,
        #[serde(with = "date_format")]
        start_time: DateTime<Utc>,
    },
    #[serde(rename_all = "camelCase")]
    CollectionPhotoIssueResolution {
        issue_id: Uuid,
        photos: Vec<Uuid>,
    }
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SolveNegotiationIssuesRequest {
    pub resolutions: Vec<ResolutionModel>
}

#[derive(Deserialize, Serialize)]
pub enum IssueModel {
    WasteVolumeIssue {
        id: Uuid,
        destination: Uuid,
        collection: Uuid,
        comment: String
    },
    CollectionStartTimeIssue {
        id: Uuid,
        destination: Uuid,
        comment: String
    },
    CollectionPhotoIssue {
        id: Uuid,
        destination: Uuid,
        collection: Uuid,
        comment: String
    }
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct DeclareNegotiationIssuesRequest {
    pub issues: Vec<IssueModel>
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct CompleteNegotiationRequest {

}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct List<T> {
    pub items: Vec<T>,
    pub limit: i32,
    pub offset: i32,
    pub total: i64
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct NegotiationListItem {
    pub id: Uuid,
    pub district: String,
    #[serde(with = "date_format")]
    pub transportation_date: DateTime<Utc>,
    pub transportation: Uuid,
    pub truck: String,
    pub transporter: String,
    pub status: String,
}


#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct NegotiationSearchCriteria {
    pub transportation_date: Option<DateTime<Utc>>,
    pub truck: String,
    pub district: String,
    pub transporter: String,
    pub status: Option<String>,
    pub limit: Option<i32>,
    pub offset: Option<i32>,
    pub consolidated: Option<bool>
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct GeneratorSearchCriteria {
    pub inn: String,
    pub name: String,
    pub legal_entity_type: Option<String>,
    pub limit: Option<i32>,
    pub offset: Option<i32>,
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct TimelinePoint {
    pub stage_id: Uuid,
    pub negotiation: Uuid,
    pub transportation_version: i32,
    pub timestamp: DateTime<Utc>,
    pub status: String
}

#[derive(Serialize)]
pub struct IssuesDeclaration {
    pub issues: Vec<IssueModel>,
    pub date: DateTime<Utc>
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct NegotiationModel {
    pub id: Uuid,
    pub transportation: Uuid,
    pub timeline: Vec<TimelinePoint>,
    pub declarations: Vec<IssuesDeclaration>
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ReportSearchCriteria {
    pub target_transporter: String,
    pub target_month: Option<DateTime<Utc>>,
    pub reporting_date: Option<DateTime<Utc>>,
    pub limit: Option<i32>,
    pub offset: Option<i32>,
}


#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct ReportListItem {
    pub id: Uuid,
    pub target_transporter: String,
    pub target_month: DateTime<Utc>,
    pub transportations: usize,
    pub reporting_date: DateTime<Utc>
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct CreateReportForm {
    pub target_transporter: Uuid,
    pub target_month: DateTime<Utc>,
    pub transportations: Vec<Uuid>
}