use actix_web::HttpResponse;
use actix_web::web::Data;

use crate::application::EventStore;
use crate::application::projections::GeneratorProjection;
use crate::application::aggregates::{AccessUser, ReplayError, ensure_access, Permission, UserAccessError};
use crate::extractors::AccessToken;
use crate::model::SelectOption;

pub async fn handle_get_parental_generators(
    projection: Data<GeneratorProjection>,
    store: Data<EventStore>,
    access_token: AccessToken
) -> HttpResponse {
    let consumer = access_token.user;

    let result = match AccessUser::replay(&consumer, &store).await {
        Ok(consumer) => {
            match ensure_access(consumer, Permission::GeneratorGetParental) {
                Ok(_) => match projection.get_parental_generators().await {
                    Ok(users) => Ok(users),
                    Err(error) => Err(Fail::ProjectionError(error))
                },
                Err(error) => Err(Fail::ActionForbidden(error))
            }
        }
        Err(error) => Err(Fail::ConsumerReplayError(error))
    };

    let result = result
        .and_then(|generators| Ok(generators
            .into_iter()
            .map(|generator| SelectOption {
                id: generator.id,
                name: generator.name,
            }).collect::<Vec<SelectOption>>()));

    match result {
        Ok(parental) => {
            HttpResponse::Ok().json(parental)
        }
        Err(error) => return match error {
            Fail::ConsumerReplayError(error) => {
                error!("Unable to list parental generators because of consumer replay, {:?}", error);
                HttpResponse::UnprocessableEntity().finish()
            }
            Fail::ActionForbidden(error) => {
                info!("Unable to list parental generators, action forbidden, {:?}", error);
                HttpResponse::Forbidden().finish()
            }
            Fail::ProjectionError(error) => {
                error!("Unable to list parental generators, {}", error);
                HttpResponse::InternalServerError().finish()
            }
        }
    }
}

enum Fail {
    ConsumerReplayError(ReplayError),
    ActionForbidden(UserAccessError),
    ProjectionError(sqlx::Error),
}