use actix_web::HttpResponse;
use actix_web::web::Data;
use uuid::Uuid;

use crate::application::aggregates::{AccessUser, ensure_access, Permission, ReplayError, UserAccessError};
use crate::application::EventStore;
use crate::application::projections::{TransporterOption, TransporterProjection};
use crate::extractors::AccessToken;
use crate::model::SelectOption;

pub async fn handle_get_transporter_options(
    store: Data<EventStore>,
    projection: Data<TransporterProjection>,
    access_token: AccessToken,
) -> HttpResponse {
    let consumer = access_token.user;

    match get_transporter_options(&store, &projection, &consumer).await {
        Ok(transporters) => {
            let data = transporters.into_iter().map(|transporter| SelectOption {
                id: transporter.id,
                name: transporter.name,
            }).collect::<Vec<SelectOption>>();
            HttpResponse::Ok().json(data)
        }
        Err(error) => match error {
            GetTransporterOptionsError::ConsumerReplayError(error) => {
                error!("Unable to get transporter options because of consumer replay, {:?}", error);
                HttpResponse::Unauthorized().finish()
            }
            GetTransporterOptionsError::ConsumerAccessDenied(error) => {
                warn!("Unable to get transporter options, consumer access denied, {:?}", error);
                HttpResponse::Forbidden().finish()
            }
            GetTransporterOptionsError::TransporterProjectionError(error) => {
                error!("Unable to get transporter options because of projection, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
        }
    }
}

pub enum GetTransporterOptionsError {
    ConsumerReplayError(ReplayError),
    ConsumerAccessDenied(UserAccessError),
    TransporterProjectionError(sqlx::Error),
}

pub async fn get_transporter_options(
    store: &EventStore,
    projection: &TransporterProjection,
    consumer: &Uuid,
) -> Result<Vec<TransporterOption>, GetTransporterOptionsError> {
    let user = AccessUser::replay(consumer, store)
        .await
        .map_err(GetTransporterOptionsError::ConsumerReplayError)?;

    ensure_access(user, Permission::TransporterList)
        .map_err(GetTransporterOptionsError::ConsumerAccessDenied)?;

    projection.get_transporter_options()
        .await
        .map_err(GetTransporterOptionsError::TransporterProjectionError)
}