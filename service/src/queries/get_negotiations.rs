use actix_web::HttpResponse;
use actix_web::web::{Data, Query};
use uuid::Uuid;

use crate::application::aggregates::{AccessUser, ensure_access, Permission, ReplayError, UserAccessError};
use crate::application::EventStore;
use crate::application::projections::NegotiationProjection;
use crate::extractors::AccessToken;
use crate::model::{List, NegotiationListItem, NegotiationSearchCriteria};

pub async fn handle_get_negotiations(
    criteria: Query<NegotiationSearchCriteria>,
    store: Data<EventStore>,
    projection: Data<NegotiationProjection>,
    access_token: AccessToken
) -> HttpResponse {
    let consumer = access_token.user;

    match list_negotiations(&store, &projection, &consumer, &criteria).await {
        Ok(list) => {
            HttpResponse::Ok().json(list)
        }
        Err(error) => match error {
            Fail::ConsumerReplayError(error) => {
                error!("Unable to get negotiations because of consumer replay, {:?}", error);
                HttpResponse::Unauthorized().finish()
            }
            Fail::ConsumerAccessDenied(error) => {
                warn!("Unable to get negotiations, consumer access denied, {:?}", error);
                HttpResponse::Forbidden().finish()
            }
            Fail::NegotiationProjectionError(error) => {
                error!("Unable to get negotiations because of projection, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
        }
    }
}

pub enum Fail {
    ConsumerReplayError(ReplayError),
    ConsumerAccessDenied(UserAccessError),
    NegotiationProjectionError(sqlx::Error)
}

pub async fn list_negotiations(
    store: &EventStore,
    projection: &NegotiationProjection,
    consumer: &Uuid,
    criteria: &NegotiationSearchCriteria
) -> Result<List<NegotiationListItem>, Fail> {
    let user = AccessUser::replay(consumer, store)
        .await
        .map_err(Fail::ConsumerReplayError)?;

    ensure_access(user, Permission::NegotiationList)
        .map_err(Fail::ConsumerAccessDenied)?;

    let limit = match criteria.limit {
        Some(value) => value,
        None => 10
    };

    let offset = match criteria.offset {
        Some(value) => value,
        None => 0
    };

    let fetch = projection.get_negotiations(
        &criteria.transportation_date,
        &criteria.truck,
        &criteria.district,
        &criteria.transporter,
        &criteria.status,
        &criteria.consolidated,
        &limit,
        &offset,
    );

    let collection = fetch.await.map_err(Fail::NegotiationProjectionError)?;

    Ok(List {
        items: collection.items.into_iter().map(|negotiation| NegotiationListItem {
            id: negotiation.id,
            district: negotiation.district,
            transportation_date: negotiation.transportation_date,
            transportation: negotiation.transportation,
            truck: negotiation.truck,
            transporter: negotiation.transporter,
            status: negotiation.status
        }).collect(),
        total: collection.total,
        limit,
        offset,
    })
}