use std::path::Path;

use actix_files::NamedFile;
use actix_web::{HttpResponse, Responder};
use actix_web::{HttpRequest, Result};
use actix_web::web;
use actix_web::web::Data;
use chrono::{Datelike, DateTime, Utc, Timelike};
use rust_decimal::prelude::ToPrimitive;
use uuid::Uuid;
use xlsxwriter::{Workbook, XlsxError, FormatBorder, FormatAlignment};

use crate::application::aggregates::{ReplayError, ReportAggregate, TransportationDraft, GenerationPointAggregate};
use crate::application::EventStore;
use crate::application::projections::{TransporterProjection, TruckProjection, TruckProjectionError, GeneratorProjection};
use crate::model::{Resource, SelectOption};
use crate::application::dictionary::Dictionary;

pub async fn handle_download_report(
    request: HttpRequest,
    resource: web::Path<Resource>,
    store: Data<EventStore>,
    transporter_projection: Data<TransporterProjection>,
    truck_projection: Data<TruckProjection>,
    dictionary: Data<Dictionary>,
    generator_projection: Data<GeneratorProjection>,
) -> impl Responder {
    let report = resource.id;

    match make_report(&report, &store, &transporter_projection, &truck_projection, &dictionary, &generator_projection).await {
        Ok(path) => {
            NamedFile::open(Path::new(&path))
                .unwrap()
                .into_response(&request)
                .unwrap()
        }
        Err(error) => match error {
            Fail::WorkSheetError(error) => {
                error!("Unable to download report because of work sheet, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
            Fail::ReportReplayError(error) => {
                error!("Unable to download report because of report replay, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
            Fail::TransporterReplayError(error) => {
                error!("Unable to download report because of transporter replay, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
            Fail::TransportationReplayError(error) => {
                error!("Unable to download report because of transportation replay, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
            Fail::TruckOptionsReplay(error) => {
                error!("Unable to download report because of truck options replay, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
            Fail::GenerationPointReplayError(error) => {
                error!("Unable to download report because of generation point replay, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
            Fail::DestinationMapError => {
                error!("Unable to download report because of destination map");
                HttpResponse::InternalServerError().finish()
            }
            Fail::GeneratorReplayError(error) => {
                error!("Unable to download report because of generator replay error, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
        }
    }
}

enum Fail {
    WorkSheetError(XlsxError),
    ReportReplayError(ReplayError),
    TransporterReplayError(sqlx::Error),
    TransportationReplayError(ReplayError),
    GenerationPointReplayError(ReplayError),
    GeneratorReplayError(sqlx::Error),
    TruckOptionsReplay(TruckProjectionError),
    DestinationMapError,
}

async fn make_report(
    report: &Uuid,
    store: &EventStore,
    transporter_projection: &TransporterProjection,
    truck_projection: &TruckProjection,
    dictionary: &Dictionary,
    generator_projection: &GeneratorProjection
) -> Result<String, Fail> {
    let report = ReportAggregate::replay(&report, store)
        .await
        .map_err(Fail::ReportReplayError)?;

    let transporter = transporter_projection.get_transporter_item(report.target_transporter)
        .await
        .map_err(Fail::TransporterReplayError)?;

    let truck_options = truck_projection.get_truck_options(&transporter.id)
        .await
        .map_err(Fail::TruckOptionsReplay)?;

    let get_registration_number = |truck: Uuid| {
        match truck_options.iter().find(|option| option.id == truck) {
            Some(option) => option.registration_number.clone(),
            None => truck.to_string()
        }
    };

    let organization_legal_type_options = dictionary.get_organization_legal_type_records().await;

    let waste_types = dictionary.get_waste_type_records().await;

    let name_of = |value: Uuid, options: &Vec<SelectOption>| {
        for option in options {
            if option.id == value {
                return option.name.clone()
            }
        }
        return value.to_string()
    };

    let generator_list = generator_projection.get_generators(&"".to_string(), &"".to_string(), &None, &1000, &0)
        .await
        .map_err(Fail::GeneratorReplayError)?;

    let get_generator = |id: Uuid| {
        match generator_list.items.iter().find(|item| item.id == id) {
            Some(item) => (item.name.clone(), item.inn.clone()),
            None => (id.to_string(), id.to_string())
        }
    };

    let months = vec!["январь", "февраль", "март", "апрель", "май", "июнь", "июль", "август", "сентябрь", "октябрь", "ноябрь", "декабрь"];
    let i = report.target_month.month0() as usize;
    let month = months[i];

    let filepath = format!(
        "./upload/{} {}, {}.xlsx",
        &transporter.name,
        month,
        &report.target_month.year()
    );
    let workbook = Workbook::new(&filepath);

    let date_format = workbook.add_format()
        .set_num_format("dd.mm.yyyy")
        .set_border(FormatBorder::Double);

    let header_format = workbook.add_format()
        .set_border(FormatBorder::Medium);

    let index_format = workbook.add_format()
        .set_align(FormatAlignment::Center)
        .set_border(FormatBorder::Double);

    let format = workbook.add_format()
        .set_border(FormatBorder::Double);


    let mut sheet = workbook.add_worksheet(None).map_err(Fail::WorkSheetError)?;
    sheet.write_string(0, 0, "Номер КП", Some(&header_format)).map_err(Fail::WorkSheetError)?;
    sheet.write_string(1, 0, "1", Some(&index_format)).map_err(Fail::WorkSheetError)?;

    sheet.write_string(0, 1, "Дата оказания услуги", Some(&header_format)).map_err(Fail::WorkSheetError)?;
    sheet.write_string(1, 1, "2", Some(&index_format)).map_err(Fail::WorkSheetError)?;

    sheet.write_string(0, 2, "Регистрационный знак Автотранспорта", Some(&header_format)).map_err(Fail::WorkSheetError)?;
    sheet.write_string(1, 2, "3", Some(&index_format)).map_err(Fail::WorkSheetError)?;

    sheet.write_string(0, 3, "Указать (ТКО/КГО)", Some(&header_format)).map_err(Fail::WorkSheetError)?;
    sheet.write_string(1, 3, "4", Some(&index_format)).map_err(Fail::WorkSheetError)?;

    sheet.write_string(0, 4, "Категория объекта образования ТКО (МКД/ИЖС/СНТ/юр.лицо/ИП)", Some(&header_format)).map_err(Fail::WorkSheetError)?;
    sheet.write_string(1, 4, "5", Some(&index_format)).map_err(Fail::WorkSheetError)?;

    sheet.write_string(0, 5, "ИНН Образователя", Some(&header_format)).map_err(Fail::WorkSheetError)?;
    sheet.write_string(1, 5, "6", Some(&index_format)).map_err(Fail::WorkSheetError)?;

    sheet.write_string(0, 6, "Наименование Образователя", Some(&header_format)).map_err(Fail::WorkSheetError)?;
    sheet.write_string(1, 6, "7", Some(&index_format)).map_err(Fail::WorkSheetError)?;

    sheet.write_string(0, 7, "Адрес КП", Some(&header_format)).map_err(Fail::WorkSheetError)?;
    sheet.write_string(1, 7, "8", Some(&index_format)).map_err(Fail::WorkSheetError)?;

    sheet.write_string(0, 8, "Время начала погрузки", Some(&header_format)).map_err(Fail::WorkSheetError)?;
    sheet.write_string(1, 8, "9", Some(&index_format)).map_err(Fail::WorkSheetError)?;

    sheet.write_string(0, 9, "Объем контейнера, м3", Some(&header_format)).map_err(Fail::WorkSheetError)?;
    sheet.write_string(1, 9, "10", Some(&index_format)).map_err(Fail::WorkSheetError)?;

    sheet.write_string(0, 10, "Кол-во вывезенных контейнеров, шт.", Some(&header_format)).map_err(Fail::WorkSheetError)?;
    sheet.write_string(1, 10, "11", Some(&index_format)).map_err(Fail::WorkSheetError)?;

    sheet.write_string(0, 11, "Итого объем вывоза с КП, м3", Some(&header_format)).map_err(Fail::WorkSheetError)?;
    sheet.write_string(1, 11, "12", Some(&index_format)).map_err(Fail::WorkSheetError)?;

    fn date(value: &DateTime<Utc>) -> xlsxwriter::DateTime {
        xlsxwriter::DateTime {
            year: value.year() as i16,
            month: value.month() as i8,
            day: value.day() as i8,
            hour: 0,
            min: 0,
            second: 0.0
        }
    }

    let mut row = 2;
    for transportation in report.transportations {
        let transportation = TransportationDraft::replay(&transportation, store)
            .await
            .map_err(Fail::TransportationReplayError)?;

        let destinations = transportation.destinations();

        for collection in transportation.waste_collections.iter() {
            let destination = match destinations.get(&collection.destination) {
                Some(destination) => destination,
                None => return Err(Fail::DestinationMapError)
            };
            let start = destination.collection_start_time;

            let point = GenerationPointAggregate::replay(&destination.waste_generation_point, store)
                .await
                .map_err(Fail::GenerationPointReplayError)?;

            let (name, inn) = get_generator(point.generator);

            sheet.write_string(row, 0, "", Some(&format)).map_err(Fail::WorkSheetError)?;
            sheet.write_datetime(row, 1, &date(&transportation.execution_date), Some(&date_format)).map_err(Fail::WorkSheetError)?;
            sheet.write_string(row, 2, &get_registration_number(transportation.truck), Some(&format)).map_err(Fail::WorkSheetError)?;
            sheet.write_string(row, 3, &name_of(point.waste_type, &waste_types), Some(&format)).map_err(Fail::WorkSheetError)?;
            sheet.write_string(row, 4, &name_of(point.organization_legal_type, &organization_legal_type_options), Some(&format)).map_err(Fail::WorkSheetError)?;
            sheet.write_string(row, 5, &inn, Some(&format)).map_err(Fail::WorkSheetError)?;
            sheet.write_string(row, 6, &name, Some(&format)).map_err(Fail::WorkSheetError)?;
            sheet.write_string(row, 7, &point.full_address, Some(&format)).map_err(Fail::WorkSheetError)?;
            sheet.write_string(row, 8, &format!("{}:{}", start.hour(), start.minute()), Some(&format)).map_err(Fail::WorkSheetError)?;
            sheet.write_number(row, 9, collection.waste_volume_m3.to_f64().unwrap(), Some(&format)).map_err(Fail::WorkSheetError)?;
            sheet.write_number(row, 10, collection.waste_volume_m3.to_f64().unwrap(), Some(&format)).map_err(Fail::WorkSheetError)?;
            sheet.write_number(row, 11, collection.waste_volume_m3.to_f64().unwrap(), Some(&format)).map_err(Fail::WorkSheetError)?;

            row += 1;
        }
    }

    workbook.close().map_err(Fail::WorkSheetError)?;

    Ok(filepath)
}