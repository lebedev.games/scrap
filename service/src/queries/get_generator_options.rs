use actix_web::HttpResponse;
use actix_web::web::Data;
use uuid::Uuid;

use crate::application::aggregates::{AccessUser, ensure_access, Permission, ReplayError, UserAccessError};
use crate::application::EventStore;
use crate::application::projections::{GeneratorProjection, GeneratorListItem, Collection};
use crate::extractors::AccessToken;
use crate::model::SelectOption;

pub async fn handle_get_generator_options(
    store: Data<EventStore>,
    projection: Data<GeneratorProjection>,
    access_token: AccessToken,
) -> HttpResponse {
    let consumer = access_token.user;

    match get_generators(&store, &projection, &consumer).await {
        Ok(collection) => {
            let data = collection.items.into_iter().map(|generator| SelectOption {
                id: generator.id,
                name: generator.name,
            }).collect::<Vec<SelectOption>>();
            HttpResponse::Ok().json(data)
        }
        Err(error) => match error {
            Fail::ConsumerReplayError(error) => {
                error!("Unable to get generator options because of consumer replay, {:?}", error);
                HttpResponse::Unauthorized().finish()
            }
            Fail::ConsumerAccessDenied(error) => {
                warn!("Unable to get generator options, consumer access denied, {:?}", error);
                HttpResponse::Forbidden().finish()
            }
            Fail::GeneratorProjectionError(error) => {
                error!("Unable to get generator options because of projection, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
        }
    }
}

pub enum Fail {
    ConsumerReplayError(ReplayError),
    ConsumerAccessDenied(UserAccessError),
    GeneratorProjectionError(sqlx::Error),
}

pub async fn get_generators(
    store: &EventStore,
    projection: &GeneratorProjection,
    consumer: &Uuid,
) -> Result<Collection<GeneratorListItem>, Fail> {
    let user = AccessUser::replay(consumer, store)
        .await
        .map_err(Fail::ConsumerReplayError)?;

    ensure_access(user, Permission::GeneratorList)
        .map_err(Fail::ConsumerAccessDenied)?;

    projection.get_generators(&"".to_string(), &"".to_string(), &None, &100, &0)
        .await
        .map_err(Fail::GeneratorProjectionError)
}