use std::path::Path;

use actix_files::NamedFile;
use actix_web::{HttpRequest, Result};

pub async fn handle_get_upload_file(request: HttpRequest) -> Result<NamedFile> {
    let file = request.match_info().query("file");
    let path = format!("./upload/{}", file);
    let path = Path::new(&path);
    Ok(NamedFile::open(path)?)
}