use actix_web::HttpResponse;
use actix_web::web::Data;
use uuid::Uuid;

use crate::application::aggregates::{AccessUser, ReplayError};
use crate::application::EventStore;
use crate::application::projections::{GenerationPointOption, GenerationPointProjection};
use crate::extractors::AccessToken;
use crate::model::SelectOption;

pub async fn handle_get_generation_point_options(
    store: Data<EventStore>,
    projection: Data<GenerationPointProjection>,
    access_token: AccessToken,
) -> HttpResponse {
    let consumer = access_token.user;

    match get_generation_point_options(&store, &projection, &consumer).await {
        Ok(options) => {
            let data = options.into_iter().map(|option| SelectOption {
                id: option.id,
                name: option.name,
            }).collect::<Vec<SelectOption>>();
            HttpResponse::Ok().json(data)
        }
        Err(error) => match error {
            Fail::ConsumerReplayError(error) => {
                error!("Unable to get generation point options because of consumer replay, {:?}", error);
                HttpResponse::Unauthorized().finish()
            }
            Fail::GenerationPointProjectionError(error) => {
                error!("Unable to get generation point options because of projection, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
        }
    }
}

pub enum Fail {
    ConsumerReplayError(ReplayError),
    GenerationPointProjectionError(sqlx::Error),
}

pub async fn get_generation_point_options(
    store: &EventStore,
    projection: &GenerationPointProjection,
    consumer: &Uuid,
) -> Result<Vec<GenerationPointOption>, Fail> {
    AccessUser::replay(consumer, store)
        .await
        .map_err(Fail::ConsumerReplayError)?;

    projection.get_generation_point_options()
        .await
        .map_err(Fail::GenerationPointProjectionError)
}