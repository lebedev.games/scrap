use actix_web::HttpResponse;
use actix_web::web::{Data, Query};
use uuid::Uuid;

use crate::application::aggregates::{AccessUser, ensure_access, Permission, ReplayError, UserAccessError};
use crate::application::EventStore;
use crate::application::projections::ReportProjection;
use crate::extractors::AccessToken;
use crate::model::{List, ReportListItem, ReportSearchCriteria};

pub async fn handle_get_reports(
    criteria: Query<ReportSearchCriteria>,
    store: Data<EventStore>,
    projection: Data<ReportProjection>,
    access_token: AccessToken
) -> HttpResponse {
    let consumer = access_token.user;

    match list_reports(&store, &projection, &consumer, &criteria).await {
        Ok(list) => {
            HttpResponse::Ok().json(list)
        }
        Err(error) => match error {
            Fail::ConsumerReplayError(error) => {
                error!("Unable to get reports because of consumer replay, {:?}", error);
                HttpResponse::Unauthorized().finish()
            }
            Fail::ConsumerAccessDenied(error) => {
                warn!("Unable to get reports, consumer access denied, {:?}", error);
                HttpResponse::Forbidden().finish()
            }
            Fail::ReportProjectionError(error) => {
                error!("Unable to get reports because of projection, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
        }
    }
}

pub enum Fail {
    ConsumerReplayError(ReplayError),
    ConsumerAccessDenied(UserAccessError),
    ReportProjectionError(sqlx::Error)
}

pub async fn list_reports(
    store: &EventStore,
    projection: &ReportProjection,
    consumer: &Uuid,
    criteria: &ReportSearchCriteria
) -> Result<List<ReportListItem>, Fail> {
    let user = AccessUser::replay(consumer, store)
        .await
        .map_err(Fail::ConsumerReplayError)?;

    ensure_access(user, Permission::Reports)
        .map_err(Fail::ConsumerAccessDenied)?;

    let limit = match criteria.limit {
        Some(value) => value,
        None => 10
    };

    let offset = match criteria.offset {
        Some(value) => value,
        None => 0
    };

    let fetch = projection.get_reports(
        &criteria.target_month,
        &criteria.target_transporter,
        &criteria.reporting_date,
        &limit,
        &offset,
    );

    let collection = fetch.await.map_err(Fail::ReportProjectionError)?;

    Ok(List {
        items: collection.items.into_iter().map(|report| ReportListItem {
            id: report.id,
            target_transporter: report.target_transporter,
            target_month: report.target_month,
            transportations: report.transportations,
            reporting_date: report.reporting_date
        }).collect(),
        total: collection.total,
        limit,
        offset,
    })
}