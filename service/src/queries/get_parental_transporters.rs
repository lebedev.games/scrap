use actix_web::HttpResponse;

use crate::extractors::AccessToken;
use crate::model::SelectOption;

pub async fn handle_get_parental_transporters(
    _access_token: AccessToken
) -> HttpResponse {
    let parental: Vec<SelectOption> = vec![];
    HttpResponse::Ok().json(parental)
}
