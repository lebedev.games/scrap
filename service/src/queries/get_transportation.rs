use actix_web::HttpResponse;
use actix_web::web::{Data, Path};
use uuid::Uuid;

use crate::application::aggregates::{AccessUser, ensure_access, Permission, ReplayError, TransportationDraft, UserAccessError};
use crate::application::EventStore;
use crate::extractors::AccessToken;
use crate::model::{TransportationDestination, TransportationForm, VersionedTransportationResource, WasteCollection};

pub async fn handle_get_transportation(
    resource: Path<VersionedTransportationResource>,
    store: Data<EventStore>,
    access_token: AccessToken,
) -> HttpResponse {
    let consumer = access_token.user;

    match get_transportation(resource.into_inner(), &store, &consumer).await {
        Ok(transportation) => {
            let data = TransportationForm {
                truck: transportation.truck,
                execution_date: transportation.execution_date,
                destinations: transportation
                    .destinations
                    .iter()
                    .map(|destination| TransportationDestination {
                        id: destination.id,
                        collection_start_time: destination.collection_start_time,
                        waste_generation_point: destination.waste_generation_point,
                        collections: transportation
                            .waste_collections
                            .iter()
                            .filter(|collection| collection.destination == destination.id)
                            .map(|collection| WasteCollection {
                                id: collection.id,
                                destination: collection.destination,
                                container_type: collection.container_type,
                                container_volume: collection.container_volume,
                                container_utilization: collection.container_utilization,
                                waste_volume_m3: collection.waste_volume_m3,
                                photos: collection.photos.clone(),
                            }).collect(),
                    }).collect(),
            };
            HttpResponse::Ok().json(data)
        }
        Err(error) => match error {
            GetTransportationError::ConsumerReplayError(error) => {
                error!("Unable to get transportation because of consumer replay, {:?}", error);
                HttpResponse::Unauthorized().finish()
            }
            GetTransportationError::ConsumerAccessDenied(error) => {
                warn!("Unable to get transportation, consumer access denied, {:?}", error);
                HttpResponse::Forbidden().finish()
            }
            GetTransportationError::TransportationReplayError(error) => {
                error!("Unable to get transportation because of replay, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
        }
    }
}

pub enum GetTransportationError {
    ConsumerReplayError(ReplayError),
    TransportationReplayError(ReplayError),
    ConsumerAccessDenied(UserAccessError),
}

pub async fn get_transportation(
    resource: VersionedTransportationResource,
    store: &EventStore,
    consumer: &Uuid,
) -> Result<TransportationDraft, GetTransportationError> {
    let user = AccessUser::replay(consumer, store)
        .await
        .map_err(GetTransportationError::ConsumerReplayError)?;

    ensure_access(user, Permission::TransportationDraftGet)
        .map_err(GetTransportationError::ConsumerAccessDenied)?;

    let draft = match resource.version {
        Some(version) => {
            TransportationDraft::replay_by_version(&resource.id, version, store)
                .await
                .map_err(GetTransportationError::TransportationReplayError)?
        }
        None => {
            TransportationDraft::replay(&resource.id, store)
                .await
                .map_err(GetTransportationError::TransportationReplayError)?
        }
    };

    Ok(draft)
}


