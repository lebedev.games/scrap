use actix_web::HttpResponse;
use actix_web::web::{Data, Query};
use uuid::Uuid;

use crate::application::aggregates::{AccessUser, ensure_access, Permission, ReplayError, UserAccessError};
use crate::application::EventStore;
use crate::application::projections::{GenerationPointProjection, GenerationPointProjectionError};
use crate::extractors::AccessToken;
use crate::model::{GenerationPointListItemModel, GenerationPointSearchCriteria, List};

pub async fn handle_get_generation_points(
    criteria: Query<GenerationPointSearchCriteria>,
    access_token: AccessToken,
    store: Data<EventStore>,
    projection: Data<GenerationPointProjection>,
) -> HttpResponse {
    let consumer = access_token.user;

    match list_generation_points(&projection, &consumer, &store, &criteria).await {
        Ok(list) => {
            HttpResponse::Ok().json(list)
        }
        Err(GetGenerationPointsError::ConsumerReplayError(error)) => {
            error!("Unable to get generation points because of consumer replay, {:?}", error);
            HttpResponse::InternalServerError().finish()
        }
        Err(GetGenerationPointsError::ConsumerAccessDenied(error)) => {
            warn!("Unable to get generation points, consumer access denied, {:?}", error);
            HttpResponse::Forbidden().finish()
        }
        Err(GetGenerationPointsError::LoadError(error)) => {
            error!("Unable to get generation points because of load error, {:?}", error);
            HttpResponse::InternalServerError().finish()
        }
    }
}

enum GetGenerationPointsError {
    ConsumerReplayError(ReplayError),
    ConsumerAccessDenied(UserAccessError),
    LoadError(GenerationPointProjectionError),
}

async fn list_generation_points(
    projection: &GenerationPointProjection,
    consumer: &Uuid,
    store: &EventStore,
    criteria: &GenerationPointSearchCriteria
) -> Result<List<GenerationPointListItemModel>, GetGenerationPointsError> {
    let user = AccessUser::replay(consumer, store)
        .await
        .map_err(GetGenerationPointsError::ConsumerReplayError)?;

    ensure_access(user, Permission::GenerationPointList)
        .map_err(GetGenerationPointsError::ConsumerAccessDenied)?;

    let limit = match criteria.limit {
        Some(value) => value,
        None => 10
    };

    let offset = match criteria.offset {
        Some(value) => value,
        None => 0
    };

    let fetch = projection.get_generation_points(
        &criteria.internal_system_number,
        &criteria.transporter,
        &criteria.address,
        &limit,
        &offset,
    );

    let collection = fetch.await.map_err(GetGenerationPointsError::LoadError)?;

    Ok(List {
        items: collection.items.into_iter().map(|generation_point| GenerationPointListItemModel {
            id: generation_point.id,
            internal_system_number: generation_point.internal_system_number,
            full_address: generation_point.full_address,
            transporter_name: generation_point.transporter_name,
            area_m2: generation_point.area_m2
        }).collect(),
        total: collection.total,
        limit,
        offset,
    })
}