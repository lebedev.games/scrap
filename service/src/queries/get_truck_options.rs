use crate::extractors::AccessToken;
use actix_web::HttpResponse;
use crate::model::SelectOption;
use uuid::Uuid;
use crate::application::EventStore;
use crate::application::projections::{TruckProjection, TruckOption, TruckProjectionError};
use crate::application::aggregates::{AccessUser, ensure_access, Permission, ReplayError, UserAccessError};
use actix_web::web::Data;

pub async fn handle_get_truck_options(
    access_token: AccessToken,
    store: Data<EventStore>,
    projection: Data<TruckProjection>,
) -> HttpResponse {
    let transporter = access_token.user;

    match get_truck_options(&store, &projection, &transporter).await {
        Ok(trucks) => {
            let data = trucks.into_iter().map(|truck| SelectOption {
                id: truck.id,
                name: truck.registration_number,
            }).collect::<Vec<SelectOption>>();
            HttpResponse::Ok().json(data)
        }
        Err(error) => match error {
            GetTruckOptionsError::TransporterReplayError(error) => {
                error!("Unable to get truck options because of consumer replay, {:?}", error);
                HttpResponse::Unauthorized().finish()
            }
            GetTruckOptionsError::TransporterAccessDenied(error) => {
                warn!("Unable to get truck options, consumer access denied, {:?}", error);
                HttpResponse::Forbidden().finish()
            }
            GetTruckOptionsError::TruckProjectionError(error) => {
                error!("Unable to get truck options because of projection, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
        }
    }
}

pub enum GetTruckOptionsError {
    TransporterReplayError(ReplayError),
    TransporterAccessDenied(UserAccessError),
    TruckProjectionError(TruckProjectionError),
}

pub async fn get_truck_options(
    store: &EventStore,
    projection: &TruckProjection,
    transporter: &Uuid,
) -> Result<Vec<TruckOption>, GetTruckOptionsError> {
    let user = AccessUser::replay(transporter, store)
        .await
        .map_err(GetTruckOptionsError::TransporterReplayError)?;

    ensure_access(user, Permission::TruckList)
        .map_err(GetTruckOptionsError::TransporterAccessDenied)?;

    projection.get_truck_options(transporter)
        .await
        .map_err(GetTruckOptionsError::TruckProjectionError)
}