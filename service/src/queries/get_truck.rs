use actix_web::HttpResponse;
use actix_web::web::{Data, Path};
use uuid::Uuid;

use crate::application::aggregates::{AccessUser, ensure_access, Permission, ReplayError, TruckAggregate, UserAccessError};
use crate::application::EventStore;
use crate::extractors::AccessToken;
use crate::model::{Resource, TruckModel};

pub async fn handle_get_truck(
    resource: Path<Resource>,
    store: Data<EventStore>,
    access_token: AccessToken,
) -> HttpResponse {
    let consumer = access_token.user;

    match get_truck(&resource.id, &store, &consumer).await {
        Ok(truck) => {
            let data = TruckModel {
                owner: truck.owner,
                transporter: truck.transporter,
                fleet_type: truck.fleet_type,
                vin: truck.vin,
                registration_number: truck.registration_number,
                garaging_number: truck.garaging_number,
                vehicle_model: truck.vehicle_model,
                waste_equipment_model: truck.waste_equipment_model,
                cargo_body_type: truck.cargo_body_type,
                required_driver_license_category: truck.required_driver_license_category,
                axle_configuration: truck.axle_configuration,
                production_year: truck.production_year,
                navigation_equipment_name: truck.navigation_equipment_name,
                empty_weight_kg: truck.empty_weight_kg,
                cargo_capacity_kg: truck.cargo_capacity_kg,
            };

            HttpResponse::Ok().json(data)
        }
        Err(error) => match error {
            GetTruckError::ConsumerReplayError(error) => {
                error!("Unable to get truck because of consumer replay, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
            GetTruckError::ConsumerAccessDenied(_) => {
                warn!("Unable to get truck, consumer access denied");
                HttpResponse::Forbidden().finish()
            }
            GetTruckError::TruckReplayError(error) => {
                match error {
                    ReplayError::EntityNotFound(id) => {
                        error!("Truck not found, {:?}", id);
                        HttpResponse::NotFound().finish()
                    }
                    ReplayError::ParsingError(_) => {
                        error!("Unable to truck because of replay, {:?}", error);
                        HttpResponse::InternalServerError().finish()
                    }
                }
            }
        }
    }
}

pub enum GetTruckError {
    ConsumerReplayError(ReplayError),
    TruckReplayError(ReplayError),
    ConsumerAccessDenied(UserAccessError),
}

pub async fn get_truck(
    id: &Uuid, store: &EventStore, consumer: &Uuid,
) -> Result<TruckAggregate, GetTruckError> {
    let user = AccessUser::replay(consumer, store)
        .await
        .map_err(GetTruckError::ConsumerReplayError)?;

    ensure_access(user, Permission::GenerationPointGet)
        .map_err(GetTruckError::ConsumerAccessDenied)?;

    let aggregate = TruckAggregate::replay(id, store)
        .await
        .map_err(GetTruckError::TruckReplayError)?;

    Ok(aggregate)
}