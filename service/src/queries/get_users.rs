use actix_web::HttpResponse;
use actix_web::web::Data;

use crate::application::aggregates::{AccessUser, ensure_access, Permission, ReplayError, UserAccessError};
use crate::application::EventStore;
use crate::application::projections::UserProjection;
use crate::extractors::AccessToken;

pub async fn handle_get_users(
    projection: Data<UserProjection>,
    store: Data<EventStore>,
    access_token: AccessToken
) -> HttpResponse {
    let consumer = access_token.user;

    let result = match AccessUser::replay(&consumer, &store).await {
        Ok(consumer) => {
            match ensure_access(consumer, Permission::UserList) {
                Ok(_) => match projection.get_users().await {
                    Ok(users) => Ok(users),
                    Err(error) => Err(Fail::ProjectionError(error))
                },
                Err(error) => Err(Fail::ActionForbidden(error))
            }
        }
        Err(error) => Err(Fail::ConsumerReplayError(error))
    };

    match result {
        Ok(users) => HttpResponse::Ok().json(users),
        Err(error) => return match error {
            Fail::ConsumerReplayError(error) => {
                error!("Unable to list users because of consumer replay, {:?}", error);
                HttpResponse::UnprocessableEntity().finish()
            }
            Fail::ActionForbidden(error) => {
                info!("Unable to list users, permission denied, {:?}", error);
                HttpResponse::Forbidden().finish()
            }
            Fail::ProjectionError(error) => {
                error!("Unable to list users, {}", error);
                HttpResponse::InternalServerError().finish()
            }
        }
    }
}

enum Fail {
    ConsumerReplayError(ReplayError),
    ActionForbidden(UserAccessError),
    ProjectionError(sqlx::Error),
}