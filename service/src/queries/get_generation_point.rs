use actix_web::HttpResponse;
use actix_web::web::{Data, Path};
use uuid::Uuid;

use crate::application::aggregates::{AccessUser, GenerationPointAggregate, ReplayError, ensure_access, Permission, UserAccessError};
use crate::application::EventStore;
use crate::model::{GenerationPointModel, GenerationPointModelContainer, GenerationPointModelTransporter, Resource};
use crate::extractors::AccessToken;

pub async fn handle_get_generation_point(
    resource: Path<Resource>,
    store: Data<EventStore>,
    access_token: AccessToken
) -> HttpResponse {
    let consumer = access_token.user;

    match get_generation_point(&resource.id, &store, &consumer).await {
        Ok(generation_point) => {
            let data = GenerationPointModel {
                id: generation_point.id,
                internal_system_number: generation_point.internal_system_number,
                external_registry_number: generation_point.external_registry_number,
                waste_type: generation_point.waste_type,
                organization_legal_type: generation_point.organization_legal_type,
                operational_status: generation_point.operational_status,
                full_address: generation_point.full_address,
                coordinates: generation_point.coordinates,
                area_m2: generation_point.area_m2,
                covering_type: generation_point.covering_type,
                fence_material: generation_point.fence_material,
                service_company_name: generation_point.service_company_name,
                generator: generation_point.generator,
                description: generation_point.description,
                containers: generation_point.containers.into_iter().map(
                    |container| GenerationPointModelContainer {
                        container_type: container.container_type
                    }
                ).collect(),
                transporter: GenerationPointModelTransporter {
                    transporter: generation_point.transporter.transporter,
                    start_transportation_date: generation_point.transporter.start_transportation_date,
                    end_transportation_date: generation_point.transporter.end_transportation_date,
                },
                photos: generation_point.photos,
                accrual_method: generation_point.accrual_method,
                measurement_unit: generation_point.measurement_unit,
                transportation_frequency: generation_point.transportation_frequency,
            };
            HttpResponse::Ok().json(data)
        }
        Err(error) => match error {
            GetGenerationPointError::ConsumerReplayError(error) => {
                error!("Unable to get generation point because of consumer replay, {:?}", error);
                HttpResponse::Unauthorized().finish()
            }
            GetGenerationPointError::ConsumerAccessDenied(error) => {
                warn!("Unable to get generation point, consumer access denied, {:?}", error);
                HttpResponse::Forbidden().finish()
            }
            GetGenerationPointError::GenerationPointReplayError(error) => {
                match error {
                    ReplayError::EntityNotFound(id) => {
                        error!("Generation point not found, {:?}", id);
                        HttpResponse::NotFound().finish()
                    }
                    _ => {
                        error!("Unable to get generation point because of replay, {:?}", error);
                        HttpResponse::InternalServerError().finish()
                    }
                }
            }
        }
    }
}

pub enum GetGenerationPointError {
    ConsumerReplayError(ReplayError),
    GenerationPointReplayError(ReplayError),
    ConsumerAccessDenied(UserAccessError),
}

pub async fn get_generation_point(
    id: &Uuid, store: &EventStore, consumer: &Uuid,
) -> Result<GenerationPointAggregate, GetGenerationPointError> {
    let user = AccessUser::replay(consumer, store)
        .await
        .map_err(GetGenerationPointError::ConsumerReplayError)?;

    ensure_access(user, Permission::GenerationPointGet)
        .map_err(GetGenerationPointError::ConsumerAccessDenied)?;

    let aggregate = GenerationPointAggregate::replay(id, store)
        .await
        .map_err(GetGenerationPointError::GenerationPointReplayError)?;

    Ok(aggregate)
}
