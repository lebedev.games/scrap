use crate::application::aggregates::{AccessUser, ensure_access, Permission, UserAccessError, ReplayError, NegotiationStage, Issue};
use uuid::Uuid;
use crate::application::EventStore;
use actix_web::HttpResponse;
use crate::extractors::AccessToken;
use crate::model::{Resource, NegotiationModel, TimelinePoint, IssuesDeclaration, IssueModel};
use actix_web::web::{Path, Data};
use crate::application::projections::NegotiationProjection;

pub async fn handle_get_negotiation(
    resource: Path<Resource>,
    projection: Data<NegotiationProjection>,
    store: Data<EventStore>,
    access_token: AccessToken
) -> HttpResponse {
    let consumer = access_token.user;
    let negotiation = resource.id;

    match get_negotiation(&negotiation, &store, &projection, &consumer).await {
        Ok(negotiation) => HttpResponse::Ok().json(negotiation),
        Err(error) => match error {
            Fail::ConsumerReplayError(error) => {
                error!("Unable to get negotiation because of consumer replay, {:?}", error);
                HttpResponse::Unauthorized().finish()
            }
            Fail::ConsumerAccessDenied(error) => {
                warn!("Unable to get negotiation, consumer access denied, {:?}", error);
                HttpResponse::Forbidden().finish()
            }
            Fail::NegotiationReplayError(error) => {
                error!("Unable to get negotiation, replay error, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
            Fail::NegotiationTimelineProjectionError(error) => {
                error!("Unable to get negotiation, timeline projection error, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
        }
    }
}


enum Fail {
    ConsumerReplayError(ReplayError),
    ConsumerAccessDenied(UserAccessError),
    NegotiationReplayError(ReplayError),
    NegotiationTimelineProjectionError(sqlx::Error)
}

async fn get_negotiation(
    negotiation: &Uuid,
    store: &EventStore,
    projection: &NegotiationProjection,
    consumer: &Uuid
) -> Result<NegotiationModel, Fail> {
    let user = AccessUser::replay(consumer, store)
        .await
        .map_err(Fail::ConsumerReplayError)?;

    ensure_access(user, Permission::NegotiationGet)
        .map_err(Fail::ConsumerAccessDenied)?;

    let negotiation = NegotiationStage::replay(negotiation, store)
        .await
        .map_err(Fail::NegotiationReplayError)?;

    let timeline = projection.get_negotiation_timeline(&negotiation.id)
        .await
        .map_err(Fail::NegotiationTimelineProjectionError)?;

    let negotiation = NegotiationModel {
        id: negotiation.id,
        transportation: negotiation.transportation,
        timeline: timeline.into_iter().map(|point| TimelinePoint {
            stage_id: point.stage_id,
            negotiation: point.negotiation,
            transportation_version: point.transportation_version,
            timestamp: point.timestamp,
            status: point.status
        }).collect(),
        declarations: negotiation.declarations.into_iter().map(|declaration| IssuesDeclaration {
            date: declaration.date,
            issues: declaration.issues.into_iter().map(|issue| match issue {
                Issue::WasteVolumeIssue { id, destination, collection, comment } => {
                    IssueModel::WasteVolumeIssue {
                        id,
                        destination,
                        collection,
                        comment
                    }
                }
                Issue::CollectionStartTimeIssue { id, destination, comment } => {
                    IssueModel::CollectionStartTimeIssue {
                        id,
                        destination,
                        comment
                    }
                }
                Issue::CollectionPhotoIssue { id, destination, collection, comment } => {
                    IssueModel::CollectionPhotoIssue {
                        id,
                        destination,
                        collection,
                        comment
                    }
                }
            }).collect()
        }).collect()
    };



    Ok(negotiation)
}
