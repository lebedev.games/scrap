use actix_web::HttpResponse;
use actix_web::web::Data;

use crate::application::dictionary::Dictionary;
use crate::extractors::AccessToken;

pub async fn handle_get_accrual_method_options(
    _access_token: AccessToken,
    dictionary: Data<Dictionary>,
) -> HttpResponse {
    let options = dictionary.get_accrual_method_records().await;

    HttpResponse::Ok().json(options)
}

pub async fn handle_get_container_type_options(
    _access_token: AccessToken,
    dictionary: Data<Dictionary>,
) -> HttpResponse {
    let options = dictionary.get_container_type_records().await;

    HttpResponse::Ok().json(options)
}

pub async fn handle_get_covering_type_options(
    _access_token: AccessToken,
    dictionary: Data<Dictionary>,
) -> HttpResponse {
    let options = dictionary.get_covering_type_records().await;

    HttpResponse::Ok().json(options)
}

pub async fn handle_get_fence_material_options(
    _access_token: AccessToken,
    dictionary: Data<Dictionary>,
) -> HttpResponse {
    let options = dictionary.get_fence_material_records().await;

    HttpResponse::Ok().json(options)
}


pub async fn handle_get_measurement_unit_options(
    _access_token: AccessToken,
    dictionary: Data<Dictionary>,
) -> HttpResponse {
    let options = dictionary.get_measurement_unit_records().await;

    HttpResponse::Ok().json(options)
}


pub async fn handle_get_operational_status_options(
    _access_token: AccessToken,
    dictionary: Data<Dictionary>,
) -> HttpResponse {
    let options = dictionary.get_operational_status_records().await;

    HttpResponse::Ok().json(options)
}

pub async fn handle_get_organization_legal_type_options(
    _access_token: AccessToken,
    dictionary: Data<Dictionary>,
) -> HttpResponse {
    let options = dictionary.get_organization_legal_type_records().await;

    HttpResponse::Ok().json(options)
}


pub async fn handle_get_waste_type_options(
    _access_token: AccessToken,
    dictionary: Data<Dictionary>,
) -> HttpResponse {
    let options = dictionary.get_waste_type_records().await;

    HttpResponse::Ok().json(options)
}

pub async fn handle_get_truck_fleet_type_options(
    _access_token: AccessToken,
    dictionary: Data<Dictionary>,
) -> HttpResponse {
    let options = dictionary.get_truck_fleet_type_records().await;

    HttpResponse::Ok().json(options)
}

pub async fn handle_get_cargo_body_type_options(
    _access_token: AccessToken,
    dictionary: Data<Dictionary>,
) -> HttpResponse {
    let options = dictionary.get_cargo_body_type_records().await;

    HttpResponse::Ok().json(options)
}

pub async fn handle_get_driver_license_category_options(
    _access_token: AccessToken,
    dictionary: Data<Dictionary>,
) -> HttpResponse {
    let options = dictionary.get_driver_license_category_records().await;

    HttpResponse::Ok().json(options)
}

pub async fn handle_get_axle_configuration_options(
    _access_token: AccessToken,
    dictionary: Data<Dictionary>,
) -> HttpResponse {
    let options = dictionary.get_axle_configuration_records().await;

    HttpResponse::Ok().json(options)
}
