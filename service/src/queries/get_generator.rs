use actix_web::HttpResponse;
use actix_web::web::{Data, Path};
use uuid::Uuid;

use crate::application::aggregates::{AccessUser, ensure_access, Permission, ReplayError, UserAccessError};
use crate::application::EventStore;
use crate::domain::{BusinessEntity, Entrepreneur, Generator};
use crate::extractors::AccessToken;
use crate::model::{BankDetails, BusinessEntityModel, EntrepreneurModel, GeneratorLegalEntity, GeneratorModel, ResponsiblePerson, Resource};

pub async fn handle_get_generator(
    resource: Path<Resource>,
    store: Data<EventStore>,
    access_token: AccessToken
) -> HttpResponse {
    let generator = resource.id;
    let consumer = access_token.user;

    let result = match AccessUser::replay(&consumer, &store).await {
        Ok(consumer) => {
            match ensure_access(consumer, Permission::GeneratorGet) {
                Ok(_) => match store.replay(generator, apply_generator).await {
                    Ok(generator) => match generator.legal_entity {
                        LegalEntityState::Unassigned => Err(Fail::LegalEntityUnassigned),
                        LegalEntityState::BusinessEntity(business_entity) => {
                            match store.load(business_entity).await {
                                Ok(records) => {
                                    let mut business_entity = BusinessEntityModel::default();
                                    for record in records {
                                        business_entity = apply_business_entity(business_entity, record.event);
                                    }
                                    Ok(GeneratorModel {
                                        legal_entity: GeneratorLegalEntity::BusinessEntity(business_entity),
                                        parent: generator.parent,
                                    })
                                }
                                Err(error) => Err(Fail::BusinessEntityLoadError(error))
                            }
                        }
                        LegalEntityState::Entrepreneur(entrepreneur) => {
                            match store.load(entrepreneur).await {
                                Ok(records) => {
                                    let mut entrepreneur = EntrepreneurModel::default();
                                    for record in records {
                                        entrepreneur = apply_entrepreneur(entrepreneur, record.event);
                                    }
                                    Ok(GeneratorModel {
                                        legal_entity: GeneratorLegalEntity::Entrepreneur(entrepreneur),
                                        parent: generator.parent,
                                    })
                                }
                                Err(error) => Err(Fail::EntrepreneurLoadError(error))
                            }
                        }
                    },
                    Err(error) => Err(Fail::GeneratorReplayError(error))
                },
                Err(error) => Err(Fail::ConsumerAccessDenied(error))
            }
        }
        Err(error) => Err(Fail::ConsumerReplayError(error))
    };

    match result {
        Ok(generator) => {
            HttpResponse::Ok().json(generator)
        }
        Err(error) => match error {
            Fail::ConsumerReplayError(error) => {
                error!("Unable to get generator because of consumer replay, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
            Fail::ConsumerAccessDenied(error) => {
                warn!("Unable to get generator, consumer access denied, {:?}", error);
                HttpResponse::Forbidden().finish()
            }
            Fail::GeneratorReplayError(error) => {
                error!("Unable to get generator because of generator replay, {}", error);
                HttpResponse::InternalServerError().finish()
            }
            Fail::LegalEntityUnassigned => {
                error!("Unable to get generator, legal entity unassigned");
                HttpResponse::Conflict().finish()
            }
            Fail::BusinessEntityLoadError(error) => {
                error!("Unable to get generator because of business entity load, {}", error);
                HttpResponse::InternalServerError().finish()
            }
            Fail::EntrepreneurLoadError(error) => {
                error!("Unable to get generator because of entrepreneur load, {}", error);
                HttpResponse::InternalServerError().finish()
            }
        }
    }
}

enum Fail {
    ConsumerReplayError(ReplayError),
    GeneratorReplayError(sqlx::Error),
    LegalEntityUnassigned,
    ConsumerAccessDenied(UserAccessError),
    BusinessEntityLoadError(sqlx::Error),
    EntrepreneurLoadError(sqlx::Error),
}


pub fn apply_business_entity(model: BusinessEntityModel, event: BusinessEntity) -> BusinessEntityModel {
    match event {
        BusinessEntity::Registered { .. } => model,
        BusinessEntity::AdministrativeNameUpdated { full_name, short_name, .. } => BusinessEntityModel {
            short_name,
            full_name,
            ..model
        },
        BusinessEntity::TaxIdentificationUpdated { inn, .. } => BusinessEntityModel {
            inn,
            ..model
        },
        BusinessEntity::TaxRegistrationUpdated { kpp, .. } => BusinessEntityModel {
            kpp,
            ..model
        },
        BusinessEntity::PrimaryRegistrationUpdated { ogrn, .. } => BusinessEntityModel {
            ogrn,
            ..model
        },
        BusinessEntity::PhoneContactUpdated { phone, .. } => BusinessEntityModel {
            phone,
            ..model
        },
        BusinessEntity::EmailContactUpdated { email, .. } => BusinessEntityModel {
            email: Some(email),
            ..model
        },
        BusinessEntity::AddressUpdated { full_address, .. } => BusinessEntityModel {
            address: full_address,
            ..model
        },
        BusinessEntity::PostalAddressUpdated { full_address, .. } => BusinessEntityModel {
            postal_address: full_address,
            ..model
        },
        BusinessEntity::ActualAddressUpdated { full_address, .. } => BusinessEntityModel {
            actual_address: full_address,
            ..model
        },
        BusinessEntity::ResponsiblePersonAdded { full_name, phone, .. } => BusinessEntityModel {
            responsible_persons: extend(model.responsible_persons, ResponsiblePerson { full_name, phone }),
            ..model
        },
        BusinessEntity::BankDetailsUpdated { account, correspondent_account, bank_name, rcbic, .. } => BusinessEntityModel {
            bank_details: BankDetails {
                account,
                correspondent_account,
                bank_name,
                rcbic,
            },
            ..model
        }
    }
}

pub fn apply_entrepreneur(model: EntrepreneurModel, event: Entrepreneur) -> EntrepreneurModel {
    match event {
        Entrepreneur::Registered { .. } => model,
        Entrepreneur::PersonalNameUpdated { first_name, last_name, patronymic, .. } => EntrepreneurModel {
            first_name,
            last_name,
            patronymic,
            ..model
        },
        Entrepreneur::TaxIdentificationUpdated { inn, .. } => EntrepreneurModel {
            inn,
            ..model
        },
        Entrepreneur::PrimaryRegistrationUpdated { ogrnip, .. } => EntrepreneurModel {
            ogrnip,
            ..model
        },
        Entrepreneur::PhoneContactUpdated { phone, .. } => EntrepreneurModel {
            phone,
            ..model
        },
        Entrepreneur::EmailContactUpdated { email, .. } => EntrepreneurModel {
            email: Some(email),
            ..model
        },
        Entrepreneur::AddressUpdated { full_address, .. } => EntrepreneurModel {
            address: full_address,
            ..model
        },
        Entrepreneur::ActualAddressUpdated { full_address, .. } => EntrepreneurModel {
            actual_address: full_address,
            ..model
        },
        Entrepreneur::BankDetailsUpdated { account, correspondent_account, bank_name, rcbic, .. } => EntrepreneurModel {
            bank_details: BankDetails {
                account,
                correspondent_account,
                bank_name,
                rcbic,
            },
            ..model
        }
    }
}


pub enum LegalEntityState {
    Unassigned,
    BusinessEntity(Uuid),
    Entrepreneur(Uuid),
}

impl Default for LegalEntityState {
    fn default() -> Self {
        Self::Unassigned
    }
}

#[derive(Default)]
pub struct GeneratorState {
    pub parent: Option<Uuid>,
    pub legal_entity: LegalEntityState,
}

pub fn apply_generator(generator: GeneratorState, event: Generator) -> GeneratorState {
    match event {
        Generator::Registered { .. } => generator,
        Generator::EntrepreneurAssigned { entrepreneur, .. } => GeneratorState {
            legal_entity: LegalEntityState::Entrepreneur(entrepreneur),
            ..generator
        },
        Generator::BusinessEntityAssigned { business_entity, .. } => GeneratorState {
            legal_entity: LegalEntityState::BusinessEntity(business_entity),
            ..generator
        },
        Generator::ParentUpdated { parent, .. } => GeneratorState {
            parent: Some(parent),
            ..generator
        }
    }
}

fn extend<T: Clone>(vec: Vec<T>, value: T) -> Vec<T> {
    let mut values = vec.clone();
    values.push(value);
    values
}