use actix_web::HttpResponse;
use actix_web::web::{Data, Path};
use uuid::Uuid;

use crate::application::aggregates::{AccessUser, BusinessEntityAggregate, ensure_access, EntrepreneurAggregate, Permission, ReplayError, TransporterAggregate, TransporterLegalEntity, UserAccessError};
use crate::application::EventStore;
use crate::model::{BankDetails, BusinessEntityModel, EntrepreneurModel, Resource, TransporterLegalEntityModel, TransporterModel};
use crate::extractors::AccessToken;

pub async fn handle_get_transporter(
    resource: Path<Resource>,
    store: Data<EventStore>,
    access_token: AccessToken
) -> HttpResponse {
    let consumer = access_token.user;

    match get_transporter(&resource.id, &store, &consumer).await {
        Ok(transporter) => {
            let (transporter, legal_entity) = match transporter {
                Transporter::Entrepreneur(transporter_aggregate, entrepreneur_aggregate) => {
                    let legal_entity = TransporterLegalEntityModel::Entrepreneur(
                        EntrepreneurModel {
                            first_name: entrepreneur_aggregate.first_name,
                            last_name: entrepreneur_aggregate.last_name,
                            patronymic: entrepreneur_aggregate.patronymic,
                            inn: entrepreneur_aggregate.inn,
                            ogrnip: entrepreneur_aggregate.ogrnip,
                            phone: entrepreneur_aggregate.phone,
                            email: entrepreneur_aggregate.email,
                            address: entrepreneur_aggregate.address,
                            actual_address: entrepreneur_aggregate.actual_address,
                            bank_details: BankDetails {
                                account: entrepreneur_aggregate.bank_details.account,
                                correspondent_account: entrepreneur_aggregate.bank_details.correspondent_account,
                                bank_name: entrepreneur_aggregate.bank_details.bank_name,
                                rcbic: entrepreneur_aggregate.bank_details.rcbic,
                            },
                        }
                    );
                    (transporter_aggregate, legal_entity)
                }
                Transporter::BusinessEntity(transporter_aggregate, business_entity_aggregate) => {
                    let legal_entity = TransporterLegalEntityModel::BusinessEntity(
                        BusinessEntityModel {
                            full_name: business_entity_aggregate.full_name,
                            short_name: business_entity_aggregate.short_name,
                            inn: business_entity_aggregate.inn,
                            kpp: business_entity_aggregate.kpp,
                            ogrn: business_entity_aggregate.ogrn,
                            phone: business_entity_aggregate.phone,
                            email: business_entity_aggregate.email,
                            address: business_entity_aggregate.address,
                            postal_address: business_entity_aggregate.postal_address,
                            actual_address: business_entity_aggregate.actual_address,
                            responsible_persons: vec![],
                            bank_details: BankDetails {
                                account: business_entity_aggregate.bank_details.account,
                                correspondent_account: business_entity_aggregate.bank_details.correspondent_account,
                                bank_name: business_entity_aggregate.bank_details.bank_name,
                                rcbic: business_entity_aggregate.bank_details.rcbic,
                            },
                        }
                    );
                    (transporter_aggregate, legal_entity)
                }
            };
            let data = TransporterModel {
                legal_entity,
                fkko: transporter.fkko,
                parent: transporter.parent,
                truck_fleet_types: transporter.truck_fleet_types,
                trucks: transporter.trucks,
                target_generation_points: transporter.target_generation_points,
                note: transporter.note,
            };
            HttpResponse::Ok().json(data)
        }
        Err(error) => match error {
            GetTransporterError::ConsumerReplayError(error) => {
                error!("Unable to get transporter because of consumer replay, {:?}", error);
                HttpResponse::Unauthorized().finish()
            }
            GetTransporterError::ConsumerAccessDenied(_) => {
                warn!("Unable to get transporter, consumer access denied");
                HttpResponse::Forbidden().finish()
            }
            GetTransporterError::TransporterLegalEntityUnassigned => {
                error!("Unable to get transporter, legal entity unassigned");
                return HttpResponse::Conflict().finish();
            },
            GetTransporterError::TransporterReplayError(error) => {
                error!("Unable to get transporter because of replay, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
            GetTransporterError::EntrepreneurReplayError(error) => {
                error!("Unable to get transporter because of entrepreneur replay, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
            GetTransporterError::BusinessEntityReplayError(error) => {
                error!("Unable to get transporter because of business entity replay, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
        }
    }
}

enum Transporter {
    BusinessEntity(TransporterAggregate, BusinessEntityAggregate),
    Entrepreneur(TransporterAggregate, EntrepreneurAggregate),
}

pub enum GetTransporterError {
    ConsumerReplayError(ReplayError),
    TransporterReplayError(ReplayError),
    ConsumerAccessDenied(UserAccessError),
    BusinessEntityReplayError(ReplayError),
    EntrepreneurReplayError(ReplayError),
    TransporterLegalEntityUnassigned
}

async fn get_transporter(
    id: &Uuid,
    store: &EventStore,
    consumer: &Uuid,
) -> Result<Transporter, GetTransporterError> {
    let user = AccessUser::replay(consumer, store)
        .await
        .map_err(GetTransporterError::ConsumerReplayError)?;

    ensure_access(user, Permission::TransporterGet)
        .map_err(GetTransporterError::ConsumerAccessDenied)?;

    let transporter = TransporterAggregate::replay(id, store)
        .await
        .map_err(GetTransporterError::TransporterReplayError)?;

    match &transporter.legal_entity {
        TransporterLegalEntity::Unassigned => Err(GetTransporterError::TransporterLegalEntityUnassigned),
        TransporterLegalEntity::BusinessEntity(id) => {
            let business_entity = BusinessEntityAggregate::replay(&id, &store).await
                .map_err(GetTransporterError::BusinessEntityReplayError)?;
            Ok(Transporter::BusinessEntity(transporter, business_entity))
        }
        TransporterLegalEntity::Entrepreneur(id) => {
            let entrepreneur = EntrepreneurAggregate::replay(&id, &store).await
                .map_err(GetTransporterError::EntrepreneurReplayError)?;
            Ok(Transporter::Entrepreneur(transporter, entrepreneur))
        }
    }
}
