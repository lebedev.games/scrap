use actix_web::HttpResponse;
use actix_web::web::{Data, Query};
use uuid::Uuid;

use crate::application::aggregates::{AccessUser, ensure_access, Permission, ReplayError, UserAccessError};
use crate::application::EventStore;
use crate::application::projections::{TransporterProjection};
use crate::extractors::AccessToken;
use crate::model::{TransporterListItemModel, TransporterSearchCriteria, List};

pub async fn handle_get_transporters(
    criteria: Query<TransporterSearchCriteria>,
    store: Data<EventStore>,
    projection: Data<TransporterProjection>,
    access_token: AccessToken,
) -> HttpResponse {
    let consumer = access_token.user;

    match list_transporters(&store, &projection, &consumer, &criteria).await {
        Ok(list) => {
            HttpResponse::Ok().json(list)
        }
        Err(error) => match error {
            GetTransportersError::ConsumerReplayError(error) => {
                error!("Unable to get transporters because of consumer replay, {:?}", error);
                HttpResponse::Unauthorized().finish()
            }
            GetTransportersError::ConsumerAccessDenied(error) => {
                warn!("Unable to get transporters, consumer access denied, {:?}", error);
                HttpResponse::Forbidden().finish()
            }
            GetTransportersError::TransporterProjectionError(error) => {
                error!("Unable to get transporters because of projection, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
        }
    }
}

pub enum GetTransportersError {
    ConsumerReplayError(ReplayError),
    ConsumerAccessDenied(UserAccessError),
    TransporterProjectionError(sqlx::Error),
}

pub async fn list_transporters(
    store: &EventStore,
    projection: &TransporterProjection,
    consumer: &Uuid,
    criteria: &TransporterSearchCriteria,
) -> Result<List<TransporterListItemModel>, GetTransportersError> {
    let user = AccessUser::replay(consumer, store)
        .await
        .map_err(GetTransportersError::ConsumerReplayError)?;

    ensure_access(user, Permission::TransporterList)
        .map_err(GetTransportersError::ConsumerAccessDenied)?;

    let limit = match criteria.limit {
        Some(value) => value,
        None => 10
    };

    let offset = match criteria.offset {
        Some(value) => value,
        None => 0
    };

    let fetch = projection.get_transporters(
        &criteria.inn,
        &criteria.name,
        &criteria.truck_fleet_type,
        &criteria.legal_entity_type,
        &limit,
        &offset,
    );

    let collection = fetch.await.map_err(GetTransportersError::TransporterProjectionError)?;

    Ok(List {
        items: collection.items.into_iter().map(|transporter| TransporterListItemModel {
            id: transporter.id,
            legal_entity_type: transporter.legal_entity_type,
            inn: transporter.inn,
            name: transporter.name,
            truck_fleet_types: transporter.truck_fleet_types,
            districts: transporter.districts,
        }).collect(),
        total: collection.total,
        limit,
        offset,
    })
}