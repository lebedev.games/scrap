use actix_web::HttpResponse;
use actix_web::web::{Data, Query};

use crate::application::aggregates::{AccessUser, ensure_access, Permission, ReplayError, UserAccessError};
use crate::application::EventStore;
use crate::application::projections::GeneratorProjection;
use crate::extractors::AccessToken;
use crate::model::{Affiliate, GeneratorListItem, GeneratorSearchCriteria, List};

pub async fn handle_get_generators(
    criteria: Query<GeneratorSearchCriteria>,
    projection: Data<GeneratorProjection>,
    store: Data<EventStore>,
    access_token: AccessToken
) -> HttpResponse {
    let consumer = access_token.user;

    let limit = match criteria.limit {
        Some(value) => value,
        None => 10
    };

    let offset = match criteria.offset {
        Some(value) => value,
        None => 0
    };

    let result = match AccessUser::replay(&consumer, &store).await {
        Ok(consumer) => {
            match ensure_access(consumer, Permission::GeneratorList) {
                Ok(_) => match projection.get_generators(&criteria.inn, &criteria.name, &criteria.legal_entity_type, &limit, &offset).await {
                    Ok(users) => Ok(users),
                    Err(error) => Err(Fail::ProjectionError(error))
                },
                Err(error) => Err(Fail::ActionForbidden(error))
            }
        }
        Err(error) => Err(Fail::ConsumerReplayError(error))
    };

    match result {
        Ok(collection) => {
            let list = List {
                items: collection.items.into_iter().map(|generator| GeneratorListItem {
                    id: generator.id,
                    name: generator.name,
                    inn: generator.inn,
                    legal_entity_type: generator.legal_entity_type,
                    affiliates: generator.affiliates.into_iter().map(|affiliate| Affiliate {
                        id: affiliate.id,
                        name: affiliate.name,
                        inn: affiliate.inn,
                        legal_entity_type: affiliate.legal_entity_type
                    }).collect()
                }).collect(),
                total: collection.total,
                limit,
                offset,
            };

            HttpResponse::Ok().json(list)
        },
        Err(error) => return match error {
            Fail::ConsumerReplayError(error) => {
                error!("Unable to list generators because of consumer replay, {:?}", error);
                HttpResponse::UnprocessableEntity().finish()
            },
            Fail::ActionForbidden(error) => {
                info!("Unable to list generators, permission denied, {:?}", error);
                HttpResponse::Forbidden().finish()
            },
            Fail::ProjectionError(error) => {
                error!("Unable to list generators, {}", error);
                HttpResponse::InternalServerError().finish()
            }
        }
    }
}

enum Fail {
    ConsumerReplayError(ReplayError),
    ActionForbidden(UserAccessError),
    ProjectionError(sqlx::Error)
}