use actix_web::HttpResponse;
use actix_web::web::Data;
use uuid::Uuid;

use crate::application::aggregates::{AccessUser, ensure_access, Permission, ReplayError, UserAccessError};
use crate::application::EventStore;
use crate::application::projections::{TransportationListItem, TransportationProjection};
use crate::extractors::AccessToken;
use crate::model::TransportationListItemModel;

pub async fn handle_get_transportations(
    store: Data<EventStore>,
    projection: Data<TransportationProjection>,
    access_token: AccessToken
) -> HttpResponse {
    let consumer = access_token.user;

    match get_transportations(&store, &projection, &consumer).await {
        Ok(transportations) => {
            let data = transportations.into_iter().map(|transportation| TransportationListItemModel {
                id: transportation.id,
                district: transportation.district,
                execution_date: transportation.execution_date,
                truck_registration_number: transportation.truck_registration_number,
                status: transportation.status,
            }).collect::<Vec<TransportationListItemModel>>();
            HttpResponse::Ok().json(data)
        }
        Err(error) => match error {
            GetTransportationsError::ConsumerReplayError(error) => {
                error!("Unable to get transportations because of consumer replay, {:?}", error);
                HttpResponse::Unauthorized().finish()
            }
            GetTransportationsError::ConsumerAccessDenied(error) => {
                warn!("Unable to get transportations, consumer access denied, {:?}", error);
                HttpResponse::Forbidden().finish()
            }
            GetTransportationsError::TransportationProjectionError(error) => {
                error!("Unable to get transportations because of projection, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
        }
    }
}


pub enum GetTransportationsError {
    ConsumerReplayError(ReplayError),
    ConsumerAccessDenied(UserAccessError),
    TransportationProjectionError(sqlx::Error),
}

pub async fn get_transportations(
    store: &EventStore,
    projection: &TransportationProjection,
    consumer: &Uuid,
) -> Result<Vec<TransportationListItem>, GetTransportationsError> {
    let user = AccessUser::replay(consumer, store)
        .await
        .map_err(GetTransportationsError::ConsumerReplayError)?;

    let transporter = match &user.role_entity {
        Some(entity_id) => entity_id.clone(),
        None => return Err(GetTransportationsError::ConsumerAccessDenied(UserAccessError::RoleEntityUnassigned))
    };

    ensure_access(user, Permission::TransportationDraftList)
        .map_err(GetTransportationsError::ConsumerAccessDenied)?;

    projection.get_transportations(&transporter)
        .await
        .map_err(GetTransportationsError::TransportationProjectionError)
}