use actix_web::HttpResponse;
use actix_web::web::Data;
use uuid::Uuid;

use crate::application::aggregates::{AccessUser, ensure_access, Permission, ReplayError, UserAccessError};
use crate::application::EventStore;
use crate::application::projections::{GenerationPointProjection, GenerationPointProjectionError, TargetGenerationPointListItem};
use crate::extractors::AccessToken;
use crate::model::{TargetContainerType, TargetGenerationPoint};

pub async fn handle_get_target_generation_points(
    access_token: AccessToken,
    store: Data<EventStore>,
    projection: Data<GenerationPointProjection>,
) -> HttpResponse {
    let transporter = access_token.user;

    match get_target_generation_points(&transporter, &projection, &store).await {
        Ok(inner_generation_points) => {
            let data: Vec<TargetGenerationPoint> = inner_generation_points.into_iter().map(
                |generation_point| TargetGenerationPoint {
                    id: generation_point.id,
                    address: generation_point.address,
                    container_types: generation_point.container_types.into_iter().map(
                        |container_type| TargetContainerType {
                            id: container_type.id,
                            name: container_type.name,
                            volume_limit_m3: container_type.volume_limit_m3
                        }
                    ).collect()
                }
            ).collect();
            HttpResponse::Ok().json(data)
        }
        Err(GetTargetGenerationPointsError::ConsumerReplayError(error)) => {
            error!("Unable to get target generation points because of consumer replay, {:?}", error);
            HttpResponse::InternalServerError().finish()
        }
        Err(GetTargetGenerationPointsError::ConsumerAccessDenied(error)) => {
            warn!("Unable to get target generation points, consumer access denied, {:?}", error);
            HttpResponse::Forbidden().finish()
        }
        Err(GetTargetGenerationPointsError::LoadError(error)) => {
            error!("Unable to get target generation points because of load error, {:?}", error);
            HttpResponse::InternalServerError().finish()
        }
    }
}

enum GetTargetGenerationPointsError {
    ConsumerReplayError(ReplayError),
    ConsumerAccessDenied(UserAccessError),
    LoadError(GenerationPointProjectionError),
}

async fn get_target_generation_points(
    transporter: &Uuid, projection: &GenerationPointProjection, store: &EventStore
) -> Result<Vec<TargetGenerationPointListItem>, GetTargetGenerationPointsError> {
    let transporter = AccessUser::replay(transporter, store)
        .await
        .map_err(GetTargetGenerationPointsError::ConsumerReplayError)?;
    let transporter_id = transporter.id.clone();

    ensure_access(transporter, Permission::TargetGenerationPointList)
        .map_err(GetTargetGenerationPointsError::ConsumerAccessDenied)?;

    let target_generation_points = projection
        .get_target_generation_points(&transporter_id)
        .await
        .map_err(GetTargetGenerationPointsError::LoadError)?;

    Ok(target_generation_points)
}