use crate::application::aggregates::{ReplayError, UserAccessError, AccessUser, ensure_access, Permission};
use crate::application::projections::{TruckProjectionError, TruckProjection};
use crate::application::EventStore;
use uuid::Uuid;
use crate::model::{TruckSearchCriteria, List, TruckListItemModel};
use actix_web::web::{Query, Data};
use crate::extractors::AccessToken;
use actix_web::HttpResponse;


pub async fn handle_get_trucks(
    criteria: Query<TruckSearchCriteria>,
    store: Data<EventStore>,
    projection: Data<TruckProjection>,
    access_token: AccessToken,
) -> HttpResponse {
    let consumer = access_token.user;

    match list_trucks(&store, &projection, &consumer, &criteria).await {
        Ok(list) => {
            HttpResponse::Ok().json(list)
        }
        Err(error) => match error {
            GetTrucksError::ConsumerReplayError(error) => {
                error!("Unable to get trucks because of consumer replay, {:?}", error);
                HttpResponse::Unauthorized().finish()
            }
            GetTrucksError::ConsumerAccessDenied(error) => {
                warn!("Unable to get trucks, consumer access denied, {:?}", error);
                HttpResponse::Forbidden().finish()
            }
            GetTrucksError::TruckProjectionError(error) => {
                error!("Unable to get trucks because of projection, {:?}", error);
                HttpResponse::InternalServerError().finish()
            }
        }
    }
}


pub enum GetTrucksError {
    ConsumerReplayError(ReplayError),
    ConsumerAccessDenied(UserAccessError),
    TruckProjectionError(TruckProjectionError),
}

pub async fn list_trucks(
    store: &EventStore,
    projection: &TruckProjection,
    consumer: &Uuid,
    criteria: &TruckSearchCriteria,
) -> Result<List<TruckListItemModel>, GetTrucksError> {
    let user = AccessUser::replay(consumer, store)
        .await
        .map_err(GetTrucksError::ConsumerReplayError)?;

    ensure_access(user, Permission::TruckList)
        .map_err(GetTrucksError::ConsumerAccessDenied)?;

    let limit = match criteria.limit {
        Some(value) => value,
        None => 10
    };

    let offset = match criteria.offset {
        Some(value) => value,
        None => 0
    };

    let fetch = projection.get_trucks(
        &criteria.registration_number,
        &criteria.cargo_body_type,
        &criteria.transporter_name,
        &criteria.truck_fleet_type,
        &limit,
        &offset,
    );

    let collection = fetch.await.map_err(GetTrucksError::TruckProjectionError)?;

    Ok(List {
        items: collection.items.into_iter().map(|truck| TruckListItemModel {
            id: truck.id,
            registration_number: truck.registration_number,
            cargo_body_type: truck.cargo_body_type,
            transporter_name: truck.transporter_name,
            truck_fleet_type: truck.truck_fleet_type
        }).collect(),
        total: collection.total,
        limit,
        offset,
    })
}