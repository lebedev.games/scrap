use chrono::{DateTime, Utc};
use sqlx::{Executor, Pool, Postgres, Row};
use sqlx::postgres::PgRow;
use uuid::Uuid;

use crate::application::projections::data::map_rows;
use crate::domain::Transportation;

pub struct TransportationListItem {
    pub id: Uuid,
    pub district: String,
    pub execution_date: DateTime<Utc>,
    pub truck_registration_number: String,
    pub status: String,
}

#[derive(Clone)]
pub struct TransportationProjection {
    pub connection_pool: Pool<Postgres>
}

impl TransportationProjection {
    pub async fn get_transportations(&self, transporter: &Uuid) -> Result<Vec<TransportationListItem>, sqlx::Error> {
        fn transportation_mapping(row: &PgRow) -> Result<TransportationListItem, sqlx::Error> {
            Ok(TransportationListItem {
                id: row.try_get("id")?,
                district: row.try_get("district")?,
                execution_date: row.try_get("execution_date")?,
                status: row.try_get("status")?,
                truck_registration_number: row.try_get("truck_registration_number")?,
            })
        }

        let select_statement = r#"
            SELECT
                transportations.id,
                'todo!' as district,
                execution_date,
                status,
                coalesce(trucks.registration_number, 'none!') as truck_registration_number
            FROM transportations
            LEFT JOIN transportations_trucks trucks ON trucks.id = transportations.truck
            WHERE transporter = $1
        "#;

        let transportations = sqlx::query(select_statement)
            .bind(transporter)
            .fetch_all(&self.connection_pool)
            .await
            .and_then(|rows| map_rows(rows, transportation_mapping))?;

        Ok(transportations)
    }

    pub async fn apply_transportation_events(&self, entity_id: Uuid, events: &Vec<Transportation>) -> Result<(), sqlx::Error> {
        let connection_pool = &self.connection_pool;
        let mut transaction = connection_pool.begin().await?;

        for event in events {
            match event {
                Transportation::Registered { transporter, .. } => {
                    let insert_statement = r#"
                        INSERT INTO transportations (id, transporter, execution_date, truck, status)
                        VALUES ($1, $2, $3, $4, $5)
                    "#;
                    let insert = sqlx::query(insert_statement)
                        .bind(entity_id)
                        .bind(transporter)
                        .bind(Utc::now())
                        .bind(Uuid::nil())
                        .bind("created");
                    Executor::execute(&mut transaction, insert).await?;
                }
                Transportation::Submitted { transporter: _, date: _ } => {
                    let update_statement = r#"
                        UPDATE transportations
                        SET status = $2
                        WHERE id = $1
                    "#;
                    let update = sqlx::query(update_statement)
                        .bind(entity_id)
                        .bind("submitted");
                    Executor::execute(&mut transaction, update).await?;
                }
                Transportation::Deleted { .. } => {
                    let delete_statement = r#"
                        DELETE FROM transportations
                        WHERE id = $1
                    "#;
                    let delete = sqlx::query(delete_statement)
                        .bind(entity_id);
                    Executor::execute(&mut transaction, delete).await?;
                }
                Transportation::TruckAssigned { execution_date, truck, .. } => {
                    let update_statement = r#"
                        UPDATE transportations
                        SET execution_date = $2, truck = $3
                        WHERE id = $1
                    "#;
                    let update = sqlx::query(update_statement)
                        .bind(entity_id)
                        .bind(execution_date)
                        .bind(truck);
                    Executor::execute(&mut transaction, update).await?;
                }
                Transportation::DestinationAdded { .. } => {}
                Transportation::CollectionStartTimeChanged { .. } => {}
                Transportation::WasteGenerationPointChanged { .. } => {}
                Transportation::DestinationRemoved { .. } => {}
                Transportation::WasteCollected { .. } => {}
                Transportation::WasteVolumeChanged { .. } => {}
                Transportation::ContainerChanged { .. } => {}
                Transportation::ContainerUtilizationChanged { .. } => {}
                Transportation::WasteCollectionPhotographed { .. } => {}
                Transportation::WasteCollectionRevoked { .. } => {}
            }
        }

        transaction.commit().await
    }
}