use sqlx::{Executor, Pool, Postgres, Row};
use sqlx::postgres::PgRow;
use uuid::Uuid;

use crate::application::projections::data::{Collection, map_rows};
use crate::domain::{BusinessEntity, Entrepreneur, Transporter};

pub struct TransporterListItem {
    pub id: Uuid,
    pub legal_entity_type: String,
    pub inn: String,
    pub name: String,
    pub truck_fleet_types: Vec<String>,
    pub districts: Vec<String>,
}

pub struct TransporterOption {
    pub id: Uuid,
    pub name: String,
}

#[derive(Clone)]
pub struct TransporterProjection {
    pub connection_pool: Pool<Postgres>
}

impl TransporterProjection {

    pub async fn get_transporter_item(&self, id: Uuid) -> Result<TransporterListItem, sqlx::Error> {
        let transporters = self.get_transporters(&"".to_string(), &"".to_string(), &None, &None, &1000, &0).await?;

        match transporters.items.into_iter().find(|transporter| transporter.id == id) {
            Some(transporter) => Ok(transporter),
            None => Err(sqlx::Error::RowNotFound)
        }
    }

    pub async fn get_transporters(
        &self,
        inn: &String,
        name: &String,
        truck_fleet_type: &Option<Uuid>,
        legal_entity_type: &Option<String>,
        limit: &i32,
        offset: &i32,
    ) -> Result<Collection<TransporterListItem>, sqlx::Error> {
        fn transporter_mapping(row: &PgRow) -> Result<TransporterListItem, sqlx::Error> {
            Ok(TransporterListItem {
                id: row.try_get("id")?,
                legal_entity_type: row.try_get("legal_entity_type")?,
                inn: row.try_get("inn")?,
                name: row.try_get("name")?,
                truck_fleet_types: row.try_get("truck_fleet_types")?,
                districts: row.try_get("districts")?,
            })
        }

        let query_statement = r#"
            SELECT
                transporters.id,
                transporters.legal_entity_type,
                coalesce(transporters_legal_entities.inn, 'none!') as inn,
                coalesce(transporters_legal_entities.name, 'none!') as name,
                array_agg(dictionaries.truck_fleet_types.type_name) as truck_fleet_types,
                ARRAY['todo!'] as districts
            FROM transporters
            LEFT JOIN transporters_truck_fleet_types ON transporters_truck_fleet_types.transporter_id = transporters.id
            LEFT JOIN dictionaries.truck_fleet_types ON transporters_truck_fleet_types.truck_fleet_type_id = dictionaries.truck_fleet_types.id
            LEFT JOIN transporters_legal_entities ON transporters.legal_entity_id = transporters_legal_entities.legal_entity_id
            WHERE
                lower(transporters_legal_entities.inn) LIKE $1
                AND lower(transporters_legal_entities.name) LIKE $2
                AND transporters_truck_fleet_types.truck_fleet_type_id = coalesce($3, transporters_truck_fleet_types.truck_fleet_type_id)
                AND transporters.legal_entity_type = coalesce($4, transporters.legal_entity_type)
            GROUP BY
                transporters.id,
                transporters.entry_id,
                transporters.legal_entity_type,
                transporters_legal_entities.inn,
                transporters_legal_entities.name
            ORDER BY transporters.entry_id
        "#;
        let select_statement = format!("{} OFFSET $5 LIMIT $6", query_statement);
        let transporters = sqlx::query(&select_statement)
            .bind(format!("%{}%", inn).to_lowercase())
            .bind(format!("%{}%", name).to_lowercase())
            .bind(truck_fleet_type)
            .bind(legal_entity_type)
            .bind(offset)
            .bind(limit)
            .fetch_all(&self.connection_pool)
            .await
            .and_then(|rows| map_rows(rows, transporter_mapping))?;

        let count_statement = format!("select count(*) from ({}) count", query_statement);
        let count: (i64,) = sqlx::query_as(&count_statement)
            .bind(format!("%{}%", inn).to_lowercase())
            .bind(format!("%{}%", name).to_lowercase())
            .bind(truck_fleet_type)
            .bind(legal_entity_type)
            .fetch_one(&self.connection_pool)
            .await?;

        Ok(Collection {
            items: transporters,
            total: count.0
        })
    }

    pub async fn get_transporter_options(&self) -> Result<Vec<TransporterOption>, sqlx::Error> {
        fn transporter_option_mapping(row: &PgRow) -> Result<TransporterOption, sqlx::Error> {
            Ok(TransporterOption {
                id: row.try_get("id")?,
                name: row.try_get("name")?,
            })
        }

        let select_statement = r#"
            SELECT
                transporters.id,
                coalesce(transporters_legal_entities.name, 'none!') as name
            FROM transporters
            LEFT JOIN transporters_legal_entities ON transporters.legal_entity_id = transporters_legal_entities.legal_entity_id
        "#;
        let transporters = sqlx::query(select_statement)
            .fetch_all(&self.connection_pool)
            .await
            .and_then(|rows| map_rows(rows, transporter_option_mapping))?;

        Ok(transporters)
    }

    pub async fn apply_transporter_events(&self, entity_id: Uuid, events: &Vec<Transporter>) -> Result<(), sqlx::Error> {
        let connection_pool = &self.connection_pool;
        let mut transaction = connection_pool.begin().await?;

        for event in events {
            match event {
                Transporter::Registered { .. } => {
                    let legal_entity_type = "Unassigned".to_string();
                    let legal_entity_id = Uuid::nil();
                    let insert_statement = r#"
                        INSERT INTO transporters (id, legal_entity_type, legal_entity_id)
                        VALUES ($1, $2, $3)
                    "#;
                    let insert = sqlx::query(insert_statement)
                        .bind(entity_id)
                        .bind(legal_entity_type)
                        .bind(legal_entity_id);
                    Executor::execute(&mut transaction, insert).await?;
                }
                Transporter::BusinessEntityAssigned { business_entity, .. } => {
                    let legal_entity_type = "BusinessEntity".to_string();
                    let legal_entity_id = business_entity;
                    let update_statement = r#"
                        UPDATE transporters
                        SET legal_entity_type = $2, legal_entity_id = $3
                        WHERE id = $1
                    "#;
                    let update = sqlx::query(update_statement)
                        .bind(entity_id)
                        .bind(legal_entity_type)
                        .bind(legal_entity_id);
                    Executor::execute(&mut transaction, update).await?;
                }
                Transporter::EntrepreneurAssigned { entrepreneur, .. } => {
                    let legal_entity_type = "Entrepreneur".to_string();
                    let legal_entity_id = entrepreneur;
                    let update_statement = r#"
                        UPDATE transporters
                        SET legal_entity_type = $2, legal_entity_id = $3
                        WHERE id = $1
                    "#;
                    let update = sqlx::query(update_statement)
                        .bind(entity_id)
                        .bind(legal_entity_type)
                        .bind(legal_entity_id);
                    Executor::execute(&mut transaction, update).await?;
                }
                Transporter::TruckFleetTypeSpecified { truck_fleet_type, .. } => {
                    let insert_statement = r#"
                        INSERT INTO transporters_truck_fleet_types (transporter_id, truck_fleet_type_id)
                        VALUES ($1, $2)
                    "#;
                    let insert = sqlx::query(insert_statement)
                        .bind(entity_id)
                        .bind(truck_fleet_type);
                    Executor::execute(&mut transaction, insert).await?;
                }
                _ => {}
            }
        }
        transaction.commit().await
    }

    pub async fn apply_business_entity_events(&self, entity_id: Uuid, events: &Vec<BusinessEntity>) -> Result<(), sqlx::Error> {
        let connection_pool = &self.connection_pool;
        let mut transaction = connection_pool.begin().await?;

        for event in events {
            match event {
                BusinessEntity::Registered { .. } => {
                    let name = entity_id.to_string();
                    let inn = "000000000000".to_string();
                    let insert_statement = r#"
                        INSERT INTO transporters_legal_entities (
                            legal_entity_id,
                            inn,
                            name
                        )
                        VALUES ($1, $2, $3)
                    "#;
                    let insert = sqlx::query(insert_statement)
                        .bind(entity_id)
                        .bind(inn)
                        .bind(name);
                    Executor::execute(&mut transaction, insert).await?;
                }
                BusinessEntity::AdministrativeNameUpdated { short_name, .. } => {
                    let update_statement = r#"
                        UPDATE transporters_legal_entities
                        SET name = $2
                        WHERE legal_entity_id = $1
                    "#;
                    let update = sqlx::query(update_statement)
                        .bind(entity_id)
                        .bind(short_name);
                    Executor::execute(&mut transaction, update).await?;
                }
                BusinessEntity::TaxIdentificationUpdated { inn, .. } => {
                    let update_statement = r#"
                        UPDATE transporters_legal_entities
                        SET inn = $2
                        WHERE legal_entity_id = $1
                    "#;
                    let update = sqlx::query(update_statement)
                        .bind(entity_id)
                        .bind(inn);
                    Executor::execute(&mut transaction, update).await?;
                }
                _ => {}
            }
        }
        transaction.commit().await
    }

    pub async fn apply_entrepreneur_events(&self, entity_id: Uuid, events: &Vec<Entrepreneur>) -> Result<(), sqlx::Error> {
        let connection_pool = &self.connection_pool;
        let mut transaction = connection_pool.begin().await?;

        for event in events {
            match event {
                Entrepreneur::Registered { .. } => {
                    let name = entity_id.to_string();
                    let inn = "000000000000".to_string();
                    let insert_statement = r#"
                        INSERT INTO transporters_legal_entities (
                            legal_entity_id,
                            inn,
                            name
                        )
                        VALUES ($1, $2, $3)
                    "#;
                    let insert = sqlx::query(insert_statement)
                        .bind(entity_id)
                        .bind(inn)
                        .bind(name);
                    Executor::execute(&mut transaction, insert).await?;
                }
                Entrepreneur::PersonalNameUpdated { first_name, last_name, patronymic, .. } => {
                    let full_name = format!("{} {} {}", last_name, first_name, patronymic);
                    let update_statement = r#"
                        UPDATE transporters_legal_entities
                        SET name = $2
                        WHERE legal_entity_id = $1
                    "#;
                    let update = sqlx::query(update_statement)
                        .bind(entity_id)
                        .bind(full_name);
                    Executor::execute(&mut transaction, update).await?;
                }
                Entrepreneur::TaxIdentificationUpdated { inn, .. } => {
                    let update_statement = r#"
                        UPDATE transporters_legal_entities
                        SET inn = $2
                        WHERE legal_entity_id = $1
                    "#;
                    let update = sqlx::query(update_statement)
                        .bind(entity_id)
                        .bind(inn);
                    Executor::execute(&mut transaction, update).await?;
                }
                _ => {}
            }
        }
        transaction.commit().await
    }
}