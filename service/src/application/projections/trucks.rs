use sqlx::{Executor, Pool, Postgres, Row};
use sqlx::postgres::PgRow;
use uuid::Uuid;

use crate::application::projections::Collection;
use crate::application::projections::data::map_rows;
use crate::domain::{BusinessEntity, Entrepreneur, Transporter, Truck};

pub struct TruckListItem {
    pub id: Uuid,
    pub registration_number: String,
    pub cargo_body_type: Uuid,
    pub transporter_name: String,
    pub truck_fleet_type: Uuid,
}

pub struct TruckOption {
    pub id: Uuid,
    pub registration_number: String,
}


#[derive(Debug)]
pub enum TruckProjectionError {
    LoadError(sqlx::Error),
    ApplyEventError(sqlx::Error),
}

#[derive(Clone)]
pub struct TruckProjection {
    pub connection_pool: Pool<Postgres>
}

impl TruckProjection {
    pub async fn get_trucks(
        &self,
        registration_number: &String,
        cargo_body_type: &Option<Uuid>,
        transporter_name: &String,
        truck_fleet_type: &Option<Uuid>,
        limit: &i32,
        offset: &i32,
    ) -> Result<Collection<TruckListItem>, TruckProjectionError> {
        fn truck_mapping(row: &PgRow) -> Result<TruckListItem, sqlx::Error> {
            Ok(TruckListItem {
                id: row.try_get("truck_id")?,
                registration_number: row.try_get("registration_number")?,
                cargo_body_type: row.try_get("cargo_body_type")?,
                transporter_name: row.try_get("transporter_name")?,
                truck_fleet_type: row.try_get("truck_fleet_type")?,
            })
        }

        let query_statement = r#"
            SELECT
                truck_id,
                registration_number,
                cargo_body_type,
                truck_fleet_type,
                coalesce(trucks_legal_entities.name, 'none!') as transporter_name
            FROM trucks
            LEFT JOIN trucks_transporters ON trucks_transporters.transporter_id = trucks.transporter_id
            LEFT JOIN trucks_legal_entities ON trucks_transporters.legal_entity_id = trucks_legal_entities.legal_entity_id
            WHERE
                lower(trucks.registration_number) LIKE $1 AND
                trucks.cargo_body_type = coalesce($2, trucks.cargo_body_type) AND
                trucks.truck_fleet_type = coalesce($3, trucks.truck_fleet_type) AND
                lower(coalesce(trucks_legal_entities.name, 'none!')) LIKE $4
            ORDER BY trucks.entry_id
        "#;
        let select_statement = format!("{} OFFSET $5 LIMIT $6", query_statement);

        let trucks = sqlx::query(&select_statement)
            .bind(format!("%{}%", registration_number).to_lowercase())
            .bind(cargo_body_type)
            .bind(truck_fleet_type)
            .bind(format!("%{}%", transporter_name).to_lowercase())
            .bind(offset)
            .bind(limit)
            .fetch_all(&self.connection_pool)
            .await
            .and_then(|rows| map_rows(rows, truck_mapping))
            .map_err(TruckProjectionError::LoadError)?;

        let count_statement = format!("select count(*) from ({}) count", query_statement);
        let count: (i64, ) = sqlx::query_as(&count_statement)
            .bind(format!("%{}%", registration_number).to_lowercase())
            .bind(cargo_body_type)
            .bind(truck_fleet_type)
            .bind(format!("%{}%", transporter_name).to_lowercase())
            .fetch_one(&self.connection_pool)
            .await
            .map_err(TruckProjectionError::LoadError)?;

        Ok(Collection {
            items: trucks,
            total: count.0,
        })
    }

    pub async fn get_truck_options(&self, _transporter: &Uuid) -> Result<Vec<TruckOption>, TruckProjectionError> {
        fn truck_option_mapping(row: &PgRow) -> Result<TruckOption, sqlx::Error> {
            Ok(TruckOption {
                id: row.try_get("truck_id")?,
                registration_number: row.try_get("registration_number")?,
            })
        }

        let select_statement = r#"
            SELECT
                trucks.truck_id,
                trucks.registration_number
            FROM trucks
            "#;
        let trucks = sqlx::query(select_statement)
            .fetch_all(&self.connection_pool)
            .await
            .and_then(|rows| map_rows(rows, truck_option_mapping))
            .map_err(TruckProjectionError::LoadError)?;

        Ok(trucks)
    }

    pub async fn apply_truck_events(&self, entity_id: Uuid, events: &Vec<Truck>) -> Result<(), TruckProjectionError> {
        let connection_pool = &self.connection_pool;
        let mut transaction = connection_pool
            .begin()
            .await
            .map_err(TruckProjectionError::ApplyEventError)?;

        for event in events {
            match event {
                Truck::Registered { .. } => {
                    let insert_statement = r#"
                        INSERT INTO trucks (
                            truck_id,
                            registration_number,
                            cargo_body_type,
                            transporter_id,
                            truck_fleet_type
                        )
                        VALUES ($1, $2, $3, $4, $5);
                    "#;
                    let insert = sqlx::query(insert_statement)
                        .bind(entity_id)
                        .bind("".to_string())
                        .bind(Uuid::default())
                        .bind(Uuid::default())
                        .bind(Uuid::default());

                    Executor::execute(&mut transaction, insert)
                        .await
                        .map_err(TruckProjectionError::ApplyEventError)?;

                    let insert_statement = r#"
                        INSERT INTO transportations_trucks (
                            id,
                            registration_number
                        )
                        VALUES ($1, $2);
                        "#;
                    let insert = sqlx::query(insert_statement)
                        .bind(entity_id)
                        .bind("".to_string());

                    Executor::execute(&mut transaction, insert)
                        .await
                        .map_err(TruckProjectionError::ApplyEventError)?;
                }
                Truck::RegistrationNumberChanged { registration_number, .. } => {
                    let update_statement = r#"
                        UPDATE trucks
                        SET registration_number = $2
                        WHERE truck_id = $1;
                    "#;
                    let update = sqlx::query(update_statement)
                        .bind(entity_id)
                        .bind(registration_number);
                    Executor::execute(&mut transaction, update)
                        .await
                        .map_err(TruckProjectionError::ApplyEventError)?;

                    let update_statement = r#"
                        UPDATE transportations_trucks
                        SET registration_number = $2
                        WHERE id = $1;
                    "#;
                    let update = sqlx::query(update_statement)
                        .bind(entity_id)
                        .bind(registration_number);
                    Executor::execute(&mut transaction, update)
                        .await
                        .map_err(TruckProjectionError::ApplyEventError)?;
                }
                Truck::CargoBodyTypeChanged { cargo_body_type, .. } => {
                    let update_statement = r#"
                        UPDATE trucks
                        SET cargo_body_type = $2
                        WHERE truck_id = $1
                    "#;
                    let update = sqlx::query(update_statement)
                        .bind(entity_id)
                        .bind(cargo_body_type);
                    Executor::execute(&mut transaction, update)
                        .await
                        .map_err(TruckProjectionError::ApplyEventError)?;
                }
                Truck::TransporterAssigned { transporter, .. } => {
                    let update_statement = r#"
                        UPDATE trucks
                        SET transporter_id = $2
                        WHERE truck_id = $1
                    "#;
                    let update = sqlx::query(update_statement)
                        .bind(entity_id)
                        .bind(transporter);
                    Executor::execute(&mut transaction, update)
                        .await
                        .map_err(TruckProjectionError::ApplyEventError)?;
                }
                Truck::FleetTypeChanged { fleet_type, .. } => {
                    let update_statement = r#"
                        UPDATE trucks
                        SET truck_fleet_type = $2
                        WHERE truck_id = $1
                    "#;
                    let update = sqlx::query(update_statement)
                        .bind(entity_id)
                        .bind(fleet_type);
                    Executor::execute(&mut transaction, update)
                        .await
                        .map_err(TruckProjectionError::ApplyEventError)?;
                }
                _ => {}
            }
        }

        transaction.commit()
            .await
            .map_err(TruckProjectionError::ApplyEventError)
    }

    pub async fn apply_transporter_events(&self, entity_id: &Uuid, events: &Vec<Transporter>) -> Result<(), TruckProjectionError> {
        let connection_pool = &self.connection_pool;
        let mut transaction = connection_pool.begin()
            .await
            .map_err(TruckProjectionError::ApplyEventError)?;

        for event in events {
            match event {
                Transporter::Registered { .. } => {
                    let insert_statement = r#"
                        INSERT INTO trucks_transporters (
                            transporter_id,
                            legal_entity_id
                        )
                        VALUES ($1, $2)
                    "#;
                    let insert = sqlx::query(insert_statement)
                        .bind(entity_id)
                        .bind(Uuid::default());
                    Executor::execute(&mut transaction, insert)
                        .await
                        .map_err(TruckProjectionError::ApplyEventError)?;
                }
                Transporter::BusinessEntityAssigned { business_entity, .. } => {
                    let update_statement = r#"
                        UPDATE trucks_transporters
                        SET legal_entity_id = $2
                        WHERE transporter_id = $1
                    "#;
                    let update = sqlx::query(update_statement)
                        .bind(entity_id)
                        .bind(business_entity);
                    Executor::execute(&mut transaction, update)
                        .await
                        .map_err(TruckProjectionError::ApplyEventError)?;
                }
                Transporter::EntrepreneurAssigned { entrepreneur, .. } => {
                    let update_statement = r#"
                        UPDATE trucks_transporters
                        SET legal_entity_id = $2
                        WHERE transporter_id = $1
                    "#;
                    let update = sqlx::query(update_statement)
                        .bind(entity_id)
                        .bind(entrepreneur);
                    Executor::execute(&mut transaction, update)
                        .await
                        .map_err(TruckProjectionError::ApplyEventError)?;
                }
                _ => {}
            }
        }
        transaction.commit()
            .await
            .map_err(TruckProjectionError::ApplyEventError)?;
        Ok(())
    }

    pub async fn apply_entrepreneur_events(&self, entity_id: &Uuid, events: &Vec<Entrepreneur>) -> Result<(), TruckProjectionError> {
        let connection_pool = &self.connection_pool;
        let mut transaction = connection_pool.begin()
            .await
            .map_err(TruckProjectionError::ApplyEventError)?;

        for event in events {
            match event {
                Entrepreneur::Registered { .. } => {
                    let insert_statement = r#"
                        INSERT INTO trucks_legal_entities (
                            legal_entity_id,
                            name
                        )
                        VALUES ($1, $2)
                    "#;
                    let insert = sqlx::query(insert_statement)
                        .bind(entity_id)
                        .bind(String::default());
                    Executor::execute(&mut transaction, insert)
                        .await
                        .map_err(TruckProjectionError::ApplyEventError)?;
                }
                Entrepreneur::PersonalNameUpdated {
                    first_name,
                    last_name,
                    patronymic,
                    ..
                } => {
                    let name = format!("{} {} {}", last_name, first_name, patronymic);
                    let update_statement = r#"
                        UPDATE trucks_legal_entities
                        SET name = $2
                        WHERE legal_entity_id = $1
                    "#;
                    let update = sqlx::query(update_statement)
                        .bind(entity_id)
                        .bind(name);
                    Executor::execute(&mut transaction, update)
                        .await
                        .map_err(TruckProjectionError::ApplyEventError)?;
                }
                _ => {}
            }
        }

        transaction.commit()
            .await
            .map_err(TruckProjectionError::ApplyEventError)?;

        Ok(())
    }

    pub async fn apply_business_entity_events(&self, entity_id: &Uuid, events: &Vec<BusinessEntity>) -> Result<(), TruckProjectionError> {
        let connection_pool = &self.connection_pool;
        let mut transaction = connection_pool.begin()
            .await
            .map_err(TruckProjectionError::ApplyEventError)?;

        for event in events {
            match event {
                BusinessEntity::Registered { .. } => {
                    let insert_statement = r#"
                        INSERT INTO trucks_legal_entities (
                            legal_entity_id,
                            name
                        )
                        VALUES ($1, $2)
                    "#;
                    let insert = sqlx::query(insert_statement)
                        .bind(entity_id)
                        .bind(String::default());
                    Executor::execute(&mut transaction, insert)
                        .await
                        .map_err(TruckProjectionError::ApplyEventError)?;
                }
                BusinessEntity::AdministrativeNameUpdated { short_name, .. } => {
                    let update_statement = r#"
                        UPDATE trucks_legal_entities
                        SET name = $2
                        WHERE legal_entity_id = $1
                    "#;
                    let update = sqlx::query(update_statement)
                        .bind(entity_id)
                        .bind(short_name);
                    Executor::execute(&mut transaction, update)
                        .await
                        .map_err(TruckProjectionError::ApplyEventError)?;
                }
                _ => {}
            }
        }

        transaction.commit()
            .await
            .map_err(TruckProjectionError::ApplyEventError)?;

        Ok(())
    }
}
