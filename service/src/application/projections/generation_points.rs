use std::collections::HashMap;

use rust_decimal::Decimal;
use sqlx::{Executor, Pool, Postgres, Row};
use sqlx::postgres::PgRow;
use uuid::Uuid;

use crate::application::projections::Collection;
use crate::application::projections::data::map_rows;
use crate::domain::{BusinessEntity, Entrepreneur, GenerationPoint, Transporter};

pub struct GenerationPointListItem {
    pub id: Uuid,
    pub internal_system_number: String,
    pub full_address: String,
    pub transporter_name: String,
    pub area_m2: Decimal,
}

pub struct TargetContainerTypeListItem {
    pub id: Uuid,
    pub name: String,
    pub volume_limit_m3: Option<Decimal>,
}

pub struct TargetGenerationPointListItem {
    pub id: Uuid,
    pub address: String,
    pub container_types: Vec<TargetContainerTypeListItem>,
}

#[derive(Debug)]
pub enum GenerationPointProjectionError {
    LoadError(sqlx::Error),
    ApplyEventError(sqlx::Error),
}

#[derive(Clone)]
pub struct GenerationPointProjection {
    pub connection_pool: Pool<Postgres>
}

pub struct GenerationPointOption {
    pub id: Uuid,
    pub name: String
}

impl GenerationPointProjection {

    pub async fn get_generation_point_options(&self) -> Result<Vec<GenerationPointOption>, sqlx::Error> {
        fn option_mapping(row: &PgRow) -> Result<GenerationPointOption, sqlx::Error> {
            Ok(GenerationPointOption {
                id: row.try_get("generation_point_id")?,
                name: row.try_get("full_address")?,
            })
        }

        let select_statement = r#"
            SELECT
                generation_point_id,
                full_address
            FROM generation_points
        "#;
        let generation_points = sqlx::query(select_statement)
            .fetch_all(&self.connection_pool)
            .await
            .and_then(|rows| map_rows(rows, option_mapping))?;

        Ok(generation_points)
    }

    pub async fn get_generation_points(
        &self,
        system_number: &String,
        transporter: &String,
        address: &String,
        limit: &i32,
        offset: &i32
    ) -> Result<Collection<GenerationPointListItem>, GenerationPointProjectionError> {
        fn generation_point_mapping(row: &PgRow) -> Result<GenerationPointListItem, sqlx::Error> {
            Ok(GenerationPointListItem {
                id: row.try_get("generation_point_id")?,
                internal_system_number: row.try_get("internal_system_number")?,
                full_address: row.try_get("full_address")?,
                transporter_name: row.try_get("transporter_name")?,
                area_m2: row.try_get("area_m2")?,
            })
        }

        let query_statement = r#"
            SELECT
                generation_point_id,
                internal_system_number,
                full_address,
                area_m2,
                coalesce(generation_point_legal_entities.name, 'none!') as transporter_name
            FROM generation_points
            LEFT JOIN generation_point_transporters
            ON generation_point_transporters.transporter_id = generation_points.transporter_id
            LEFT JOIN generation_point_legal_entities
            ON generation_point_transporters.legal_entity_id = generation_point_legal_entities.legal_entity_id
            WHERE
                lower(internal_system_number) LIKE $1
                AND lower(full_address) LIKE $2
                AND lower(coalesce(generation_point_legal_entities.name, 'none!')) LIKE $3
        "#;

        let select_statement = format!("{} OFFSET $4 LIMIT $5", query_statement);
        let generation_points = sqlx::query(&select_statement)
            .bind(format!("%{}%", system_number).to_lowercase())
            .bind(format!("%{}%", address).to_lowercase())
            .bind(format!("%{}%", transporter).to_lowercase())
            .bind(offset)
            .bind(limit)
            .fetch_all(&self.connection_pool)
            .await
            .and_then(|rows| map_rows(rows, generation_point_mapping))
            .map_err(GenerationPointProjectionError::LoadError)?;

        let count_statement = format!("select count(*) from ({}) count", query_statement);
        let count: (i64,) = sqlx::query_as(&count_statement)
            .bind(format!("%{}%", system_number).to_lowercase())
            .bind(format!("%{}%", address).to_lowercase())
            .bind(format!("%{}%", transporter).to_lowercase())
            .fetch_one(&self.connection_pool)
            .await
            .map_err(GenerationPointProjectionError::LoadError)?;

        Ok(Collection {
            items: generation_points,
            total: count.0
        })
    }

    pub async fn get_target_generation_points(&self, _transporter: &Uuid) -> Result<Vec<TargetGenerationPointListItem>, GenerationPointProjectionError> {
        fn target_generation_points_mapping(rows: Vec<PgRow>) -> Result<Vec<TargetGenerationPointListItem>, sqlx::Error> {
            let mut target_generation_points: HashMap<Uuid, TargetGenerationPointListItem> = HashMap::new();
            for row in rows {
                let generation_point_id: Uuid = row.try_get("generation_point_id")?;
                let container_type = TargetContainerTypeListItem {
                    id: row.try_get("container_type_id")?,
                    name: row.try_get("container_type_name")?,
                    volume_limit_m3: row.try_get("volume_limit_m3")?,
                };
                let generation_point = target_generation_points
                    .entry(generation_point_id)
                    .or_insert(TargetGenerationPointListItem {
                        id: generation_point_id,
                        address: row.try_get("address")?,
                        container_types: vec![],
                    });
                generation_point.container_types.push(container_type);
            };

            Ok(target_generation_points
                .into_iter()
                .map(|(_id, generation_point)| generation_point)
                .collect())
        }

        let target_generation_points = sqlx::query(r#"
            SELECT target_generation_points.generation_point_id,
                target_generation_points.address,
                target_generation_points_container_types.container_type_id as container_type_id,
                dictionaries.container_types.type_name as container_type_name,
                dictionaries.container_types.volume_limit_m3 as volume_limit_m3
            FROM target_generation_points
            LEFT JOIN target_generation_points_container_types
            ON target_generation_points_container_types.generation_point_id = target_generation_points.generation_point_id
            LEFT JOIN dictionaries.container_types
            ON dictionaries.container_types.id = target_generation_points_container_types.container_type_id
        "#)
            .fetch_all(&self.connection_pool)
            .await
            .and_then(target_generation_points_mapping)
            .map_err(GenerationPointProjectionError::LoadError)?;

        Ok(target_generation_points)
    }

    pub async fn apply_generation_point_events(&self, entity_id: &Uuid, events: &Vec<GenerationPoint>) -> Result<(), GenerationPointProjectionError> {
        self.materialize_generation_points(entity_id, events)
            .await
            .map_err(GenerationPointProjectionError::ApplyEventError)?;

        self.materialize_target_generation_points(entity_id, events)
            .await
            .map_err(GenerationPointProjectionError::ApplyEventError)?;

        Ok(())
    }

    async fn materialize_generation_points(&self, entity_id: &Uuid, events: &Vec<GenerationPoint>) -> Result<(), sqlx::Error> {
        let connection_pool = &self.connection_pool;
        let mut transaction = connection_pool.begin()
            .await?;

        for event in events {
            match event {
                GenerationPoint::Registered { .. } => {
                    let insert_statement = r#"
                        INSERT INTO generation_points (
                            generation_point_id,
                            internal_system_number,
                            full_address,
                            transporter_id,
                            area_m2
                        )
                        VALUES ($1, $2, $3, $4, $5)
                    "#;
                    let insert = sqlx::query(insert_statement)
                        .bind(entity_id)
                        .bind(String::default())
                        .bind(String::default())
                        .bind(Uuid::default())
                        .bind(Decimal::default());
                    Executor::execute(&mut transaction, insert)
                        .await?;
                }
                GenerationPoint::InternalIdentificationChanged { system_number, .. } => {
                    let update_statement = r#"
                        UPDATE generation_points
                        SET internal_system_number = $2
                        WHERE generation_point_id = $1
                    "#;
                    let update = sqlx::query(update_statement)
                        .bind(entity_id)
                        .bind(system_number);
                    Executor::execute(&mut transaction, update).await?;
                }
                GenerationPoint::AddressChanged { full_address, .. } => {
                    let update_statement = r#"
                        UPDATE generation_points
                        SET full_address = $2
                        WHERE generation_point_id = $1
                    "#;
                    let update = sqlx::query(update_statement)
                        .bind(entity_id)
                        .bind(full_address);
                    Executor::execute(&mut transaction, update).await?;
                }
                GenerationPoint::AreaChanged { area_m2, .. } => {
                    let update_statement = r#"
                        UPDATE generation_points
                        SET area_m2 = $2
                        WHERE generation_point_id = $1
                    "#;
                    let update = sqlx::query(update_statement)
                        .bind(entity_id)
                        .bind(area_m2);
                    Executor::execute(&mut transaction, update).await?;
                }
                GenerationPoint::TransporterAssigned { transporter, .. } => {
                    let update_statement = r#"
                        UPDATE generation_points
                        SET transporter_id = $2
                        WHERE generation_point_id = $1
                    "#;
                    let update = sqlx::query(update_statement)
                        .bind(entity_id)
                        .bind(transporter);
                    Executor::execute(&mut transaction, update).await?;
                }
                GenerationPoint::ExternalRegistrationChanged { .. } => {}
                GenerationPoint::WasteTypeChanged { .. } => {}
                GenerationPoint::OrganizationLegalTypeChanged { .. } => {}
                GenerationPoint::OperationalStatusChanged { .. } => {}
                GenerationPoint::CoordinatesChanged { .. } => {}
                GenerationPoint::CoveringTypeChanged { .. } => {}
                GenerationPoint::FenceMaterialChanged { .. } => {}
                GenerationPoint::ServiceCompanyChanged { .. } => {}
                GenerationPoint::GeneratorAssigned { .. } => {}
                GenerationPoint::DescriptionChanged { .. } => {}
                GenerationPoint::TransportationFrequencyChanged { .. } => {}
                GenerationPoint::PhotosChanged { .. } => {}
                GenerationPoint::AccrualMethodChanged { .. } => {}
                GenerationPoint::MeasurementUnitChanged { .. } => {}
                GenerationPoint::ContainerAdded { .. } => {}
                GenerationPoint::ContainerRemoved { .. } => {}
            }
        }
        transaction.commit().await?;
        Ok(())
    }

    async fn materialize_target_generation_points(&self, entity_id: &Uuid, events: &Vec<GenerationPoint>) -> Result<(), sqlx::Error> {
        let connection_pool = &self.connection_pool;
        let mut transaction = connection_pool.begin()
            .await?;

        for event in events {
            match event {
                GenerationPoint::Registered { .. } => {
                    let insert_statement = r#"
                        INSERT INTO target_generation_points (
                            generation_point_id,
                            address,
                            transporter
                        )
                        VALUES ($1, $2, $3)
                    "#;
                    let insert = sqlx::query(insert_statement)
                        .bind(entity_id)
                        .bind(String::default())
                        .bind(Uuid::nil());
                    Executor::execute(&mut transaction, insert)
                        .await?;
                }
                GenerationPoint::AddressChanged { full_address, .. } => {
                    let update_statement = r#"
                        UPDATE target_generation_points
                        SET address = $2
                        WHERE generation_point_id = $1
                    "#;
                    let update = sqlx::query(update_statement)
                        .bind(entity_id)
                        .bind(full_address);
                    Executor::execute(&mut transaction, update).await?;
                }
                GenerationPoint::ContainerAdded { container_type, .. } => {
                    let insert_statement = r#"
                        INSERT INTO target_generation_points_container_types (
                            generation_point_id,
                            container_type_id
                        )
                        VALUES ($1, $2)
                    "#;
                    let insert = sqlx::query(insert_statement)
                        .bind(entity_id)
                        .bind(container_type);
                    Executor::execute(&mut transaction, insert)
                        .await?;
                }
                GenerationPoint::ContainerRemoved { container_type, .. } => {
                    let delete_statement = r#"
                        DELETE target_generation_points_container_types (
                        WHERE entry_id in (
                            SELECT entry_id FROM target_generation_points_container_types
                            WHERE generation_point_id = $1 AND container_type_id = $2
                            LIMIT 1
                        )
                    "#;
                    let insert = sqlx::query(delete_statement)
                        .bind(entity_id)
                        .bind(container_type);
                    Executor::execute(&mut transaction, insert)
                        .await?;
                }
                GenerationPoint::TransporterAssigned { transporter, .. } => {
                    let update_statement = r#"
                        UPDATE target_generation_points
                        SET transporter = $2
                        WHERE generation_point_id = $1
                    "#;
                    let update = sqlx::query(update_statement)
                        .bind(entity_id)
                        .bind(transporter);
                    Executor::execute(&mut transaction, update).await?;
                }
                GenerationPoint::InternalIdentificationChanged { .. } => {}
                GenerationPoint::ExternalRegistrationChanged { .. } => {}
                GenerationPoint::WasteTypeChanged { .. } => {}
                GenerationPoint::OrganizationLegalTypeChanged { .. } => {}
                GenerationPoint::OperationalStatusChanged { .. } => {}
                GenerationPoint::CoordinatesChanged { .. } => {}
                GenerationPoint::AreaChanged { .. } => {}
                GenerationPoint::CoveringTypeChanged { .. } => {}
                GenerationPoint::FenceMaterialChanged { .. } => {}
                GenerationPoint::ServiceCompanyChanged { .. } => {}
                GenerationPoint::GeneratorAssigned { .. } => {}
                GenerationPoint::DescriptionChanged { .. } => {}
                GenerationPoint::TransportationFrequencyChanged { .. } => {}
                GenerationPoint::PhotosChanged { .. } => {}
                GenerationPoint::AccrualMethodChanged { .. } => {}
                GenerationPoint::MeasurementUnitChanged { .. } => {}
            }
        }
        transaction.commit().await?;
        Ok(())
    }

    pub async fn apply_transporter_events(&self, entity_id: &Uuid, events: &Vec<Transporter>) -> Result<(), GenerationPointProjectionError> {
        let connection_pool = &self.connection_pool;
        let mut transaction = connection_pool.begin()
            .await
            .map_err(GenerationPointProjectionError::ApplyEventError)?;

        for event in events {
            match event {
                Transporter::Registered { .. } => {
                    let insert_statement = r#"
                        INSERT INTO generation_point_transporters (
                            transporter_id,
                            legal_entity_id
                        )
                        VALUES ($1, $2)
                    "#;
                    let insert = sqlx::query(insert_statement)
                        .bind(entity_id)
                        .bind(Uuid::default());
                    Executor::execute(&mut transaction, insert)
                        .await
                        .map_err(GenerationPointProjectionError::ApplyEventError)?;
                }
                Transporter::BusinessEntityAssigned { business_entity, .. } => {
                    let update_statement = r#"
                        UPDATE generation_point_transporters
                        SET legal_entity_id = $2
                        WHERE transporter_id = $1
                    "#;
                    let update = sqlx::query(update_statement)
                        .bind(entity_id)
                        .bind(business_entity);
                    Executor::execute(&mut transaction, update)
                        .await
                        .map_err(GenerationPointProjectionError::ApplyEventError)?;
                }
                Transporter::EntrepreneurAssigned { entrepreneur, .. } => {
                    let update_statement = r#"
                        UPDATE generation_point_transporters
                        SET legal_entity_id = $2
                        WHERE transporter_id = $1
                    "#;
                    let update = sqlx::query(update_statement)
                        .bind(entity_id)
                        .bind(entrepreneur);
                    Executor::execute(&mut transaction, update)
                        .await
                        .map_err(GenerationPointProjectionError::ApplyEventError)?;
                }
                _ => {}
            }
        }
        transaction.commit()
            .await
            .map_err(GenerationPointProjectionError::ApplyEventError)?;
        Ok(())
    }

    pub async fn apply_entrepreneur_events(&self, entity_id: &Uuid, events: &Vec<Entrepreneur>) -> Result<(), GenerationPointProjectionError> {
        let connection_pool = &self.connection_pool;
        let mut transaction = connection_pool.begin()
            .await
            .map_err(GenerationPointProjectionError::ApplyEventError)?;

        for event in events {
            match event {
                Entrepreneur::Registered { .. } => {
                    let insert_statement = r#"
                        INSERT INTO generation_point_legal_entities (
                            legal_entity_id,
                            name
                        )
                        VALUES ($1, $2)
                    "#;
                    let insert = sqlx::query(insert_statement)
                        .bind(entity_id)
                        .bind(String::default());
                    Executor::execute(&mut transaction, insert)
                        .await
                        .map_err(GenerationPointProjectionError::ApplyEventError)?;
                }
                Entrepreneur::PersonalNameUpdated {
                    first_name,
                    last_name,
                    patronymic,
                    ..
                } => {
                    let name = format!("{} {} {}", last_name, first_name, patronymic);
                    let update_statement = r#"
                        UPDATE generation_point_legal_entities
                        SET name = $2
                        WHERE legal_entity_id = $1
                    "#;
                    let update = sqlx::query(update_statement)
                        .bind(entity_id)
                        .bind(name);
                    Executor::execute(&mut transaction, update)
                        .await
                        .map_err(GenerationPointProjectionError::ApplyEventError)?;
                }
                _ => {}
            }
        }

        transaction.commit()
            .await
            .map_err(GenerationPointProjectionError::ApplyEventError)?;

        Ok(())
    }

    pub async fn apply_business_entity_events(&self, entity_id: &Uuid, events: &Vec<BusinessEntity>) -> Result<(), GenerationPointProjectionError> {
        let connection_pool = &self.connection_pool;
        let mut transaction = connection_pool.begin()
            .await
            .map_err(GenerationPointProjectionError::ApplyEventError)?;

        for event in events {
            match event {
                BusinessEntity::Registered { .. } => {
                    let insert_statement = r#"
                        INSERT INTO generation_point_legal_entities (
                            legal_entity_id,
                            name
                        )
                        VALUES ($1, $2)
                    "#;
                    let insert = sqlx::query(insert_statement)
                        .bind(entity_id)
                        .bind(String::default());
                    Executor::execute(&mut transaction, insert)
                        .await
                        .map_err(GenerationPointProjectionError::ApplyEventError)?;
                }
                BusinessEntity::AdministrativeNameUpdated { full_name, .. } => {
                    let update_statement = r#"
                        UPDATE generation_point_legal_entities
                        SET name = $2
                        WHERE legal_entity_id = $1
                    "#;
                    let update = sqlx::query(update_statement)
                        .bind(entity_id)
                        .bind(full_name);
                    Executor::execute(&mut transaction, update)
                        .await
                        .map_err(GenerationPointProjectionError::ApplyEventError)?;
                }
                _ => {}
            }
        }

        transaction.commit()
            .await
            .map_err(GenerationPointProjectionError::ApplyEventError)?;

        Ok(())
    }
}
