use sqlx::postgres::PgRow;

pub struct Collection<T> {
    pub items: Vec<T>,
    pub total: i64
}

pub fn map_rows<T>(rows: Vec<PgRow>, mapping: fn(&PgRow) -> Result<T, sqlx::Error>) -> Result<Vec<T>, sqlx::Error> {
    let mut collection = Vec::new();
    for row in rows.iter() {
        let item = mapping(row)?;
        collection.push(item);
    }
    Ok(collection)
}
