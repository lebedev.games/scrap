use sqlx::{Postgres, Pool, Row, Executor};
use chrono::{Utc, DateTime};
use uuid::Uuid;
use crate::application::projections::Collection;
use sqlx::postgres::PgRow;
use crate::application::projections::data::map_rows;
use crate::domain::{Negotiation, Report};

pub struct NegotiationListItem {
    pub id: Uuid,
    pub district: String,
    pub transportation_date: DateTime<Utc>,
    pub transportation: Uuid,
    pub truck: String,
    pub transporter: String,
    pub status: String,
}

pub struct TimelinePoint {
    pub stage_id: Uuid,
    pub negotiation: Uuid,
    pub transportation_version: i32,
    pub timestamp: DateTime<Utc>,
    pub status: String
}

#[derive(Clone)]
pub struct NegotiationProjection {
    pub connection_pool: Pool<Postgres>
}

impl NegotiationProjection {
    pub async fn get_negotiation_timeline(&self, negotiation: &Uuid) -> Result<Vec<TimelinePoint>, sqlx::Error> {
        fn mapping(row: &PgRow) -> Result<TimelinePoint, sqlx::Error> {
            Ok(TimelinePoint {
                stage_id: row.try_get("stage_id")?,
                negotiation: row.try_get("negotiation_id")?,
                transportation_version: row.try_get("transportation_version")?,
                timestamp: row.try_get("timestamp")?,
                status: row.try_get("status")?
            })
        }

        let select_statement = r#"
            SELECT
                stage_id,
                negotiation_id,
                transportation_version,
                timestamp,
                status
            FROM negotiations_timeline
            WHERE
                negotiation_id = $1
            ORDER BY timestamp
        "#;
        let trucks = sqlx::query(select_statement)
            .bind(negotiation)
            .fetch_all(&self.connection_pool)
            .await
            .and_then(|rows| map_rows(rows, mapping))?;

        Ok(trucks)
    }

    pub async fn get_negotiations(
        &self,
        transportation_date: &Option<DateTime<Utc>>,
        truck: &String,
        district: &String,
        transporter: &String,
        status: &Option<String>,
        consolidated: &Option<bool>,
        limit: &i32,
        offset: &i32,
    ) -> Result<Collection<NegotiationListItem>, sqlx::Error> {
        fn mapping(row: &PgRow) -> Result<NegotiationListItem, sqlx::Error> {
            Ok(NegotiationListItem {
                id: row.try_get("id")?,
                district: row.try_get("district")?,
                transportation_date: row.try_get("transportation_date")?,
                transportation: row.try_get("transportation")?,
                truck: row.try_get("truck")?,
                transporter: row.try_get("transporter_name")?,
                status: row.try_get("status")?
            })
        }

        let query_statement = r#"
            SELECT
                id,
                district,
                transportation_date,
                transportation,
                truck,
                transporter_name,
                status
            FROM negotiations
            WHERE
                transportation_date::date = coalesce($1, transportation_date)::date
                AND lower(truck) LIKE $2
                AND lower(district) LIKE $3
                AND lower(transporter_name) LIKE $4
                AND status = coalesce($5, status)
                AND consolidated = coalesce($6, consolidated)
            ORDER BY entry_id
        "#;
        let select_statement = format!("{} OFFSET $7 LIMIT $8", query_statement);

        let negotiations = sqlx::query(&select_statement)
            .bind(transportation_date)
            .bind(format!("%{}%", truck).to_lowercase())
            .bind(format!("%{}%", district).to_lowercase())
            .bind(format!("%{}%", transporter).to_lowercase())
            .bind(status)
            .bind(consolidated)
            .bind(offset)
            .bind(limit)
            .fetch_all(&self.connection_pool)
            .await
            .and_then(|rows| map_rows(rows, mapping))?;


        let count_statement = format!("select count(*) from ({}) count", query_statement);
        let count: (i64, ) = sqlx::query_as(&count_statement)
            .bind(transportation_date)
            .bind(format!("%{}%", truck).to_lowercase())
            .bind(format!("%{}%", district).to_lowercase())
            .bind(format!("%{}%", transporter).to_lowercase())
            .bind(status)
            .bind(consolidated)
            .fetch_one(&self.connection_pool)
            .await?;

        Ok(Collection {
            items: negotiations,
            total: count.0,
        })
    }

    pub async fn apply_report_events(&self, events: &Vec<Report>) -> Result<(), sqlx::Error> {
        let connection_pool = &self.connection_pool;
        let mut transaction = connection_pool.begin().await?;

        for event in events {
            match event {
                Report::Created { .. } => {}
                Report::TransportationCovered { transportations } => {
                    let update_statement = r#"
                        UPDATE negotiations
                        SET consolidated = true
                        WHERE transportation = any($1);
                    "#;
                    let update = sqlx::query(update_statement)
                        .bind(transportations);

                    Executor::execute(&mut transaction, update).await?;
                }
                Report::TargetSpecified { .. } => {}
            }
        }

        transaction.commit().await?;

        Ok(())

    }

    pub async fn apply_negotiation_events(&self, entity_id: Uuid, events: &Vec<Negotiation>) -> Result<(), sqlx::Error> {
        let connection_pool = &self.connection_pool;
        let mut transaction = connection_pool.begin().await?;

        for event in events {
            match event {
                Negotiation::Started { transporter, transportation, transportation_version, date } => {
                    let insert_statement = r#"
                        INSERT INTO negotiations (
                            id,
                            transportation,
                            transportation_version,
                            transportation_date,
                            truck,
                            started,
                            transporter,
                            transporter_name,
                            status,
                            district
                        )
                        SELECT
                            $1,
                            $2,
                            $3,
                            transportations.execution_date,
                            transportations_trucks.registration_number,
                            $4,
                            $5,
                            transporters_legal_entities.name,
                            $6,
                            $7
                        FROM transportations
                        LEFT JOIN transportations_trucks ON transportations_trucks.id = transportations.truck
                        LEFT JOIN transporters ON transporters.id = $5
                        LEFT JOIN transporters_legal_entities ON transporters_legal_entities.legal_entity_id = transporters.legal_entity_id
                        WHERE transportations.id = $2;
                    "#;
                    let insert = sqlx::query(insert_statement)
                        .bind(entity_id)
                        .bind(transportation)
                        .bind(transportation_version)
                        .bind(date)
                        .bind(transporter)
                        .bind("pending_approval")
                        .bind("none!");

                    Executor::execute(&mut transaction, insert).await?;

                    let insert_statement = r#"
                        INSERT INTO negotiations_timeline (
                            stage_id,
                            negotiation_id,
                            transportation_version,
                            timestamp,
                            status
                        )
                        VALUES ($1, $2, $3, $4, $5);
                    "#;
                    let insert = sqlx::query(insert_statement)
                        .bind(Uuid::new_v4())
                        .bind(entity_id)
                        .bind(transportation_version)
                        .bind(date)
                        .bind("pending_approval");

                    Executor::execute(&mut transaction, insert).await?;
                }
                Negotiation::IssuesDeclarationStagePassed { id, date, transportation_version, .. } => {
                    let update_statement = r#"
                        UPDATE negotiations
                        SET status = $2
                        WHERE id = $1;
                    "#;
                    let update = sqlx::query(update_statement)
                        .bind(entity_id)
                        .bind("pending_resolution");

                    Executor::execute(&mut transaction, update).await?;

                    let insert_statement = r#"
                        INSERT INTO negotiations_timeline (
                            stage_id,
                            negotiation_id,
                            transportation_version,
                            timestamp,
                            status
                        )
                        VALUES ($1, $2, $3, $4, $5);
                    "#;
                    let insert = sqlx::query(insert_statement)
                        .bind(id)
                        .bind(entity_id)
                        .bind(transportation_version)
                        .bind(date)
                        .bind("pending_resolution");

                    Executor::execute(&mut transaction, insert).await?;
                }
                Negotiation::TransportationIssueOpened { .. } => {}
                Negotiation::TransportationIssueReopened { .. } => {}
                Negotiation::WasteVolumeIssueOpened { .. } => {}
                Negotiation::WasteVolumeIssueReopened { .. } => {}
                Negotiation::CollectionStartTimeIssueOpened { .. } => {}
                Negotiation::CollectionStartTimeIssueReopened { .. } => {}
                Negotiation::CollectionPhotoIssueOpened { .. } => {}
                Negotiation::CollectionPhotoIssueReopened { .. } => {}
                Negotiation::IssuesResolutionStagePassed { id, date, transportation_version, .. } => {
                    let update_statement = r#"
                        UPDATE negotiations
                        SET status = $2
                        WHERE id = $1;
                    "#;
                    let update = sqlx::query(update_statement)
                        .bind(entity_id)
                        .bind("pending_approval");

                    Executor::execute(&mut transaction, update).await?;

                    let insert_statement = r#"
                        INSERT INTO negotiations_timeline (
                            stage_id,
                            negotiation_id,
                            transportation_version,
                            timestamp,
                            status
                        )
                        VALUES ($1, $2, $3, $4, $5);
                    "#;
                    let insert = sqlx::query(insert_statement)
                        .bind(id)
                        .bind(entity_id)
                        .bind(transportation_version)
                        .bind(date)
                        .bind("pending_approval");

                    Executor::execute(&mut transaction, insert).await?;
                }
                Negotiation::TransportationIssueResolved { .. } => {}
                Negotiation::TransportationIssueRejected { .. } => {}
                Negotiation::WasteVolumeIssueResolved { .. } => {}
                Negotiation::WasteVolumeIssueRejected { .. } => {}
                Negotiation::CollectionStartTimeIssueResolved { .. } => {}
                Negotiation::CollectionStartTimeIssueRejected { .. } => {}
                Negotiation::CollectionPhotoIssueResolved { .. } => {}
                Negotiation::CollectionPhotoIssueRejected { .. } => {}
                Negotiation::Completed { date, transportation_version, .. } => {
                    let update_statement = r#"
                        UPDATE negotiations
                        SET status = $2
                        WHERE id = $1;
                    "#;
                    let update = sqlx::query(update_statement)
                        .bind(entity_id)
                        .bind("completed");

                    Executor::execute(&mut transaction, update).await?;

                    let insert_statement = r#"
                        INSERT INTO negotiations_timeline (
                            stage_id,
                            negotiation_id,
                            transportation_version,
                            timestamp,
                            status
                        )
                        VALUES ($1, $2, $3, $4, $5);
                    "#;
                    let insert = sqlx::query(insert_statement)
                        .bind(Uuid::new_v4())
                        .bind(entity_id)
                        .bind(transportation_version)
                        .bind(date)
                        .bind("completed");

                    Executor::execute(&mut transaction, insert).await?;
                }
            }
        }

        transaction.commit().await?;

        Ok(())
    }

}