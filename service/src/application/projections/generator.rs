use sqlx::{Executor, Pool, Postgres, Row};
use sqlx::postgres::PgRow;
use uuid::Uuid;

use crate::application::projections::data::map_rows;
use crate::domain::{BusinessEntity, Entrepreneur, Generator};
use crate::application::projections::Collection;

#[derive(Clone)]
pub struct GeneratorProjection {
    pub connection_pool: Pool<Postgres>
}

pub struct GeneratorListItem {
    pub id: Uuid,
    pub name: String,
    pub inn: String,
    pub legal_entity_type: String,
    pub affiliates: Vec<Affiliate>
}

pub struct Affiliate {
    pub id: Uuid,
    pub name: String,
    pub inn: String,
    pub legal_entity_type: String
}

impl GeneratorProjection {
    pub async fn get_parental_generators(&self) -> Result<Vec<GeneratorListItem>, sqlx::Error> {
        match self.get_generators(&"".to_string(), &"".to_string(), &None, &100, &0).await {
            Ok(collection) => {
                // TODO (ilebedev): database query
                let parental: Vec<GeneratorListItem> = collection
                    .items
                    .into_iter()
                    .filter(|generator| generator.legal_entity_type.eq("BusinessEntity"))
                    .collect();
                Ok(parental)
            },
            Err(error) => Err(error)
        }
    }

    pub async fn get_generators(
        &self,
        inn: &String,
        name: &String,
        legal_entity_type: &Option<String>,
        limit: &i32,
        offset: &i32,
    ) -> Result<Collection<GeneratorListItem>, sqlx::Error> {
        fn generator_mapping(row: &PgRow) -> Result<GeneratorListItem, sqlx::Error> {
            Ok(GeneratorListItem {
                id: row.try_get("id")?,
                name: row.try_get("name")?,
                inn: row.try_get("inn")?,
                legal_entity_type: row.try_get("legal_entity_type")?,
                affiliates: vec![]
            })
        }

        fn affiliate_mapping(row: &PgRow) -> Result<Affiliate, sqlx::Error> {
            Ok(Affiliate {
                id: row.try_get("id")?,
                name: row.try_get("name")?,
                inn: row.try_get("inn")?,
                legal_entity_type: row.try_get("legal_entity_type")?,
            })
        }

        let query_statement = r#"
            SELECT
                id,
                name,
                inn,
                legal_entity_type
            FROM generators
            WHERE
                lower(inn) LIKE $1
                AND lower(name) LIKE $2
                AND legal_entity_type = coalesce($3, legal_entity_type)
        "#;

        let select_statement = format!("{} OFFSET $4 LIMIT $5", query_statement);

        let mut generators = sqlx::query(&select_statement)
            .bind(format!("%{}%", inn).to_lowercase())
            .bind(format!("%{}%", name).to_lowercase())
            .bind(legal_entity_type)
            .bind(offset)
            .bind(limit)
            .fetch_all(&self.connection_pool)
            .await
            .and_then(|rows| map_rows(rows, generator_mapping))?;

        let count_statement = format!("select count(*) from ({}) count", query_statement);
        let count: (i64,) = sqlx::query_as(&count_statement)
            .bind(format!("%{}%", inn).to_lowercase())
            .bind(format!("%{}%", name).to_lowercase())
            .bind(legal_entity_type)
            .fetch_one(&self.connection_pool)
            .await?;

        for generator in generators.iter_mut() {
            let mut affiliates = sqlx::query("SELECT * FROM generators WHERE parent_id = $1")
                .bind(generator.id)
                .fetch_all(&self.connection_pool)
                .await
                .and_then(|rows| map_rows(rows, affiliate_mapping))?;

            generator.affiliates.append(&mut affiliates);
        }

        Ok(Collection {
            items: generators,
            total: count.0
        })
    }

    pub async fn apply_generator_events(&self, entity_id: Uuid, events: &Vec<Generator>) {
        let projection = self.materialize_generator_view(entity_id, events).await;
        if let Err(error) = projection {
            error!("Unable to materialize generator view, {}", error);
        }
    }

    pub async fn apply_business_entity(&self, entity_id: Uuid, events: &Vec<BusinessEntity>) {
        let projection = self.materialize_business_entity_view(entity_id, events).await;
        if let Err(error) = projection {
            error!("Unable to materialize business entity view, {}", error);
        }
    }

    pub async fn apply_entrepreneur(&self, entity_id: Uuid, events: &Vec<Entrepreneur>) {
        let projection = self.materialize_entrepreneur_view(entity_id, events).await;
        if let Err(error) = projection {
            error!("Unable to materialize entrepreneur view, {}", error);
        }
    }

    async fn materialize_entrepreneur_view(&self, entity_id: Uuid, events: &Vec<Entrepreneur>) -> Result<(), sqlx::Error> {
        let connection_pool = &self.connection_pool;
        let mut transaction = connection_pool.begin().await?;

        for event in events {
            match event {
                Entrepreneur::PersonalNameUpdated { first_name, last_name, patronymic, .. } => {
                    let full_name = format!("{} {} {}", last_name, first_name, patronymic);
                    let update_statement = r#"
                        UPDATE generators
                        SET name = $2
                        WHERE legal_entity_id = $1
                    "#;
                    let update = sqlx::query(update_statement)
                        .bind(entity_id)
                        .bind(full_name);
                    Executor::execute(&mut transaction, update).await?;
                }
                Entrepreneur::TaxIdentificationUpdated { inn, .. } => {
                    let update_statement = r#"
                        UPDATE generators
                        SET inn = $2
                        WHERE legal_entity_id = $1
                    "#;
                    let update = sqlx::query(update_statement)
                        .bind(entity_id)
                        .bind(inn);
                    Executor::execute(&mut transaction, update).await?;
                },
                _ => {}
            }
        }

        transaction.commit().await
    }

    async fn materialize_business_entity_view(&self, entity_id: Uuid, events: &Vec<BusinessEntity>) -> Result<(), sqlx::Error> {
        let connection_pool = &self.connection_pool;
        let mut transaction = connection_pool.begin().await?;

        for event in events {
            match event {
                BusinessEntity::AdministrativeNameUpdated { short_name, .. } => {
                    let update_statement = r#"
                        UPDATE generators
                        SET name = $2
                        WHERE legal_entity_id = $1
                    "#;
                    let update = sqlx::query(update_statement)
                        .bind(entity_id)
                        .bind(short_name);
                    Executor::execute(&mut transaction, update).await?;
                }
                BusinessEntity::TaxIdentificationUpdated { inn, .. } => {
                    let update_statement = r#"
                        UPDATE generators
                        SET inn = $2
                        WHERE legal_entity_id = $1
                    "#;
                    let update = sqlx::query(update_statement)
                        .bind(entity_id)
                        .bind(inn);
                    Executor::execute(&mut transaction, update).await?;
                }
                _ => {}
            }
        }

        transaction.commit().await
    }

    async fn materialize_generator_view(&self, entity_id: Uuid, events: &Vec<Generator>) -> Result<(), sqlx::Error> {
        let connection_pool = &self.connection_pool;
        let mut transaction = connection_pool.begin().await?;

        for event in events {
            match event {
                Generator::Registered { .. } => {
                    let legal_entity_type = "Unassigned".to_string();
                    let legal_entity_id = Uuid::nil();
                    let name = entity_id.to_string();
                    let inn = "000000000000".to_string();
                    let insert_statement = r#"
                        INSERT INTO generators (id, legal_entity_type, legal_entity_id, name, inn)
                        VALUES ($1, $2, $3, $4, $5)
                    "#;
                    let insert = sqlx::query(insert_statement)
                        .bind(entity_id)
                        .bind(legal_entity_type)
                        .bind(legal_entity_id)
                        .bind(name)
                        .bind(inn);
                    Executor::execute(&mut transaction, insert).await?;
                }
                Generator::EntrepreneurAssigned { entrepreneur, .. } => {
                    let legal_entity_type = "Entrepreneur".to_string();
                    let legal_entity_id = entrepreneur;
                    let update_statement = r#"
                        UPDATE generators
                        SET legal_entity_type = $2, legal_entity_id = $3
                        WHERE id = $1
                    "#;
                    let update = sqlx::query(update_statement)
                        .bind(entity_id)
                        .bind(legal_entity_type)
                        .bind(legal_entity_id);
                    Executor::execute(&mut transaction, update).await?;
                }
                Generator::BusinessEntityAssigned { business_entity, .. } => {
                    let legal_entity_type = "BusinessEntity".to_string();
                    let legal_entity_id = business_entity;
                    let update_statement = r#"
                        UPDATE generators
                        SET legal_entity_type = $2, legal_entity_id = $3
                        WHERE id = $1
                    "#;
                    let update = sqlx::query(update_statement)
                        .bind(entity_id)
                        .bind(legal_entity_type)
                        .bind(legal_entity_id);
                    Executor::execute(&mut transaction, update).await?;
                }
                Generator::ParentUpdated { parent, .. } =>  {
                    let update_statement = r#"
                        UPDATE generators
                        SET parent_id = $2
                        WHERE id = $1
                    "#;
                    let update = sqlx::query(update_statement)
                        .bind(entity_id)
                        .bind(parent);
                    Executor::execute(&mut transaction, update).await?;
                }
            }
        }

        transaction.commit().await
    }
}