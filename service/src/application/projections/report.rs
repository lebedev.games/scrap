use sqlx::{Pool, Postgres, Executor, Row};
use chrono::{DateTime, Utc};
use uuid::Uuid;
use crate::application::projections::Collection;
use crate::domain::Report;
use sqlx::postgres::PgRow;
use crate::application::projections::data::map_rows;

pub struct ReportListItem {
    pub id: Uuid,
    pub target_transporter: String,
    pub target_month: DateTime<Utc>,
    pub transportations: usize,
    pub reporting_date: DateTime<Utc>
}

#[derive(Clone)]
pub struct ReportProjection {
    pub connection_pool: Pool<Postgres>
}

impl ReportProjection {

    pub async fn get_reports(
        &self,
        target_month: &Option<DateTime<Utc>>,
        target_transporter: &String,
        reporting_date: &Option<DateTime<Utc>>,
        limit: &i32,
        offset: &i32,
    ) -> Result<Collection<ReportListItem>, sqlx::Error> {
        fn mapping(row: &PgRow) -> Result<ReportListItem, sqlx::Error> {
            let transportations: Vec<Uuid> = row.try_get("transportations")?;
            Ok(ReportListItem {
                id: row.try_get("id")?,
                target_transporter: row.try_get("target_transporter")?,
                target_month: row.try_get("target_month")?,
                transportations: transportations.len(),
                reporting_date: row.try_get("reporting_date")?
            })
        }

        let query_statement = r#"
            SELECT
                id,
                target_transporter,
                target_month,
                transportations,
                reporting_date
            FROM reports
            WHERE
                lower(target_transporter) LIKE $1
                AND date_part('month', target_month) = date_part('month', coalesce($2, target_month))
                AND reporting_date::date = coalesce($3, reporting_date)::date
            ORDER BY entry_id
        "#;
        let select_statement = format!("{} OFFSET $4 LIMIT $5", query_statement);

        let negotiations = sqlx::query(&select_statement)
            .bind(format!("%{}%", target_transporter).to_lowercase())
            .bind(target_month)
            .bind(reporting_date)
            .bind(offset)
            .bind(limit)
            .fetch_all(&self.connection_pool)
            .await
            .and_then(|rows| map_rows(rows, mapping))?;


        let count_statement = format!("select count(*) from ({}) count", query_statement);
        let count: (i64, ) = sqlx::query_as(&count_statement)
            .bind(format!("%{}%", target_transporter).to_lowercase())
            .bind(target_month)
            .bind(reporting_date)
            .fetch_one(&self.connection_pool)
            .await?;

        Ok(Collection {
            items: negotiations,
            total: count.0,
        })
    }

    pub async fn apply_report_events(&self, entity_id: Uuid, events: &Vec<Report>) -> Result<(), sqlx::Error> {
        let connection_pool = &self.connection_pool;
        let mut transaction = connection_pool.begin().await?;

        for event in events {
            match event {
                Report::Created { date, .. } => {
                    let insert_statement = r#"
                        INSERT INTO reports (
                            id,
                            target_transporter,
                            target_month,
                            transportations,
                            reporting_date
                        )
                        VALUES ($1, $2, $3, $4, $5)
                    "#;
                    let insert = sqlx::query(insert_statement)
                        .bind(entity_id)
                        .bind("none!")
                        .bind(Utc::now())
                        .bind(Vec::<Uuid>::new())
                        .bind(date);

                    Executor::execute(&mut transaction, insert).await?;
                }
                Report::TargetSpecified { transporter, month } => {
                    let update_statement = r#"
                        UPDATE reports
                        SET target_month = $3, target_transporter = (
                            SELECT DISTINCT transporters_legal_entities.name
                            FROM transporters
                            LEFT JOIN transporters_legal_entities ON transporters_legal_entities.legal_entity_id = transporters.legal_entity_id
                            WHERE transporters.id = $2
                        )
                        WHERE id = $1
                    "#;
                    let update = sqlx::query(update_statement)
                        .bind(entity_id)
                        .bind(transporter)
                        .bind(month);

                    Executor::execute(&mut transaction, update).await?;
                }
                Report::TransportationCovered { transportations } => {
                    let update_statement = r#"
                        UPDATE reports
                        SET transportations = $2
                        WHERE id = $1;
                    "#;
                    let update = sqlx::query(update_statement)
                        .bind(entity_id)
                        .bind(transportations);

                    Executor::execute(&mut transaction, update).await?;
                }
            }
        }

        transaction.commit().await?;

        Ok(())
    }

}