use sqlx::{Executor, Pool, Postgres, Row};
use sqlx::postgres::PgRow;
use uuid::Uuid;

use crate::application::inspect_mapping_errors;
use crate::domain::User;
use crate::model::UserListItem;

#[derive(Clone)]
pub struct UserProjection {
    pub connection_pool: Pool<Postgres>
}

pub struct UserAuthorizationTarget {
    pub user_name: String,
    pub user_id: Uuid,
    pub user_role: String
}

impl UserProjection {
    pub async fn apply(&self, entity_id: &Uuid, events: &Vec<User>) {
        let projection = self.materialize_user_view(entity_id.clone(), events).await;
        if let Err(error) = projection {
            error!("Unable to materialize user view, {}", error);
        }
    }

    pub async fn get_user_by_login(&self, login: &String, password: &String) -> Result<UserAuthorizationTarget, sqlx::Error> {
        fn mapping(row: &PgRow) -> Result<UserAuthorizationTarget, sqlx::Error> {
            Ok(UserAuthorizationTarget {
                user_name: row.try_get("name")?,
                user_id: row.try_get("id")?,
                user_role: row.try_get("role")?,
            })
        }

        let row = sqlx::query("SELECT id, role, name FROM users WHERE login = $1 AND password = $2")
            .bind(login)
            .bind(password)
            .fetch_one(&self.connection_pool)
            .await?;

        Ok(mapping(&row)?)
    }

    pub async fn get_users(&self) -> Result<Vec<UserListItem>, sqlx::Error> {
        fn map_user_list_item(row: &PgRow) -> Result<UserListItem, sqlx::Error> {
            Ok(UserListItem {
                id: row.try_get("id")?,
                permissions: row.try_get("permissions")?,
            })
        }

        fn map_users_gracefully(rows: Vec<PgRow>) -> Vec<UserListItem> {
            rows.iter()
                .map(map_user_list_item)
                .inspect(inspect_mapping_errors)
                .filter_map(Result::ok)
                .collect()
        }

        sqlx::query("SELECT id, permissions FROM users")
            .fetch_all(&self.connection_pool)
            .await
            .map(map_users_gracefully)
    }

    async fn materialize_user_view(&self, entity_id: Uuid, events: &Vec<User>) -> Result<(), sqlx::Error> {
        let connection_pool = &self.connection_pool;
        let mut transaction = connection_pool.begin().await?;

        for event in events {
            match event {
                User::Registered { .. } => {
                    let insert_statement = r#"
                        INSERT INTO users (id, permissions, role, name)
                        VALUES ($1, $2, $3, $4)
                    "#;
                    let insert = sqlx::query(insert_statement)
                        .bind(entity_id)
                        .bind(Vec::<String>::new())
                        .bind("Unspecified")
                        .bind(entity_id.to_string());
                    Executor::execute(&mut transaction, insert).await?;
                }
                User::PermissionGranted { permission, .. } => {
                    let update_statement = r#"
                        UPDATE users
                        SET permissions = array_append(permissions, $2::varchar)
                        WHERE id = $1
                    "#;
                    let update = sqlx::query(update_statement)
                        .bind(entity_id)
                        .bind(permission);
                    Executor::execute(&mut transaction, update).await?;
                }
                User::Deleted {..} => {
                    let delete_statement = r#"
                        DELETE FROM users WHERE id = $1
                    "#;
                    let delete = sqlx::query(delete_statement)
                        .bind(entity_id);
                    Executor::execute(&mut transaction, delete).await?;
                }
                User::RoleSpecified { role, .. } => {
                    let update_statement = r#"
                        UPDATE users
                        SET role = $2
                        WHERE id = $1
                    "#;
                    let update = sqlx::query(update_statement)
                        .bind(entity_id)
                        .bind(role);
                    Executor::execute(&mut transaction, update).await?;
                }
                User::NameSpecified { name, .. } => {
                    let update_statement = r#"
                        UPDATE users
                        SET name = $2
                        WHERE id = $1
                    "#;
                    let update = sqlx::query(update_statement)
                        .bind(entity_id)
                        .bind(name);
                    Executor::execute(&mut transaction, update).await?;
                }
                User::PasswordCredentialsSpecified { login, password, .. } => {
                    let update_statement = r#"
                        UPDATE users
                        SET login = $2, password = $3
                        WHERE id = $1
                    "#;
                    let update = sqlx::query(update_statement)
                        .bind(entity_id)
                        .bind(login)
                        .bind(password);
                    Executor::execute(&mut transaction, update).await?;
                }
                User::RoleEntityAssigned { entity_id: role_entity_id, .. } => {
                    let update_statement = r#"
                        UPDATE users
                        SET role_entity_id = $2
                        WHERE id = $1
                    "#;
                    let update = sqlx::query(update_statement)
                        .bind(entity_id)
                        .bind(role_entity_id);
                    Executor::execute(&mut transaction, update).await?;
                }
            }
        }

        transaction.commit().await
    }
}