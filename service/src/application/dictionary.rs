use sqlx::{Pool, Postgres, Row};
use sqlx::postgres::PgRow;

use crate::application::inspect_mapping_errors;
use crate::model::SelectOption;

#[derive(Clone)]
pub struct Dictionary {
    pub connection_pool: Pool<Postgres>
}

fn map_record(row: &PgRow) -> Result<SelectOption, sqlx::Error> {
    Ok(SelectOption {
        id: row.try_get("id")?,
        name: row.try_get("name")?,
    })
}

fn map_records_gracefully(rows: Vec<PgRow>) -> Vec<SelectOption> {
    rows.iter()
        .map(map_record)
        .inspect(inspect_mapping_errors)
        .filter_map(Result::ok)
        .collect()
}

impl Dictionary {

    pub async fn get_accrual_method_records(&self) -> Vec<SelectOption> {
        if let Ok(records) = sqlx::query("SELECT id, method_name as name FROM dictionaries.accrual_methods")
            .fetch_all(&self.connection_pool)
            .await
            .map(map_records_gracefully) {
            records
        } else {
            vec![]
        }
    }

    pub async fn get_container_type_records(&self) -> Vec<SelectOption> {
        if let Ok(records) = sqlx::query("SELECT id, type_name as name FROM dictionaries.container_types")
            .fetch_all(&self.connection_pool)
            .await
            .map(map_records_gracefully) {
            records
        } else {
            vec![]
        }
    }

    pub async fn get_covering_type_records(&self) -> Vec<SelectOption> {
        if let Ok(records) = sqlx::query("SELECT id, type_name as name FROM dictionaries.covering_types")
            .fetch_all(&self.connection_pool)
            .await
            .map(map_records_gracefully) {
            records
        } else {
            vec![]
        }
    }

    pub async fn get_fence_material_records(&self) -> Vec<SelectOption> {
        if let Ok(records) = sqlx::query("SELECT id, material_name as name FROM dictionaries.fence_materials")
            .fetch_all(&self.connection_pool)
            .await
            .map(map_records_gracefully) {
            records
        } else {
            vec![]
        }
    }

    pub async fn get_measurement_unit_records(&self) -> Vec<SelectOption> {
        if let Ok(records) = sqlx::query("SELECT id, unit_name as name FROM dictionaries.measurement_units")
            .fetch_all(&self.connection_pool)
            .await
            .map(map_records_gracefully) {
            records
        } else {
            vec![]
        }
    }

    pub async fn get_operational_status_records(&self) -> Vec<SelectOption> {
        if let Ok(records) = sqlx::query("SELECT id, status_name as name FROM dictionaries.operational_statuses")
            .fetch_all(&self.connection_pool)
            .await
            .map(map_records_gracefully) {
            records
        } else {
            vec![]
        }
    }

    pub async fn get_organization_legal_type_records(&self) -> Vec<SelectOption> {
        if let Ok(records) = sqlx::query("SELECT id, type_name as name FROM dictionaries.organization_legal_types")
            .fetch_all(&self.connection_pool)
            .await
            .map(map_records_gracefully) {
            records
        } else {
            vec![]
        }
    }

    pub async fn get_waste_type_records(&self) -> Vec<SelectOption> {
        if let Ok(records) = sqlx::query("SELECT id, type_name as name FROM dictionaries.waste_types")
            .fetch_all(&self.connection_pool)
            .await
            .map(map_records_gracefully) {
            records
        } else {
            vec![]
        }
    }

    pub async fn get_truck_fleet_type_records(&self) -> Vec<SelectOption> {
        if let Ok(records) = sqlx::query("SELECT id, type_name as name FROM dictionaries.truck_fleet_types")
            .fetch_all(&self.connection_pool)
            .await
            .map(map_records_gracefully) {
            records
        } else {
            vec![]
        }
    }

    pub async fn get_cargo_body_type_records(&self) -> Vec<SelectOption> {
        if let Ok(records) = sqlx::query("SELECT id, type_name as name FROM dictionaries.cargo_body_types")
            .fetch_all(&self.connection_pool)
            .await
            .map(map_records_gracefully) {
            records
        } else {
            vec![]
        }
    }

    pub async fn get_driver_license_category_records(&self) -> Vec<SelectOption> {
        if let Ok(records) = sqlx::query("SELECT id, category_name as name FROM dictionaries.driver_license_categories")
            .fetch_all(&self.connection_pool)
            .await
            .map(map_records_gracefully) {
            records
        } else {
            vec![]
        }
    }

    pub async fn get_axle_configuration_records(&self) -> Vec<SelectOption> {
        if let Ok(records) = sqlx::query("SELECT id, arrangement as name FROM dictionaries.axle_configurations")
            .fetch_all(&self.connection_pool)
            .await
            .map(map_records_gracefully) {
            records
        } else {
            vec![]
        }
    }

}