use std::any::type_name;
use std::cmp::Ordering;
use std::collections::BTreeSet;
use std::ops::Deref;
use std::sync::Arc;

use sqlx::{Executor, Pool, Postgres, Row};
use uuid::Uuid;

use service_macro::Persisted;

pub fn inspect_mapping_errors<T>(result: &Result<T, sqlx::Error>) {
    if let Err(error) = result {
        error!("Unable to parse row into {}, {}", type_name::<T>(), error);
    }
}

#[derive(Clone)]
pub struct EventStore {
    pub connection_pool: Pool<Postgres>
}


pub struct Record<T: Persisted> {
    pub version: i32,
    pub timestamp: i64,
    pub event: T,
}

impl<T: Persisted> Eq for Record<T> {}

impl<T: Persisted> Ord for Record<T> {
    fn cmp(&self, other: &Self) -> Ordering {
        self.timestamp.cmp(&other.timestamp)
    }
}

impl<T: Persisted> PartialOrd for Record<T> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl<T: Persisted> PartialEq for Record<T> {
    fn eq(&self, other: &Self) -> bool {
        self.timestamp == other.timestamp
    }
}

#[derive(Debug)]
pub enum Version {
    Initial,
    Increment(i32),
}


#[derive(Debug)]
pub struct Aggregate<T> {
    pub entity_id: Uuid,
    pub entity_type: String,
    pub version: Version,
    state: Arc<T>,
}

impl<T> Aggregate<T> {
    pub fn new(state: T, entity_id: Uuid, version: Version) -> Self {
        Aggregate {
            entity_id,
            version,
            entity_type: type_name::<T>().into(),
            state: Arc::new(state),
        }
    }
}

impl<T> Deref for Aggregate<T> {
    type Target = Arc<T>;

    fn deref(&self) -> &Arc<T> {
        &self.state
    }
}

impl EventStore {
    pub async fn replay<T, R, F>(&self, entity_id: Uuid, mut apply: F) -> Result<Aggregate<R>, sqlx::Error>
        where
            T: Persisted,
            R: Default,
            F: FnMut(R, T) -> R,
    {
        let loading = self.load(entity_id).await;
        match loading {
            Ok(records) => {
                let entity = (R::default(), 0);
                let (state, version) = records
                    .into_iter()
                    .fold(entity, |entity, record| (apply(entity.0, record.event), record.version));
                Ok(Aggregate::new(state, entity_id, Version::Increment(version)))
            }
            Err(error) => Err(error)
        }
    }

    pub async fn store<T: Persisted>(&self, publisher: &Uuid, entity_id: &Uuid, events: &Vec<T>, version: &Version) -> Result<i32, sqlx::Error> {
        let version = match version {
            Version::Initial => 0,
            Version::Increment(current) => current + 1
        };

        let source = type_name::<T>();

        let mut transaction = self.connection_pool.begin().await?;

        let insert_statement = r#"
            INSERT INTO entities (id, source, version, publisher)
            VALUES ($1, $2, $3, $4)
            RETURNING entry_id
        "#;
        let statement = sqlx::query(insert_statement)
            .bind(entity_id)
            .bind(source)
            .bind(version)
            .bind(publisher);

        let entity_entry_id: i64 = Executor::fetch_one(&mut transaction, statement)
            .await?
            .try_get("entry_id")?;

        for event in events {
            Executor::execute(&mut transaction, event.persist(entity_entry_id)).await?;
        }

        transaction.commit().await?;

        Ok(version)
    }

    pub async fn load_by_version<T: Persisted>(&self, entity_id: Uuid, version: i32) -> Result<BTreeSet<Record<T>>, sqlx::Error> {
        let source = type_name::<T>();
        let select_statement = r#"
            SELECT * FROM entities
            WHERE id = $1 AND source = $2 AND version = $3
            ORDER BY version DESC LIMIT 1
        "#;
        let entity_row = sqlx::query(select_statement)
            .bind(entity_id)
            .bind(source)
            .bind(version)
            .fetch_one(&self.connection_pool)
            .await?;

        let current_state_entry: i64 = entity_row.try_get("entry_id")?;

        self.load_from_entry(entity_id, current_state_entry).await
    }

    pub async fn load<T: Persisted>(&self, entity_id: Uuid) -> Result<BTreeSet<Record<T>>, sqlx::Error> {
        let source = type_name::<T>();
        let select_statement = r#"
            SELECT * FROM entities
            WHERE id = $1 AND source = $2
            ORDER BY version DESC LIMIT 1
        "#;
        let entity_row = sqlx::query(select_statement)
            .bind(entity_id)
            .bind(source)
            .fetch_one(&self.connection_pool)
            .await?;

        let current_state_entry: i64 = entity_row.try_get("entry_id")?;

        self.load_from_entry(entity_id, current_state_entry).await
    }

    async fn load_from_entry<T: Persisted>(&self, entity_id: Uuid, current_state_entry: i64) -> Result<BTreeSet<Record<T>>, sqlx::Error> {
        let mut timeline = BTreeSet::new();

        for (table, parser) in T::loaders() {
            let select_statement = format!(r#"
                SELECT {0}.*, version, timestamp FROM {0}
                JOIN entities ON entities.entry_id = {0}.entity_entry_id
                WHERE entities.id = $1 AND entities.entry_id <= $2
            "#, table);
            let records = sqlx::query(&select_statement)
                .bind(entity_id)
                .bind(current_state_entry)
                .fetch_all(&self.connection_pool)
                .await?;
            for row in records {
                let record = Record {
                    version: row.try_get("version")?,
                    timestamp: row.try_get("timestamp")?,
                    event: (parser)(row)?,
                };
                timeline.insert(record);
            }
        }

        Ok(timeline)
    }
}
