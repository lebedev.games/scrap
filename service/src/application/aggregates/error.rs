use uuid::Uuid;

#[derive(Debug)]
pub enum ReplayError {
    EntityNotFound(Uuid),
    ParsingError(sqlx::Error),
}

#[derive(Debug)]
pub enum PublishError {
    EventStorageError(sqlx::Error)
}