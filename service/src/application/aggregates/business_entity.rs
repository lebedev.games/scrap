use uuid::Uuid;

use crate::application::aggregates::{BankDetailsAggregate, ReplayError};
use crate::application::EventStore;
use crate::domain::BusinessEntity;

pub struct ResponsiblePerson {
    pub full_name: String,
    pub phone: String,
}

pub struct BusinessEntityAggregate {
    pub id: Uuid,
    pub version: i32,
    pub full_name: String,
    pub short_name: String,
    pub inn: String,
    pub kpp: String,
    pub ogrn: String,
    pub phone: String,
    pub email: Option<String>,
    pub address: String,
    pub postal_address: String,
    pub actual_address: String,
    pub responsible_persons: Vec<ResponsiblePerson>,
    pub bank_details: BankDetailsAggregate,
}

impl Default for BusinessEntityAggregate {
    fn default() -> Self {
        BusinessEntityAggregate {
            id: Uuid::nil(),
            version: 0,
            full_name: String::default(),
            short_name: String::default(),
            inn: String::default(),
            kpp: String::default(),
            ogrn: String::default(),
            phone: String::default(),
            email: None,
            address: String::default(),
            postal_address: String::default(),
            actual_address: String::default(),
            responsible_persons: vec![],
            bank_details: BankDetailsAggregate::default(),
        }
    }
}

impl BusinessEntityAggregate {
    pub async fn replay(id: &Uuid, store: &EventStore) -> Result<Self, ReplayError> {
        let loading = store.load(id.clone()).await;
        match loading {
            Ok(records) => {
                let mut entity = BusinessEntityAggregate::default();
                for record in records {
                    entity.apply(&record.event);
                    entity.version = record.version;
                }
                entity.id = id.clone();
                Ok(entity)
            }
            Err(error) => match error {
                sqlx::Error::RowNotFound => Err(ReplayError::EntityNotFound(id.clone())),
                _ => Err(ReplayError::ParsingError(error))
            }
        }
    }

    pub(crate) fn apply(&mut self, event: &BusinessEntity) {
        match event {
            BusinessEntity::Registered { .. } => {}
            BusinessEntity::AdministrativeNameUpdated { full_name, short_name, .. } => {
                self.full_name = full_name.clone();
                self.short_name = short_name.clone();
            }
            BusinessEntity::TaxIdentificationUpdated { inn, .. } => {
                self.inn = inn.clone();
            }
            BusinessEntity::TaxRegistrationUpdated { kpp, .. } => {
                self.kpp = kpp.clone();
            }
            BusinessEntity::PrimaryRegistrationUpdated { ogrn, .. } => {
                self.ogrn = ogrn.clone();
            }
            BusinessEntity::PhoneContactUpdated { phone, .. } => {
                self.phone = phone.clone();
            }
            BusinessEntity::EmailContactUpdated { email, .. } => {
                self.email = Some(email.clone());
            }
            BusinessEntity::AddressUpdated { full_address, .. } => {
                self.address = full_address.clone();
            }
            BusinessEntity::PostalAddressUpdated { full_address, .. } => {
                self.actual_address = full_address.clone();
            }
            BusinessEntity::ActualAddressUpdated { full_address, .. } => {
                self.actual_address = full_address.clone();
            }
            BusinessEntity::ResponsiblePersonAdded { full_name, phone, .. } => {
                self.responsible_persons.push(
                    ResponsiblePerson {
                        full_name: full_name.clone(),
                        phone: phone.clone()
                    }
                )
            }
            BusinessEntity::BankDetailsUpdated {
                account,
                correspondent_account,
                bank_name,
                rcbic,
                ..
            } => {
                self.bank_details = BankDetailsAggregate {
                    account: account.clone(),
                    correspondent_account: correspondent_account.clone(),
                    bank_name: bank_name.clone(),
                    rcbic: rcbic.clone(),
                }
            }
        }
    }
}

