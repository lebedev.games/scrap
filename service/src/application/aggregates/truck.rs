use rust_decimal::Decimal;
use uuid::Uuid;

use crate::application::aggregates::ReplayError;
use crate::application::EventStore;
use crate::domain::Truck;

pub struct TruckAggregate {
    pub id: Uuid,
    pub version: i32,
    pub owner: String,
    pub transporter: Uuid,
    pub fleet_type: Uuid,
    pub vin: String,
    pub registration_number: String,
    pub garaging_number: String,
    pub vehicle_model: String,
    pub waste_equipment_model: String,
    pub cargo_body_type: Uuid,
    pub required_driver_license_category: Uuid,
    pub axle_configuration: Uuid,
    pub production_year: i32,
    pub navigation_equipment_name: String,
    pub empty_weight_kg: Decimal,
    pub cargo_capacity_kg: Decimal,
}

impl Default for TruckAggregate {
    fn default() -> Self {
        TruckAggregate {
            id: Default::default(),
            version: 0,
            owner: "".to_string(),
            transporter: Default::default(),
            fleet_type: Default::default(),
            vin: "".to_string(),
            registration_number: "".to_string(),
            garaging_number: "".to_string(),
            vehicle_model: "".to_string(),
            waste_equipment_model: "".to_string(),
            cargo_body_type: Default::default(),
            required_driver_license_category: Default::default(),
            axle_configuration: Default::default(),
            production_year: 0,
            navigation_equipment_name: "".to_string(),
            empty_weight_kg: Default::default(),
            cargo_capacity_kg: Default::default(),
        }
    }
}

impl TruckAggregate {
    pub async fn replay(id: &Uuid, store: &EventStore) -> Result<Self, ReplayError> {
        let loading = store.load(id.clone()).await;
        match loading {
            Ok(records) => {
                let mut entity = TruckAggregate::default();
                for record in records {
                    entity.apply(&record.event);
                    entity.version = record.version;
                }
                entity.id = id.clone();
                Ok(entity)
            }
            Err(error) => match error {
                sqlx::Error::RowNotFound => Err(ReplayError::EntityNotFound(id.clone())),
                _ => Err(ReplayError::ParsingError(error))
            }
        }
    }

    fn apply(&mut self, event: &Truck) {
        match event {
            Truck::Registered { .. } => {}
            Truck::OwnerChanged { owner, .. } => {
                self.owner = owner.clone();
            }
            Truck::TransporterAssigned { transporter, .. } => {
                self.transporter = transporter.clone();
            }
            Truck::FleetTypeChanged { fleet_type, .. } => {
                self.fleet_type = fleet_type.clone();
            }
            Truck::VinChanged { vin, .. } => {
                self.vin = vin.clone();
            }
            Truck::RegistrationNumberChanged { registration_number, .. } => {
                self.registration_number = registration_number.clone();
            }
            Truck::GaragingNumberChanged { garaging_number, .. } => {
                self.garaging_number = garaging_number.clone();
            }
            Truck::VehicleModelChanged { vehicle_model, .. } => {
                self.vehicle_model = vehicle_model.clone();
            }
            Truck::WasteEquipmentModelChanged { waste_equipment_model, .. } => {
                self.waste_equipment_model = waste_equipment_model.clone();
            }
            Truck::CargoBodyTypeChanged { cargo_body_type, .. } => {
                self.cargo_body_type = cargo_body_type.clone();
            }
            Truck::RequiredDriverLicenseChanged { category, .. } => {
                self.required_driver_license_category = category.clone();
            }
            Truck::AxleConfigurationChanged { axle_configuration, .. } => {
                self.axle_configuration = axle_configuration.clone();
            }
            Truck::ProductionYearChanged { production_year, .. } => {
                self.production_year = production_year.clone();
            }
            Truck::NavigationEquipmentNameChanged { navigation_equipment_name, .. } => {
                self.navigation_equipment_name = navigation_equipment_name.clone();
            }
            Truck::EmptyWeightChanged { empty_weight_kg, .. } => {
                self.empty_weight_kg = empty_weight_kg.clone();
            }
            Truck::CargoCapacityChanged { cargo_capacity_kg, .. } => {
                self.cargo_capacity_kg = cargo_capacity_kg.clone();
            }
        }
    }
}