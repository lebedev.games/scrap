use chrono::{DateTime, Utc};
use rust_decimal::Decimal;
use uuid::Uuid;

use crate::application::aggregates::ReplayError;
use crate::application::EventStore;
use crate::domain::GenerationPoint;

pub struct GenerationPointAggregateContainer {
    pub container_type: Uuid,
}

pub struct GenerationPointAggregateTransporter {
    pub transporter: Uuid,
    pub start_transportation_date: DateTime<Utc>,
    pub end_transportation_date: DateTime<Utc>,
}

impl Default for GenerationPointAggregateTransporter {
    fn default() -> Self {
        GenerationPointAggregateTransporter {
            transporter: Uuid::nil(),
            start_transportation_date: Utc::now(),
            end_transportation_date: Utc::now(),
        }
    }
}

pub struct GenerationPointAggregate {
    pub id: Uuid,
    pub version: i32,
    pub internal_system_number: String,
    pub external_registry_number: String,
    pub waste_type: Uuid,
    pub organization_legal_type: Uuid,
    pub operational_status: Uuid,
    pub full_address: String,
    pub coordinates: String,
    pub area_m2: Decimal,
    pub covering_type: Uuid,
    pub fence_material: Uuid,
    pub service_company_name: Option<String>,
    pub generator: Uuid,
    pub description: String,
    pub containers: Vec<GenerationPointAggregateContainer>,
    pub transporter: GenerationPointAggregateTransporter,
    pub photos: Vec<Uuid>,
    pub accrual_method: Uuid,
    pub measurement_unit: Uuid,
    pub transportation_frequency: String,
}

impl Default for GenerationPointAggregate {
    fn default() -> Self {
        GenerationPointAggregate {
            id: Uuid::nil(),
            version: 0,
            internal_system_number: String::default(),
            external_registry_number: String::default(),
            waste_type: Uuid::nil(),
            organization_legal_type: Uuid::nil(),
            operational_status: Uuid::nil(),
            full_address: String::default(),
            coordinates: String::default(),
            area_m2: Decimal::from(0),
            covering_type: Uuid::nil(),
            fence_material: Uuid::nil(),
            service_company_name: None,
            generator: Uuid::nil(),
            description: String::default(),
            containers: vec![],
            transporter: GenerationPointAggregateTransporter::default(),
            photos: vec![],
            accrual_method: Uuid::nil(),
            measurement_unit: Uuid::nil(),
            transportation_frequency: String::default(),
        }
    }
}

impl GenerationPointAggregate {
    pub async fn replay(id: &Uuid, store: &EventStore) -> Result<Self, ReplayError> {
        let loading = store.load(id.clone()).await;
        match loading {
            Ok(records) => {
                let mut entity = GenerationPointAggregate::default();
                for record in records {
                    entity.apply(record.event);
                    entity.version = record.version;
                }
                entity.id = id.clone();
                Ok(entity)
            }
            Err(error) => match error {
                sqlx::Error::RowNotFound => Err(ReplayError::EntityNotFound(id.clone())),
                _ => Err(ReplayError::ParsingError(error))
            }
        }
    }

    fn apply(&mut self, event: GenerationPoint) {
        match event {
            GenerationPoint::Registered { .. } => {}
            GenerationPoint::InternalIdentificationChanged { system_number, .. } => {
                self.internal_system_number = system_number;
            }
            GenerationPoint::ExternalRegistrationChanged { registry_number, .. } => {
                self.external_registry_number = registry_number;
            }
            GenerationPoint::WasteTypeChanged { waste_type, .. } => {
                self.waste_type = waste_type;
            }
            GenerationPoint::OrganizationLegalTypeChanged { organization_legal_type, .. } => {
                self.organization_legal_type = organization_legal_type;
            }
            GenerationPoint::OperationalStatusChanged { status, .. } => {
                self.operational_status = status;
            }
            GenerationPoint::AddressChanged { full_address, .. } => {
                self.full_address = full_address;
            }
            GenerationPoint::CoordinatesChanged { coordinates, .. } => {
                self.coordinates = coordinates;
            }
            GenerationPoint::AreaChanged { area_m2, .. } => {
                self.area_m2 = area_m2;
            }
            GenerationPoint::CoveringTypeChanged { covering_type, .. } => {
                self.covering_type = covering_type;
            }
            GenerationPoint::FenceMaterialChanged { fence_material, .. } => {
                self.fence_material = fence_material;
            }
            GenerationPoint::ServiceCompanyChanged { service_company_name, .. } => {
                self.service_company_name = Some(service_company_name);
            }
            GenerationPoint::GeneratorAssigned { generator, .. } => {
                self.generator = generator;
            }
            GenerationPoint::DescriptionChanged { description, .. } => {
                self.description = description;
            }
            GenerationPoint::ContainerAdded { container_type, .. } => {
                self.containers.push(GenerationPointAggregateContainer {
                    container_type
                })
            }
            GenerationPoint::ContainerRemoved { container_type, .. } => {
                if let Some(index) = self.containers.iter().position(
                    |container| container.container_type == container_type
                ) {
                    self.containers.remove(index);
                }
            }
            GenerationPoint::TransporterAssigned { transporter, start_transportation_date, end_transportation_date, .. } => {
                self.transporter.transporter = transporter;
                self.transporter.start_transportation_date = start_transportation_date;
                self.transporter.end_transportation_date = end_transportation_date;
            }
            GenerationPoint::TransportationFrequencyChanged { frequency, .. } => {
                self.transportation_frequency = frequency;
            }
            GenerationPoint::PhotosChanged { photos, .. } => {
                self.photos = photos;
            }
            GenerationPoint::AccrualMethodChanged { method, .. } => {
                self.accrual_method = method;
            }
            GenerationPoint::MeasurementUnitChanged { unit, .. } => {
                self.measurement_unit = unit;
            }
        }
    }
}