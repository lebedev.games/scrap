use uuid::Uuid;
use crate::application::EventStore;
use crate::application::aggregates::ReplayError;
use crate::domain::Report;
use chrono::{DateTime, Utc};

pub struct ReportAggregate {
    pub id: Uuid,
    pub version: i32,
    pub target_transporter: Uuid,
    pub target_month: DateTime<Utc>,
    pub transportations: Vec<Uuid>,
    pub reporting_date: DateTime<Utc>,
}

impl Default for ReportAggregate {
    fn default() -> Self {
        ReportAggregate {
            id: Default::default(),
            version: 0,
            target_transporter: Default::default(),
            target_month: Utc::now(),
            transportations: vec![],
            reporting_date: Utc::now(),
        }
    }
}

impl ReportAggregate {
    pub async fn replay(id: &Uuid, store: &EventStore) -> Result<Self, ReplayError> {
        let loading = store.load(id.clone()).await;
        match loading {
            Ok(records) => {
                let mut entity = ReportAggregate::default();
                for record in records {
                    entity.apply(record.event);
                    entity.version = record.version;
                }
                entity.id = id.clone();
                Ok(entity)
            }
            Err(error) => match error {
                sqlx::Error::RowNotFound => Err(ReplayError::EntityNotFound(id.clone())),
                _ => Err(ReplayError::ParsingError(error))
            }
        }
    }

    pub fn apply(&mut self, event: Report) {
        match event {
            Report::Created { date, .. } => {
                self.reporting_date = date;
            }
            Report::TransportationCovered { transportations } => {
                self.transportations = transportations;
            }
            Report::TargetSpecified { month, transporter } => {
                self.target_month = month;
                self.target_transporter = transporter;
            }
        }
    }
}