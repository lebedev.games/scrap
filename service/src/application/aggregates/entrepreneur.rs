use uuid::Uuid;

use crate::application::aggregates::{BankDetailsAggregate, ReplayError};
use crate::application::EventStore;
use crate::domain::Entrepreneur;

pub struct EntrepreneurAggregate {
    pub id: Uuid,
    pub version: i32,
    pub first_name: String,
    pub last_name: String,
    pub patronymic: String,
    pub inn: String,
    pub ogrnip: String,
    pub phone: String,
    pub email: Option<String>,
    pub address: String,
    pub actual_address: String,
    pub bank_details: BankDetailsAggregate,
}

impl Default for EntrepreneurAggregate {
    fn default() -> Self {
        EntrepreneurAggregate {
            id: Uuid::nil(),
            version: 0,
            first_name: String::default(),
            last_name: String::default(),
            patronymic: String::default(),
            inn: String::default(),
            ogrnip: String::default(),
            phone: String::default(),
            email: None,
            address: String::default(),
            actual_address: String::default(),
            bank_details: BankDetailsAggregate::default(),
        }
    }
}

impl EntrepreneurAggregate {
    pub async fn replay(id: &Uuid, store: &EventStore) -> Result<Self, ReplayError> {
        let loading = store.load(id.clone()).await;
        match loading {
            Ok(records) => {
                let mut entity = EntrepreneurAggregate::default();
                for record in records {
                    entity.apply(&record.event);
                    entity.version = record.version;
                }
                entity.id = id.clone();
                Ok(entity)
            }
            Err(error) => match error {
                sqlx::Error::RowNotFound => Err(ReplayError::EntityNotFound(id.clone())),
                _ => Err(ReplayError::ParsingError(error))
            }
        }
    }

    pub(crate) fn apply(&mut self, event: &Entrepreneur) {
        match event {
            Entrepreneur::Registered { .. } => {}
            Entrepreneur::PersonalNameUpdated { first_name, last_name, patronymic, .. } => {
                self.first_name = first_name.clone();
                self.last_name = last_name.clone();
                self.patronymic = patronymic.clone();
            }
            Entrepreneur::TaxIdentificationUpdated { inn, .. } => {
                self.inn = inn.clone();
            }
            Entrepreneur::PrimaryRegistrationUpdated { ogrnip, .. } => {
                self.ogrnip = ogrnip.clone();
            }
            Entrepreneur::PhoneContactUpdated { phone, .. } => {
                self.phone = phone.clone();
            }
            Entrepreneur::EmailContactUpdated { email, .. } => {
                self.email = Some(email.clone());
            }
            Entrepreneur::AddressUpdated { full_address, .. } => {
                self.address = full_address.clone();
            }
            Entrepreneur::ActualAddressUpdated { full_address, .. } => {
                self.actual_address = full_address.clone();
            }
            Entrepreneur::BankDetailsUpdated {
                account,
                correspondent_account,
                bank_name,
                rcbic,
                ..
            } => {
                self.bank_details = BankDetailsAggregate {
                    account: account.clone(),
                    correspondent_account: correspondent_account.clone(),
                    bank_name: bank_name.clone(),
                    rcbic: rcbic.clone(),
                }
            }
        }
    }
}
