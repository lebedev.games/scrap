use chrono::{DateTime, Utc};
use rust_decimal::Decimal;
use uuid::Uuid;

use service_macro::Persisted;

use crate::application::{EventStore, Version};
use crate::application::aggregates::{PublishError, ReplayError};
use crate::domain::Transportation;
use std::collections::HashMap;

pub struct TransportationDraft {
    pub id: Uuid,
    pub version: i32,
    pub submitted: bool,
    pub deleted: bool,
    pub truck: Uuid,
    pub execution_date: DateTime<Utc>,
    pub destinations: Vec<Destination>,
    pub waste_collections: Vec<WasteCollection>,
}

#[derive(Clone)]
pub struct Destination {
    pub id: Uuid,
    pub collection_start_time: DateTime<Utc>,
    pub waste_generation_point: Uuid,
}

pub struct WasteCollection {
    pub id: Uuid,
    pub destination: Uuid,
    pub container_type: Uuid,
    pub container_volume: Decimal,
    pub container_utilization: Decimal,
    pub waste_volume_m3: Decimal,
    pub photos: Vec<Uuid>,
}


impl Default for TransportationDraft {
    fn default() -> Self {
        TransportationDraft {
            id: Uuid::nil(),
            version: 0,
            submitted: false,
            deleted: false,
            truck: Uuid::nil(),
            execution_date: Utc::now(),
            destinations: vec![],
            waste_collections: vec![],
        }
    }
}

impl TransportationDraft {

    pub fn destinations(&self) -> HashMap<Uuid, Destination> {
        let mut map = HashMap::new();

        for destination in self.destinations.iter() {
            map.insert(destination.id, destination.clone());
        }

        map
    }

    pub async fn replay(id: &Uuid, store: &EventStore) -> Result<Self, ReplayError> {
        let loading = store.load(id.clone()).await;
        match loading {
            Ok(records) => {
                let mut entity = TransportationDraft::default();
                for record in records {
                    entity.apply(&record.event);
                    entity.version = record.version;
                }
                entity.id = id.clone();
                Ok(entity)
            }
            Err(error) => match error {
                sqlx::Error::RowNotFound => Err(ReplayError::EntityNotFound(id.clone())),
                _ => Err(ReplayError::ParsingError(error))
            }
        }
    }

    pub async fn replay_by_version(id: &Uuid, version: i32, store: &EventStore) -> Result<Self, ReplayError> {
        let loading = store.load_by_version(id.clone(), version).await;
        match loading {
            Ok(records) => {
                let mut entity = TransportationDraft::default();
                for record in records {
                    entity.apply(&record.event);
                    entity.version = record.version;
                }
                entity.id = id.clone();
                Ok(entity)
            }
            Err(error) => match error {
                sqlx::Error::RowNotFound => Err(ReplayError::EntityNotFound(id.clone())),
                _ => Err(ReplayError::ParsingError(error))
            }
        }
    }

    pub async fn publish<T: Persisted>(&self, publisher: &Uuid, event_store: &EventStore, events: &Vec<T>) -> Result<i32, PublishError> {
        event_store.store(publisher, &self.id, events, &Version::Increment(self.version))
            .await
            .map_err(PublishError::EventStorageError)
    }

    pub fn apply(&mut self, event: &Transportation) {
        match event {
            Transportation::Registered { .. } => {}
            Transportation::Submitted { .. } => {
                self.submitted = true;
            }
            Transportation::Deleted { .. } => {
                self.deleted = true;
            }
            Transportation::TruckAssigned { truck, execution_date, .. } => {
                self.execution_date = execution_date.clone();
                self.truck = truck.clone();
            }
            Transportation::DestinationAdded { id, collection_start_time, waste_generation_point, .. } => {
                self.destinations.push(Destination {
                    id: id.clone(),
                    collection_start_time: collection_start_time.clone(),
                    waste_generation_point: waste_generation_point.clone(),
                })
            }
            Transportation::CollectionStartTimeChanged { destination, start_time, .. } => {
                for target in self.destinations.iter_mut() {
                    if &target.id == destination {
                        target.collection_start_time = start_time.clone();
                    }
                }
            }
            Transportation::WasteGenerationPointChanged { destination, generation_point, .. } => {
                for target in self.destinations.iter_mut() {
                    if &target.id == destination {
                        target.waste_generation_point = generation_point.clone();
                    }
                }
            }
            Transportation::DestinationRemoved { id, .. } => {
                self.destinations.retain(|destination| &destination.id != id);
            }
            Transportation::WasteCollected {
                id,
                destination,
                container_type,
                container_volume,
                container_utilization,
                waste_volume_m3,
                ..
            } => {
                self.waste_collections.push(WasteCollection {
                    id: id.clone(),
                    destination: destination.clone(),
                    container_type: container_type.clone(),
                    container_volume: container_volume.clone(),
                    container_utilization: container_utilization.clone(),
                    waste_volume_m3: waste_volume_m3.clone(),
                    photos: vec![],
                })
            }
            Transportation::WasteVolumeChanged { waste_collection, waste_volume_m3, .. } => {
                for target in self.waste_collections.iter_mut() {
                    if &target.id == waste_collection {
                        target.waste_volume_m3 = waste_volume_m3.clone();
                    }
                }
            }
            Transportation::ContainerChanged { waste_collection, container_type, container_volume, .. } => {
                for target in self.waste_collections.iter_mut() {
                    if &target.id == waste_collection {
                        target.container_type = container_type.clone();
                        target.container_volume = container_volume.clone();
                    }
                }
            }
            Transportation::ContainerUtilizationChanged { waste_collection, container_utilization, .. } => {
                for target in self.waste_collections.iter_mut() {
                    if &target.id == waste_collection {
                        target.container_utilization = container_utilization.clone();
                    }
                }
            }
            Transportation::WasteCollectionPhotographed { id, photos, .. } => {
                for target in self.waste_collections.iter_mut() {
                    if &target.id == id {
                        target.photos = photos.clone();
                    }
                }
            }
            Transportation::WasteCollectionRevoked { id, .. } => {
                self.waste_collections.retain(|collection| &collection.id != id);
            }
        }
    }
}