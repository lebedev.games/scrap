pub struct BankDetailsAggregate {
    pub account: String,
    pub correspondent_account: String,
    pub bank_name: String,
    pub rcbic: String,
}

impl Default for BankDetailsAggregate {
    fn default() -> Self {
        BankDetailsAggregate {
            account: String::default(),
            correspondent_account: String::default(),
            bank_name: String::default(),
            rcbic: String::default(),
        }
    }
}