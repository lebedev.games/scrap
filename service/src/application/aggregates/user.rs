use service_macro::Persisted;
use uuid::Uuid;

use crate::application::{EventStore, Version};
use crate::application::aggregates::{PublishError, ReplayError};
use crate::domain::User;

#[derive(PartialEq, Debug)]
pub enum Role {
    Transporter,
    Contractor,
    Supervisor
}

impl Role {
    pub fn to_string(&self) -> String {
        match self {
            Role::Transporter => "transporter".to_string(),
            Role::Contractor => "contractor".to_string(),
            Role::Supervisor => "supervisor".to_string()
        }
    }
}

#[derive(PartialEq, Debug)]
pub enum Permission {
    Unused(String),
    FileUpload,
    UserRegister,
    UserGrantPermission,
    UserDelete,
    UserList,
    UserGet,
    GeneratorRegister,
    GeneratorList,
    GeneratorGet,
    GeneratorGetParental,
    TransportationDraftRegister,
    TransportationDraftChange,
    TransportationDraftList,
    TransportationDraftGet,
    TargetGenerationPointList,
    GenerationPointRegister,
    GenerationPointGet,
    GenerationPointList,
    TransporterRegister,
    TransporterGet,
    TransporterList,
    TruckRegister,
    TruckGet,
    TruckList,
    NegotiationComplete,
    NegotiationIssuesDeclare,
    NegotiationIssuesSolve,
    NegotiationGet,
    NegotiationList,
    Reports
}

impl Permission {
    pub fn to_string(&self) -> String {
        match self {
            Permission::Unused(value) => value.clone(),
            Permission::FileUpload => "file/upload".to_string(),
            Permission::UserRegister => "user/register".to_string(),
            Permission::UserGrantPermission => "user/grant_permission".to_string(),
            Permission::UserDelete => "user/delete".to_string(),
            Permission::UserList => "user/list".to_string(),
            Permission::UserGet => "user/get".to_string(),
            Permission::GeneratorRegister => "generator/register".to_string(),
            Permission::GeneratorList => "generator/list".to_string(),
            Permission::GeneratorGet => "generator/get".to_string(),
            Permission::GeneratorGetParental => "generator/get_parental".to_string(),
            Permission::TransportationDraftRegister => "transportation_draft/register".to_string(),
            Permission::TransportationDraftChange => "transportation_draft/change".to_string(),
            Permission::TransportationDraftList => "transportation_draft/list".to_string(),
            Permission::TransportationDraftGet => "transportation_draft/get".to_string(),
            Permission::TargetGenerationPointList => "target_generation_point/list".to_string(),
            Permission::GenerationPointRegister => "generation_point/get".to_string(),
            Permission::GenerationPointGet => "generation_point/register".to_string(),
            Permission::GenerationPointList => "generation_point/list".to_string(),
            Permission::TransporterRegister => "transporter/register".to_string(),
            Permission::TransporterGet => "transporter/get".to_string(),
            Permission::TransporterList => "transporter/list".to_string(),
            Permission::TruckRegister => "truck/register".to_string(),
            Permission::TruckGet => "truck/get".to_string(),
            Permission::TruckList => "truck/list".to_string(),
            Permission::NegotiationComplete => "negotiation/complete".to_string(),
            Permission::NegotiationIssuesDeclare => "negotiation/issues/declare".to_string(),
            Permission::NegotiationIssuesSolve => "negotiation/issues/solve".to_string(),
            Permission::NegotiationGet => "negotiation/get".to_string(),
            Permission::NegotiationList => "negotiation/list".to_string(),
            Permission::Reports => "reports/*".to_string(),
        }
    }

    pub fn parse(value: String) -> Self {
        match &value[..] {
            "file/upload" => Permission::FileUpload,
            "user/register" => Permission::UserRegister,
            "user/grant_permission" => Permission::UserGrantPermission,
            "user/delete" => Permission::UserDelete,
            "user/list" => Permission::UserList,
            "user/get" => Permission::UserGet,
            "generator/register" => Permission::GeneratorRegister,
            "generator/list" => Permission::GeneratorList,
            "generator/get" => Permission::GeneratorGet,
            "generator/get_parental" => Permission::GeneratorGetParental,
            "transportation_draft/register" => Permission::TransportationDraftRegister,
            "transportation_draft/change" => Permission::TransportationDraftChange,
            "transportation_draft/list" => Permission::TransportationDraftList,
            "transportation_draft/get" => Permission::TransportationDraftGet,
            "target_generation_point/list" => Permission::TargetGenerationPointList,
            "generation_point/get" => Permission::GenerationPointGet,
            "generation_point/register" => Permission::GenerationPointRegister,
            "generation_point/list" => Permission::GenerationPointList,
            "transporter/register" => Permission::TransporterRegister,
            "transporter/get" => Permission::TransporterGet,
            "transporter/list" => Permission::TransporterList,
            "truck/register" => Permission::TruckRegister,
            "truck/get" => Permission::TruckGet,
            "truck/list" => Permission::TruckList,
            "negotiation/complete" => Permission::NegotiationComplete,
            "negotiation/issues/declare" => Permission::NegotiationIssuesDeclare,
            "negotiation/issues/solve" => Permission::NegotiationIssuesSolve,
            "negotiation/get" => Permission::NegotiationGet,
            "negotiation/list" => Permission::NegotiationList,
            "reports/*" => Permission::Reports,
            _ => Permission::Unused(value)
        }
    }
}

#[derive(Default)]
pub struct AccessUser {
    pub version: i32,
    pub id: Uuid,
    pub is_deleted: bool,
    pub permissions: Vec<Permission>,
    pub role: String,
    pub role_entity: Option<Uuid>,
    pub login: String,
    pub password: String,
}

impl AccessUser {
    pub async fn replay(id: &Uuid, store: &EventStore) -> Result<Self, ReplayError> {
        let loading = store.load(id.clone()).await;
        match loading {
            Ok(records) => {
                let mut entity = Self::default();
                for record in records {
                    entity.apply(record.event);
                    entity.version = record.version;
                }
                entity.id = id.clone();
                Ok(entity)
            }
            Err(error) => match error {
                sqlx::Error::RowNotFound => Err(ReplayError::EntityNotFound(id.clone())),
                _ => Err(ReplayError::ParsingError(error))
            }
        }
    }

    pub async fn publish<T: Persisted>(&self, publisher: &Uuid, event_store: &EventStore, events: &Vec<T>) -> Result<i32, PublishError> {
        event_store.store(publisher, &self.id, events, &Version::Increment(self.version))
            .await
            .map_err(PublishError::EventStorageError)
    }

    pub fn apply(&mut self, event: User) {
        match event {
            User::Registered { .. } => {}
            User::PermissionGranted { permission, .. } => {
                self.permissions.push(Permission::parse(permission));
            }
            User::Deleted { .. } => {
                self.is_deleted = true;
            }
            User::RoleSpecified { role, .. } => {
                self.role = role;
            }
            User::RoleEntityAssigned { entity_id, .. } => {
                self.role_entity = Some(entity_id);
            }
            User::PasswordCredentialsSpecified { login, password, .. } => {
                self.login = login;
                self.password = password;
            }
            User::NameSpecified { .. } => {}
        }
    }
}

#[derive(Debug)]
pub enum UserAccessError {
    PermissionNotGranted(Permission),
    RoleEntityUnassigned
}

pub fn ensure_access(user: AccessUser, permission: Permission) -> Result<AccessUser, UserAccessError> {
    if is_user_has_permission(&user, &permission) {
        return Ok(user)
    }

    Err(UserAccessError::PermissionNotGranted(permission))
}

pub fn is_user_has_permission(user: &AccessUser, permission: &Permission) -> bool {
    for target in &user.permissions {
        if target == permission {
            return true
        }
    }

    false
}
