use uuid::Uuid;
use crate::application::{EventStore, Version};
use crate::application::aggregates::{ReplayError, PublishError};
use service_macro::Persisted;
use crate::domain::Negotiation;
use chrono::{DateTime, Utc};
use std::collections::HashMap;

#[derive(Clone)]
pub enum Issue {
    WasteVolumeIssue {
        id: Uuid,
        destination: Uuid,
        collection: Uuid,
        comment: String
    },
    CollectionStartTimeIssue {
        id: Uuid,
        destination: Uuid,
        comment: String
    },
    CollectionPhotoIssue {
        id: Uuid,
        destination: Uuid,
        collection: Uuid,
        comment: String
    }
}

pub struct IssuesDeclaration {
    pub id: Uuid,
    pub issues: Vec<Issue>,
    pub date: DateTime<Utc>
}

pub struct NegotiationStage {
    pub id: Uuid,
    pub version: i32,
    pub transportation: Uuid,
    pub declarations: Vec<IssuesDeclaration>
}

impl Default for NegotiationStage {
    fn default() -> Self {
        Self {
            id: Default::default(),
            version: 0,
            transportation: Default::default(),
            declarations: vec![]
        }
    }
}

impl NegotiationStage {

    pub fn last_declaration_issues(&self) -> HashMap<Uuid, Issue> {
        let mut map = HashMap::new();

        if let Some(declaration) = self.declarations.iter().last() {
            for issue in declaration.issues.iter() {
                let issue_id = match issue {
                    Issue::WasteVolumeIssue { id, ..} => id,
                    Issue::CollectionStartTimeIssue { id, .. } => id,
                    Issue::CollectionPhotoIssue { id, .. } => id,
                };
                map.insert(issue_id.clone(), issue.clone());
            }
        }

        map
    }

    pub async fn replay(id: &Uuid, store: &EventStore) -> Result<Self, ReplayError> {
        let loading = store.load(id.clone()).await;
        match loading {
            Ok(records) => {
                let mut entity = NegotiationStage::default();
                for record in records {
                    entity.apply(record.event);
                    entity.version = record.version;
                }
                entity.id = id.clone();
                Ok(entity)
            }
            Err(error) => match error {
                sqlx::Error::RowNotFound => Err(ReplayError::EntityNotFound(id.clone())),
                _ => Err(ReplayError::ParsingError(error))
            }
        }
    }

    pub async fn publish<T: Persisted>(&self, publisher: &Uuid, event_store: &EventStore, events: &Vec<T>) -> Result<i32, PublishError> {
        event_store.store(publisher, &self.id, events, &Version::Increment(self.version))
            .await
            .map_err(PublishError::EventStorageError)
    }

    pub fn apply(&mut self, event: Negotiation) {
        match event {
            Negotiation::Started { transportation, .. } => {
                self.transportation = transportation;
            }
            Negotiation::IssuesDeclarationStagePassed { id, date, .. } => {
                self.declarations.push(IssuesDeclaration {
                    id,
                    issues: vec![],
                    date
                })
            }
            Negotiation::TransportationIssueOpened { .. } => {}
            Negotiation::TransportationIssueReopened { .. } => {}
            Negotiation::WasteVolumeIssueOpened { id, declaration, comment, waste_collection, destination } => {
                for target in self.declarations.iter_mut() {
                    if target.id == declaration {
                        target.issues.push(Issue::WasteVolumeIssue {
                            id,
                            destination,
                            collection: waste_collection,
                            comment: comment.clone()
                        })
                    }
                }
            }
            Negotiation::WasteVolumeIssueReopened { .. } => {}
            Negotiation::CollectionStartTimeIssueOpened { id, declaration, comment, destination } => {
                for target in self.declarations.iter_mut() {
                    if target.id == declaration {
                        target.issues.push(Issue::CollectionStartTimeIssue {
                            id,
                            destination,
                            comment: comment.clone()
                        })
                    }
                }
            }
            Negotiation::CollectionStartTimeIssueReopened { .. } => {}
            Negotiation::CollectionPhotoIssueOpened { id, declaration, comment, waste_collection, destination } => {
                for target in self.declarations.iter_mut() {
                    if target.id == declaration {
                        target.issues.push(Issue::CollectionPhotoIssue {
                            id,
                            destination,
                            collection: waste_collection,
                            comment: comment.clone()
                        })
                    }
                }
            }
            Negotiation::CollectionPhotoIssueReopened { .. } => {}
            Negotiation::IssuesResolutionStagePassed { id: _, transporter: _, date: _, transportation_version: _ } => {}
            Negotiation::TransportationIssueResolved { id: _, resolution: _, comment: _ } => {}
            Negotiation::TransportationIssueRejected { id: _, resolution: _, comment: _ } => {}
            Negotiation::WasteVolumeIssueResolved { id: _, resolution: _, waste_volume_m3: _ } => {}
            Negotiation::WasteVolumeIssueRejected { id: _, resolution: _, comment: _ } => {}
            Negotiation::CollectionStartTimeIssueResolved { id: _, resolution: _, start_time: _ } => {}
            Negotiation::CollectionStartTimeIssueRejected { id: _, resolution: _, comment: _ } => {}
            Negotiation::CollectionPhotoIssueResolved { id: _, resolution: _, photos: _ } => {}
            Negotiation::CollectionPhotoIssueRejected { id: _, resolution: _, comment: _ } => {}
            Negotiation::Completed { contractor: _, date: _, transportation_version: _ } => {}
        }
    }
}