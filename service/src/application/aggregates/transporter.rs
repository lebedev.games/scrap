use uuid::Uuid;

use crate::application::aggregates::ReplayError;
use crate::application::EventStore;
use crate::domain::Transporter;

pub struct TransporterAggregate {
    pub id: Uuid,
    pub version: i32,
    pub legal_entity: TransporterLegalEntity,
    pub fkko: String,
    pub parent: Option<Uuid>,
    pub truck_fleet_types: Vec<Uuid>,
    pub trucks: Vec<Uuid>,
    pub target_generation_points: Vec<Uuid>,
    pub note: String,
}

pub enum TransporterLegalEntity {
    Unassigned,
    BusinessEntity(Uuid),
    Entrepreneur(Uuid),
}

impl Default for TransporterLegalEntity {
    fn default() -> Self {
        TransporterLegalEntity::Unassigned
    }
}

impl Default for TransporterAggregate {
    fn default() -> Self {
        TransporterAggregate {
            id: Uuid::nil(),
            version: 0,
            legal_entity: TransporterLegalEntity::default(),
            fkko: String::default(),
            parent: None,
            truck_fleet_types: vec![],
            trucks: vec![],
            target_generation_points: vec![],
            note: String::default(),
        }
    }
}

impl TransporterAggregate {
    pub async fn replay(id: &Uuid, store: &EventStore) -> Result<Self, ReplayError> {
        let loading = store.load(id.clone()).await;
        match loading {
            Ok(records) => {
                let mut entity = TransporterAggregate::default();
                for record in records {
                    entity.apply(&record.event);
                    entity.version = record.version;
                }
                entity.id = id.clone();
                Ok(entity)
            }
            Err(error) => match error {
                sqlx::Error::RowNotFound => Err(ReplayError::EntityNotFound(id.clone())),
                _ => Err(ReplayError::ParsingError(error))
            }
        }
    }

    pub(crate) fn apply(&mut self, event: &Transporter) {
        match event {
            Transporter::Registered { .. } => {}
            Transporter::EntrepreneurAssigned { entrepreneur, .. } => {
                self.legal_entity = TransporterLegalEntity::Entrepreneur(entrepreneur.clone());
            }
            Transporter::BusinessEntityAssigned { business_entity, .. } => {
                self.legal_entity = TransporterLegalEntity::BusinessEntity(business_entity.clone());
            }
            Transporter::FederalClassificationUpdated { fkko, .. } => {
                self.fkko = fkko.clone();
            }
            Transporter::ParentUpdated { parent, .. } => {
                self.parent = Some(parent.clone());
            }
            Transporter::TruckFleetTypeSpecified { truck_fleet_type, .. } => {
                self.truck_fleet_types.push(truck_fleet_type.clone());
            }
            Transporter::TruckAdded { truck, .. } => {
                self.trucks.push(truck.clone());
            }
            Transporter::TargetGenerationPointAdded { generation_point, .. } => {
                self.target_generation_points.push(generation_point.clone());
            }
            Transporter::NoteUpdated { note, .. } => {
                self.note = note.clone();
            }
        }
    }
}
