extern crate proc_macro;

use proc_macro::TokenStream;

use quote::{quote};
use regex::{Regex};
use syn::{Data, DeriveInput, Fields};

#[proc_macro_derive(Persisted)]
pub fn persisted_derive(input: TokenStream) -> TokenStream {
    let ast = syn::parse(input).unwrap();
    impl_persisted(&ast)
}

fn to_snake_case(input: String) -> String {
    let value = &Regex::new(r"(.+?)([A-Z])")
        .unwrap()
        .replace_all(&input, "$1 _ $2");
    let value = value.to_lowercase().replace(" _ ", "_");
    String::from(value)
}

fn impl_persisted(ast: &DeriveInput) -> TokenStream {
    let name = &ast.ident;

    match &ast.data {
        Data::Enum(data) => {
            let mut serializers = vec![];
            let mut deserializers = vec![];
            for variant in data.variants.iter() {
                let value = &variant.ident;
                if let Fields::Named(n) = &variant.fields {
                    let mut fields = vec![];
                    let mut binders = vec![];
                    let mut indices = vec![];
                    let mut columns = vec![];
                    let mut index = 2;
                    for named_field in n.named.iter() {
                        let field = &named_field.ident;
                        let field_str = format!("{}", field.as_ref().unwrap());
                        fields.push(field);
                        let binder_name = &named_field.ident;
                        binders.push(quote! {.bind(#binder_name)});
                        indices.push(format!("${}", index));
                        index += 1;
                        let column = quote! {
                            #field: sqlx::Row::try_get(&row, #field_str)?,
                        };
                        columns.push(column);
                    }
                    let table = format!(
                        "{}_{}",
                        to_snake_case(format!("{}", name)),
                        to_snake_case(format!("{}", value)));
                    let insert_statement = format!(
                        r#"
                        INSERT INTO {} (entity_entry_id, {})
                        VALUES ($1, {})
                        "#,
                        table,
                        quote! { #(#fields),* },
                        indices.join(", ")
                    );
                    let serializer = quote! {
                        #name::#value { #(#fields),* } => {
                            let insert_statement = #insert_statement;
                            sqlx::query(insert_statement)
                                .bind(entity_entry_id)
                                #(#binders)*
                        }
                    };

                    serializers.push(serializer);

                    // let columns = vec![];

                    let deserializer = quote! {
                        (#table.into(), |row| Ok(#name::#value {
                            #(#columns)*
                        })),
                    };

                    deserializers.push(deserializer);
                }
            }


            let gen = quote! {
                impl Persisted for #name {

                    fn persist(&self, entity_entry_id: i64) -> sqlx::query::Query<'_, sqlx::Postgres, sqlx::postgres::PgArguments> {
                        match self {
                            #(#serializers)*
                        }
                    }

                    fn loaders() -> Vec<(String, fn(sqlx::postgres::PgRow) -> Result<Self, sqlx::Error>)> {
                        vec![
                            #(#deserializers)*
                        ]
                    }
                }
            };

            // println!("{}", gen);

            gen.into()
        }
        Data::Struct(_) => panic!("Persisted trait for struct not implemented yet"),
        Data::Union(_) => panic!("Persisted trait for union not implemented yet")
    }
}

/*
impl Persisted for User {
    fn persist(&self, entity_entry_id: i64) -> Query<'_, Postgres, PgArguments> {
        match self {
            User::Registered { registrar, date } => {
                let insert_statement = r#"
                    INSERT INTO user_registered (entity_entry_id, registrar, date)
                    VALUES ($1, $2, $3)
                "#;
                sqlx::query(insert_statement)
                    .bind(entity_entry_id)
                    .bind(registrar)
                    .bind(date)
            }
            User::PermissionGranted { grantor, permission, date } => {
                let insert_statement = r#"
                    INSERT INTO user_permission_granted (entity_entry_id, grantor, permission, date)
                    VALUES ($1, $2, $3, $4)
                "#;
                sqlx::query(insert_statement)
                    .bind(entity_entry_id)
                    .bind(grantor)
                    .bind(permission)
                    .bind(date)
            }
            User::Deleted { registrar, date } => {
                let insert_statement = r#"
                    INSERT INTO user_deleted (entity_entry_id, registrar, date)
                    VALUES ($1, $2, $3)
                "#;
                sqlx::query(insert_statement)
                    .bind(entity_entry_id)
                    .bind(registrar)
                    .bind(date)
            }
        }
    }

    fn loaders() -> Vec<(String, fn(PgRow) -> Result<Self, Error>)> {
        vec![
            ("user_registered".into(), |row| Ok(User::Registered {
                registrar: row.try_get("registrar")?,
                date: row.try_get("date")?,
            })),
            ("user_permission_granted".into(), |row| Ok(User::PermissionGranted {
                grantor: row.try_get("grantor")?,
                date: row.try_get("date")?,
                permission: row.try_get("permission")?,
            })),
            ("user_deleted".into(), |row| Ok(User::Deleted {
                registrar: row.try_get("registrar")?,
                date: row.try_get("date")?,
            })),
        ]
    }
}
 */