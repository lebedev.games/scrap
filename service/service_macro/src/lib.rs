pub trait Persisted: Sized {
    fn persist(&self, entity_entry_id: i64) -> sqlx::query::Query<'_, sqlx::Postgres, sqlx::postgres::PgArguments>;
    fn loaders() -> Vec<(String, fn(sqlx::postgres::PgRow) -> Result<Self, sqlx::Error>)>;
}
