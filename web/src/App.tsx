import React, {Component} from 'react';
import './App.css';
import {BrowserRouter, Redirect, Route, Switch} from "react-router-dom";
import {TransporterPage} from "./components/transporter/TransporterPage";
import {Subscription} from "rxjs";
import {logInCurrentUser, observeCurrentUser} from "./service/context";
import {AuthorizationUser} from "./service/model";
import {Login} from "./components/app/Login";
import {ContractorPage} from "./components/contractor/ContractorPage";
import {SupervisorPage} from "./components/supervisor/SupervisorPage";

interface AppProps {

}

interface AppState {
    user: AuthorizationUser | null;
}

class App extends Component<AppProps, AppState> {
    subscription = new Subscription();

    constructor(props: AppProps) {
        super(props);
        this.state = {
            user: null
        };
    }

    componentDidMount() {
        this.subscription.add(observeCurrentUser().subscribe(user => this.setState({user})))
    }

    componentWillUnmount() {
        this.subscription.unsubscribe();
    }

    render() {
        const {user} = this.state;

        if (user) {
            if (user.userRole === 'transporter') {
                return (
                    <BrowserRouter>
                        <Switch>
                            <Route path="/transporter">
                                <TransporterPage user={user}/>
                            </Route>
                            <Route path="/" render={() => <Redirect to="/transporter" />}/>
                        </Switch>
                    </BrowserRouter>
                );
            } else if (user.userRole === 'contractor') {
                return (
                    <BrowserRouter>
                        <Switch>
                            <Route path="/contractor">
                                <ContractorPage user={user}/>
                            </Route>
                            <Route path="/" render={() => <Redirect to="/contractor"/>}/>
                        </Switch>
                    </BrowserRouter>
                );
            } else if (user.userRole === 'supervisor') {
                return (
                    <BrowserRouter>
                        <Switch>
                            <Route path="/supervisor">
                                <SupervisorPage user={user}/>
                            </Route>
                            <Route path="/" render={() => <Redirect to="/supervisor"/>}/>
                        </Switch>
                    </BrowserRouter>
                )
            } else {
                return <div>User role "{user.userRole}" not implemented yet</div>
            }
        } else {
            return <Login onComplete={user => logInCurrentUser(user)}/>
        }
    }
}

export default App;
