
export function parsePhone(value: string): string {
    value = value.replace(/\s/g, '');

    if (value.startsWith('8')) {
        value = value.replace('8', '+7')
    }
    else if (value.length === 1 && /\d/.test(value))
    {
        if (value === '8') {
            value = '+7 ';
        } else  {
            value = '+7 ' + value;
        }
    }
    return value;
}

export function parseDecimal(value: string): string {
    return value;
}

export function parseInteger(value: string): number {
    return Number.parseInt(value);
}