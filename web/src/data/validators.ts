import {Validator} from "../components/kit/forms";


export function required(error?: string): Validator {
    return (value) => {
        return value ? null : (error || 'Обязательно для заполнения');
    }
}

export function notEqual(other: any, error?: string): Validator {
    return (value) => {
        return value != other ? null : (error || 'Должно отличаться');
    }
}

export function notEmptyCollection(error?: string): Validator {
    return (value) => {
        return value && value.length > 0 ? null : (error || 'Обязательно для заполнения')
    }
}

export function splitted(parts: number, error: string): Validator {
    return (value) => {
        return value.split(' ').length === parts ? null : error;
    }
}
