import moment from "moment";
import 'moment/locale/ru';

export function choosePluralForm(forms: string[], count: number) {
    return count % 10 === 1 && count % 100 !== 11
        ? forms[0]
        : count % 10 >= 2 && count % 10 <= 4 && (count % 100 < 10 || count % 100 >= 20)
            ? forms[1]
            : forms[2];
}

export function formatLegalEntityType(value: string) {
    const mapping = {
        'Entrepreneur': 'ИП',
        'BusinessEntity': 'Юр. лицо'
    } as any;
    return mapping[value] || value;
}

export function formatPhone(value: string): string {
    let match = value.match(/^(\+7)(\d{0,3})?(\d{0,3})?(\d{0,2})?(\d{0,2})?$/);
    if (match) {
        let parts = match.slice(1, 6).filter(group => !!group);
        return parts.join(' ')
    }
    return value;
}

export function formatDate(value: string): string {
    moment.locale('ru');
    return moment(value).format('L');
}

export function formatPeriod(value: string): string {
    moment.locale('ru');
    return moment(value).format('MMMM, YYYY');
}

export function formatDateTime(value: string): string {
    moment.locale('ru');
    return moment(value).format('L LT');
}

export function formatTime(value: string): string {
    moment.locale('ru');
    return moment(value).format('LT');
}

export function formatDecimal(value: string): string {
    return value;
}

export function formatInteger(value: number): string {
    if (Number.isNaN(value)) {
        return ''
    }
    return '' + value;
}