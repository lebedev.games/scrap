import {BehaviorSubject, Observable, ReplaySubject} from "rxjs";
import {AuthorizationUser} from "./model";

export const authorizationSubject = new ReplaySubject<AuthorizationUser | null>(1);

export const accessTokenSubject = new BehaviorSubject<string | null>(null);

let authorizationData = localStorage.getItem('authorization');
if (authorizationData) {
    let user: AuthorizationUser = JSON.parse(authorizationData);
    authorizationSubject.next(user);
    accessTokenSubject.next(user.accessToken);
}

export function observeCurrentUser(): Observable<AuthorizationUser | null> {
    return authorizationSubject.asObservable();
}

export function logInCurrentUser(user: AuthorizationUser) {
    let data = JSON.stringify(user);
    localStorage.setItem('authorization', data);
    accessTokenSubject.next(user.accessToken);
    authorizationSubject.next(user);
}

export function logOutCurrentUser() {
    localStorage.removeItem('authorization');
    authorizationSubject.next(null);
    accessTokenSubject.next(null);
}