export interface UserListItem {
    id: string
    permissions: string[]
}

export interface BankDetails {
    account: string
    correspondentAccount: string
    bankName: string
    rcbic: string
}

export type LegalEntityType = 'BusinessEntity' | 'Entrepreneur';

export interface EntrepreneurModel {
    firstName: string
    lastName: string
    patronymic: string
    inn: string
    ogrnip: string
    phone: string
    email: string | null
    address: string
    actualAddress: string
    bankDetails: BankDetails
    type: LegalEntityType
}

export interface ResponsiblePerson {
    fullName: string
    phone: string
}

export interface BusinessEntityModel {
    fullName: string
    shortName: string
    inn: string
    kpp: string
    ogrn: string
    phone: string
    email: string | null
    address: string
    postalAddress: string
    actualAddress: string
    responsiblePersons: ResponsiblePerson[]
    bankDetails: BankDetails
    type: LegalEntityType
}

export interface GeneratorModel {
    parent: string | null
    legalEntity: BusinessEntityModel | EntrepreneurModel
}


export interface RegisterGeneratorForm {
    parent: string | null
    legalEntity: EntrepreneurModel | BusinessEntityModel
}

export interface RegisterGeneratorResponse {
    id: string
}

export interface Affiliate {
    id: string
    name: string
    inn: string
    legalEntityType: string
}

export interface GeneratorListItem {
    id: string
    name: string
    inn: string
    legalEntityType: string
    affiliates: Affiliate[]
}

export interface GeneratorOption {
    id: string
    name: string
}

export interface Option {
    id: string
    name: string
}

export interface FileUpload {
    files: string[]
}

export interface TransportationListItem {
    id: string
    district: string
    executionDate: string
    truckRegistrationNumber: string
    status: string
}

export interface RegisterTransportationForm {
    truck: string
    executionDate: string
    destinations: TransportationDestination[]
}

export interface TransportationForm {
    truck: string
    executionDate: string
    destinations: TransportationDestination[]
}

export interface TransportationDestination {
    id: string
    collectionStartTime: string
    wasteGenerationPoint: string
    collections: WasteCollection[]
}

export interface WasteCollection {
    id: string
    destination: string
    containerType: string
    containerVolume: string
    containerUtilization: string
    wasteVolumeM3: string
    photos: string[]
}

export interface TargetContainerType {
    id: string
    name: string
    volumeLimitM3: '' | null
}

export interface TargetGenerationPoint {
    id: string
    address: string
    containerTypes: TargetContainerType[]
}

export interface Resource {
    id: string
}

export interface GenerationPointListItemModel {
    id: string
    internalSystemNumber: string
    fullAddress: string
    transporterName: string
    areaM2: string
}

export interface RegisterGenerationPointFormContainer {
    containerType: string
}

export interface RegisterGenerationPointFormTransporter {
    transporter: string
    startTransportationDate: string
    endTransportationDate: string
    frequency: string
}

export interface RegisterGenerationPointForm {
    internalSystemNumber: string
    externalRegistryNumber: string
    wasteType: string
    organizationLegalType: string
    operationalStatus: string
    fullAddress: string
    coordinates: string
    areaM2: string,
    coveringType: string
    fenceMaterial: string
    serviceCompanyName: string | null
    generator: string
    description: string
    containers: RegisterGenerationPointFormContainer[]
    transporter: RegisterGenerationPointFormTransporter
    photos: string[]
    accrualMethod: string
    measurementUnit: string
}

export interface GenerationPointModelContainer {
    containerType: string
}

export interface GenerationPointModelTransporter {
    transporter: string
    startTransportationDate: string
    endTransportationDate: string
}

export interface GenerationPointModel {
    id: string
    internalSystemNumber: string
    externalRegistryNumber: string
    wasteType: string
    organizationLegalType: string
    operationalStatus: string
    fullAddress: string
    coordinates: string
    areaM2: string
    coveringType: string
    fenceMaterial: string
    serviceCompanyName: string | null
    generator: string
    description: string
    containers: GenerationPointModelContainer[],
    transporter: GenerationPointModelTransporter,
    photos: string[]
    accrualMethod: string
    measurementUnit: string
    transportationFrequency: string
}

export interface TransporterModel {
    legalEntity: BusinessEntityModel | EntrepreneurModel
    fkko: string
    parent: string | null
    truckFleetTypes: string[]
    trucks: string[]
    targetGenerationPoints: string[]
    note: string
}

export interface RegisterTransporterForm {
    legalEntity: BusinessEntityModel | EntrepreneurModel
    fkko: string
    parent: string | null
    truckFleetTypes: string[]
    note: String,
}


export interface TransporterSearchCriteria {
    inn: string,
    name: string,
    truckFleetType: string | null,
    legalEntityType: string | null,
    limit: number | null,
    offset: number | null
}

export interface TransporterListItemModel {
    id: string
    legalEntityType: string
    inn: string
    name: string
    truckFleetTypes: string[]
    districts: string[]
}

export interface AuthorizationUser {
    userName: string
    userId: string
    userRole: string
    accessToken: string
}

export interface PasswordCredentials {
    login: string
    password: string
}

type AuthorizationForm = PasswordCredentials;

export interface AuthorizationRequest {
    form: {
        [key: string]: AuthorizationForm
    }
}

export interface List<T> {
    items: T[],
    limit: number,
    offset: number,
    total: number
}

export function emptyList<T>(): List<T> {
    return {
        items: [],
        limit: 0,
        offset: 0,
        total: 0
    }
}
export interface TruckModel {
    id: string,
    owner: string,
    transporter: string,
    fleetType: string,
    vin: string,
    registrationNumber: string,
    garagingNumber: string,
    vehicleModel: string,
    wasteEquipmentModel: string,
    cargoBodyType: string,
    requiredDriverLicenseCategory: string,
    axleConfiguration: string,
    productionYear: string,
    navigationEquipmentName: string,
    emptyWeightKg: string,
    cargoCapacityKg: string,
}

export interface RegisterTruckForm {
    owner: string,
    transporter: string,
    fleetType: string,
    vin: string,
    registrationNumber: string,
    garagingNumber: string,
    vehicleModel: string,
    wasteEquipmentModel: string,
    cargoBodyType: string,
    requiredDriverLicenseCategory: string,
    axleConfiguration: string,
    productionYear: string,
    navigationEquipmentName: string,
    emptyWeightKg: string,
    cargoCapacityKg: string,
}

export interface TruckListItemModel {
    id: string,
    registrationNumber: string,
    cargoBodyType: string,
    transporterName: string,
    truckFleetType: string,
}

export interface TruckSearchCriteria {
    registrationNumber: string,
    cargoBodyType: string | null,
    transporterName: string,
    truckFleetType: string | null,
    limit: string | null,
    offset: string | null,
}

export interface NegotiationSearchCriteria {
    transportationDate: string | null,
    truck: string,
    district: string,
    transporter: string,
    status: string | null,
    limit: number | null,
    offset: number | null,
    consolidated?: boolean
}

export interface NegotiationListItem {
    id: string,
    district: string,
    transportation: string,
    transportationDate: string,
    truck: string,
    transporter: string,
    status: string,
}

export interface GenerationPointSearchCriteria {
    internalSystemNumber: string
    transporter: string
    address: string
    limit: number | null
    offset: number | null
}

export interface GeneratorSearchCriteria {
    inn: string
    name: string
    legalEntityType: string | null
    limit: number | null
    offset: number | null
}

export interface IssuesDeclaration {
    issues: Issue[],
    date: string
}

export interface NegotiationModel {
    id: string
    transportation: string
    timeline: TimelinePoint[]
    declarations: IssuesDeclaration[]
}

export interface TimelinePoint {
    stageId: string
    negotiation: string
    transportationVersion: number
    timestamp: string
    status: string
}


export type Issue = WasteVolumeIssue | CollectionStartTimeIssue | CollectionPhotoIssue;


export class WasteVolumeIssue {
    id: string
    destination: string
    collection: string
    comment: string
    declared: boolean

    constructor(issue: Required<WasteVolumeIssue>) {
        this.id = issue.id;
        this.destination = issue.destination;
        this.collection = issue.collection;
        this.comment = issue.comment;
        this.declared = issue.declared || false;
    }
}

export class CollectionStartTimeIssue {
    id: string
    destination: string
    comment: string
    declared: boolean

    constructor(issue: Required<CollectionStartTimeIssue>) {
        this.id = issue.id;
        this.destination = issue.destination;
        this.comment = issue.comment;
        this.declared = issue.declared || false;
    }
}

export class CollectionPhotoIssue {
    id: string
    destination: string
    collection: string
    comment: string
    declared: boolean

    constructor(issue: Required<CollectionPhotoIssue>) {
        this.id = issue.id;
        this.destination = issue.destination;
        this.collection = issue.collection;
        this.comment = issue.comment;
        this.declared = issue.declared || false;
    }
}

export interface DeclareNegotiationIssuesRequest {
    issues: Issue[]
}

type ResolutionModel = WasteVolumeIssueResolution | CollectionStartTimeIssueResolution | CollectionPhotoIssueResolution;

export interface WasteVolumeIssueResolution {
    issueId: string
    wasteVolume: string
}

export interface CollectionStartTimeIssueResolution {
    issueId: string
    startTime: string
}

export interface CollectionPhotoIssueResolution {
    issueId: string
    photos: string[]
}

export interface SolveNegotiationIssuesRequest {
    resolutions: {[key: string]: ResolutionModel}[]
}

export interface CreateReportForm {
    targetTransporter: string
    targetMonth: string
    transportations: string[]
}

export interface ReportListItem {
    id: string
    targetTransporter: string
    targetMonth: string
    transportations: number
    reportingDate: string
}

export interface ReportSearchCriteria {
    targetTransporter: string
    targetMonth: string | null
    reportingDate: string | null
    limit: number | null
    offset: number | null
}