import axios from 'axios';
import {
    AuthorizationRequest,
    AuthorizationUser,
    CollectionPhotoIssue,
    CollectionStartTimeIssue,
    CreateReportForm,
    DeclareNegotiationIssuesRequest,
    FileUpload,
    GenerationPointListItemModel,
    GenerationPointModel,
    GenerationPointSearchCriteria,
    GeneratorListItem,
    GeneratorModel,
    GeneratorOption,
    GeneratorSearchCriteria,
    Issue,
    List,
    NegotiationListItem,
    NegotiationModel,
    NegotiationSearchCriteria,
    Option,
    RegisterGenerationPointForm,
    RegisterGeneratorForm,
    RegisterGeneratorResponse,
    RegisterTransportationForm,
    RegisterTransporterForm,
    RegisterTruckForm,
    ReportListItem,
    ReportSearchCriteria,
    Resource,
    SolveNegotiationIssuesRequest,
    TargetGenerationPoint,
    TransportationForm,
    TransportationListItem,
    TransporterListItemModel,
    TransporterModel,
    TransporterSearchCriteria,
    TruckListItemModel,
    TruckModel,
    TruckSearchCriteria,
    WasteVolumeIssue
} from "./model";
import {accessTokenSubject} from "./context";


const baseUrl = process.env.REACT_APP_SERVICE_URL || 'http://127.0.0.1:8004';

export function backend(path: string): string {
    return baseUrl + path;
}

function getHeaders(): any {
    let headers: any = {};
    let accessToken = accessTokenSubject.value;
    if (accessToken) {
        headers['Authorization'] = 'Bearer ' + accessToken;
    }
    return headers;
}

export async function authorize(request: AuthorizationRequest): Promise<AuthorizationUser> {
    let response = await axios.post(baseUrl + '/authorization', request);
    return response.data;
}

export async function getGenerators(search: GeneratorSearchCriteria): Promise<List<GeneratorListItem>> {
    const response = await axios.get(baseUrl + '/generators', {params: search, headers: getHeaders()});
    return response.data;
}

export async function getGeneratorOptions(): Promise<GeneratorListItem[]> {
    const response = await axios.get(baseUrl + '/generators/options', {headers: getHeaders()});
    return response.data;
}

export async function getGenerator(id: string): Promise<GeneratorModel> {
    const response = await axios.get(baseUrl + '/generators/' + id, {headers: getHeaders()});
    return response.data;
}

export async function getParentalGeneratorOptions(): Promise<GeneratorOption[]> {
    const response = await axios.get(baseUrl + '/generators/parental', {headers: getHeaders()});
    return response.data;
}

export async function registerGenerator(form: RegisterGeneratorForm): Promise<RegisterGeneratorResponse> {
    let response = await axios.post(baseUrl + '/generators', form, {headers: getHeaders()});
    return response.data;
}

export async function uploadFiles(files: FileList): Promise<FileUpload> {
    let data = new FormData();

    for (let index = 0; index < files.length; index++) {
        let file = files[index];
        data.append('file', file);
    }

    let response = await axios.post(baseUrl + '/files/upload', data, {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    });

    return response.data;
}

export async function getTransportations(): Promise<TransportationListItem[]> {
    const response = await axios.get(baseUrl + '/transportations', {headers: getHeaders()});
    return response.data;
}

export async function getTransportation(id: string): Promise<TransportationForm> {
    const response = await axios.get(baseUrl + '/transportations/' + id, {headers: getHeaders()});
    return response.data;
}

export async function getTransportationByVersion(id: string, version: number): Promise<TransportationForm> {
    const response = await axios.get(baseUrl + '/transportations/' + id + '/versions/' + version, {headers: getHeaders()});
    return response.data;
}

export async function getTransporterTruckOptions(): Promise<Option[]> {
    const response = await axios.get(baseUrl + '/transporter/trucks/options', {headers: getHeaders()});
    return response.data;
}

export async function getTargetGenerationPoints(): Promise<TargetGenerationPoint[]> {
    const response = await axios.get(baseUrl + '/transporter/generation-points/target', {headers: getHeaders()});
    return response.data;
}

export async function registerTransportation(form: RegisterTransportationForm): Promise<Resource> {
    let response = await axios.post(baseUrl + '/transportations', form, {headers: getHeaders()});
    return response.data;
}


export async function getGenerationPoints(search: GenerationPointSearchCriteria): Promise<List<GenerationPointListItemModel>> {
    const response = await axios.get(baseUrl + '/generation_points', {params: search, headers: getHeaders()});
    return response.data;
}

export async function getGenerationPointOptions(): Promise<Option[]> {
    const response = await axios.get(baseUrl + '/generation-points/options', {headers: getHeaders()});
    return response.data;
}

export async function getGenerationPoint(id: string): Promise<GenerationPointModel> {
    const response = await axios.get(baseUrl + '/generation_points/' + id, {headers: getHeaders()});
    return response.data;
}

export async function registerGenrationPoint(form: RegisterGenerationPointForm): Promise<Resource> {
    let response = await axios.post(baseUrl + '/generation_points', form, {headers: getHeaders()});
    return response.data;
}

export async function getTransporterOptions(): Promise<Option[]> {
    const response = await axios.get(baseUrl + '/transporters/options', {headers: getHeaders()});
    return response.data;
}

export async function registerTransporter(form: RegisterTransporterForm): Promise<Resource> {
    let response = await axios.post(baseUrl + '/transporters', form, {headers: getHeaders()});
    return response.data;
}

export async function getTransporter(id: string): Promise<TransporterModel> {
    const response = await axios.get(baseUrl + '/transporters/' + id, {headers: getHeaders()});
    return response.data;
}

export async function getTransporters(search: TransporterSearchCriteria): Promise<List<TransporterListItemModel>> {
    const response = await axios.get(baseUrl + '/transporters', {params: search, headers: getHeaders()});
    return response.data;
}

export async function getParentalTransporterOptions(): Promise<GeneratorOption[]> {
    const response = await axios.get(baseUrl + '/transporters/parental', {headers: getHeaders()});
    return response.data;
}

export async function getAccrualMethodOptions(): Promise<Option[]> {
    const response = await axios.get(baseUrl + '/dictionary/accrual-methods', {headers: getHeaders()});
    return response.data;
}

export async function getContainerTypeOptions(): Promise<Option[]> {
    const response = await axios.get(baseUrl + '/dictionary/container-types', {headers: getHeaders()});
    return response.data;
}

export async function getCoveringTypeOptions(): Promise<Option[]> {
    const response = await axios.get(baseUrl + '/dictionary/covering-types', {headers: getHeaders()});
    return response.data;
}

export async function getFenceMaterialOptions(): Promise<Option[]> {
    const response = await axios.get(baseUrl + '/dictionary/fence-materials', {headers: getHeaders()});
    return response.data;
}

export async function getMeasurementUnitOptions(): Promise<Option[]> {
    const response = await axios.get(baseUrl + '/dictionary/measurement-units', {headers: getHeaders()});
    return response.data;
}

export async function getOperationalStatusOptions(): Promise<Option[]> {
    const response = await axios.get(baseUrl + '/dictionary/operational-statuses', {headers: getHeaders()});
    return response.data;
}

export async function getOrganizationLegalTypeOptions(): Promise<Option[]> {
    const response = await axios.get(baseUrl + '/dictionary/organization-legal-types', {headers: getHeaders()});
    return response.data;
}

export async function getWasteTypeOptions(): Promise<Option[]> {
    const response = await axios.get(baseUrl + '/dictionary/waste-types', {headers: getHeaders()});
    return response.data;
}

export async function getTruck(id: string): Promise<TruckModel> {
    const response = await axios.get(baseUrl + '/trucks/' + id, {headers: getHeaders()});
    return response.data;
}

export async function registerTruck(form: RegisterTruckForm): Promise<Resource> {
    let response = await axios.post(baseUrl + '/trucks', form, {headers: getHeaders()});
    return response.data;
}

export async function getTrucks(search: TruckSearchCriteria): Promise<List<TruckListItemModel>> {
    const response = await axios.get(baseUrl + '/trucks', {params: search, headers: getHeaders()});
    return response.data;
}

export async function getFleetTypeOptions(): Promise<Option[]> {
    const response = await axios.get(baseUrl + '/dictionary/truck-fleet-types', {headers: getHeaders()});
    return response.data;
}

export async function getCargoBodyTypeOptions(): Promise<Option[]> {
    const response = await axios.get(baseUrl + '/dictionary/cargo-body-types', {headers: getHeaders()});
    return response.data;
}

export async function getRequiredDriverLicenseCategoryOptions(): Promise<Option[]> {
    const response = await axios.get(baseUrl + '/dictionary/driver-license-categories', {headers: getHeaders()});
    return response.data;
}

export async function getAxleConfigurationOptions(): Promise<Option[]> {
    const response = await axios.get(baseUrl + '/dictionary/axle-configurations', {headers: getHeaders()});
    return response.data;
}

export async function getNegotiations(search: NegotiationSearchCriteria): Promise<List<NegotiationListItem>> {
    const response = await axios.get(baseUrl + '/negotiations', {params: search, headers: getHeaders()});
    return response.data;
}

export async function getNegotiation(id: string): Promise<NegotiationModel> {
    const response = await axios.get(baseUrl + '/negotiations/' + id, {headers: getHeaders()});
    let data = response.data;
    for (let declaration of data.declarations) {
        declaration.issues = declaration.issues.map((issueWrapper: any) => {
            let constructorName = Object.keys(issueWrapper)[0];
            let issueData = issueWrapper[constructorName];
            issueData.declared = true;
            let mapping = {
                'WasteVolumeIssue': WasteVolumeIssue,
                'CollectionStartTimeIssue': CollectionStartTimeIssue,
                'CollectionPhotoIssue': CollectionPhotoIssue
            } as any;
            let constructor = mapping[constructorName];
            return new constructor(issueData);
        })
    }
    return data;
}

export async function declareNegotiationIssues(id: string, request: DeclareNegotiationIssuesRequest) {
    let data = {
        issues: request.issues.map(issue => {
            if (issue instanceof WasteVolumeIssue) {
                return {
                    'WasteVolumeIssue': {
                        id: issue.id,
                        destination: issue.destination,
                        collection: issue.collection,
                        comment: issue.comment
                    }
                }
            }
            if (issue instanceof CollectionPhotoIssue) {
                return {
                    'CollectionPhotoIssue': {
                        id: issue.id,
                        destination: issue.destination,
                        collection: issue.collection,
                        comment: issue.comment
                    }
                }
            }

            return {
                'CollectionStartTimeIssue': {
                    id: issue.id,
                    destination: issue.destination,
                    comment: issue.comment
                }
            }
        })
    }
    console.log('sumbit declaration request', data);
    let response = await axios.post(baseUrl + '/negotiations/' + id + '/issues/declaration', data, {headers: getHeaders()});
    return response.data;
}

export async function completeNegotiation(id: string): Promise<any> {
    let response = await axios.post(baseUrl + '/negotiations/' + id + '/completion', {}, {headers: getHeaders()});
    return response.data;
}

export async function solveNegotiationIssues(id: string, request: SolveNegotiationIssuesRequest) {
    let response = await axios.post(baseUrl + '/negotiations/' + id + '/issues/resolution', request, {headers: getHeaders()});
    return response.data;
}

export async function getReports(search: ReportSearchCriteria): Promise<List<ReportListItem>> {
    const response = await axios.get(baseUrl + '/reports', {params: search, headers: getHeaders()});
    return response.data;
}

export async function createReport(form: CreateReportForm): Promise<Resource> {
    let response = await axios.post(baseUrl + '/reports', form, {headers: getHeaders()});
    return response.data;
}
