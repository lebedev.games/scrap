import React, {Component} from "react";
import {AuthorizationUser} from "../../service/model";
import styles from "./Login.module.scss";
import {Field} from "../kit/Field";
import {TextInput} from "../kit/TextInput";
import {Button} from "../kit/Button";
import {Control, Form} from "../kit/forms";
import {required} from "../../data/validators";
import {authorize} from "../../service/handlers";

interface LoginProps {
    onComplete: (user: AuthorizationUser) => void;
}

interface LoginState {
    form: {
        login: Control,
        password: Control
    }
}

export class Login extends Component<LoginProps, LoginState> {

    constructor(props: LoginProps) {
        super(props);
        const control = Form.getControlConstructor(this);
        this.state = {
            form: {
                login: control({
                    value: '',
                    validators: [required()]
                }),
                password: control({
                    value: '',
                    validators: [required()]
                })
            }
        }
    }

    async logIn() {
        const {form} = this.state;
        const {onComplete} = this.props;

        Form.validate(form);
        if (!Form.isValid(form)) {
            return;
        }
        let request = {
            form: {
                'PasswordCredentials': {
                    login: form.login.value,
                    password: form.password.value
                }
            }
        }
        let user = await authorize(request)
        onComplete(user);
    }

    render() {
        const {form} = this.state;

        return (
            <div className={styles.container}>
                <div className={styles.form}>
                    <Field error={form.login.getError()}>
                        <label>Логин</label>
                        <TextInput control={form.login}/>
                    </Field>
                    <Field error={form.password.getError()}>
                        <label>Пароль</label>
                        <TextInput type="password" control={form.password}/>
                    </Field>
                    <Button onClick={() => this.logIn()}>Войти</Button>
                </div>
            </div>
        );
    }
}