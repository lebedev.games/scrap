import {emptyList, List, NegotiationListItem, NegotiationSearchCriteria, Option} from "../../service/model";
import React, {Component} from "react";
import {getNegotiations} from "../../service/handlers";
import {Surface} from "../kit/Surface";
import {Cell, Column, Row, Table, TableBody, TableHeader, TablePaginator} from "../kit/Table";
import {Button} from "../kit/Button";
import {formatDate} from "../../data/formatters";
import {PageContainer} from "../kit/PageContainer";
import {PagePanel} from "../kit/PagePanel";
import {DateInput} from "../kit/DateInput";
import {TextInput} from "../kit/TextInput";
import {OptionInput} from "../kit/OptionInput";
import {NegotiationStatus} from "../kit/NegotiationStatus";
import {debounceTime, switchMap} from "rxjs/operators";
import {Control, Form} from "../kit/forms";
import {Subject, Subscription} from "rxjs";

interface TransportationsProps {
    onRegisterTransportation: () => void;
    onOpenNegotiation: (negotiation: NegotiationListItem) => void;
}


interface TransportationsState {
    negotiationList: List<NegotiationListItem>,
    statusOptions: Option[],
    search: {
        transportationDate: Control,
        truck: Control,
        district: Control,
        status: Control,
        limit: Control,
        offset: Control
    }
}

export class Transportations extends Component<TransportationsProps, TransportationsState> {
    subscription = new Subscription();
    searchRequest: Subject<NegotiationSearchCriteria>;

    constructor(props: TransportationsProps) {
        super(props);
        let control = Form.getControlConstructor(this, () => this.searchRequest.next(this.getSearchCriteria()));
        this.state = {
            negotiationList: emptyList<NegotiationListItem>(),
            statusOptions: [
                {'id': 'pending_approval', name: 'На согласовании'},
                {'id': 'pending_resolution', name: 'Требует доработки'},
                {'id': 'completed', name: 'Согласован'},
            ],
            search: {
                transportationDate: control({value: null}),
                truck: control({value: ''}),
                district: control({value: ''}),
                status: control({value: null}),
                limit: control({value: 10}),
                offset: control({value: 0})
            }
        };
        this.searchRequest = new Subject<NegotiationSearchCriteria>();
    }

    async componentDidMount() {
        this.subscription.add(
            this.searchRequest.pipe(
                debounceTime(350),
                switchMap(search => getNegotiations(search))
            ).subscribe(negotiationList => {
                this.setState({negotiationList})
            })
        );

        const negotiationList = await getNegotiations(this.getSearchCriteria())
        this.setState({negotiationList})
    }

    componentWillUnmount() {
        this.subscription.unsubscribe()
    }

    getSearchCriteria(): NegotiationSearchCriteria {
        const {search} = this.state;
        return {
            transportationDate: search.transportationDate.value,
            truck: search.truck.value,
            district: search.district.value,
            transporter: '',
            status: search.status.value,
            limit: search.limit.value,
            offset: search.offset.value
        };
    }

    render() {
        const {onOpenNegotiation, onRegisterTransportation} = this.props;
        const {negotiationList, search, statusOptions} = this.state;

        return (
            <PageContainer>
                <PagePanel>
                    <h1>Маршрутные журналы</h1>
                    <Button onClick={onRegisterTransportation}>
                        Добавить маршрутный журнал
                    </Button>
                </PagePanel>
                <Surface>
                    <Table>
                        <TableHeader>
                            <Column>Дата вывоза</Column>
                            <Column>Номер ТС</Column>
                            <Column>Район</Column>
                            <Column>Статус</Column>
                        </TableHeader>
                        <TableHeader>
                            <Column>
                                <DateInput control={search.transportationDate}/>
                            </Column>
                            <Column>
                                <TextInput control={search.truck} icon="search"/>
                            </Column>
                            <Column>
                                <TextInput control={search.district} icon="search"/>
                            </Column>
                            <Column>
                                <OptionInput
                                    nullable
                                    control={search.status}
                                    options={statusOptions}/>
                            </Column>
                        </TableHeader>
                        <TableBody>
                            {negotiationList.items.map(negotiation => (
                                <Row key={negotiation.id} onClick={() => onOpenNegotiation(negotiation)}>
                                    <Cell>{formatDate(negotiation.transportationDate)}</Cell>
                                    <Cell>{negotiation.truck}</Cell>
                                    <Cell>{negotiation.district}</Cell>
                                    <Cell><NegotiationStatus status={negotiation.status}/></Cell>
                                </Row>
                            ))}
                        </TableBody>
                    </Table>
                    <TablePaginator
                        count={negotiationList.items.length}
                        total={negotiationList.total}
                        forms={['маршрутный журнал', 'маршрутных журнала', 'маршрутных журналов']}
                        limit={search.limit}
                        offset={search.offset}
                    />
                </Surface>
            </PageContainer>
        )
    }
}