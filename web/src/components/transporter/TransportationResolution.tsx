import React, {Component} from "react";
import {
    CollectionPhotoIssue,
    CollectionStartTimeIssue,
    NegotiationModel,
    Option,
    TransportationDestination,
    TransportationForm,
    WasteCollection,
    WasteVolumeIssue
} from "../../service/model";
import {
    getContainerTypeOptions,
    getGenerationPointOptions,
    getNegotiation,
    getTransportation,
    getTransporterTruckOptions,
    solveNegotiationIssues
} from "../../service/handlers";
import {choosePluralForm, formatDate, formatDateTime, formatDecimal, formatTime} from "../../data/formatters";
import {Surface} from "../kit/Surface";
import {Cell, Column, PhotosCell, Row, Table, TableBody, TableHeader} from "../kit/Table";
import {PageContainer} from "../kit/PageContainer";
import {PagePanel} from "../kit/PagePanel";
import {Field} from "../kit/Field";
import {TextInput} from "../kit/TextInput";
import styles from "./TransportationResolution.module.scss";
import {Button, TextButton} from "../kit/Button";
import {Claim, ClaimComponentStatus} from "../kit/Claim";
import {Control, Form} from "../kit/forms";
import {notEqual, required} from "../../data/validators";
import {parseDecimal} from "../../data/parsers";
import {Icon} from "../kit/Icon";
import {ImageInput} from "../kit/ImageInput";
import {TimeInput} from "../kit/TimeInput";
import {PhotoViewer} from "../kit/PhotoViewer";


export type Resolution = WasteVolumeIssueResolution | CollectionStartTimeIssueResolution | CollectionPhotoIssueResolution;



export class WasteVolumeIssueResolution {
    issueId: string
    issueDestination: string
    issueCollection: string
    issueComment: string
    issueDate: string

    value: any
    wasteVolume: Control
    resolved: boolean

    constructor(resolution: Required<WasteVolumeIssueResolution>) {
        this.issueId = resolution.issueId;
        this.issueDestination = resolution.issueDestination;
        this.issueCollection = resolution.issueCollection;
        this.issueComment = resolution.issueComment;
        this.issueDate = resolution.issueDate;
        this.wasteVolume = resolution.wasteVolume;
        this.resolved = resolution.resolved;
        this.value = resolution.value;
    }
}



export class CollectionStartTimeIssueResolution {
    issueId: string
    issueDestination: string
    issueComment: string
    issueDate: string

    value: any
    startTime: Control
    resolved: boolean

    constructor(resolution: Required<CollectionStartTimeIssueResolution>) {
        this.issueId = resolution.issueId;
        this.issueDestination = resolution.issueDestination;
        this.issueComment = resolution.issueComment;
        this.issueDate = resolution.issueDate;
        this.startTime = resolution.startTime;
        this.resolved = resolution.resolved;
        this.value = resolution.value;
    }
}


export class CollectionPhotoIssueResolution {
    issueId: string
    issueDestination: string
    issueCollection: string
    issueComment: string
    issueDate: string

    value: any
    photos: Control
    resolved: boolean

    constructor(resolution: Required<CollectionPhotoIssueResolution>) {
        this.issueId = resolution.issueId;
        this.issueDestination = resolution.issueDestination;
        this.issueCollection = resolution.issueCollection;
        this.issueComment = resolution.issueComment;
        this.issueDate = resolution.issueDate;
        this.photos = resolution.photos;
        this.resolved = resolution.resolved;
        this.value = resolution.value;
    }
}


interface IssueForm {
    resolution: Resolution | null
}

interface TransportationResolutionProps {
    id: string,
    onCancel: () => void;
    onSolveNegotiationIssues: (transportation: string) => void;
}


interface TransportationResolutionState {
    negotiation: NegotiationModel | null
    transportation: TransportationForm | null
    issueForm: IssueForm
    resolutions: Resolution[]
    containerTypeOptions: Option[]
    truckOptions: Option[]
    generationPointOptions: Option[]
}

export class TransportationResolution extends Component<TransportationResolutionProps, TransportationResolutionState> {

    photoViewer = React.createRef<PhotoViewer>();

    constructor(props: TransportationResolutionProps) {
        super(props);
        this.state = {
            negotiation: null,
            transportation: null,
            issueForm: {
                resolution: null
            },
            resolutions: [],
            containerTypeOptions: [],
            truckOptions: [],
            generationPointOptions: []
        }
    }

    async componentDidMount() {
        const {id} = this.props;
        let negotiation = await getNegotiation(id);
        this.setState({negotiation})
        let transportation = await getTransportation(negotiation.transportation);
        this.setState({transportation})
        this.defineTransportationIssues(negotiation, transportation);
        let containerTypeOptions = await getContainerTypeOptions()
        this.setState({containerTypeOptions})
        let truckOptions = await getTransporterTruckOptions()
        this.setState({truckOptions})
        let generationPointOptions = await getGenerationPointOptions()
        this.setState({generationPointOptions})
    }

    defineTransportationIssues(negotiation: NegotiationModel, transportation: TransportationForm) {
        let declaration = negotiation.declarations[negotiation.declarations.length - 1];
        let resolutions = [];
        let control = Form.getControlConstructor(this);
        for (let issueObject of declaration.issues) {

            if (issueObject instanceof WasteVolumeIssue) {
                let issue = issueObject as WasteVolumeIssue;
                let destination = transportation
                    .destinations
                    .find(destination => destination.id === issue.destination) as TransportationDestination;
                let collection = destination
                    .collections
                    .find(collection => collection.id === issue.collection) as WasteCollection;
                let value = collection.wasteVolumeM3;
                resolutions.push(new WasteVolumeIssueResolution({
                    issueId: issue.id,
                    issueDestination: issue.destination,
                    issueCollection: issue.collection,
                    issueComment: issue.comment,
                    issueDate: declaration.date,
                    value,
                    wasteVolume: control({
                        value,
                        setter: parseDecimal,
                        getter: formatDecimal,
                        validators: [
                            required(),
                            notEqual(value)
                        ]
                    }),
                    resolved: false
                }))
            }
            if (issueObject instanceof CollectionStartTimeIssue) {
                let issue = issueObject as CollectionStartTimeIssue;
                let destination = transportation
                    .destinations
                    .find(destination => destination.id === issue.destination) as TransportationDestination;
                let value = destination.collectionStartTime;
                resolutions.push(new CollectionStartTimeIssueResolution({
                    issueId: issue.id,
                    issueDestination: issue.destination,
                    issueComment: issue.comment,
                    issueDate: declaration.date,
                    value,
                    startTime: control({
                        value,
                        validators: [
                            required(),
                            notEqual(value)
                        ]
                    }),
                    resolved: false
                }))
            }
            if (issueObject instanceof CollectionPhotoIssue) {
                let issue = issueObject as CollectionPhotoIssue;
                let destination = transportation
                    .destinations
                    .find(destination => destination.id === issue.destination) as TransportationDestination;
                let collection = destination
                    .collections
                    .find(collection => collection.id === issue.collection) as WasteCollection;
                let value = collection.photos;
                resolutions.push(new CollectionPhotoIssueResolution({
                    issueId: issue.id,
                    issueDestination: issue.destination,
                    issueCollection: issue.collection,
                    issueComment: issue.comment,
                    issueDate: declaration.date,
                    value,
                    photos: control({
                        value,
                        validators: [
                            required(),
                            notEqual(value)
                        ]
                    }),
                    resolved: false
                }))
            }
        }

        this.setState({resolutions})
    }

    toggleEditing(resolution: Resolution) {
        let {issueForm} = this.state;
        if (issueForm.resolution?.issueId !== resolution.issueId) {
            issueForm.resolution = resolution;
        } else {
            issueForm.resolution = null;
        }
        this.setState({issueForm})
    }

    solveIssue(issueId: string) {
        const {resolutions} = this.state;
        for (let resolution of resolutions) {
            if (resolution.issueId === issueId) {
                Form.validate(resolution);
                if (Form.isValid(resolution)) {
                    resolution.resolved = true;
                    this.setState({resolutions})
                    this.closeEditingForm();
                    return;
                }
            }
        }
    }

    removeIssueResolution(issueId: string) {
        const {resolutions} = this.state;
        for (let resolution of resolutions) {
            if (resolution.issueId === issueId) {
                resolution.resolved = false;
                if (resolution instanceof WasteVolumeIssueResolution) {
                    resolution.wasteVolume.setValue(resolution.value);
                }
                if (resolution instanceof CollectionStartTimeIssueResolution) {
                    resolution.startTime.setValue(resolution.value);
                }
                if (resolution instanceof CollectionPhotoIssueResolution) {
                    resolution.photos.setValue(resolution.value);
                }
                this.setState({resolutions})
                this.closeEditingForm();
                return;
            }
        }
    }

    closeEditingForm() {
        const {issueForm} = this.state;
        issueForm.resolution = null;
        this.setState({issueForm})
    }

    countUnResolvedIssues(): number {
        const {resolutions} = this.state;
        let count = 0;
        for (let resolution of resolutions) {
            if (!resolution.resolved) {
                count += 1;
            }
        }
        return count;
    }

    async solveNegotiationIssues() {
        const {onSolveNegotiationIssues} = this.props;
        const {resolutions, negotiation} = this.state;
        if (!negotiation) {
            return;
        }
        let request = {
            resolutions: resolutions.map(resolution => {
                if (resolution instanceof WasteVolumeIssueResolution) {
                    return {
                        'WasteVolumeIssueResolution' : {
                            issueId: resolution.issueId,
                            wasteVolume: resolution.wasteVolume.value
                        }
                    }
                }
                if (resolution instanceof CollectionPhotoIssueResolution) {
                    return {
                        'CollectionPhotoIssueResolution' : {
                            issueId: resolution.issueId,
                            photos: resolution.photos.value
                        }
                    }
                }

                return {
                    'CollectionStartTimeIssueResolution': {
                        issueId: resolution.issueId,
                        startTime: resolution.startTime.value
                    }
                }
            })
        } as any;
        console.log('sumbit resolution request', request);
        await solveNegotiationIssues(negotiation.id, request);
        onSolveNegotiationIssues(negotiation.transportation);
    }

    render() {
        const {
            transportation,
            negotiation,
            issueForm,
            resolutions,
            containerTypeOptions,
            truckOptions,
            generationPointOptions,
        } = this.state;

        if (!transportation || !negotiation) {
            return <div>Загрузка...</div>
        }

        const unresolvedIssues = this.countUnResolvedIssues();

        const nameOf = (value: string, options: Option[]) => {
            for (let option of options) {
                if (option.id === value) {
                    return option.name;
                }
            }
            return value;
        }

        const rows = [];

        let destinationNumber = 0;

        const getClaimStatus = (resolution: Resolution): ClaimComponentStatus => {
            if (issueForm.resolution?.issueId === resolution.issueId) {
                return 'editing';
            }
            if (resolution.resolved) {
                return 'resolved';
            }
            return 'open';
        }

        const renderClaim = (resolution: Resolution) => {
            return <Claim status={getClaimStatus(resolution)} onClick={() => this.toggleEditing(resolution)}/>
        }

        const getWasteVolumeResolution = (destination: string, collection: string): WasteVolumeIssueResolution | null => {
            for (let resolution of resolutions as Resolution[]) {
                if (resolution instanceof WasteVolumeIssueResolution && resolution.issueDestination === destination && resolution.issueCollection === collection) {
                    return resolution;
                }
            }
            return null
        }

        const getCollectionPhotoResolution = (destination: string, collection: string): CollectionPhotoIssueResolution | null => {
            for (let resolution of resolutions as Resolution[]) {
                if (resolution instanceof CollectionPhotoIssueResolution && resolution.issueDestination === destination && resolution.issueCollection === collection) {
                    return resolution;
                }
            }
            return null
        }

        const getCollectionStartResolution = (destination: string): CollectionStartTimeIssueResolution | null => {
            for (let resolution of resolutions as Resolution[]) {
                if (resolution instanceof CollectionStartTimeIssueResolution && resolution.issueDestination === destination) {
                    return resolution;
                }
            }
            return null
        }

        for (let destination of transportation.destinations) {
            destinationNumber += 1;

            let collectionRows = [];
            for (let collection of destination.collections) {
                let wasteVolumeResolution = getWasteVolumeResolution(destination.id, collection.id);
                let collectionPhotoResolution = getCollectionPhotoResolution(destination.id, collection.id);

                let wasteVolume = wasteVolumeResolution && wasteVolumeResolution.resolved ?
                    wasteVolumeResolution.wasteVolume.value
                    :
                    collection.wasteVolumeM3;

                let photos = collectionPhotoResolution && collectionPhotoResolution.resolved ?
                    collectionPhotoResolution.photos.value
                    :
                    collection.photos;

                let collectionRow = (
                    <Row key={collection.id}>
                        <Cell width={160}>
                            {nameOf(collection.containerType, containerTypeOptions)}
                        </Cell>
                        <Cell width={120}>
                            {wasteVolume}
                            {wasteVolumeResolution && renderClaim(wasteVolumeResolution)}
                        </Cell>
                        <PhotosCell width={308} photos={photos} onPreview={
                            (photo) => {
                                this.photoViewer.current?.showPhotos(photo, collection.photos)
                            }
                        }>
                            {collectionPhotoResolution && renderClaim(collectionPhotoResolution)}
                        </PhotosCell>
                    </Row>
                )
                collectionRows.push(collectionRow)
            }

            let collectionStartResolution = getCollectionStartResolution(destination.id);
            let collectionStartTime = collectionStartResolution && collectionStartResolution.resolved ?
                collectionStartResolution.startTime.value
                :
                destination.collectionStartTime;

            let row = (
                <Row key={destination.id}>
                    <Cell>
                        {destinationNumber}
                    </Cell>
                    <Cell>
                        {formatTime(collectionStartTime)}
                        {collectionStartResolution && renderClaim(collectionStartResolution)}
                    </Cell>
                    <Cell>
                        {nameOf(destination.wasteGenerationPoint, generationPointOptions)}
                    </Cell>
                    <Cell merge columns={3}>
                        <Table>
                            <TableBody>
                                {collectionRows}
                            </TableBody>
                        </Table>
                    </Cell>
                </Row>
            )
            rows.push(row);

            if (issueForm.resolution && issueForm.resolution.issueDestination === destination.id) {
                let resolutionObject = issueForm.resolution;
                let resolutionForm;
                if (resolutionObject instanceof WasteVolumeIssueResolution) {
                    let resolution = resolutionObject;
                    resolutionForm = (
                        <section className={styles.issueForm}>
                            <div className={styles.issueComment}>
                                <div className={styles.issueCommentIcon}><Icon name="claimOpen"/></div>
                                {resolution.issueComment}
                                <label>{formatDateTime(resolution.issueDate)}</label>
                            </div>
                            <Field width={160} error={resolution.wasteVolume.getError()}>
                                <label>Суммарный объем, м3</label>
                                <TextInput readonly={resolution.resolved} control={resolution.wasteVolume}/>
                            </Field>
                            {resolution.resolved ?
                                <div className={styles.issueButtons}>
                                    <TextButton
                                        type="regular"
                                        onClick={() => this.removeIssueResolution(resolution.issueId)}
                                    >
                                        <div className={styles.closeIcon}><Icon name="erase"/></div>
                                        Удалить изменения
                                    </TextButton>
                                </div>
                                :
                                <div className={styles.issueButtons}>
                                    <TextButton type="regular" onClick={() => this.solveIssue(resolution.issueId)}>
                                        <div className={styles.resolveIcon}><Icon name="claimResolved"/></div>
                                        Применить изменения
                                    </TextButton>
                                    <TextButton type="regular" onClick={() => this.closeEditingForm()}>
                                        <div className={styles.closeIcon}><Icon name="erase"/></div>
                                        Закрыть
                                    </TextButton>
                                </div>
                            }
                        </section>
                    );
                }
                if (resolutionObject instanceof CollectionPhotoIssueResolution) {
                    let resolution = resolutionObject;
                    resolutionForm = (
                        <section className={styles.issueForm}>
                            <div className={styles.issueComment}>
                                <div className={styles.issueCommentIcon}><Icon name="claimOpen"/></div>
                                {resolution.issueComment}
                                <label>{formatDateTime(resolution.issueDate)}</label>
                            </div>
                            <Field error={resolution.photos.getError()}>
                                <label>Фотографии контейнера</label>
                                <ImageInput control={resolution.photos}/>
                            </Field>
                            {resolution.resolved ?
                                <div className={styles.issueButtons}>
                                    <TextButton
                                        type="regular"
                                        onClick={() => this.removeIssueResolution(resolution.issueId)}
                                    >
                                        <div className={styles.closeIcon}><Icon name="erase"/></div>
                                        Удалить изменения
                                    </TextButton>
                                </div>
                                :
                                <div className={styles.issueButtons}>
                                    <TextButton type="regular" onClick={() => this.solveIssue(resolution.issueId)}>
                                        <div className={styles.resolveIcon}><Icon name="claimResolved"/></div>
                                        Применить изменения
                                    </TextButton>
                                    <TextButton type="regular" onClick={() => this.closeEditingForm()}>
                                        <div className={styles.closeIcon}><Icon name="erase"/></div>
                                        Закрыть
                                    </TextButton>
                                </div>
                            }
                        </section>
                    );
                }
                if (resolutionObject instanceof CollectionStartTimeIssueResolution) {
                    let resolution = resolutionObject;
                    resolutionForm = (
                        <section className={styles.issueForm}>
                            <div className={styles.issueComment}>
                                <div className={styles.issueCommentIcon}><Icon name="claimOpen"/></div>
                                {resolution.issueComment}
                                <label>{formatDateTime(resolution.issueDate)}</label>
                            </div>
                            <Field width={160} error={resolution.startTime.getError()}>
                                <label>Время сбора</label>
                                <TimeInput control={resolution.startTime}/>
                            </Field>
                            {resolution.resolved ?
                                <div className={styles.issueButtons}>
                                    <TextButton
                                        type="regular"
                                        onClick={() => this.removeIssueResolution(resolution.issueId)}
                                    >
                                        <div className={styles.closeIcon}><Icon name="erase"/></div>
                                        Удалить изменения
                                    </TextButton>
                                </div>
                                :
                                <div className={styles.issueButtons}>
                                    <TextButton type="regular" onClick={() => this.solveIssue(resolution.issueId)}>
                                        <div className={styles.resolveIcon}><Icon name="claimResolved"/></div>
                                        Применить изменения
                                    </TextButton>
                                    <TextButton type="regular" onClick={() => this.closeEditingForm()}>
                                        <div className={styles.closeIcon}><Icon name="erase"/></div>
                                        Закрыть
                                    </TextButton>
                                </div>
                            }
                        </section>
                    );
                }
                let row = (
                    <Row key={resolutionObject.issueId}>
                        <Cell merge columns={6}>
                            {resolutionForm}
                        </Cell>
                    </Row>
                )
                rows.push(row)
            }
        }

        return (
            <PageContainer>
                <PagePanel>
                    <h1>
                        Исправление маршрутного
                        журнала {nameOf(transportation.truck, truckOptions)} на {formatDate(transportation.executionDate)}
                    </h1>
                    {unresolvedIssues > 0 ?
                        <Button
                            disabled>Еще {unresolvedIssues} {choosePluralForm(['замечание', 'замечания', 'замечаний'], unresolvedIssues)} нужно
                            исправить</Button>
                        :
                        <Button onClick={() => this.solveNegotiationIssues()}>Отправить на согласование</Button>
                    }

                </PagePanel>
                <Surface>
                    <Table>
                        <TableHeader>
                            <Column>#</Column>
                            <Column width={80}>Время</Column>
                            <Column>Адрес КП</Column>
                            <Column width={160}>Тип контейнера</Column>
                            <Column width={120}>Объем, м3</Column>
                            <Column width={308}>Фотографии</Column>
                        </TableHeader>
                        <TableBody>
                            {rows}
                        </TableBody>
                    </Table>
                </Surface>
                <div style={{paddingTop: '72px'}}/>
                <PhotoViewer ref={this.photoViewer}/>
            </PageContainer>
        );
    }

}