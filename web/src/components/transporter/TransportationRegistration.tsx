import React, {Component} from "react";
import {Control, Form} from "../kit/forms";
import moment from "moment";
import styles from "./TransportationRegistration.module.scss";
import {Surface, SurfaceFooter} from "../kit/Surface";
import {Button} from "../kit/Button";
import {Compound, Field, FieldsSeparator, FormDivision, FormSection, FormSurface} from "../kit/Field";
import {OptionInput} from "../kit/OptionInput";
import {Option, TargetGenerationPoint, TransportationDestination} from "../../service/model";
import {getTargetGenerationPoints, getTransporterTruckOptions, registerTransportation} from "../../service/handlers";
import {DateInput} from "../kit/DateInput";
import {Cell, Column, PhotosCell, Row, Table, TableBody, TableHeader} from "../kit/Table";
import {formatDecimal, formatTime} from "../../data/formatters";
import {TextInput} from "../kit/TextInput";
import {TimeInput} from "../kit/TimeInput";
import {ImageInput} from "../kit/ImageInput";
import * as uuid from "uuid";
import {notEmptyCollection, required} from "../../data/validators";
import {parseDecimal} from "../../data/parsers";
import {Icon} from "../kit/Icon";
import {PageContainer} from "../kit/PageContainer";
import {PagePanel} from "../kit/PagePanel";
import {PhotoViewer} from "../kit/PhotoViewer";

type Mode = 'insert' | 'update';

interface TransportationRegistrationProps {
    onRegistrationComplete: (id: string) => void
    onCancel: () => void
}

interface CollectionForm {
    id: string,
    containerType: string,
    volume: Control
    photos: Control
}


interface TransportationRegistrationState {
    truckOptions: Option[],
    targetGenerationPoints: TargetGenerationPoint[],
    targetGenerationPointOptions: Option[],
    destinations: TransportationDestination[],
    destinationForm: {
        id: string
        collectionStartTime: Control,
        wasteGenerationPoint: Control
    },
    destinationCollectionForms: CollectionForm[],
    form: {
        truck: Control,
        executionDate: Control
    },
    mode: Mode
}

export class TransportationRegistration extends Component<TransportationRegistrationProps, TransportationRegistrationState> {

    photoViewer = React.createRef<PhotoViewer>();

    constructor(props: TransportationRegistrationProps) {
        super(props);
        const control = Form.getControlConstructor(this);
        this.state = {
            mode: 'insert',
            truckOptions: [],
            targetGenerationPoints: [],
            targetGenerationPointOptions: [],
            destinationForm: {
                id: uuid.v4(),
                collectionStartTime: control({
                    value: moment().toISOString(),
                    validators: [
                        required()
                    ]
                }),
                wasteGenerationPoint: control({
                    value: null,
                    setter: (value) => this.setWasteGenerationPoint(value),
                    validators: [
                        required()
                    ]
                })
            },
            destinationCollectionForms: [],
            destinations: [],
            form: {
                truck: control({
                    value: null
                }),
                executionDate: control({
                    value: moment().toISOString()
                })
            }
        }
    }

    async componentDidMount() {
        let truckOptions = await getTransporterTruckOptions();
        this.setState({truckOptions});
        let targetGenerationPoints = await getTargetGenerationPoints();
        this.setState({targetGenerationPoints});
        let targetGenerationPointOptions = [];
        for (let target of targetGenerationPoints) {
            targetGenerationPointOptions.push({
                id: target.id,
                name: target.address
            })
        }
        this.setState({targetGenerationPointOptions})
        const {destinationForm} = this.state;
        if (targetGenerationPoints.length > 0) {
            destinationForm.wasteGenerationPoint.setValue(targetGenerationPoints[0].id);
        }
        const {form} = this.state;
        if (truckOptions.length > 0) {
            form.truck.setValue(truckOptions[0].id);
        }
    }

    async registerTransportation() {
        const {form, destinations} = this.state;
        const {onRegistrationComplete} = this.props;

        Form.validate(form);
        if (!Form.isValid(form)) {
            return;
        }

        const registration = {
            truck: form.truck.value,
            executionDate: form.executionDate.value,
            destinations: destinations
        }
        let generator = await registerTransportation(registration);
        onRegistrationComplete(generator.id);
    }

    setWasteGenerationPoint(id: string): string {
        let generationPoint = this.getTargetGenerationPoint(id);
        let destinationCollectionForms = [];
        let control = Form.getControlConstructor(this);
        if (generationPoint) {
            for (let containerType of generationPoint.containerTypes) {
                destinationCollectionForms.push({
                    id: uuid.v4(),
                    containerType: containerType.id,
                    volume: control({
                        value: '0.0',
                        setter: parseDecimal,
                        getter: formatDecimal,
                        validators: [
                            required()
                        ]
                    }),
                    photos: control({
                        value: [],
                        validators: [
                            notEmptyCollection('Необходимо добавить фотографии контейнера')
                        ]
                    })
                })
            }
        }
        this.setState({destinationCollectionForms})
        return id;
    }

    formatContainerTypeName(generationPointId: string, id: string): string {
        const {targetGenerationPoints} = this.state;
        for (let generationPoint of targetGenerationPoints) {
            if (generationPoint.id === generationPointId) {
                for (let containerType of generationPoint.containerTypes) {
                    if (containerType.id === id) {
                        return containerType.name;
                    }
                }

                break;
            }
        }
        return 'none!';
    }

    formatGenerationPointAddress(generationPointId: string): string {
        const {targetGenerationPoints} = this.state;
        for (let generationPoint of targetGenerationPoints) {
            if (generationPoint.id === generationPointId) {
                return generationPoint.address;
            }
        }
        return 'none!';
    }

    getTargetGenerationPoint(generationPointId: string): TargetGenerationPoint | null {
        const {targetGenerationPoints} = this.state;
        for (let generationPoint of targetGenerationPoints) {
            if (generationPoint.id === generationPointId) {
                return generationPoint;
            }
        }
        return null;
    }

    appendDestination() {
        const {destinationCollectionForms, destinationForm} = this.state;
        Form.validate(destinationForm);
        for (let form of destinationCollectionForms) {
            Form.validate(form);
        }

        if (!Form.isValid(destinationForm)) {
            return;
        }
        for (let form of destinationCollectionForms) {
            if (!Form.isValid(form)) {
                return;
            }
        }

        const {destinations, targetGenerationPoints} = this.state;
        let collections = [];
        for (let form of destinationCollectionForms) {
            collections.push({
                id: form.id,
                destination: destinationForm.id,
                containerType: form.containerType,
                containerVolume: '0.0',
                containerUtilization: '0.0',
                wasteVolumeM3: form.volume.value,
                photos: form.photos.value
            })
        }
        destinations.push(
            {
                id: destinationForm.id,
                collectionStartTime: destinationForm.collectionStartTime.value,
                wasteGenerationPoint: destinationForm.wasteGenerationPoint.value,
                collections: collections
            }
        )
        this.setState({destinations});

        // reset to new destination
        destinationForm.id = uuid.v4();
        this.setState({destinationForm});
        if (targetGenerationPoints.length > 0) {
            this.setWasteGenerationPoint(targetGenerationPoints[0].id);
        }
    }

    applyDesintationChanges() {
        const {destinationCollectionForms, destinationForm} = this.state;
        Form.validate(destinationForm);
        for (let form of destinationCollectionForms) {
            Form.validate(form);
        }

        if (!Form.isValid(destinationForm)) {
            return;
        }
        for (let form of destinationCollectionForms) {
            if (!Form.isValid(form)) {
                return;
            }
        }

        const {destinations, targetGenerationPoints} = this.state;
        let collections = [];
        for (let form of destinationCollectionForms) {
            collections.push({
                id: form.id,
                destination: destinationForm.id,
                containerType: form.containerType,
                containerVolume: '0.0',
                containerUtilization: '0.0',
                wasteVolumeM3: form.volume.value,
                photos: form.photos.value
            })
        }

        const index = destinations.findIndex(destination => destination.id === destinationForm.id);
        if (index !== -1) {
            destinations[index] = {
                id: destinationForm.id,
                collectionStartTime: destinationForm.collectionStartTime.value,
                wasteGenerationPoint: destinationForm.wasteGenerationPoint.value,
                collections: collections
            }
            this.setState({destinations});
        }

        // reset to new destination
        destinationForm.id = uuid.v4();
        this.setState({destinationForm, mode: 'insert'});
        if (targetGenerationPoints.length > 0) {
            this.setWasteGenerationPoint(targetGenerationPoints[0].id)
        }
    }

    cancelDestinationChanges() {
        const {targetGenerationPoints, destinationForm} = this.state;
        destinationForm.id = uuid.v4();
        this.setState({destinationForm, mode: 'insert'});
        if (targetGenerationPoints.length > 0) {
            this.setWasteGenerationPoint(targetGenerationPoints[0].id)
        }
    }


    startUpdateDestination(destination: TransportationDestination) {
        const {destinationForm} = this.state;
        destinationForm.id = destination.id;
        destinationForm.collectionStartTime.value = destination.collectionStartTime;
        destinationForm.wasteGenerationPoint.value = destination.wasteGenerationPoint;

        let destinationCollectionForms = [];
        let control = Form.getControlConstructor(this);
        for (let collection of destination.collections) {
            destinationCollectionForms.push({
                id: collection.id,
                containerType: collection.containerType,
                volume: control({
                    value: collection.wasteVolumeM3,
                    setter: parseDecimal,
                    getter: formatDecimal,
                    validators: [
                        required()
                    ]
                }),
                photos: control({
                    value: collection.photos,
                    validators: [
                        notEmptyCollection('Необходимо добавить фотографии контейнера')
                    ]
                })
            })
        }

        this.setState({mode: 'update', destinationForm, destinationCollectionForms})
    }

    getDestinationIndex() {
        const {destinations, destinationForm} = this.state;
        let index = destinations.findIndex(destination => destination.id === destinationForm.id);
        return index + 1;
    }

    render() {
        const {
            form,
            truckOptions,
            destinations,
            targetGenerationPointOptions,
            destinationForm,
            destinationCollectionForms,
            mode
        } = this.state;
        const {onCancel} = this.props;

        const rows = [];

        let destinationNumber = 0;
        for (let destination of destinations) {
            destinationNumber += 1;
            for (let index = 0; index < destination.collections.length; index++) {
                let collection = destination.collections[index];
                let row = index === 0 ?
                    <Row key={collection.id} onClick={() => this.startUpdateDestination(destination)}>
                        <Cell rowspan={destination.collections.length}>
                            {(mode === 'update' && destination.id === destinationForm.id) ?
                                <div className={styles.actionIcon}>
                                    <Icon name="edit" />
                                </div>
                                :
                                <div >{destinationNumber}</div>
                            }

                        </Cell>
                        <Cell rowspan={destination.collections.length}>
                            {formatTime(destination.collectionStartTime)}
                        </Cell>
                        <Cell rowspan={destination.collections.length}>
                            {this.formatGenerationPointAddress(destination.wasteGenerationPoint)}
                        </Cell>
                        <Cell>
                            {this.formatContainerTypeName(destination.wasteGenerationPoint, collection.containerType)}
                        </Cell>
                        <Cell>{collection.wasteVolumeM3}</Cell>
                        <PhotosCell width={280} photos={collection.photos} onPreview={
                            (photo) => {
                                this.photoViewer.current?.showPhotos(photo, collection.photos)
                            }
                        }>
                        </PhotosCell>
                    </Row>
                    :
                    <Row key={collection.id} onClick={() => this.startUpdateDestination(destination)}>
                        <Cell>
                            {this.formatContainerTypeName(destination.wasteGenerationPoint, collection.containerType)}
                        </Cell>
                        <Cell>{collection.wasteVolumeM3}</Cell>
                        <PhotosCell width={280} photos={collection.photos} onPreview={
                            (photo) => {
                                this.photoViewer.current?.showPhotos(photo, collection.photos)
                            }
                        }>
                        </PhotosCell>
                    </Row>;
                rows.push(row);
            }
        }

        return (
            <PageContainer>
                <PagePanel>
                    <h1>Добавление маршрутного журнала</h1>
                </PagePanel>
                <Surface>
                    <FormSection title="Вывоз">
                        <Compound>
                            <Field width={160}>
                                <label>Дата</label>
                                <DateInput control={form.executionDate}/>
                            </Field>
                            <Field width={160}>
                                <label>Транспортное средство</label>
                                <OptionInput control={form.truck} options={truckOptions}/>
                            </Field>
                        </Compound>
                    </FormSection>
                    <FormSurface title="Маршрутный журнал">
                        <Table>
                            <TableHeader>
                                <Column>#</Column>
                                <Column>Время</Column>
                                <Column>Адрес КП</Column>
                                <Column>Тип контейнера</Column>
                                <Column>Объем, м3</Column>
                                <Column>Фотографии</Column>
                            </TableHeader>
                            <TableBody>
                                {rows}
                            </TableBody>
                        </Table>
                    </FormSurface>
                    <FormSection
                        title={mode === 'insert' ? "Добавление маршрутной точки" : "Изменение маршрутной точки #" + this.getDestinationIndex()}>
                        <Compound>
                            <Field width={84}>
                                <label>Время</label>
                                <TimeInput control={destinationForm.collectionStartTime}/>
                            </Field>
                            <Field>
                                <label>Адрес контейнерной площадки (КП)</label>
                                <OptionInput control={destinationForm.wasteGenerationPoint}
                                             options={targetGenerationPointOptions}/>
                            </Field>
                        </Compound>
                        <FieldsSeparator/>
                        {destinationCollectionForms.map(form => (
                            <FormDivision key={form.id}>
                                <header className={styles.collectionHeader}>
                                    {this.formatContainerTypeName(destinationForm.wasteGenerationPoint.value, form.containerType)}
                                </header>
                                <Compound>
                                    <Field width={160} error={form.volume.getError()}>
                                        <label>Суммарный объем, м3</label>
                                        <TextInput control={form.volume}/>
                                    </Field>
                                    <Field error={form.photos.getError()}>
                                        <label>Фотографии контейнера</label>
                                        <ImageInput control={form.photos}/>
                                    </Field>
                                </Compound>

                            </FormDivision>
                        ))}
                        <FieldsSeparator/>
                        {mode === 'insert' ?
                            <div className={styles.formButtons}>
                                <Button onClick={() => this.appendDestination()}>
                                    Добавить точку
                                </Button>
                            </div>
                            :
                            <div className={styles.formButtons}>
                                <Button onClick={() => this.applyDesintationChanges()}>
                                    Применить изменения
                                </Button>
                                <Button type="outline" onClick={() => this.cancelDestinationChanges()}>
                                    Отменить изменения
                                </Button>
                            </div>
                        }

                    </FormSection>
                    <SurfaceFooter>
                        <Button type="primary" onClick={() => this.registerTransportation()}>
                            Добавить маршрутный журнал
                        </Button>
                        <Button type="outline" onClick={onCancel}>Отмена</Button>
                    </SurfaceFooter>
                </Surface>
                <div style={{paddingTop: '72px'}}/>
                <PhotoViewer ref={this.photoViewer}/>
            </PageContainer>
        )
    }
}