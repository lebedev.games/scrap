import React, {Component} from "react";
import {BrowserRouter, Redirect, Route, Switch} from "react-router-dom";
import {Link, PageHeader} from "../kit/PageHeader";
import {Transportations} from "./Transportations";
import {TransportationRegistration} from "./TransportationRegistration";
import {Transportation} from "./Transportation";
import {Icon} from "../kit/Icon";
import styles from "./TransporterPage.module.scss";
import {AuthorizationUser, NegotiationListItem} from "../../service/model";
import {logOutCurrentUser} from "../../service/context";
import {TransportationResolution} from "./TransportationResolution";

interface TransporterPageProps {
    user: AuthorizationUser
}

interface TransporterPageState {
}

export class TransporterPage extends Component<TransporterPageProps, TransporterPageState> {

    constructor(props: TransporterPageProps) {
        super(props);
        this.state = {}
    }

    render() {
        const {user} = this.props;

        return (
            <BrowserRouter>
                <PageHeader>
                    <Link to="/transporter/transportations">Маршрутные журналы</Link>
                    <aside>
                        <span className={styles.user}>{user.userName}</span>
                        <div className={styles.userExitIcon} onClick={() => logOutCurrentUser()}>
                            <Icon name="update"/>
                        </div>
                    </aside>
                </PageHeader>
                <Switch>
                    <Route exact path="/transporter/transportations/registration">
                        <TransportationRegistration
                            onCancel={() => this.openTransportations()}
                            onRegistrationComplete={id => this.openTransportationReadonly(id)}
                        />
                    </Route>
                    <Route path="/transporter/transportations/resolution/:id" render={props => {
                        let {id} = props.match.params;
                        return <TransportationResolution
                            id={id}
                            onCancel={() => this.openTransportations()}
                            onSolveNegotiationIssues={id => this.openTransportationReadonly(id)}
                        />
                    }}/>
                    <Route path="/transporter/transportations/:id" render={props => {
                        let {id} = props.match.params;
                        return <Transportation id={id}/>
                    }}/>
                    <Route path="/transporter/transportations">
                        <Transportations
                            onOpenNegotiation={negotiation => this.openNegotiation(negotiation)}
                            onRegisterTransportation={() => this.registerTransportation()}
                        />
                    </Route>
                    <Route path="/transporter/trucks">
                        Транспортные средства
                    </Route>
                    <Route path="/" render={() => this.openTransporterInitialPage()}/>
                </Switch>
            </BrowserRouter>
        )
    }

    private openTransportations() {
        this.openPage('/transporter/transportations');
    }

    private registerTransportation() {
        this.openPage('/transporter/transportations/registration')
    }

    private openNegotiation(negotiation: NegotiationListItem) {
        if (negotiation.status === 'pending_approval') {
            this.openTransportationReadonly(negotiation.transportation);
        }
        if (negotiation.status === 'completed') {
            this.openTransportationReadonly(negotiation.transportation);
        }
        if (negotiation.status === 'pending_resolution') {
            this.openTransportationResolution(negotiation.id);
        }
    }

    private openTransportationReadonly(id: string) {
        this.openPage('/transporter/transportations/' + id);
    }

    private openTransportationResolution(negotiation: string) {
        this.openPage('/transporter/transportations/resolution/' + negotiation);
    }

    openPage(path: string) {
        window.history.pushState(null, "Title", path);
        window.dispatchEvent(new window.PopStateEvent('popstate'));
    }

    private openTransporterInitialPage() {
        return <Redirect to="/transporter/transportations"/>
    }
}