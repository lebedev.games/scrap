import React, {Component} from "react";
import styles from "./Transportation.module.scss";
import {Option, TargetGenerationPoint, TransportationForm} from "../../service/model";
import {getTargetGenerationPoints, getTransportation, getTransporterTruckOptions} from "../../service/handlers";
import {formatDate, formatTime} from "../../data/formatters";
import {Surface} from "../kit/Surface";
import {Cell, Column, PhotosCell, Row, Table, TableBody, TableHeader} from "../kit/Table";
import {PhotoViewer} from "../kit/PhotoViewer";

interface TransportationProps {
    id: string
}

interface TransportationState {
    truckOptions: Option[],
    targetGenerationPoints: TargetGenerationPoint[],
    transportation: TransportationForm | null
}

export class Transportation extends Component<TransportationProps, TransportationState> {

    photoViewer = React.createRef<PhotoViewer>();

    constructor(props: TransportationProps) {
        super(props);
        this.state = {
            truckOptions: [],
            targetGenerationPoints: [],
            transportation: null
        }
    }

    async componentDidMount() {
        const {id} = this.props;
        let transportation = await getTransportation(id);
        this.setState({transportation})
        let targetGenerationPoints = await getTargetGenerationPoints();
        this.setState({targetGenerationPoints});
        let truckOptions = await getTransporterTruckOptions();
        this.setState({truckOptions});
    }

    formatTruck(id: string) {
        const {truckOptions} = this.state;
        for (let option of truckOptions) {
            if (option.id === id) {
                return option.name;
            }
        }
        return 'none!';
    }

    formatContainerTypeName(generationPointId: string, id: string): string {

        const {targetGenerationPoints, transportation} = this.state;
        for (let generationPoint of targetGenerationPoints) {
            if (generationPoint.id === generationPointId) {
                for (let containerType of generationPoint.containerTypes) {
                    if (containerType.id === id) {
                        return containerType.name;
                    }
                }

                break;
            }
        }
        return 'none!';
    }

    formatGenerationPointAddress(generationPointId: string): string {
        const {targetGenerationPoints} = this.state;
        for (let generationPoint of targetGenerationPoints) {
            if (generationPoint.id === generationPointId) {
                return generationPoint.address;
            }
        }
        return 'none!';
    }

    render() {
        const {transportation} = this.state;

        if (!transportation) {
            return <div>Загрузка...</div>
        }

        const rows = [];

        let destinationNumber = 0;

        for (let destination of transportation.destinations) {
            destinationNumber += 1;
            for (let index = 0; index < destination.collections.length; index++) {
                let collection = destination.collections[index];
                let row = index === 0 ?
                    <Row key={collection.id}>
                        <Cell rowspan={destination.collections.length}>
                            {destinationNumber}
                        </Cell>
                        <Cell rowspan={destination.collections.length}>
                            {formatTime(destination.collectionStartTime)}
                        </Cell>
                        <Cell rowspan={destination.collections.length}>
                            {this.formatGenerationPointAddress(destination.wasteGenerationPoint)}
                        </Cell>
                        <Cell>
                            {this.formatContainerTypeName(destination.wasteGenerationPoint, collection.containerType)}
                        </Cell>
                        <Cell>{collection.wasteVolumeM3}</Cell>
                        <PhotosCell width={280} photos={collection.photos} onPreview={
                            (photo) => {
                                this.photoViewer.current?.showPhotos(photo, collection.photos)
                            }
                        }>
                        </PhotosCell>
                    </Row>
                    :
                    <Row key={collection.id}>
                        <Cell>
                            {this.formatContainerTypeName(destination.wasteGenerationPoint, collection.containerType)}
                        </Cell>
                        <Cell>{collection.wasteVolumeM3}</Cell>
                        <PhotosCell width={280} photos={collection.photos} onPreview={
                            (photo) => {
                                this.photoViewer.current?.showPhotos(photo, collection.photos)
                            }
                        }>
                        </PhotosCell>
                    </Row>;
                rows.push(row);
            }
        }

        return (
            <div className={styles.container}>
                <div className={styles.panel}>
                    <h1 className={styles.headline}>
                        МЖ: {this.formatTruck(transportation.truck)} на {formatDate(transportation.executionDate)}
                    </h1>
                    <span className={styles.status}>На согласовании</span>
                </div>
                <Surface>
                    <Table>
                        <TableHeader>
                            <Column>#</Column>
                            <Column>Время</Column>
                            <Column>Адрес КП</Column>
                            <Column>Тип контейнера</Column>
                            <Column>Объем, м3</Column>
                            <Column>Фотографии</Column>
                        </TableHeader>
                        <TableBody>
                            {rows}
                        </TableBody>
                    </Table>
                </Surface>
                <PhotoViewer ref={this.photoViewer}/>
            </div>
        );
    }

}