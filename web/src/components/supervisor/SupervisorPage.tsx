import React, {Component} from "react";
import {Redirect, Route, Switch} from "react-router-dom";

import {Link, PageHeader} from "../kit/PageHeader";
import {AuthorizationUser, GeneratorListItem, NegotiationListItem} from "../../service/model";
import styles from "./SupervisorPage.module.scss";
import {logOutCurrentUser} from "../../service/context";
import {Icon} from "../kit/Icon";
import {Reports} from "../contractor/Reports";
import {ReportCreation} from "../contractor/ReportCreation";
import {Negotiations} from "../contractor/Negotiations";
import {NegotiationReview} from "../contractor/NegotiationReview";
import {GenerationPoints} from "../contractor/GenerationPoints";
import {GenerationPoint} from "../contractor/GenerationPoint";
import {GenerationPointRegistration} from "../contractor/GenerationPointRegistration";
import {Generators} from "../contractor/Generators";
import {BusinessEntityGenerator, EntrepreneurGenerator} from "../contractor/Generator";
import {
    BusinessEntityGeneratorRegistration,
    EntrepreneurGeneratorRegistration
} from "../contractor/GeneratorRegistration";

interface SupervisorPageProps {
    user: AuthorizationUser
}

interface SupervisorPageState {
}

export class SupervisorPage extends Component<SupervisorPageProps, SupervisorPageState> {

    constructor(props: SupervisorPageProps) {
        super(props);
        this.state = {}
    }

    render() {
        const {user} = this.props;

        return (
            <div>
                <PageHeader>
                    <Link to="/supervisor/generators">Мусорообразователи</Link>
                    <Link to="/supervisor/generation-points">Контейнерные площадки</Link>
                    <Link to="/supervisor/negotiations">
                        Маршрутные журналы
                    </Link>
                    <Link to="/supervisor/reports">Отчеты</Link>
                    <aside>
                        <span className={styles.user}>{user.userName}</span>
                        <div className={styles.userExitIcon} onClick={() => logOutCurrentUser()}>
                            <Icon name="update"/>
                        </div>
                    </aside>
                </PageHeader>
                <Switch>
                    <Route axact path="/supervisor/generators/business-entity/registration">
                        <BusinessEntityGeneratorRegistration
                            onCancel={() => this.openGenerators()}
                            onRegistrationComplete={id => this.openBusinessEntityGenerator(id)}
                        />
                    </Route>
                    <Route exact path="/supervisor/generators/entrepreneur/registration">
                        <EntrepreneurGeneratorRegistration
                            onCancel={() => this.openGenerators()}
                            onRegistrationComplete={id => this.openEntrepreneurGenerator(id)}
                        />
                    </Route>
                    <Route path="/supervisor/generators/business-entity/:id" render={props => {
                        let {id} = props.match.params;
                        return <BusinessEntityGenerator id={id}/>
                    }}/>
                    <Route path="/supervisor/generators/entrepreneur/:id" render={props => {
                        let {id} = props.match.params;
                        return <EntrepreneurGenerator id={id}/>
                    }}/>
                    <Route path="/supervisor/generators">
                        <Generators
                            readonly={false}
                            onRegisterBusinessEntityGenerator={() => this.registerBusinessEntityGenerator()}
                            onRegisterEntrepreneurGenerator={() => this.registerEntrepreneurGenerator()}
                            onOpenGenerator={generator => this.openGenerator(generator)}
                        />
                    </Route>
                    <Route exact path="/supervisor/generation-points/registration">
                        <GenerationPointRegistration
                            onCancel={() => this.openGenerationPoints()}
                            onRegistrationComplete={id => this.openGenerationPoint(id)}
                        />
                    </Route>
                    <Route path="/supervisor/generation-points/:id" render={props => {
                        let {id} = props.match.params;
                        return <GenerationPoint id={id}/>
                    }}/>
                    <Route path="/supervisor/generation-points">
                        <GenerationPoints
                            readonly={false}
                            onRegisterGenerationPoint={() => this.registerGenerationPoint()}
                            onOpenGenerationPoint={id => this.openGenerationPoint(id)}
                        />
                    </Route>
                    <Route path="/supervisor/negotiations/review/:id" render={props => {
                        let {id} = props.match.params;
                        return <NegotiationReview
                            id={id}
                            onCancel={() => this.openNegotiations()}
                        />
                    }}/>
                    <Route path="/supervisor/negotiations">
                        <Negotiations readonly={true} onOpenNegotiation={negotiation => this.openNegotiation(negotiation)}/>
                    </Route>
                    <Route path="/supervisor/reports/creation">
                        <ReportCreation onCreationComplete={() => this.openReports()} onCancel={() => this.openReports()}/>
                    </Route>
                    <Route path="/supervisor/reports">
                        <Reports readonly={true} onCreateReport={() => this.createReport()} />
                    </Route>
                    <Route path="/supervisor" render={() => this.openContractorInitialPage()}/>
                </Switch>
            </div>
        )
    }

    private openReports() {
        this.openPage('/supervisor/reports');
    }

    private createReport() {
        this.openPage('/supervisor/reports/creation')
    }

    private openNegotiations() {
        this.openPage('/supervisor/negotiations')
    }

    private openNegotiation(negotiation: NegotiationListItem) {
        if (negotiation.status === 'completed') {
            this.openNegotiationReview(negotiation.id);
        }
    }

    private openNegotiationReview(id: string) {
        this.openPage('/supervisor/negotiations/review/' + id);
    }

    private registerBusinessEntityGenerator() {
        this.openPage('/supervisor/generators/business-entity/registration')
    }

    private registerEntrepreneurGenerator() {
        this.openPage('/supervisor/generators/entrepreneur/registration')
    }

    private registerGenerationPoint() {
        this.openPage('/supervisor/generation-points/registration')
    }

    private openGenerationPoint(id: string) {
        this.openPage('/supervisor/generation-points/' + id);
    }

    private openGenerationPoints() {
        this.openPage('/supervisor/generation-points');
    }

    private openGenerator(generator: GeneratorListItem) {
        if (generator.legalEntityType === 'BusinessEntity') {
            this.openBusinessEntityGenerator(generator.id);
        } else {
            this.openEntrepreneurGenerator(generator.id);
        }
    }

    private openEntrepreneurGenerator(id: string) {
        this.openPage('/supervisor/generators/entrepreneur/' + id);
    }

    private openBusinessEntityGenerator(id: string) {
        this.openPage('/supervisor/generators/business-entity/' + id);
    }

    private openGenerators() {
        this.openPage('/supervisor/generators');
    }

    openPage(path: string) {
        window.history.pushState(null, "Title", path);
        window.dispatchEvent(new window.PopStateEvent('popstate'));
    }

    private openContractorInitialPage() {
        return <Redirect to="/supervisor/generators"/>
    }
}