import React, {Component} from "react";
import styles from './PropertySection.module.scss';

interface PropertySectionProps {

}

interface PropertySectionState {

}

export class PropertySection extends Component<PropertySectionProps, PropertySectionState> {
    render() {
        return (
            <section className={styles.section}>
                {this.props.children as any}
            </section>
        )
    }
}

interface PropertyProps {

}

interface PropertyState {

}

export class Property extends Component<PropertyProps, PropertyState> {
    render() {
        return (
            <section className={styles.property}>
                {this.props.children as any}
            </section>
        )
    }
}