import React, {Component} from "react";
import styles from "./TextInput.module.scss";
import {Control} from "./forms";
import {Icon, IconName} from "./Icon";

interface TextInputProps {
    type?: string;
    control?: Control;
    readonly?: boolean;
    value?: string;
    onChange?: (value: string) => void;
    placeholder?: string;
    maxLength?: number;
    autofocus?: boolean;
    error?: string | null;
    icon?: IconName;
    rows?: number;
}

interface TextInputState {
    errorShown: boolean;
}

export class TextInput extends Component<TextInputProps, TextInputState> {
    constructor(props: TextInputProps) {
        super(props);
        this.state = {
            errorShown: false
        };
    }

    render() {
        const {control, value, placeholder, onChange, error, type, maxLength, autofocus, readonly, icon, rows} = this.props;

        let _value = control ? control.getValue() : value;
        let _error = control ? control.getError() : error;
        let _onChange = (control ? control.setValue.bind(control) : onChange) || (() => {
        });
        let _readonly = readonly || (control ? control.readonly : false);

        let className = styles.inputContainer;
        if (_error) {
            className += ' ' + styles.invalid;
        }

        return (
            <div className={className}>
                {type === 'multiline' ?
                    <textarea
                        className={_error ? styles.invalid : ''}
                        placeholder={placeholder}
                        autoFocus={autofocus}
                        maxLength={maxLength}
                        disabled={_readonly}
                        rows={rows ?? 4}
                        value={_value}
                        onChange={event => _onChange(event.target.value)}
                    />
                    :
                    <input
                        className={_error ? styles.invalid : ''}
                        placeholder={placeholder}
                        autoFocus={autofocus}
                        maxLength={maxLength}
                        disabled={_readonly}
                        type={type}
                        value={_value}
                        onChange={event => _onChange(event.target.value)}
                    />
                }
                {icon && <div className={styles.icon}><Icon name={icon} /></div>}
            </div>
        )
    }
}