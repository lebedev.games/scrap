import React, {Component} from "react";
import styles from "./DateInput.module.scss";
import {Control} from "./forms";
import DatePicker, {registerLocale} from "react-datepicker";
import moment from "moment";

import "react-datepicker/dist/react-datepicker.css";
import "./DateInput.scss";

import ru from 'date-fns/locale/ru';
import {Icon} from "./Icon";

registerLocale('ru', ru)

interface DateInputProps {
    control?: Control;
    readonly?: boolean;
    value?: string;
    onChange?: (value: string) => void;
    placeholder?: string;
    error?: string | null;
    clearable?: boolean;
    format?: string;
    onlyMonth?: boolean;
}

interface DateInputState {
    errorShown: boolean;
}

export class DateInput extends Component<DateInputProps, DateInputState> {
    constructor(props: DateInputProps) {
        super(props);
        this.state = {
            errorShown: false
        };
    }

    render() {
        const {control, value, placeholder, onChange, error, readonly, clearable, format, onlyMonth} = this.props;

        let _value = control ? control.getValue() : value;
        let _error = control ? control.getError() : error;
        let _onChange = (control ? control.setValue.bind(control) : onChange) || (() => {});
        let _readonly = control ? control.readonly : readonly;

        let className = styles.inputContainer;
        if (_error) {
            className += ' ' + styles.invalid;
        }

        return (
            <div className={className}>
                <DatePicker
                    placeholderText={placeholder}
                    readOnly={_readonly}
                    closeOnScroll
                    isClearable={clearable}
                    selected={_value ? moment(_value).toDate() : _value}
                    locale="ru"
                    showMonthYearPicker={onlyMonth}
                    dateFormat={format ?? "d.MM.yyyy"}
                    onChange={date => _onChange(moment(date as Date).toISOString())}
                />
                <div className={styles.icon}>
                    <Icon name="datepicker" />
                </div>
            </div>
        )
    }
}