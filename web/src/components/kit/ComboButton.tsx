import React, {Component} from "react";
import styles from "./ComboButton.module.scss";
import {Icon} from "./Icon";

interface ComboButtonOption {
    name: string,
    action: () => void
}

interface ComboButtonProps {
    options: ComboButtonOption[]
}

interface ComboButtonState {
    optionsShown: boolean
}


export class ComboButton extends Component<ComboButtonProps, ComboButtonState> {
    constructor(props: ComboButtonProps) {
        super(props);
        this.state = {
            optionsShown: false
        }
    }

    onDocumentMouseDown = (event: MouseEvent) => {
        console.log(event);
        this.setState({optionsShown: false})
    }

    componentDidMount() {
        //document.addEventListener("mousedown", this.onDocumentMouseDown);
    }

    componentWillUnmount() {
        //document.removeEventListener("mousedown", this.onDocumentMouseDown);
    }

    toggleOptions() {
        this.setState({optionsShown: !this.state.optionsShown})
    }



    render() {
        const {optionsShown} = this.state;
        const {options} = this.props;

        const onClick = (option: ComboButtonOption) => {
            return (event: React.MouseEvent) => {
                this.setState({optionsShown: false})
                option.action();
            }
        }

        return (
            <div className={styles.container}>
                <button className={styles.button} onClick={() => this.toggleOptions()} >
                    <label className={styles.label}>
                        {this.props.children as any}
                        <div className={styles.icon}>
                            {optionsShown ? <Icon name="chevronUp"/> : <Icon name="chevronDown"/>}
                        </div>
                    </label>
                </button>
                {optionsShown && (
                    <div className={styles.dropDown}>
                        {options.map(option =>
                            <button key={option.name} onClick={onClick(option)} className={styles.option}>
                                {option.name}
                            </button>
                        )}
                    </div>
                )}
            </div>
        )
    }
}
