import React, {Component} from "react";
import styles from "./ImageInput.module.scss";
import {backend, uploadFiles} from "../../service/handlers";
import * as uuid from "uuid";
import {Control} from "./forms";

interface ImageInputProps {
    control?: Control;
    onChange?: (value: string[]) => void;
    value?: string[];
    maxLength?: number;
    error?: string | null;
}

interface ImageInputState {
    isDragOver: boolean,
    uploading: boolean,
}

export class ImageInput extends Component<ImageInputProps, ImageInputState> {
    id = uuid.v4();
    formRef = React.createRef<HTMLFormElement>();

    constructor(props: ImageInputProps) {
        super(props);
        this.state = {
            isDragOver: false,
            uploading: false,
        }
    }

    async handleDrop(e: React.DragEvent) {
        e.preventDefault();

        if (this.state.isDragOver) {
            this.setState({isDragOver: false})
        }

        await this.uploadFiles(e.dataTransfer.files);
    }

    handleDragOver(e: React.DragEvent) {
        e.preventDefault();
        if (!this.state.isDragOver) {
            this.setState({isDragOver: true})
        }
    }

    handleDragLeave(e: React.DragEvent) {
        e.preventDefault();
        if (this.state.isDragOver) {
            this.setState({isDragOver: false})
        }
    }

    async uploadFiles(files: FileList) {
        this.setState({uploading: true})
        let upload = await uploadFiles(files);
        this.setState({uploading: false})

        const {onChange, control, value} = this.props;
        let _onChange = (control ? control.setValue.bind(control) : onChange) || (() => {});
        let _value = control ? control.getValue() : value;
        let images = [].concat(_value as any).concat(upload.files as any);
        _onChange(images);
    }

    render() {
        const {isDragOver, uploading} = this.state;
        const {value, control} = this.props;

        let images: string[] = control ? control.getValue() : value;

        return (
            <form ref={this.formRef} className={styles.container} method="post" action="" encType="multipart/form-data">
                <div
                    className={styles.dropArea + ' ' + (isDragOver ? styles.dragOver : '')}
                    onDrop={event => this.handleDrop(event)}
                    onDragOver={event => this.handleDragOver(event)}
                    onDragLeave={event => this.handleDragLeave(event)}
                >
                    <input onChange={event => this.uploadFiles(event.target.files as FileList)}
                           className={styles.nativeInput} type="file" name="file" id={this.id} multiple/>
                    {uploading ?
                        <span>
                            Загрузка...
                        </span>
                        :
                        <span>
                            <label htmlFor={this.id}>Выберите файлы</label>
                            &nbsp;или перетащите сюда
                        </span>
                    }
                </div>
                {images.length > 0 && (
                    <div className={styles.previews}>
                        {images.map(image =>
                            <div key={image} className={styles.preview}>
                                <img src={backend('/files/' + image)} alt={image}/>
                            </div>
                        )}
                    </div>
                )}
            </form>
        )
    }
}