import React, {Component} from "react";
import styles from "./NegotiationStatus.module.scss";

interface NegotiationStatusProps {
    status: string
}

interface NegotiationStatusState {

}

export class NegotiationStatus extends Component<NegotiationStatusProps, NegotiationStatusState> {
    render() {
        const {status} = this.props;
        let component;
        if (status === "pending_approval") {
            component = (
                <div className={styles.pendingApproval}>на согласовании</div>
            )
        }
        if (status === "pending_resolution") {
            component = (
                <div className={styles.pendingResolution}>требует доработки</div>
            )
        }
        if (status === "completed") {
            component = (
                <div className={styles.completed}>согласован</div>
            )
        }
        return <div className={styles.container}>{component}</div>;
    }
}