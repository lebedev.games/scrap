import React, {Component} from "react";
import styles from "./Claim.module.scss";
import {Icon, IconName} from "./Icon";

export type ClaimComponentStatus = 'editing' | 'open' | 'resolved';

interface ClaimProps {
    status: ClaimComponentStatus,
    onClick: () => void;
}

interface ClaimState {

}

export class Claim extends Component<ClaimProps, ClaimState> {
    render() {
        let {status, onClick} = this.props;

        let icon: IconName = 'claimOpen';
        let iconClass = styles.icon;

        if (status === 'open') {
            icon = 'claimOpen';
            iconClass += ' ' + styles.open;
        }
        if (status === 'resolved') {
            icon = 'claimResolved';
            iconClass += ' ' + styles.resolved;
        }
        if (status === 'editing') {
            icon = 'claimEditing';
            iconClass += ' ' + styles.editing;
        }

        return (
            <React.Fragment>
                {this.props.children as any}
                <div className={styles.issueClaim} onClick={onClick}>
                    <div className={iconClass}>
                        <Icon name={icon}/>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}