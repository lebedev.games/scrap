import React, {Component} from "react";
import styles from "./PagePanel.module.scss";

interface PagePanelProps {

}

interface PagePanelState {

}

export class PagePanel extends Component<PagePanelProps, PagePanelState> {
    render() {
        return (
            <div className={styles.panel}>
                {this.props.children as any}
            </div>
        )
    }
}