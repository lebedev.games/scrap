import React, {Component} from "react";
import {Control} from "./forms";
import styles from "./CheckInput.module.scss";
import {Icon} from "./Icon";

interface Option {
    id: string
    name: string
}

interface CheckInputProps {
    options: Option[]
    readonly?: boolean;
    control?: Control
    value?: string[]
    onChange?: (value: string[]) => void;
    error?: string | null;
}

interface CheckInputState {

}

export class CheckInput extends Component<CheckInputProps, CheckInputState> {

    render() {
        const {value, options, control, error, readonly, onChange} = this.props;

        let _value = control ? control.getValue() : value;
        let _error = control ? control.getError() : error;
        let _readonly = control ? control.readonly : readonly;
        let _onChange = control ? control.setValue.bind(control) : onChange;

        let className = styles.selectStyle;
        if (_error) {
            className += ' ' + styles.invalid;
        }

        if (_readonly) {
            className += ' ' + styles.disabled;
        }

        const isChecked = (option: Option) => {
            for (let item of _value) {
                if (item === option.id) {
                    return true
                }
            }
            return false;
        }

        const toggle = (option: Option, checked: boolean) => {
            let change: string[] = [].concat(_value);
            if (checked) {
                change.push(option.id);
                if (_onChange) {
                    _onChange(change);
                }
            } else {
                change = change.filter(v => v !== option.id);
                if (_onChange) {
                    _onChange(change);
                }
            }
        }

        return (
            <div className={className}>
                {options.map(option => (
                    <div key={option.id}>
                        <input
                            type="checkbox"
                            id={option.id}
                            checked={isChecked(option)}
                            onChange={event => toggle(option, event.target.checked)}
                        />
                        <label htmlFor={option.id}> {option.name}</label>
                    </div>
                ))}
            </div>
        )
    }


}