import React, {Component} from "react";
import styles from "./PageContainer.module.scss";

interface PageContainerProps {
    
}

interface PageContainerState {
    
}

export class PageContainer extends Component<PageContainerProps, PageContainerState> {
    render() {
        return (
            <div className={styles.container}>
                {this.props.children as any}
            </div>
        )
    }
}