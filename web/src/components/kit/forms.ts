import {Component} from "react";

export interface Setter {
    (value: any): void;
}

export interface Getter {
    (value: any): any;
}

export interface Validator {
    (value: any): string | null;
}

export class Form {

    static getControlConstructor(component: Component, onFormChange?: () => void) {
        const forceUpdate = component.forceUpdate.bind(component);
        return (definition: Partial<Control>): Control => {
            if (onFormChange) {
                let specifiedSetter = definition.setter;
                definition.setter = (value) => {
                    // delay form change callback before Control set value
                    requestAnimationFrame(() => onFormChange());
                    return specifiedSetter ? specifiedSetter(value) : value;
                }
            }
            return new Control(forceUpdate, definition);
        }
    }

    static validate(form: any) {
        for (let key of Object.keys(form)) {
            let control = form[key];
            if (control instanceof Control) {
                control.validate();
            }
        }
    }

    static isValid(form: any): boolean {
        for (let key of Object.keys(form)) {
            let control = form[key];
            if (control instanceof Control) {
                let error = control.getError();
                if (error) {
                    return false;
                }
            }
        }
        return true;
    }
}

export class Control {
    public value: any;
    public readonly: boolean;
    getter?: Getter;
    setter?: Setter;
    error: string | null;
    validators: Validator[];
    pristine: boolean;
    forceUpdate: any;


    constructor(forceUpdate: any, control: Partial<Control>) {
        this.value = control.value ?? null;
        this.getter = control.getter;
        this.setter = control.setter;
        this.error = null;
        this.validators = control.validators || [];
        this.pristine = true;
        this.forceUpdate = forceUpdate;
        this.readonly = control.readonly ?? false;
    }


    setValue(value: any) {
        this.value = this.setter ? this.setter(value) : value;
        this.error = null;
        this.forceUpdate();
    }

    getValue(): any {
        return this.getter ? this.getter(this.value) : this.value;
    }

    getError(): string | null {
        return this.error;
    }

    validate() {
        for (let validator of this.validators) {
            let error = validator(this.value);
            if (error) {
                this.error = error;
                this.forceUpdate();
                break;
            }
        }
    }
}