import React, {Component} from "react";
import styles from "./Table.module.scss";
import {choosePluralForm} from "../../data/formatters";
import {Control} from "./forms";
import {Icon} from "./Icon";
import {backend} from "../../service/handlers";

interface TableProps {
    width?: number;
}

interface TableState {

}

export class Table extends Component<TableProps, TableState> {
    render() {
        const {width} = this.props;
        const overrides = {
            width: width ? width + 'px' : '100%',
        }
        return (
            <table className={styles.table} style={overrides}>
                {this.props.children as any}
            </table>
        )
    }
}

interface TableHeaderProps {

}

interface TableHeaderState {

}

export class TableHeader extends Component<TableHeaderProps, TableHeaderState> {
    render() {
        return (
            <thead className={styles.tableHeader}>
            <tr>
                {this.props.children as any}
            </tr>
            </thead>
        )
    }
}

interface TableBodyProps {

}

interface TableBodyState {

}

export class TableBody extends Component<TableBodyProps, TableBodyState> {
    render() {
        return (
            <tbody className={styles.tableBody}>
            {this.props.children as any}
            </tbody>
        )
    }
}

interface ColumnProps {
    width?: number;
}

interface ColumnState {

}

export class Column extends Component<ColumnProps, ColumnState> {
    render() {
        const {width} = this.props;
        const overrides = {width};
        return (
            <th className={styles.column} style={overrides}>
                {this.props.children as any}
            </th>
        )
    }
}


interface RowProps {
    onClick?: () => void;
    embeded?: boolean;
}

interface RowState {
}

export class Row extends Component<RowProps, RowState> {

    constructor(props: RowProps) {
        super(props);
        this.state = {}
    }

    render() {
        const {onClick, embeded} = this.props;

        return (
            <tr className={embeded ? styles.row + ' ' + styles.embeded : styles.row} onClick={onClick}>
                {this.props.children as any}
            </tr>
        )
    }
}

interface PhotosCellProps {
    photos: string[]
    width?: number
    onPreview?: (photo: string) => void
}

interface PhotosCellState {

}

export class PhotosCell extends Component<PhotosCellProps, PhotosCellState> {

    render() {
        const {photos, width, onPreview} = this.props;
        const overrides = {minWidth: width};

        return (
            <td className={styles.cell} style={overrides}>
                {this.props.children as any}
                <div className={styles.photosCellPreviews}>
                    {photos.map(photo => (
                        <div key={photo} className={styles.photosCellPreview} onClick={() => {onPreview?.(photo)}}>
                            <img src={backend('/files/' + photo)} alt={photo}/>
                        </div>
                    ))}
                </div>
            </td>
        )
    }

}

interface CellProps {
    rowspan?: number;
    columns?: number;
    merge?: boolean;
    width?: number;
}

interface CellState {

}

export class Cell extends Component<CellProps, CellState> {
    render() {
        const {rowspan, columns, merge, width} = this.props;
        const overrides = {minWidth: width};

        return (
            <td rowSpan={rowspan} colSpan={columns}
                className={merge ? styles.cell + ' ' + styles.embed : styles.cell}
                style={overrides}
            >
                {this.props.children as any}
            </td>
        )
    }
}

interface TablePaginatorProps {
    total: number
    count: number
    forms: string[]
    limit: Control
    offset: Control
}

interface TablePaginatorState {
    pageSize: number,
    page: number,
    pages: number,
}

export class TablePaginator extends Component<TablePaginatorProps, TablePaginatorState> {
    constructor(props: TablePaginatorProps) {
        super(props);
        this.state = {
            pageSize: this.props.limit ? this.props.limit.value : 10,
            page: 0,
            pages: 1
        }
    }

    componentDidUpdate(prevProps: Readonly<TablePaginatorProps>) {
        if (prevProps.total != this.props.total) {
            let {pageSize} = this.state;
            let {total} = this.props;
            let pages = Math.ceil(total / pageSize);
            pages = pages ? pages : 1;
            this.setState({pages})
            this.changePage(0);
        }
    }

    nextPage() {
        let {page, pages} = this.state;
        if (page + 1 < pages) {
            page += 1;
            this.changePage(page)
        }
    }

    prevPage() {
        let {page} = this.state;
        if (page - 1 >= 0) {
            page -= 1;
            this.changePage(page)
        }
    }

    changePage(page: number) {
        const {pageSize} = this.state;
        this.setState({page});

        const {limit, offset} = this.props;
        limit.setValue(pageSize);
        offset.setValue(page * pageSize);
    }

    render() {
        let {total, forms, count} = this.props;
        let {pageSize, page, pages} = this.state;
        forms = forms ?? ['', '', ''];
        count = count ?? 0;

        let pageButtons = [];
        for (let p = 0; p < pages; p++) {
            pageButtons.push((
                <div key={p} onClick={() => this.changePage(p)}
                     className={p == page ? styles.pageButton + ' ' + styles.active : styles.pageButton}>
                    {p + 1}
                </div>
            ))
        }

        let offset = page * pageSize + 1;

        return (
            <div className={styles.tablePaginator}>
                <div onClick={() => this.changePage(0)}
                     className={page === 0 ? styles.iconButton + ' ' + styles.inactive : styles.iconButton}>
                    <Icon name="skipBack"/>
                </div>
                <div onClick={() => this.prevPage()}
                     className={page === 0 ? styles.iconButton + ' ' + styles.inactive : styles.iconButton}>
                    <Icon name="back"/>
                </div>
                {pageButtons}
                <div onClick={() => this.nextPage()}
                     className={page + 1 === pages ? styles.iconButton + ' ' + styles.inactive : styles.iconButton}>
                    <Icon name="forward"/>
                </div>
                <div onClick={() => this.changePage(pages - 1)}
                     className={page + 1 === pages ? styles.iconButton + ' ' + styles.inactive : styles.iconButton}>
                    <Icon name="skipForward"/>
                </div>
                <div className={styles.pageSize}/>
                <div className={styles.counters}>{count} {choosePluralForm(forms, count)} из {total}</div>
            </div>
        )
    }
}
