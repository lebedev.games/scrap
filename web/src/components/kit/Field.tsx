import React, {Component} from "react";
import styles from "./Field.module.scss";
import {Icon} from "./Icon";

interface FieldProps {
    width?: number
    error?: string | null
}

interface FieldState {

}

export class Field extends Component<FieldProps, FieldState> {
    render() {
        const {width, error} = this.props;
        let className = error ? styles.field + ' ' + styles.fieldError : styles.field;
        return (
            <div className={className} style={{maxWidth: width + 'px'}}>
                {this.props.children as any}
                {error && (
                    <div className={styles.errorMessage}>
                        <div className={styles.errorIcon}><Icon name="error"/></div>
                        {error}
                    </div>
                )}
            </div>
        )
    }
}

interface CompoundProps {

}

interface CompoundState {

}

export class Compound extends Component<CompoundProps, CompoundState> {
    render() {
        return (
            <div className={styles.compound}>
                {this.props.children as any}
            </div>
        )
    }
}


interface FormSectionProps {
    title?: string
}

interface FormSectionState {

}

export class FormSection extends Component<FormSectionProps, FormSectionState> {
    render() {
        const {title} = this.props;

        return (
            <section className={styles.formSection}>
                <header>{title}</header>
                <div className={styles.form}>
                    {this.props.children as any}
                </div>
            </section>
        )
    }
}

interface FormDivisionProps {
}

interface FormDivisionState {

}

export class FormDivision extends Component<FormDivisionProps, FormDivisionState> {
    render() {
        return (
            <section className={styles.formDivision}>
                {this.props.children as any}
            </section>
        )
    }
}

interface FormSurfaceProps {
    title?: string
}

interface FormSurfaceState {

}

export class FormSurface extends Component<FormSurfaceProps, FormSurfaceState> {
    render() {
        const {title} = this.props;

        return (
            <section className={styles.formSection}>
                <header>{title}</header>
                <div className={styles.formSurface}>
                    {this.props.children as any}
                </div>
            </section>
        )
    }
}

interface FieldsSeparatorProps {

}

interface FieldsSeparatorState {

}

export class FieldsSeparator extends Component<FieldsSeparatorProps, FieldsSeparatorState> {
    render() {
        return (
            <div className={styles.fieldsSeparator}/>
        )
    }
}

