import React, {Component} from "react";
import styles from "./PageHeader.module.scss";
import {Icon} from "./Icon";
import {NavLink} from "react-router-dom";

interface PageHeaderProps {

}

interface PageHeaderState {

}

export class PageHeader extends Component<PageHeaderProps, PageHeaderState> {
    render() {
        return (
            <div className={styles.mainPageHeader}>
                <div className={styles.container}>
                    <div className={styles.title}>
                        <div className={styles.logo}>
                            <Icon name="logo"/>
                        </div>
                        КАС ТКО
                    </div>
                    {this.props.children as any}
                </div>
            </div>
        );
    }
}

interface LinkProps {
    to: string
}

interface LinkState {

}

export class Link extends Component<LinkProps, LinkState> {
    render() {
        const {to} = this.props;
        return (
            <NavLink activeClassName={styles.active} to={to}>
                {this.props.children as any}
            </NavLink>
        )
    }
}
