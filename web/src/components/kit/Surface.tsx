import React, {Component} from "react";
import styles from "./Surface.module.scss";

interface SurfaceProps {

}

interface SurfaceState {

}

export class Surface extends Component<SurfaceProps, SurfaceState> {
    render() {
        return (
            <div className={styles.surface}>
                {this.props.children as any}
            </div>
        )
    }
}

interface SurfaceFooterProps {

}

interface SurfaceFooterState {

}

export class SurfaceFooter extends Component<SurfaceFooterProps, SurfaceFooterState> {
    render() {
        return (
            <div className={styles.surfaceFooter}>
                <section>
                    {this.props.children as any}
                </section>
            </div>
        )
    }
}

interface SurfaceHeaderProps {

}

interface SurfaceHeaderState {

}

export class SurfaceHeader extends Component<SurfaceHeaderProps, SurfaceHeaderState> {
    render() {
        return (
            <div className={styles.surfaceHeader}>
                <section>
                    {this.props.children as any}
                </section>
            </div>
        )
    }
}