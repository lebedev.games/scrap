import React, {Component} from "react";
import styles from "./PhotoViewer.module.scss";
import {backend} from "../../service/handlers";
import {Icon} from "./Icon";

interface PhotoViewerProps {
}

interface PhotoViewerState {
    currentPhoto: string
    photos: string[]
    shown: boolean
}

export class PhotoViewer extends Component<PhotoViewerProps, PhotoViewerState> {

    constructor(props: PhotoViewerProps) {
        super(props);
        this.state = {
            shown: false,
            currentPhoto: '',
            photos: []
        }
    }

    showPhotos(currentPhoto: string, photos: string[]) {
        this.setState({currentPhoto, photos, shown: true})
    }

    closePhotos() {
        this.setState({shown: false})
    }

    openPhoto(currentPhoto: string) {
        this.setState({currentPhoto})
    }

    render() {
        const {currentPhoto, photos, shown} = this.state;
        if (!shown) {
            return <div/>
        }

        return <div className={styles.modal}>
            <div className={styles.blackout}/>
            <div className={styles.viewer}>
                <div className={styles.modalClose} onClick={() => this.closePhotos()}>
                    <Icon name="closeModal"/>
                </div>
                <div className={styles.currentPhoto}>
                    <img src={backend('/files/' + currentPhoto)} alt={currentPhoto}/>
                </div>
                <div className={styles.previews}>
                    {photos.map(photo =>
                        <div className={styles.preview} tabIndex={0} onClick={() => this.openPhoto(photo)}>
                            <img src={backend('/files/' + photo)} alt={photo}/>
                            <div className={styles.overlay}/>
                        </div>
                    )}
                </div>
            </div>
        </div>
    }
}