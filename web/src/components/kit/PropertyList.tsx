import styles from "./PropertyList.module.scss";
import React, {Component} from "react";

interface PropertyListProps {

}

interface PropertyListState {

}

export class PropertyList extends Component<PropertyListProps, PropertyListState> {

    render() {
        return (
            <dl className={styles.mainProperties}>
                {this.props.children as any}
            </dl>
        )
    }
}