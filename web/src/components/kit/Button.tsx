import styles from "./Button.module.scss";
import React, {Component} from "react";

interface ButtonProps {
    type?: 'primary' | 'outline'
    onClick?: () => void;
    disabled?: boolean;
}

interface ButtonState {

}

export class Button extends Component <ButtonProps, ButtonState> {
    render() {
        const {type, onClick, disabled} = this.props;
        let typeClass = {
            'primary': styles.primary,
            'outline': styles.outline,
        }[type || 'primary'];
        let className = styles.button + ' ' + typeClass;
        return <button disabled={disabled} className={className} onClick={onClick}>
            {this.props.children as any}
        </button>
    }
}

interface TextButtonProps {
    type?: 'primary' | 'secondary' | 'regular'
    onClick?: () => void;
}

interface TextButtonState {

}

export class TextButton extends Component<TextButtonProps, TextButtonState> {
    render() {
        const {type, onClick} = this.props;
        let typeClass = {
            'primary': styles.textButtonPrimary,
            'secondary': styles.textButtonSecondary,
            'regular': styles.textButtonRegular
        }[type || 'primary'];
        let className = styles.textButton + ' ' + typeClass;
        return <button className={className} onClick={onClick}>
            {this.props.children as any}
        </button>
    }
}