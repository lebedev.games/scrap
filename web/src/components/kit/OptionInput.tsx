import React, {Component} from "react";
import {Control} from "./forms";
import styles from "./OptionInput.module.scss";
import {Icon} from "./Icon";

interface Option {
    id: string
    name: string
}

interface OptionInputProps {
    options: Option[]
    nullable?: boolean;
    readonly?: boolean;
    control?: Control
    value?: string | null
    onChange?: (value: string | null) => void;
    error?: string | null;
}

interface OptionInputState {

}

const NullRepr = '$NULL';

export class OptionInput extends Component<OptionInputProps, OptionInputState> {

    componentDidUpdate(prevProps: Readonly<OptionInputProps>) {
        let {options, value, control} = this.props;
        if (!this.isOptionsSame(options, prevProps.options)) {
            value = control ? control.getValue() : value;
            let index = options.findIndex(option => option.id === value);
            if (index === -1) {
                value = options.length > 0 ? options[0].id : value;
                this.changeValue(value as string);
            }
        }
    }

    isOptionsSame(options: Option[], other: Option[]) {
        if (options.length !== other.length) {
            return false;
        }

        for (let index = 0; index < options.length; index++) {
            if (options[index].id !== other[index].id) {
                return false;
            }
        }

        return true;
    }

    changeValue(value: string | null) {
        let {onChange, control, nullable} = this.props;
        onChange = control ? control.setValue.bind(control) : onChange;
        if (onChange) {
            value = (nullable && value === NullRepr) ? null : value;
            onChange(value);
        }
    }

    render() {
        const {value, options, control, error, nullable, readonly} = this.props;

        let _value = control ? control.getValue() : value;
        let _error = control ? control.getError() : error;
        let _readonly = control ? control.readonly : readonly;
        let _options = options.slice();
        if (nullable) {
            _options.splice(0, 0, {id: NullRepr, name: ''})
        }

        let className = styles.selectStyle;
        if (_error) {
            className += ' ' + styles.invalid;
        }

        if (_readonly) {
            className += ' ' + styles.disabled;
        }

        return (
            <div className={className}>
                <select disabled={_readonly} onChange={event => this.changeValue(event.target.value)} value={_value ?? NullRepr}>
                    {_options.map(option => <option key={option.id} value={option.id}>{option.name}</option>)}
                </select>
                <div className={styles.icon}>
                    <Icon name="chevronDown"/>
                </div>
            </div>
        )
    }
}