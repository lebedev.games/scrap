import React, {Component} from "react";
import styles from "./TimeInput.module.scss";
import {Control} from "./forms";

import DatePicker, {registerLocale} from "react-datepicker";
import moment from "moment";

import "react-datepicker/dist/react-datepicker.css";
import "./DateInput.scss";

import ru from 'date-fns/locale/ru';

registerLocale('ru', ru)

interface TimeInputProps {
    control?: Control;
    readonly?: boolean;
    value?: string;
    onChange?: (value: string) => void;
    placeholder?: string;
    error?: string | null;
}

interface TimeInputState {
    errorShown: boolean;
}

export class TimeInput extends Component<TimeInputProps, TimeInputState> {
    constructor(props: TimeInputProps) {
        super(props);
        this.state = {
            errorShown: false
        };
    }

    render() {
        const {control, value, placeholder, onChange, error, readonly} = this.props;

        let _value = control ? control.getValue() : value;
        let _error = control ? control.getError() : error;
        let _onChange = (control ? control.setValue.bind(control) : onChange) || (() => {
        });
        let _readonly = control ? control.readonly : readonly;

        let className = styles.inputContainer;
        if (_error) {
            className += ' ' + styles.invalid;
        }

        return (
            <div className={className}>
                <DatePicker
                    readOnly={_readonly}
                    placeholderText={placeholder}
                    selected={moment(_value).toDate()}
                    showTimeSelect
                    showTimeSelectOnly
                    timeIntervals={15}
                    timeCaption="Время"
                    locale="ru"
                    dateFormat="HH:mm"
                    onChange={date => _onChange(moment(date as Date).toISOString())}
                />
            </div>
        )
    }
}