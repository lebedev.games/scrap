import React, {Component} from "react";
import {PageContainer} from "../kit/PageContainer";
import {PagePanel} from "../kit/PagePanel";
import {Control, Form} from "../kit/forms";
import {Surface, SurfaceFooter} from "../kit/Surface";
import {emptyList, List, NegotiationListItem, NegotiationSearchCriteria, Option} from "../../service/model";
import styles from "./ReportCreation.module.scss";
import {Compound, Field} from "../kit/Field";
import {Button} from "../kit/Button";
import {DateInput} from "../kit/DateInput";
import moment from "moment";
import {debounceTime, switchMap} from "rxjs/operators";
import {createReport, getNegotiations, getTransporterOptions} from "../../service/handlers";
import {Subject, Subscription} from "rxjs";
import {Cell, Column, Row, Table, TableBody, TableHeader, TablePaginator} from "../kit/Table";
import {OptionInput} from "../kit/OptionInput";
import {formatDate} from "../../data/formatters";
import {NegotiationStatus} from "../kit/NegotiationStatus";

interface ReportCreationProps {
    onCreationComplete: (id: string) => void
    onCancel: () => void
}

interface ReportCreationState {
    negotiationList: List<NegotiationListItem>,
    transporterOptions: Option[],
    form: {
        targetDate: Control,
        targetTransporter: Control,
        offset: Control
        limit: Control
    }
}

export class ReportCreation extends Component<ReportCreationProps, ReportCreationState> {
    subscription = new Subscription();
    searchRequest: Subject<NegotiationSearchCriteria>;

    constructor(props: ReportCreationProps) {
        super(props);
        let control = Form.getControlConstructor(this, () => this.searchRequest.next(this.getSearchCriteria()));
        this.state = {
            negotiationList: emptyList<NegotiationListItem>(),
            transporterOptions: [],
            form: {
                targetDate: control({value: moment().toISOString()}),
                targetTransporter: control({value: ''}),
                limit: control({value: 10}),
                offset: control({value: 0})
            }
        }
        this.searchRequest = new Subject<NegotiationSearchCriteria>();
    }


    async componentDidMount() {
        this.subscription.add(
            this.searchRequest.pipe(
                debounceTime(350),
                switchMap(search => getNegotiations(search))
            ).subscribe(negotiationList => {
                this.setState({negotiationList})
            })
        );

        const negotiationList = await getNegotiations(this.getSearchCriteria())
        this.setState({negotiationList})

        const transporterOptions = await getTransporterOptions();
        for (let option of transporterOptions) {
            (option as any).originId = option.id;
            option.id = option.name;
        }
        this.setState({transporterOptions})
    }

    componentWillUnmount() {
        this.subscription.unsubscribe()
    }

    getSearchCriteria(): NegotiationSearchCriteria {
        const {form} = this.state;
        return {
            transportationDate: null,
            truck: '',
            district: '',
            transporter: form.targetTransporter.value,
            status: null,
            limit: form.limit.value,
            offset: form.offset.value,
            consolidated: false
        };
    }

    async createReport() {
        const {form, transporterOptions, negotiationList} = this.state;
        const {onCreationComplete} = this.props;

        let targetTransporter;
        for (let option of transporterOptions) {
            if (option.name === form.targetTransporter.value) {
                targetTransporter = (option as any).originId;
            }
        }

        const creation = {
            targetTransporter,
            targetMonth: form.targetDate.value,
            transportations: negotiationList.items.map(negotiation => negotiation.transportation)
        }
        const report = await createReport(creation);
        onCreationComplete(report.id);
    }

    render() {
        const {negotiationList, form, transporterOptions} = this.state;
        const {onCancel} = this.props;

        return (
            <PageContainer>
                <PagePanel>
                    <h1>Формирование отчета</h1>
                </PagePanel>
                <Surface>
                    <Compound>
                        <Field width={160}>
                            <label>Отчетный период</label>
                            <DateInput onlyMonth format="MMMM, yyyy" control={form.targetDate}/>
                        </Field>
                        <Field>
                            <label>Наименование перевозчика</label>
                            <OptionInput control={form.targetTransporter} options={transporterOptions}/>
                        </Field>
                    </Compound>
                </Surface>
                <div style={{paddingTop: '16px'}}/>
                <Surface>
                    {negotiationList.total > 0 ?
                        <React.Fragment>
                            <Table>
                                <TableHeader>
                                    <Column>Дата вывоза</Column>
                                    <Column>Номер ТС</Column>
                                    <Column>Район</Column>
                                    <Column>Наименование перевозчика</Column>
                                    <Column>Статус</Column>
                                </TableHeader>
                                <TableBody>
                                    {negotiationList.items.map(negotiation => (
                                        <Row key={negotiation.id}>
                                            <Cell>{formatDate(negotiation.transportationDate)}</Cell>
                                            <Cell>{negotiation.truck}</Cell>
                                            <Cell>{negotiation.district}</Cell>
                                            <Cell>{negotiation.transporter}</Cell>
                                            <Cell><NegotiationStatus status={negotiation.status}/></Cell>
                                        </Row>
                                    ))}
                                </TableBody>
                            </Table>
                            <TablePaginator
                                count={negotiationList.items.length}
                                total={negotiationList.total}
                                forms={['маршрутный журнал', 'маршрутных журнала', 'маршрутных журналов']}
                                limit={form.limit}
                                offset={form.offset}
                            />
                            <div style={{paddingTop: '32px'}} />
                        </React.Fragment>
                        :
                        <div className={styles.note}>
                            <p>
                                Выберите отчетный период и перевозчика, чтобы увидеть список маршрутных журналов.
                            </p>
                        </div>
                    }
                    <SurfaceFooter>
                        <Button disabled={negotiationList.total === 0} type="primary"
                                onClick={() => this.createReport()}>
                            Отправить отчет
                        </Button>
                        <Button type="outline" onClick={onCancel}>Отмена</Button>
                    </SurfaceFooter>
                </Surface>
            </PageContainer>
        )
    }
}