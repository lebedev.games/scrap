import React, {Component} from "react";
import {PagePanel} from "../kit/PagePanel";
import {Surface} from "../kit/Surface";
import {PageContainer} from "../kit/PageContainer";
import {Button} from "../kit/Button";
import {emptyList, List, Option, TruckListItemModel, TruckSearchCriteria} from "../../service/model";
import {Subject, Subscription} from "rxjs";
import {Control, Form} from "../kit/forms";
import {debounceTime, switchMap} from "rxjs/operators";
import {getTrucks} from "../../service/handlers";
import {Cell, Column, Row, Table, TableBody, TableHeader, TablePaginator} from "../kit/Table";
import {OptionInput} from "../kit/OptionInput";
import {TextInput} from "../kit/TextInput";

interface TrucksProps {
    onRegisterTruck: () => void;
    onOpenTruck: (truck: TruckListItemModel) => void;
}

interface TrucksState {
    truckList: List<TruckListItemModel>,
    cargoBodyTypeOptions: Option[],
    fleetTypeOptions: Option[],
    search: {
        registrationNumber: Control,
        cargoBodyType: Control,
        transporterName: Control,
        truckFleetType: Control,
        limit: Control,
        offset: Control,
    }
}

export class Trucks extends Component<TrucksProps, TrucksState> {
    subscription = new Subscription();
    searchRequest: Subject<TruckSearchCriteria>;

    constructor(props: TrucksProps) {
        super(props);
        let control = Form.getControlConstructor(this, () => this.searchRequest.next(this.getSearchCriteria()));
        this.state = {
            truckList: emptyList<TruckListItemModel>(),
            cargoBodyTypeOptions: [
                {id: '999dd103-83e8-417e-bd7a-b31f39344fd1', name: 'Мусоровоз контейнерного типа'},
                {id: 'deac883d-57af-47e3-9a0d-465a0499151f', name: 'Мусоровоз кузовного типа'},
                {id: '557361cb-e574-46f0-80cc-ccaebee3d1d1', name: 'Ломовоз'},
                {id: 'b07ff2f9-eff5-4d88-b79e-9966f4164c0e', name: 'Мусоровоз-самосвал'}
            ],
            fleetTypeOptions: [
                {id: '1964b722-2ce3-4a67-a948-26ebd2d0171b', name: 'Собственный'},
                {id: 'd11a6833-cb25-438e-8eab-05f15329491f', name: 'Арендованный'}
            ],
            search: {
                registrationNumber: control({value: ''}),
                cargoBodyType: control({value: null}),
                transporterName: control({value: ''}),
                truckFleetType: control({value: null}),
                limit: control({value: 10}),
                offset: control({value: 0})
            }
        };
        this.searchRequest = new Subject<TruckSearchCriteria>();
    }

    async componentDidMount() {
        this.subscription.add(
            this.searchRequest.pipe(
                debounceTime(350),
                switchMap(search => getTrucks(search))
            ).subscribe(truckList => {
                this.setState({truckList})
            })
        );

        let truckList = await getTrucks(this.getSearchCriteria())
        this.setState({truckList})
    }

    componentWillUnmount() {
        this.subscription.unsubscribe()
    }

    getSearchCriteria(): TruckSearchCriteria {
        const {search} = this.state;
        return {
            registrationNumber: search.registrationNumber.value,
            cargoBodyType: search.cargoBodyType.value,
            transporterName: search.transporterName.value,
            truckFleetType: search.truckFleetType.value,
            limit: search.limit.value,
            offset: search.offset.value
        };
    }

    render() {
        const {onRegisterTruck, onOpenTruck} = this.props;
        const {truckList, cargoBodyTypeOptions, fleetTypeOptions, search} = this.state;

        const nameOf = (value: string, options: Option[]) => {
            for (let option of options) {
                if (option.id === value) {
                    return option.name;
                }
            }
            return value;
        }

        let rows = truckList.items.map(truck =>
            <Row key={truck.id} onClick={() => onOpenTruck(truck)}>
                <Cell>{truck.registrationNumber}</Cell>
                <Cell>{nameOf(truck.cargoBodyType, cargoBodyTypeOptions)}</Cell>
                <Cell>{truck.transporterName}</Cell>
                <Cell>{nameOf(truck.truckFleetType, fleetTypeOptions)}</Cell>
            </Row>
        );

        return (
            <PageContainer>
                <PagePanel>
                    <h1>Транспортные средства</h1>
                    <Button onClick={onRegisterTruck}>Добавить транспортное средство</Button>
                </PagePanel>
                <Surface>
                    <Table>
                        <TableHeader>
                            <Column width={160}>Гос. номер</Column>
                            <Column width={320}>Тип</Column>
                            <Column>Перевозчик</Column>
                            <Column width={160}>Парк</Column>
                        </TableHeader>
                        <TableHeader>
                            <Column>
                                <TextInput control={search.registrationNumber} icon="search"/>
                            </Column>
                            <Column>
                                <OptionInput nullable options={cargoBodyTypeOptions} control={search.cargoBodyType}/>
                            </Column>
                            <Column>
                                <TextInput control={search.transporterName} icon="search"/>
                            </Column>
                            <Column>
                                <OptionInput nullable options={fleetTypeOptions} control={search.truckFleetType}/>
                            </Column>
                        </TableHeader>
                        <TableBody>
                            {rows}
                        </TableBody>
                    </Table>
                    <TablePaginator
                        count={truckList.items.length}
                        total={truckList.total}
                        forms={['транспортное средство', 'транспортных средства', 'транспортных средств']}
                        limit={search.limit}
                        offset={search.offset}
                    />
                </Surface>
            </PageContainer>
        );
    }
}