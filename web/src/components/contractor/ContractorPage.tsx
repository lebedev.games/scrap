import React, {Component} from "react";
import {Redirect, Route, Switch} from "react-router-dom";
import {BusinessEntityGeneratorRegistration, EntrepreneurGeneratorRegistration,} from "./GeneratorRegistration";

import {Generators} from "./Generators";
import {Link, PageHeader} from "../kit/PageHeader";
import {
    AuthorizationUser,
    emptyList,
    GeneratorListItem,
    List,
    NegotiationListItem,
    TransporterListItemModel
} from "../../service/model";
import {BusinessEntityGenerator, EntrepreneurGenerator} from "./Generator";
import {GenerationPoints} from "./GenerationPoints";
import {GenerationPoint} from "./GenerationPoint";
import {GenerationPointRegistration} from "./GenerationPointRegistration";
import {Transporters} from "./Transporters";
import {BusinessEntityTransporterRegistration, EntrepreneurTransporterRegistration} from "./TransporterRegistration";
import {BusinessEntityTransporter, EntrepreneurTransporter} from "./Transporter";
import styles from "../transporter/TransporterPage.module.scss";
import {logOutCurrentUser} from "../../service/context";
import {Icon} from "../kit/Icon";
import {Truck} from "./Truck";
import {TruckRegistration} from "./TruckRegistration";
import {Trucks} from "./Trucks";
import {Negotiations} from "./Negotiations";
import {getNegotiations} from "../../service/handlers";
import {NegotiationApproval} from "./NegotiationApproval";
import {NegotiationReview} from "./NegotiationReview";
import {NegotiationResolution} from "./NegotiationResolution";
import {Reports} from "./Reports";
import {ReportCreation} from "./ReportCreation";

interface ContractorPageProps {
    user: AuthorizationUser
}

interface ContractorPageState {
    negotiationNotifications: List<NegotiationListItem>
}

export class ContractorPage extends Component<ContractorPageProps, ContractorPageState> {

    constructor(props: ContractorPageProps) {
        super(props);
        this.state = {
            negotiationNotifications: emptyList<NegotiationListItem>()
        }
    }

    async componentDidMount() {
        this.pullNotifications()
    }

    async pullNotifications() {
        let negotiationNotifications = await getNegotiations({
            transportationDate: null,
            truck: '',
            district: '',
            transporter: '',
            status: 'pending_approval',
            limit: 1,
            offset: 0
        });
        this.setState({negotiationNotifications})
    }

    render() {
        const {user} = this.props;
        const {negotiationNotifications} = this.state;

        return (
            <div>
                <PageHeader>
                    <Link to="/contractor/generators">Мусорообразователи</Link>
                    <Link to="/contractor/generation-points">Контейнерные площадки</Link>
                    <Link to="/contractor/transporters">Перевозчики</Link>
                    <Link to="/contractor/trucks">Транспортные средства</Link>
                    <Link to="/contractor/negotiations">
                        Маршрутные журналы
                        {negotiationNotifications.total > 0 && (
                            <i>{negotiationNotifications.total}</i>
                        )}
                    </Link>
                    <Link to="/contractor/reports">Отчеты</Link>
                    <aside>
                        <span className={styles.user}>{user.userName}</span>
                        <div className={styles.userExitIcon} onClick={() => logOutCurrentUser()}>
                            <Icon name="update"/>
                        </div>
                    </aside>
                </PageHeader>
                <Switch>
                    <Route axact path="/contractor/generators/business-entity/registration">
                        <BusinessEntityGeneratorRegistration
                            onCancel={() => this.openGenerators()}
                            onRegistrationComplete={id => this.openBusinessEntityGenerator(id)}
                        />
                    </Route>
                    <Route exact path="/contractor/generators/entrepreneur/registration">
                        <EntrepreneurGeneratorRegistration
                            onCancel={() => this.openGenerators()}
                            onRegistrationComplete={id => this.openEntrepreneurGenerator(id)}
                        />
                    </Route>
                    <Route path="/contractor/generators/business-entity/:id" render={props => {
                        let {id} = props.match.params;
                        return <BusinessEntityGenerator id={id}/>
                    }}/>
                    <Route path="/contractor/generators/entrepreneur/:id" render={props => {
                        let {id} = props.match.params;
                        return <EntrepreneurGenerator id={id}/>
                    }}/>
                    <Route path="/contractor/generators">
                        <Generators
                            readonly={true}
                            onRegisterBusinessEntityGenerator={() => this.registerBusinessEntityGenerator()}
                            onRegisterEntrepreneurGenerator={() => this.registerEntrepreneurGenerator()}
                            onOpenGenerator={generator => this.openGenerator(generator)}
                        />
                    </Route>
                    <Route exact path="/contractor/generation-points/registration">
                        <GenerationPointRegistration
                            onCancel={() => this.openGenerationPoints()}
                            onRegistrationComplete={id => this.openGenerationPoint(id)}
                        />
                    </Route>
                    <Route path="/contractor/generation-points/:id" render={props => {
                        let {id} = props.match.params;
                        return <GenerationPoint id={id}/>
                    }}/>
                    <Route path="/contractor/generation-points">
                        <GenerationPoints
                            readonly={true}
                            onRegisterGenerationPoint={() => this.registerGenerationPoint()}
                            onOpenGenerationPoint={id => this.openGenerationPoint(id)}
                        />
                    </Route>
                    <Route axact path="/contractor/transporters/business-entity/registration">
                        <BusinessEntityTransporterRegistration
                            onCancel={() => this.openTransporters()}
                            onRegistrationComplete={id => this.openBusinessEntityTransporter(id)}
                        />
                    </Route>
                    <Route exact path="/contractor/transporters/entrepreneur/registration">
                        <EntrepreneurTransporterRegistration
                            onCancel={() => this.openTransporters()}
                            onRegistrationComplete={id => this.openEntrepreneurTransporter(id)}
                        />
                    </Route>
                    <Route path="/contractor/transporters/business-entity/:id" render={props => {
                        let {id} = props.match.params;
                        return <BusinessEntityTransporter id={id}/>
                    }}/>
                    <Route path="/contractor/transporters/entrepreneur/:id" render={props => {
                        let {id} = props.match.params;
                        return <EntrepreneurTransporter id={id}/>
                    }}/>
                    <Route path="/contractor/transporters">
                        <Transporters
                            onOpenTransporter={transporter => this.openTransporter(transporter)}
                            onRegisterBusinessEntityTransporter={() => this.registerBusinessEntityTransporter()}
                            onRegisterEntrepreneurTransporter={() => this.registerEntrepreneurTransporter()}
                        />
                    </Route>
                    <Route exact path="/contractor/trucks/registration">
                        <TruckRegistration
                            onCancel={() => this.openTrucks()}
                            onRegistrationComplete={id => this.openTruck(id)}
                        />
                    </Route>
                    <Route path="/contractor/trucks/:id" render={props => {
                        let {id} = props.match.params;
                        return <Truck id={id}/>
                    }}/>
                    <Route path="/contractor/trucks">
                        <Trucks
                            onRegisterTruck={() => this.registerTruck()}
                            onOpenTruck={truck => this.openTruck(truck.id)}
                        />
                    </Route>
                    <Route path="/contractor/negotiations/review/:id" render={props => {
                        let {id} = props.match.params;
                        return <NegotiationReview
                            id={id}
                            onCancel={() => this.openNegotiations()}
                        />
                    }}/>
                    <Route path="/contractor/negotiations/resolution/:id" render={props => {
                        let {id} = props.match.params;
                        return <NegotiationResolution id={id} onCancel={() => this.openNegotiations()} />
                    }} />
                    <Route path="/contractor/negotiations/approval/:id" render={props => {
                        let {id} = props.match.params;
                        return <NegotiationApproval
                            id={id}
                            onCancel={() => this.openNegotiations()}
                            onCompleteNegotiation={id => {
                                this.pullNotifications();
                                this.openNegotiationReview(id);
                            }}
                            onDeclareNegotiationIssues={() => {
                                this.pullNotifications();
                                this.openNegotiationResolution(id);
                            }}/>
                    }}/>
                    <Route path="/contractor/negotiations">
                        <Negotiations readonly={false} onOpenNegotiation={negotiation => this.openNegotiation(negotiation)}/>
                    </Route>
                    <Route path="/contractor/reports/creation">
                        <ReportCreation onCreationComplete={() => this.openReports()} onCancel={() => this.openReports()}/>
                    </Route>
                    <Route path="/contractor/reports">
                        <Reports readonly={false} onCreateReport={() => this.createReport()} />
                    </Route>
                    <Route path="/contractor" render={() => this.openContractorInitialPage()}/>
                </Switch>
            </div>
        )
    }

    private openReports() {
        this.openPage('/contractor/reports');
    }

    private createReport() {
        this.openPage('/contractor/reports/creation')
    }

    private openNegotiations() {
        this.openPage('/contractor/negotiations')
    }

    private openNegotiation(negotiation: NegotiationListItem) {
        if (negotiation.status === 'pending_approval') {
            this.openNegotiationApproval(negotiation.id);
        }
        if (negotiation.status === 'completed') {
            this.openNegotiationReview(negotiation.id);
        }
        if (negotiation.status === 'pending_resolution') {
            this.openNegotiationResolution(negotiation.id);
        }
    }

    private openNegotiationApproval(id: string) {
        this.openPage('/contractor/negotiations/approval/' + id);
    }

    private openNegotiationReview(id: string) {
        this.openPage('/contractor/negotiations/review/' + id);
    }

    private openNegotiationResolution(id: string) {
        this.openPage('/contractor/negotiations/resolution/' + id);
    }

    private registerTruck() {
        this.openPage('/contractor/trucks/registration')
    }

    private registerBusinessEntityTransporter() {
        this.openPage('/contractor/transporters/business-entity/registration')
    }

    private registerEntrepreneurTransporter() {
        this.openPage('/contractor/transporters/entrepreneur/registration')
    }

    private openTransporters() {
        this.openPage('/contractor/transporters');
    }

    private openTransporter(transporter: TransporterListItemModel) {
        if (transporter.legalEntityType === 'BusinessEntity') {
            this.openBusinessEntityTransporter(transporter.id);
        } else {
            this.openEntrepreneurTransporter(transporter.id);
        }
    }

    private openEntrepreneurTransporter(id: string) {
        this.openPage('/contractor/transporters/entrepreneur/' + id);
    }

    private openBusinessEntityTransporter(id: string) {
        this.openPage('/contractor/transporters/business-entity/' + id);
    }

    private registerBusinessEntityGenerator() {
        this.openPage('/contractor/generators/business-entity/registration')
    }

    private registerEntrepreneurGenerator() {
        this.openPage('/contractor/generators/entrepreneur/registration')
    }

    private registerGenerationPoint() {
        this.openPage('/contractor/generation-points/registration')
    }

    private openGenerationPoint(id: string) {
        this.openPage('/contractor/generation-points/' + id);
    }

    private openGenerationPoints() {
        this.openPage('/contractor/generation-points');
    }

    private openTruck(id: string) {
        this.openPage('/contractor/trucks/' + id);
    }

    private openTrucks() {
        this.openPage('/contractor/trucks');
    }

    private openGenerator(generator: GeneratorListItem) {
        if (generator.legalEntityType === 'BusinessEntity') {
            this.openBusinessEntityGenerator(generator.id);
        } else {
            this.openEntrepreneurGenerator(generator.id);
        }
    }

    private openEntrepreneurGenerator(id: string) {
        this.openPage('/contractor/generators/entrepreneur/' + id);
    }

    private openBusinessEntityGenerator(id: string) {
        this.openPage('/contractor/generators/business-entity/' + id);
    }

    private openGenerators() {
        this.openPage('/contractor/generators');
    }

    openPage(path: string) {
        window.history.pushState(null, "Title", path);
        window.dispatchEvent(new window.PopStateEvent('popstate'));
    }

    private openContractorInitialPage() {
        return <Redirect to="/contractor/generators"/>
    }
}