import React, {Component} from "react";
import {PageContainer} from "../kit/PageContainer";
import {PagePanel} from "../kit/PagePanel";
import {Control, Form} from "../kit/forms";
import {Option} from "../../service/model";
import {
    getAxleConfigurationOptions,
    getCargoBodyTypeOptions,
    getFleetTypeOptions,
    getRequiredDriverLicenseCategoryOptions,
    getTransporterOptions,
    registerTruck
} from "../../service/handlers";
import {required} from "../../data/validators";
import {Surface, SurfaceFooter} from "../kit/Surface";
import {Compound, Field, FormSection} from "../kit/Field";
import {TextInput} from "../kit/TextInput";
import {Button} from "../kit/Button";
import {OptionInput} from "../kit/OptionInput";
import {formatDecimal, formatInteger} from "../../data/formatters";
import {parseDecimal, parseInteger} from "../../data/parsers";

interface TruckRegistrationProps {
    onRegistrationComplete: (id: string) => void
    onCancel: () => void
}

interface TruckRegistrationState {
    transporterOptions: Option[]
    fleetTypeOptions: Option[]
    cargoBodyTypeOptions: Option[]
    requiredDriverLicenseCategoryOptions: Option[]
    axleConfigurationOptions: Option[]
    form: {
        owner: Control
        transporter: Control
        fleetType: Control
        vin: Control
        registrationNumber: Control
        garagingNumber: Control
        vehicleModel: Control
        wasteEquipmentModel: Control
        cargoBodyType: Control
        requiredDriverLicenseCategory: Control
        axleConfiguration: Control
        productionYear: Control
        navigationEquipmentName: Control
        emptyWeightKg: Control
        cargoCapacityKg: Control
    }
}

export class TruckRegistration extends Component<TruckRegistrationProps, TruckRegistrationState> {
    constructor(props: TruckRegistrationProps) {
        super(props);
        const control = Form.getControlConstructor(this);
        this.state = {
            transporterOptions: [],
            fleetTypeOptions: [],
            cargoBodyTypeOptions: [],
            requiredDriverLicenseCategoryOptions: [],
            axleConfigurationOptions: [],
            form: {
                owner: control({
                    value: 'Собственников Владелец Оунерович',
                    validators: [required()]
                }),
                transporter: control({
                    value: null,
                    validators: [required()]
                }),
                fleetType: control({
                    value: null,
                    validators: [required()]
                }),
                vin: control({
                    value: 'JH4KA4540LC016127',
                    validators: [required()]
                }),
                registrationNumber: control({
                    value: 'M765TP78',
                    validators: [required()]
                }),
                garagingNumber: control({
                    value: '873YT76543F56432876T',
                    validators: [required()]
                }),
                vehicleModel: control({
                    value: 'SCANIA P360 (XT) ZOELLER MEDIUM XXL 22,6',
                    validators: [required()]
                }),
                wasteEquipmentModel: control({
                    value: 'ЭКО-МБ18К56',
                    validators: [required()]
                }),
                cargoBodyType: control({
                    value: null,
                    validators: [required()]
                }),
                requiredDriverLicenseCategory: control({
                    value: null,
                    validators: [required()]
                }),
                axleConfiguration: control({
                    value: null,
                    validators: [required()]
                }),
                productionYear: control({
                    value: 1965,
                    getter: formatInteger,
                    setter: parseInteger,
                    validators: [required()]
                }),
                navigationEquipmentName: control({
                    value: 'ГЛОНАСС Carnet F900HD',
                    validators: [required()]
                }),
                emptyWeightKg: control({
                    value: 3000,
                    getter: formatDecimal,
                    setter: parseDecimal,
                    validators: [required()]
                }),
                cargoCapacityKg: control({
                    value: 2000,
                    getter: formatDecimal,
                    setter: parseDecimal,
                    validators: [required()]
                })
            }
        }
    }

    async componentDidMount() {
        let transporterOptions = await getTransporterOptions()
        this.setState({transporterOptions})
        let fleetTypeOptions = await getFleetTypeOptions()
        this.setState({fleetTypeOptions})
        let cargoBodyTypeOptions = await getCargoBodyTypeOptions()
        this.setState({cargoBodyTypeOptions})
        let requiredDriverLicenseCategoryOptions = await getRequiredDriverLicenseCategoryOptions()
        this.setState({requiredDriverLicenseCategoryOptions})
        let axleConfigurationOptions = await getAxleConfigurationOptions()
        this.setState({axleConfigurationOptions})
    }

    async registerTruck() {
        const {form} = this.state;
        const {onRegistrationComplete} = this.props;

        Form.validate(form);
        if (!Form.isValid(form)) {
            return;
        }

        const registration = {
            owner: form.owner.value,
            transporter: form.transporter.value,
            fleetType: form.fleetType.value,
            vin: form.vin.value,
            registrationNumber: form.registrationNumber.value,
            garagingNumber: form.garagingNumber.value,
            vehicleModel: form.vehicleModel.value,
            wasteEquipmentModel: form.wasteEquipmentModel.value,
            cargoBodyType: form.cargoBodyType.value,
            requiredDriverLicenseCategory: form.requiredDriverLicenseCategory.value,
            axleConfiguration: form.axleConfiguration.value,
            productionYear: form.productionYear.value,
            navigationEquipmentName: form.navigationEquipmentName.value,
            emptyWeightKg: form.emptyWeightKg.value,
            cargoCapacityKg: form.emptyWeightKg.value
        }
        let truck = await registerTruck(registration);
        onRegistrationComplete(truck.id);
    }

    render() {
        const {
            form,
            transporterOptions,
            fleetTypeOptions,
            cargoBodyTypeOptions,
            requiredDriverLicenseCategoryOptions,
            axleConfigurationOptions
        } = this.state;
        const {onCancel} = this.props;

        return (
            <PageContainer>
                <PagePanel>
                    <h1>Добавление ТС</h1>
                </PagePanel>
                <Surface>
                    <FormSection title="Основная информация">
                        <Field error={form.owner.getError()}>
                            <label>Владелец</label>
                            <TextInput control={form.owner}/>
                        </Field>
                        <Field error={form.transporter.getError()}>
                            <label>Перевозчик</label>
                            <OptionInput control={form.transporter} options={transporterOptions}/>
                        </Field>
                        <Field width={160} error={form.fleetType.getError()}>
                            <label>Парк</label>
                            <OptionInput control={form.fleetType} options={fleetTypeOptions}/>
                        </Field>
                    </FormSection>
                    <FormSection title="Идентификационные номера">
                        <Field width={332} error={form.vin.getError()}>
                            <label>VIN</label>
                            <TextInput control={form.vin}/>
                        </Field>
                        <Compound>
                            <Field error={form.registrationNumber.getError()}>
                                <label>Государственный регистрационный номер</label>
                                <TextInput control={form.registrationNumber}/>
                            </Field>
                            <Field error={form.garagingNumber.getError()}>
                                <label>Гаражный номер</label>
                                <TextInput control={form.garagingNumber}/>
                            </Field>
                        </Compound>
                    </FormSection>
                    <FormSection title="Основные параметры">
                        <Compound>
                            <Field error={form.vehicleModel.getError()}>
                                <label>Марка и модель ТС</label>
                                <TextInput control={form.vehicleModel}/>
                            </Field>
                            <Field error={form.wasteEquipmentModel.getError()}>
                                <label>Марка установки</label>
                                <TextInput control={form.wasteEquipmentModel}/>
                            </Field>
                        </Compound>
                        <Compound>
                            <Field width={332} error={form.cargoBodyType.getError()}>
                                <label>Тип ТС</label>
                                <OptionInput control={form.cargoBodyType} options={cargoBodyTypeOptions}/>
                            </Field>
                            <Field width={160} error={form.requiredDriverLicenseCategory.getError()}>
                                <label>Категория ТС</label>
                                <OptionInput control={form.requiredDriverLicenseCategory}
                                             options={requiredDriverLicenseCategoryOptions}/>
                            </Field>
                            <Field width={160} error={form.axleConfiguration.getError()}>
                                <label>Колесная формула</label>
                                <OptionInput control={form.axleConfiguration} options={axleConfigurationOptions}/>
                            </Field>
                        </Compound>
                        <Field width={160} error={form.productionYear.getError()}>
                            <label>Год выпуска ТС</label>
                            <TextInput control={form.productionYear}/>
                        </Field>
                        <Field error={form.navigationEquipmentName.getError()}>
                            <label>Установленное бортовое навигационно-связное оборудование</label>
                            <TextInput control={form.navigationEquipmentName}/>
                        </Field>
                        <Compound>
                            <Field width={160} error={form.emptyWeightKg.getError()}>
                                <label>Вес пустого ТС, кг</label>
                                <TextInput control={form.emptyWeightKg}/>
                            </Field>
                            <Field width={160} error={form.cargoCapacityKg.getError()}>
                                <label>Вес допустимого груза, кг</label>
                                <TextInput control={form.cargoCapacityKg}/>
                            </Field>
                        </Compound>
                        <Field>
                            <label>Перечень полигонов с разрешенным въездом для данного ТС</label>
                            <TextInput readonly={true}/>
                        </Field>
                    </FormSection>
                    <SurfaceFooter>
                        <Button type="primary" onClick={() => this.registerTruck()}>
                            Добавить транспортное средство
                        </Button>
                        <Button type="outline" onClick={onCancel}>Отмена</Button>
                    </SurfaceFooter>
                </Surface>
                <div style={{paddingTop: '72px'}}/>
            </PageContainer>
        )
    }
}