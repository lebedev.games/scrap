import {emptyList, GenerationPointListItemModel, GenerationPointSearchCriteria, List} from "../../service/model";
import React, {Component} from "react";
import {getGenerationPoints} from "../../service/handlers";
import {Surface} from "../kit/Surface";
import {Button} from "../kit/Button";
import {PageContainer} from "../kit/PageContainer";
import {PagePanel} from "../kit/PagePanel";
import {Cell, Column, Row, Table, TableBody, TableHeader, TablePaginator} from "../kit/Table";
import {TextInput} from "../kit/TextInput";
import {Control, Form} from "../kit/forms";
import {Subject, Subscription} from "rxjs";
import {debounceTime, switchMap} from "rxjs/operators";

interface GenerationPointsProps {
    readonly: boolean
    onRegisterGenerationPoint: () => void;
    onOpenGenerationPoint: (id: string) => void;
}


interface GenerationPointsState {
    generationPointList: List<GenerationPointListItemModel>,
    search: {
        internalSystemNumber: Control,
        transporter: Control,
        address: Control,
        limit: Control,
        offset: Control
    }
}

export class GenerationPoints extends Component<GenerationPointsProps, GenerationPointsState> {
    subscription = new Subscription();
    searchRequest: Subject<GenerationPointSearchCriteria>;

    constructor(props: GenerationPointsProps) {
        super(props);
        let control = Form.getControlConstructor(this, () => this.searchRequest.next(this.getSearchCriteria()));
        this.state = {
            generationPointList: emptyList<GenerationPointListItemModel>(),
            search: {
                internalSystemNumber: control({value: ''}),
                transporter: control({value: ''}),
                address: control({value: ''}),
                limit: control({value: 10}),
                offset: control({value: 0})
            }
        };
        this.searchRequest = new Subject<GenerationPointSearchCriteria>();
    }

    async componentDidMount() {
        this.subscription.add(
            this.searchRequest.pipe(
                debounceTime(350),
                switchMap(search => getGenerationPoints(search))
            ).subscribe(generationPointList => {
                this.setState({generationPointList})
            })
        );

        const generationPointList = await getGenerationPoints(this.getSearchCriteria())
        this.setState({generationPointList})
    }

    componentWillUnmount() {
        this.subscription.unsubscribe()
    }

    getSearchCriteria(): GenerationPointSearchCriteria {
        const {search} = this.state;
        return {
            internalSystemNumber: search.internalSystemNumber.value,
            transporter: search.transporter.value,
            address: search.address.value,
            limit: search.limit.value,
            offset: search.offset.value
        };
    }

    render() {
        const {generationPointList, search} = this.state;
        const {onOpenGenerationPoint, onRegisterGenerationPoint, readonly} = this.props;

        return (
            <PageContainer>
                <PagePanel>
                    <h1>Контейнерные площадки</h1>
                    {!readonly &&
                    <Button onClick={onRegisterGenerationPoint}>
                        Добавить контейнерную площадку
                    </Button>
                    }
                </PagePanel>
                <Surface>
                    <Table>
                        <TableHeader>
                            <Column width={120}>№ в системе</Column>
                            <Column>Перевозчик</Column>
                            <Column>Адрес</Column>
                            <Column width={120}>Площадь, м2</Column>
                        </TableHeader>
                        <TableHeader>
                            <Column>
                                <TextInput icon="search" control={search.internalSystemNumber}/>
                            </Column>
                            <Column>
                                <TextInput icon="search" control={search.transporter}/>
                            </Column>
                            <Column>
                                <TextInput icon="search" control={search.address}/>
                            </Column>
                            <Column>
                                <TextInput readonly icon="search"/>
                            </Column>
                        </TableHeader>
                        <TableBody>
                            {generationPointList.items.map(point => (
                                <Row key={point.id} onClick={() => onOpenGenerationPoint(point.id)}>
                                    <Cell>{point.internalSystemNumber}</Cell>
                                    <Cell>{point.transporterName}</Cell>
                                    <Cell>{point.fullAddress}</Cell>
                                    <Cell>{point.areaM2}</Cell>
                                </Row>
                            ))}
                        </TableBody>
                    </Table>
                    <TablePaginator
                        count={generationPointList.items.length}
                        total={generationPointList.total}
                        forms={["контейнерная площадка", "контейнерные площадки", "контейнерных площадок"]}
                        limit={search.limit}
                        offset={search.offset}
                    />
                </Surface>
            </PageContainer>
        )
    }
}