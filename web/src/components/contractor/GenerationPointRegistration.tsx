import React, {Component} from "react";
import {PageContainer} from "../kit/PageContainer";
import {PagePanel} from "../kit/PagePanel";
import {Control, Form} from "../kit/forms";
import {Option, RegisterGenerationPointFormContainer} from "../../service/model";
import {
    getAccrualMethodOptions,
    getContainerTypeOptions,
    getCoveringTypeOptions,
    getFenceMaterialOptions, getGeneratorOptions,
    getMeasurementUnitOptions,
    getOperationalStatusOptions,
    getOrganizationLegalTypeOptions, getTransporterOptions, getWasteTypeOptions,
    registerGenrationPoint
} from "../../service/handlers";
import {notEmptyCollection, required} from "../../data/validators";
import {parseInteger} from "../../data/parsers";
import {formatInteger} from "../../data/formatters";
import moment from "moment";
import {Surface, SurfaceFooter} from "../kit/Surface";
import {Compound, Field, FieldsSeparator, FormSection} from "../kit/Field";
import {TextInput} from "../kit/TextInput";
import {Button, TextButton} from "../kit/Button";
import {OptionInput} from "../kit/OptionInput";
import {ImageInput} from "../kit/ImageInput";
import {DateInput} from "../kit/DateInput";

interface GenerationPointRegistrationProps {
    onRegistrationComplete: (id: string) => void
    onCancel: () => void
}

interface ContainerDefinition {
    containerType: string
    number: number
}

interface GenerationPointRegistrationState {
    containerTypeOptions: Option[]
    accrualMethodOptions: Option[]
    measurementUnitOptions: Option[]
    coveringTypeOptions: Option[]
    fenceMaterialOptions: Option[]
    operationalStatusOptions: Option[]
    organizationLegalTypeOptions: Option[]
    wasteTypeOptions: Option[]
    transporterOptions: Option[]
    generatorOptions: Option[]
    containerForm: {
        containerType: Control
        number: Control
    }
    form: {
        internalSystemNumber: Control
        externalRegistryNumber: Control
        wasteType: Control
        organizationLegalType: Control
        operationalStatus: Control
        fullAddress: Control
        coordinates: Control
        containerDefinitions: Control
        areaM2: Control
        coveringType: Control
        fenceMaterial: Control
        serviceCompanyName: Control
        generator: Control
        description: Control
        transporter: Control
        transporterSchedule: Control
        transporterScheduleStart: Control
        transporterScheduleEnd: Control
        photos: Control
        accrualMethod: Control
        measurementUnit: Control
    }
}

export class GenerationPointRegistration extends Component<GenerationPointRegistrationProps, GenerationPointRegistrationState> {
    constructor(props: GenerationPointRegistrationProps) {
        super(props);
        const control = Form.getControlConstructor(this);
        this.state = {
            containerTypeOptions: [],
            accrualMethodOptions: [],
            measurementUnitOptions: [],
            coveringTypeOptions: [],
            fenceMaterialOptions: [],
            operationalStatusOptions: [],
            organizationLegalTypeOptions: [],
            wasteTypeOptions: [],
            generatorOptions: [],
            transporterOptions: [],
            containerForm: {
                containerType: control({
                    value: null,
                    validators: [required()]
                }),
                number: control({
                    value: 1,
                    getter: formatInteger,
                    setter: parseInteger,
                    validators: [required()]
                })
            },
            form: {
                containerDefinitions: control({
                    value: [],
                    validators: [
                        notEmptyCollection('Необходимо добавить контейнеры')
                    ]
                }),
                internalSystemNumber: control({
                    value: 'КП-0001',
                    validators: [required()]
                }),
                externalRegistryNumber: control({
                    value: '62517',
                    validators: [required()]
                }),
                wasteType: control({
                    value: null,
                    validators: [required()]
                }),
                organizationLegalType: control({
                    value: null,
                    validators: [required()]
                }),
                operationalStatus: control({
                    value: null,
                    validators: [required()]
                }),
                fullAddress: control({
                    value: 'Российская Федерация, 191002, г. Санкт-Петербург, ул. Достоевского, д. 15',
                    validators: [required()]
                }),
                coordinates: control({
                    value: '59.871994, 30.441502',
                    validators: [required()]
                }),
                areaM2: control({
                    value: 105,
                    getter: formatInteger,
                    setter: parseInteger,
                    validators: [required()]
                }),
                coveringType: control({
                    value: null,
                    validators: [required()]
                }),
                fenceMaterial: control({
                    value: null,
                    validators: [required()]
                }),
                serviceCompanyName: control({
                    value: null,
                }),
                generator: control({
                    value: null,
                    validators: [required()]
                }),
                description: control({
                    value: 'Контейнерная площадка 3-х местная для мусора и ТБО трехместная без дверей. ' +
                        'Такие контейнерные площадки неотъемлемый атрибут каждого двора, каждого микрорайона, ' +
                        'каждого города. Каркас площадки изготовлен из профильной трубы 50*25 и 40*20, ' +
                        'обшита такая контейнерная площадка крашеным оцинкованным проф листом и может ' +
                        'дополнительно устанавливаться в комбинации с оцинкованной сеткой с ячейкой 50*50.',
                    validators: [required()]
                }),
                transporter: control({
                    value: null,
                    validators: [required()]
                }),
                transporterSchedule: control({
                    value: '2 раза в неделю',
                    validators: [required()]
                }),
                transporterScheduleStart: control({
                    value: moment().toISOString(),
                    validators: [required()]
                }),
                transporterScheduleEnd: control({
                    value: moment().add(6, 'months').toISOString(),
                    validators: [required()]
                }),
                photos: control({
                    value: [],
                    validators: [
                        notEmptyCollection('Необходимо добавить фотографии контейнерной площадки')
                    ]
                }),
                accrualMethod: control({
                    value: null,
                    validators: [required()]
                }),
                measurementUnit: control({
                    value: null,
                    validators: [required()]
                })
            }
        }
    }

    async componentDidMount() {
        let accrualMethodOptions = await getAccrualMethodOptions()
        this.setState({accrualMethodOptions})
        let containerTypeOptions = await getContainerTypeOptions()
        this.setState({containerTypeOptions})
        let coveringTypeOptions = await getCoveringTypeOptions()
        this.setState({coveringTypeOptions})
        let fenceMaterialOptions = await getFenceMaterialOptions()
        this.setState({fenceMaterialOptions})
        let measurementUnitOptions = await getMeasurementUnitOptions()
        this.setState({measurementUnitOptions})
        let operationalStatusOptions = await getOperationalStatusOptions()
        this.setState({operationalStatusOptions})
        let organizationLegalTypeOptions = await getOrganizationLegalTypeOptions()
        this.setState({organizationLegalTypeOptions})
        let wasteTypeOptions = await getWasteTypeOptions()
        this.setState({wasteTypeOptions})
        let transporterOptions = await getTransporterOptions()
        this.setState({transporterOptions})
        let generatorOptions = await getGeneratorOptions()
        this.setState({generatorOptions})
    }

    async registerGenerationPoint() {
        const {form} = this.state;
        const {onRegistrationComplete} = this.props;

        Form.validate(form);
        if (!Form.isValid(form)) {
            return;
        }

        let containers: RegisterGenerationPointFormContainer[] = [];
        for (let definition of form.containerDefinitions.value) {
            let container = {containerType: definition.containerType};
            containers = containers.concat(new Array(definition.number).fill(container));
        }

        const registration = {
            internalSystemNumber: form.internalSystemNumber.value,
            externalRegistryNumber: form.externalRegistryNumber.value,
            wasteType: form.wasteType.value,
            organizationLegalType: form.organizationLegalType.value,
            operationalStatus: form.operationalStatus.value,
            fullAddress: form.fullAddress.value,
            coordinates: form.coordinates.value,
            areaM2: form.areaM2.value,
            coveringType: form.coveringType.value,
            fenceMaterial: form.fenceMaterial.value,
            serviceCompanyName: form.serviceCompanyName.value,
            generator: form.generator.value,
            description: form.description.value,
            containers: containers,
            transporter: {
                transporter: form.transporter.value,
                startTransportationDate: form.transporterScheduleStart.value,
                endTransportationDate: form.transporterScheduleEnd.value,
                frequency: form.transporterSchedule.value
            },
            photos: form.photos.value,
            accrualMethod: form.accrualMethod.value,
            measurementUnit: form.measurementUnit.value
        }
        let generator = await registerGenrationPoint(registration);
        onRegistrationComplete(generator.id);
    }


    addContainerDefinition() {
        const {containerForm, form} = this.state;

        Form.validate(containerForm);
        if (!Form.isValid(containerForm)) {
            return;
        }

        const containerDefinition = {
            containerType: containerForm.containerType.value,
            number: containerForm.number.value
        };
        let containerDefinitions = form.containerDefinitions.value;
        containerDefinitions.push(containerDefinition);
        form.containerDefinitions.setValue(containerDefinitions);
    }

    removeContainerDefinition(definition: ContainerDefinition) {
        const {form} = this.state;
        let containerDefinitions = form.containerDefinitions.value;
        let index = containerDefinitions.indexOf(definition);
        if (index > -1) {
            containerDefinitions.splice(index, 1);
        }
        form.containerDefinitions.setValue(containerDefinitions);
    }

    render() {
        const {
            form,
            containerForm,
            containerTypeOptions,
            accrualMethodOptions,
            measurementUnitOptions,
            coveringTypeOptions,
            fenceMaterialOptions,
            operationalStatusOptions,
            organizationLegalTypeOptions,
            wasteTypeOptions,
            generatorOptions,
            transporterOptions
        } = this.state;
        const {onCancel} = this.props;

        return (
            <PageContainer>
                <PagePanel>
                    <h1>Добавление контейнерной площадки</h1>
                </PagePanel>
                <Surface>
                    <FormSection title="Мусорообразователь">
                        <Field error={form.generator.getError()}>
                            <label>Наименование</label>
                            <OptionInput control={form.generator} options={generatorOptions}/>
                        </Field>
                    </FormSection>
                    <FormSection title="Характеристики площадки">
                        <Compound>
                            <Field error={form.internalSystemNumber.getError()}>
                                <label>Номер в системе</label>
                                <TextInput control={form.internalSystemNumber}/>
                            </Field>
                            <Field error={form.externalRegistryNumber.getError()}>
                                <label>Номер в реестре мест</label>
                                <TextInput control={form.externalRegistryNumber}/>
                            </Field>
                            <Field error={form.wasteType.getError()}>
                                <label>Тип площадки</label>
                                <OptionInput control={form.wasteType} options={wasteTypeOptions}/>
                            </Field>
                            <Field error={form.areaM2.getError()}>
                                <label>Площадь, м2</label>
                                <TextInput control={form.areaM2}/>
                            </Field>
                        </Compound>
                        <Compound>
                            <Field error={form.organizationLegalType.getError()}>
                                <label>Вид площадки</label>
                                <OptionInput control={form.organizationLegalType}
                                             options={organizationLegalTypeOptions}/>
                            </Field>
                            <Field error={form.coveringType.getError()}>
                                <label>Тип покрытия</label>
                                <OptionInput control={form.coveringType} options={coveringTypeOptions}/>
                            </Field>
                            <Field error={form.fenceMaterial.getError()}>
                                <label>Материал ограждения</label>
                                <OptionInput control={form.fenceMaterial} options={fenceMaterialOptions}/>
                            </Field>
                            <Field error={form.operationalStatus.getError()}>
                                <label>Статус</label>
                                <OptionInput control={form.operationalStatus} options={operationalStatusOptions}/>
                            </Field>
                        </Compound>
                        <Field error={form.description.getError()}>
                            <label>Описание</label>
                            <TextInput type="multiline" control={form.description}/>
                        </Field>
                    </FormSection>
                    <FormSection title="Расположение">
                        <Field width={332} error={form.coordinates.getError()}>
                            <label>Координаты</label>
                            <TextInput control={form.coordinates}/>
                        </Field>
                        <Field error={form.fullAddress.getError()}>
                            <label>Адрес</label>
                            <TextInput control={form.fullAddress}/>
                        </Field>
                    </FormSection>
                    <FormSection title="Контейнеры">
                        <Compound>
                            <Field width={332} error={containerForm.containerType.getError()}>
                                <label>Тип контейнера</label>
                                <OptionInput
                                    control={containerForm.containerType}
                                    options={containerTypeOptions}
                                />
                            </Field>
                            <Field width={160} error={containerForm.number.getError()}>
                                <label>Количество</label>
                                <TextInput control={containerForm.number}/>
                            </Field>
                            <TextButton onClick={() => this.addContainerDefinition()}>
                                Добавить
                            </TextButton>
                        </Compound>
                        <Field error={form.containerDefinitions.getError()} />
                        {form.containerDefinitions.value.length > 0 && (
                            <React.Fragment>
                                <FieldsSeparator/>
                                {form.containerDefinitions.value.map((definition: ContainerDefinition) => (
                                    <Compound key={definition.containerType}>
                                        <Field width={332}>
                                            <OptionInput
                                                readonly
                                                value={definition.containerType}
                                                options={containerTypeOptions}
                                            />
                                        </Field>
                                        <Field width={160}>
                                            <TextInput readonly value={formatInteger(definition.number)}/>
                                        </Field>
                                        <TextButton
                                            type="secondary"
                                            onClick={() => this.removeContainerDefinition(definition)}
                                        >
                                            Удалить
                                        </TextButton>
                                    </Compound>
                                ))}
                            </React.Fragment>
                        )}
                    </FormSection>
                    <FormSection title="Перевозчик">
                        <Field error={form.transporter.getError()}>
                            <label>Наименование</label>
                            <OptionInput control={form.transporter} options={transporterOptions}/>
                        </Field>
                        <Compound>
                            <Field width={160} error={form.transporterScheduleStart.getError()}>
                                <label>Дата начала вывоза</label>
                                <DateInput control={form.transporterScheduleStart}/>
                            </Field>
                            <Field width={160} error={form.transporterScheduleEnd.getError()}>
                                <label>Дата окончания вывоза</label>
                                <DateInput control={form.transporterScheduleEnd}/>
                            </Field>
                            <Field error={form.transporterSchedule.getError()}>
                                <label>Регулярность вывоза</label>
                                <TextInput control={form.transporterSchedule}/>
                            </Field>
                        </Compound>
                    </FormSection>
                    <FormSection title="Дополнительно">
                        <Compound>
                            <Field error={form.accrualMethod.getError()}>
                                <label>Способ начисления</label>
                                <OptionInput control={form.accrualMethod} options={accrualMethodOptions}/>
                            </Field>
                            <Field error={form.measurementUnit.getError()}>
                                <label>Расчетная единица измерения</label>
                                <OptionInput control={form.measurementUnit} options={measurementUnitOptions}/>
                            </Field>
                        </Compound>
                    </FormSection>
                    <FormSection title="Фотографии">
                        <Field error={form.photos.getError()}>
                            <ImageInput control={form.photos}/>
                        </Field>
                    </FormSection>
                    <SurfaceFooter>
                        <Button type="primary" onClick={() => this.registerGenerationPoint()}>
                            Добавить площадку
                        </Button>
                        <Button type="outline" onClick={onCancel}>Отмена</Button>
                    </SurfaceFooter>
                </Surface>
                <div style={{paddingTop: '72px'}}/>
            </PageContainer>
        )
    }
}