import React, {Component} from "react";
import {emptyList, List, NegotiationListItem, NegotiationSearchCriteria, Option} from "../../service/model";
import {getNegotiations} from "../../service/handlers";
import {Surface} from "../kit/Surface";
import {Cell, Column, Row, Table, TableBody, TableHeader, TablePaginator} from "../kit/Table";
import {formatDate} from "../../data/formatters";
import {PageContainer} from "../kit/PageContainer";
import {PagePanel} from "../kit/PagePanel";
import {TextInput} from "../kit/TextInput";
import {Subject, Subscription} from "rxjs";
import {debounceTime, switchMap} from "rxjs/operators";
import {Control, Form} from "../kit/forms";
import {OptionInput} from "../kit/OptionInput";
import {DateInput} from "../kit/DateInput";
import {NegotiationStatus} from "../kit/NegotiationStatus";

interface NegotiationsProps {
    readonly: boolean
    onOpenNegotiation: (negotiation: NegotiationListItem) => void;
}


interface NegotiationsState {
    negotiationList: List<NegotiationListItem>,
    statusOptions: Option[],
    search: {
        transportationDate: Control,
        truck: Control,
        district: Control,
        transporter: Control,
        status: Control,
        limit: Control,
        offset: Control
    }
}

export class Negotiations extends Component<NegotiationsProps, NegotiationsState> {
    subscription = new Subscription();
    searchRequest: Subject<NegotiationSearchCriteria>;

    constructor(props: NegotiationsProps) {
        super(props);
        let control = Form.getControlConstructor(this, () => this.searchRequest.next(this.getSearchCriteria()));
        this.state = {
            negotiationList: emptyList<NegotiationListItem>(),
            statusOptions: [
                {'id': 'pending_approval', name: 'На согласовании'},
                {'id': 'pending_resolution', name: 'Требует доработки'},
                {'id': 'completed', name: 'Согласован'},
            ],
            search: {
                transportationDate: control({value: null}),
                truck: control({value: ''}),
                district: control({value: ''}),
                transporter: control({value: ''}),
                status: control({value: this.props.readonly ? 'completed' : null, readonly: this.props.readonly}),
                limit: control({value: 10}),
                offset: control({value: 0})
            }
        };
        this.searchRequest = new Subject<NegotiationSearchCriteria>();
    }

    async componentDidMount() {
        this.subscription.add(
            this.searchRequest.pipe(
                debounceTime(350),
                switchMap(search => getNegotiations(search))
            ).subscribe(negotiationList => {
                this.setState({negotiationList})
            })
        );

        const negotiationList = await getNegotiations(this.getSearchCriteria())
        this.setState({negotiationList})
    }

    componentWillUnmount() {
        this.subscription.unsubscribe()
    }

    getSearchCriteria(): NegotiationSearchCriteria {
        const {search} = this.state;
        return {
            transportationDate: search.transportationDate.value,
            truck: search.truck.value,
            district: search.district.value,
            transporter: search.transporter.value,
            status: search.status.value,
            limit: search.limit.value,
            offset: search.offset.value
        };
    }

    render() {
        const {onOpenNegotiation, readonly} = this.props;
        const {negotiationList, search, statusOptions} = this.state;

        return (
            <PageContainer>
                <PagePanel>
                    <h1>Маршрутные журналы</h1>
                </PagePanel>
                <Surface>
                    <Table>
                        <TableHeader>
                            <Column>Дата вывоза</Column>
                            <Column>Номер ТС</Column>
                            <Column>Район</Column>
                            <Column>Наименование перевозчика</Column>
                            <Column>Статус</Column>
                        </TableHeader>
                        <TableHeader>
                            <Column>
                                <DateInput control={search.transportationDate}/>
                            </Column>
                            <Column>
                                <TextInput control={search.truck} icon="search"/>
                            </Column>
                            <Column>
                                <TextInput control={search.district} icon="search"/>
                            </Column>
                            <Column>
                                <TextInput control={search.transporter} icon="search"/>
                            </Column>
                            <Column>
                                <OptionInput
                                    nullable
                                    readonly={readonly}
                                    control={search.status}
                                    options={statusOptions}/>
                            </Column>
                        </TableHeader>
                        <TableBody>
                            {negotiationList.items.map(negotiation => (
                                <Row key={negotiation.id} onClick={() => onOpenNegotiation(negotiation)}>
                                    <Cell>{formatDate(negotiation.transportationDate)}</Cell>
                                    <Cell>{negotiation.truck}</Cell>
                                    <Cell>{negotiation.district}</Cell>
                                    <Cell>{negotiation.transporter}</Cell>
                                    <Cell><NegotiationStatus status={negotiation.status}/></Cell>
                                </Row>
                            ))}
                        </TableBody>
                    </Table>
                    <TablePaginator
                        count={negotiationList.items.length}
                        total={negotiationList.total}
                        forms={['маршрутный журнал', 'маршрутных журнала', 'маршрутных журналов']}
                        limit={search.limit}
                        offset={search.offset}
                    />
                </Surface>
            </PageContainer>
        )
    }
}