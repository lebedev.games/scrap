import React, {Component} from "react";
import {BusinessEntityModel, EntrepreneurModel, Option} from "../../service/model";
import {getGenerator, getParentalGeneratorOptions} from "../../service/handlers";
import styles from "./Generator.module.scss";
import {Surface, SurfaceHeader} from "../kit/Surface";
import {Cell, Column, Row, Table, TableBody, TableHeader} from "../kit/Table";
import {formatPhone} from "../../data/formatters";
import {Property, PropertySection} from "../kit/PropertySection";
import {PropertyList} from "../kit/PropertyList";
import {PageContainer} from "../kit/PageContainer";
import {PagePanel} from "../kit/PagePanel";


interface EntrepreneurGeneratorProps {
    id: string
}

interface EntrepreneurGeneratorState {
    parent: string | null,
    entrepreneur: EntrepreneurModel | null,
    parentalOptions: Option[]
}

export class EntrepreneurGenerator extends Component<EntrepreneurGeneratorProps, EntrepreneurGeneratorState> {
    constructor(props: EntrepreneurGeneratorProps) {
        super(props);
        this.state = {
            parent: null,
            entrepreneur: null,
            parentalOptions: []
        }
    }

    async componentDidMount() {
        const {id} = this.props;
        let generator = await getGenerator(id);
        let parentalOptions = await getParentalGeneratorOptions();
        console.log(id, generator, parentalOptions);
        let entrepreneur = generator.legalEntity as EntrepreneurModel;
        let parent = null;
        if (generator.parent) {
            parent = 'none!';
            for (let option of parentalOptions) {
                if (option.id === generator.parent) {
                    parent = option.name;
                    break;
                }
            }
        }
        this.setState({entrepreneur, parentalOptions, parent})
    }

    render() {
        const {entrepreneur, parent} = this.state;

        if (!entrepreneur) {
            return <div>Загрузка...</div>
        }

        return (
            <PageContainer>
                <PagePanel>
                    <h1>
                        ИП {entrepreneur.lastName} {entrepreneur.firstName} {entrepreneur.patronymic}
                    </h1>
                </PagePanel>
                {parent && <p className={styles.parent}>Головная организация — {parent}</p>}
                <Surface>
                    <SurfaceHeader>
                        <PropertyList>
                            <dt>ИНН</dt>
                            <dd>{entrepreneur.inn}</dd>
                            <dt>ОГРНИП</dt>
                            <dd>{entrepreneur.ogrnip}</dd>
                        </PropertyList>
                    </SurfaceHeader>
                    <PropertySection>
                        <header>Паспортные данные</header>
                        <Property>
                            <label>Серия и номер</label>
                            —
                        </Property>
                        <Property>
                            <label>Дата выдачи</label>
                            —
                        </Property>
                        <Property>
                            <label>Кем выдан</label>
                            —
                        </Property>
                    </PropertySection>
                    <PropertySection>
                        <header>Банковские реквизиты</header>
                        <Property>
                            <label>Номер расчетного счета</label>
                            {entrepreneur.bankDetails.account}
                        </Property>
                        <Property>
                            <label>Корреспондентский счет</label>
                            {entrepreneur.bankDetails.correspondentAccount}
                        </Property>
                        <Property>
                            <label>БИК</label>
                            {entrepreneur.bankDetails.rcbic}
                        </Property>
                        <Property>
                            <label>Полное название банка</label>
                            {entrepreneur.bankDetails.bankName}
                        </Property>
                    </PropertySection>
                    <PropertySection>
                        <header>Контакты</header>
                        <Property>
                            <label>Телефон</label>
                            {entrepreneur.phone}
                        </Property>
                        <Property>
                            <label>Электронная почта</label>
                            {entrepreneur.email ?? '—'}
                        </Property>
                        <Property>
                            <label>Адрес регистрации</label>
                            {entrepreneur.address}
                        </Property>
                        <Property>
                            <label>Адрес фактический</label>
                            {entrepreneur.actualAddress}
                        </Property>
                    </PropertySection>
                </Surface>
            </PageContainer>
        )
    }
}


interface BusinessEntityGeneratorProps {
    id: string
}

interface BusinessEntityGeneratorState {
    businessEntity: BusinessEntityModel | null,
    parentalOptions: Option[]
}

export class BusinessEntityGenerator extends Component<BusinessEntityGeneratorProps, BusinessEntityGeneratorState> {
    constructor(props: BusinessEntityGeneratorProps) {
        super(props);
        this.state = {
            businessEntity: null,
            parentalOptions: []
        }
    }

    async componentDidMount() {
        const {id} = this.props;
        let generator = await getGenerator(id);
        let parentalOptions = await getParentalGeneratorOptions();
        let businessEntity = generator.legalEntity as BusinessEntityModel;
        this.setState({businessEntity, parentalOptions})
    }

    render() {
        const {businessEntity} = this.state;

        if (!businessEntity) {
            return <div>Загрузка...</div>
        }

        return (
            <PageContainer>
                <PagePanel>
                    <h1>{businessEntity.shortName}</h1>
                </PagePanel>
                <p className={styles.parent}>{businessEntity.fullName}</p>
                <Surface>
                    <SurfaceHeader>
                        <PropertyList>
                            <dt>ИНН</dt>
                            <dd>{businessEntity.inn}</dd>
                            <dt>КПП</dt>
                            <dd>{businessEntity.kpp}</dd>
                            <dt>ОГРН</dt>
                            <dd>{businessEntity.ogrn}</dd>
                        </PropertyList>
                    </SurfaceHeader>
                    <PropertySection>
                        <header>Банковские реквизиты</header>
                        <Property>
                            <label>Номер расчетного счета</label>
                            {businessEntity.bankDetails.account}
                        </Property>
                        <Property>
                            <label>Корреспондентский счет</label>
                            {businessEntity.bankDetails.correspondentAccount}
                        </Property>
                        <Property>
                            <label>БИК</label>
                            {businessEntity.bankDetails.rcbic}
                        </Property>
                        <Property>
                            <label>Полное название банка</label>
                            {businessEntity.bankDetails.bankName}
                        </Property>
                    </PropertySection>
                    <PropertySection>
                        <header>Контакты</header>
                        <Property>
                            <label>Телефон</label>
                            {businessEntity.phone}
                        </Property>
                        <Property>
                            <label>Электронная почта</label>
                            {businessEntity.email ?? '—'}
                        </Property>
                        <Property>
                            <label>Адрес регистрации</label>
                            {businessEntity.address}
                        </Property>
                        <Property>
                            <label>Адрес почтовый</label>
                            {businessEntity.postalAddress}
                        </Property>
                        <Property>
                            <label>Адрес фактический</label>
                            {businessEntity.actualAddress}
                        </Property>
                    </PropertySection>
                    <PropertySection>
                        <header>Отвественные лица</header>
                        <Table width={500}>
                            <TableHeader>
                                <Column>ФИО</Column>
                                <Column>Телефон</Column>
                            </TableHeader>
                            <TableBody>
                                {businessEntity.responsiblePersons.map(person => (
                                    <Row key={person.fullName}>
                                        <Cell>{person.fullName}</Cell>
                                        <Cell>{formatPhone(person.phone)}</Cell>
                                    </Row>
                                ))}
                            </TableBody>
                        </Table>
                    </PropertySection>
                </Surface>
            </PageContainer>
        )
    }
}