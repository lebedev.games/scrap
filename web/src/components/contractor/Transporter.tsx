import React, {Component} from "react";
import {BusinessEntityModel, EntrepreneurModel, Option, TransporterModel} from "../../service/model";
import {getTransporter, getParentalTransporterOptions} from "../../service/handlers";
import styles from "./Transporter.module.scss";
import {Surface, SurfaceHeader} from "../kit/Surface";
import {Cell, Column, Row, Table, TableBody, TableHeader} from "../kit/Table";
import {formatPhone} from "../../data/formatters";
import {Property, PropertySection} from "../kit/PropertySection";
import {PropertyList} from "../kit/PropertyList";
import {PageContainer} from "../kit/PageContainer";
import {PagePanel} from "../kit/PagePanel";


interface EntrepreneurTransporterProps {
    id: string
}

interface EntrepreneurTransporterState {
    parent: string | null,
    entrepreneur: EntrepreneurModel | null,
    transporter: TransporterModel | null,
    truckFleetTypeOptions: Option[],
    parentalOptions: Option[],
}

export class EntrepreneurTransporter extends Component<EntrepreneurTransporterProps, EntrepreneurTransporterState> {
    constructor(props: EntrepreneurTransporterProps) {
        super(props);
        this.state = {
            parent: null,
            entrepreneur: null,
            transporter: null,
            parentalOptions: [],
            truckFleetTypeOptions: [
                {id: '1964b722-2ce3-4a67-a948-26ebd2d0171b', name: 'Собственный'},
                {id: 'd11a6833-cb25-438e-8eab-05f15329491f', name: 'Арендованный'}
            ]
        }
    }

    async componentDidMount() {
        const {id} = this.props;
        let transporter = await getTransporter(id);
        let parentalOptions = await getParentalTransporterOptions();
        let entrepreneur = transporter.legalEntity as EntrepreneurModel;
        let parent = null;
        if (transporter.parent) {
            parent = 'none!';
            for (let option of parentalOptions) {
                if (option.id === transporter.parent) {
                    parent = option.name;
                    break;
                }
            }
        }
        this.setState({entrepreneur, parentalOptions, parent, transporter})
    }

    render() {
        const {entrepreneur, parent, transporter, truckFleetTypeOptions} = this.state;

        if (!entrepreneur || !transporter) {
            return <div>Загрузка...</div>
        }

        const nameOf = (value: string, options: Option[]) => {
            for (let option of options) {
                if (option.id === value) {
                    return option.name;
                }
            }
            return value;
        }

        return (
            <PageContainer>
                <PagePanel>
                    <h1>
                        ИП {entrepreneur.lastName} {entrepreneur.firstName} {entrepreneur.patronymic}
                    </h1>
                </PagePanel>
                {parent && <p className={styles.parent}>Головная организация — {parent}</p>}
                <Surface>
                    <SurfaceHeader>
                        <PropertyList>
                            <dt>ИНН</dt>
                            <dd>{entrepreneur.inn}</dd>
                            <dt>ОГРНИП</dt>
                            <dd>{entrepreneur.ogrnip}</dd>
                        </PropertyList>
                    </SurfaceHeader>
                    <PropertySection>
                        <header>Паспортные данные</header>
                        <Property>
                            <label>Серия и номер</label>
                            —
                        </Property>
                        <Property>
                            <label>Дата выдачи</label>
                            —
                        </Property>
                        <Property>
                            <label>Кем выдан</label>
                            —
                        </Property>
                    </PropertySection>
                    <PropertySection>
                        <header>Банковские реквизиты</header>
                        <Property>
                            <label>Номер расчетного счета</label>
                            {entrepreneur.bankDetails.account}
                        </Property>
                        <Property>
                            <label>Корреспондентский счет</label>
                            {entrepreneur.bankDetails.correspondentAccount}
                        </Property>
                        <Property>
                            <label>БИК</label>
                            {entrepreneur.bankDetails.rcbic}
                        </Property>
                        <Property>
                            <label>Полное название банка</label>
                            {entrepreneur.bankDetails.bankName}
                        </Property>
                    </PropertySection>
                    <PropertySection>
                        <header>Контакты</header>
                        <Property>
                            <label>Телефон</label>
                            {entrepreneur.phone}
                        </Property>
                        <Property>
                            <label>Электронная почта</label>
                            {entrepreneur.email ?? '—'}
                        </Property>
                        <Property>
                            <label>Адрес регистрации</label>
                            {entrepreneur.address}
                        </Property>
                        <Property>
                            <label>Адрес фактический</label>
                            {entrepreneur.actualAddress}
                        </Property>
                    </PropertySection>
                    <PropertySection>
                        <header>Дополнительно</header>
                        <Property>
                            <label>Парки</label>
                            {transporter.truckFleetTypes.map(type => nameOf(type, truckFleetTypeOptions)).join(', ')}
                        </Property>
                        <Property>
                            <label>Примечание</label>
                            {transporter.note}
                        </Property>
                    </PropertySection>
                </Surface>
            </PageContainer>
        )
    }
}


interface BusinessEntityTransporterProps {
    id: string
}

interface BusinessEntityTransporterState {
    businessEntity: BusinessEntityModel | null,
    parentalOptions: Option[],
    transporter: TransporterModel | null,
    truckFleetTypeOptions: Option[],
}

export class BusinessEntityTransporter extends Component<BusinessEntityTransporterProps, BusinessEntityTransporterState> {
    constructor(props: BusinessEntityTransporterProps) {
        super(props);
        this.state = {
            businessEntity: null,
            parentalOptions: [],
            transporter: null,
            truckFleetTypeOptions: [
                {id: '1964b722-2ce3-4a67-a948-26ebd2d0171b', name: 'Собственный'},
                {id: 'd11a6833-cb25-438e-8eab-05f15329491f', name: 'Арендованный'}
            ]
        }
    }

    async componentDidMount() {
        const {id} = this.props;
        let transporter = await getTransporter(id);
        let parentalOptions = await getParentalTransporterOptions();
        let businessEntity = transporter.legalEntity as BusinessEntityModel;
        this.setState({businessEntity, parentalOptions, transporter})
    }

    render() {
        const {businessEntity, truckFleetTypeOptions, transporter} = this.state;

        if (!businessEntity || !transporter) {
            return <div>Загрузка...</div>
        }

        const nameOf = (value: string, options: Option[]) => {
            for (let option of options) {
                if (option.id === value) {
                    return option.name;
                }
            }
            return value;
        }

        return (
            <PageContainer>
                <PagePanel>
                    <h1>{businessEntity.shortName}</h1>
                </PagePanel>
                <p className={styles.parent}>{businessEntity.fullName}</p>
                <Surface>
                    <SurfaceHeader>
                        <PropertyList>
                            <dt>ИНН</dt>
                            <dd>{businessEntity.inn}</dd>
                            <dt>КПП</dt>
                            <dd>{businessEntity.kpp}</dd>
                            <dt>ОГРН</dt>
                            <dd>{businessEntity.ogrn}</dd>
                        </PropertyList>
                    </SurfaceHeader>
                    <PropertySection>
                        <header>Банковские реквизиты</header>
                        <Property>
                            <label>Номер расчетного счета</label>
                            {businessEntity.bankDetails.account}
                        </Property>
                        <Property>
                            <label>Корреспондентский счет</label>
                            {businessEntity.bankDetails.correspondentAccount}
                        </Property>
                        <Property>
                            <label>БИК</label>
                            {businessEntity.bankDetails.rcbic}
                        </Property>
                        <Property>
                            <label>Полное название банка</label>
                            {businessEntity.bankDetails.bankName}
                        </Property>
                    </PropertySection>
                    <PropertySection>
                        <header>Контакты</header>
                        <Property>
                            <label>Телефон</label>
                            {businessEntity.phone}
                        </Property>
                        <Property>
                            <label>Электронная почта</label>
                            {businessEntity.email ?? '—'}
                        </Property>
                        <Property>
                            <label>Адрес регистрации</label>
                            {businessEntity.address}
                        </Property>
                        <Property>
                            <label>Адрес почтовый</label>
                            {businessEntity.postalAddress}
                        </Property>
                        <Property>
                            <label>Адрес фактический</label>
                            {businessEntity.actualAddress}
                        </Property>
                    </PropertySection>
                    <PropertySection>
                        <header>Отвественные лица</header>
                        <Table width={500}>
                            <TableHeader>
                                <Column>ФИО</Column>
                                <Column>Телефон</Column>
                            </TableHeader>
                            <TableBody>
                                {businessEntity.responsiblePersons.map(person => (
                                    <Row key={person.fullName}>
                                        <Cell>{person.fullName}</Cell>
                                        <Cell>{formatPhone(person.phone)}</Cell>
                                    </Row>
                                ))}
                            </TableBody>
                        </Table>
                    </PropertySection>
                    <PropertySection>
                        <header>Дополнительно</header>
                        <Property>
                            <label>Парки</label>
                            {transporter.truckFleetTypes.map(type => nameOf(type, truckFleetTypeOptions)).join(', ')}
                        </Property>
                        <Property>
                            <label>Примечание</label>
                            {transporter.note}
                        </Property>
                    </PropertySection>
                </Surface>
            </PageContainer>
        )
    }
}