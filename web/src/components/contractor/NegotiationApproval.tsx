import React, {Component} from "react";
import {
    CollectionPhotoIssue,
    CollectionStartTimeIssue,
    Issue,
    NegotiationModel,
    Option,
    TransportationForm,
    WasteVolumeIssue
} from "../../service/model";
import {
    completeNegotiation,
    declareNegotiationIssues,
    getContainerTypeOptions,
    getGenerationPointOptions,
    getNegotiation,
    getTransportation,
    getTransporterTruckOptions
} from "../../service/handlers";
import {choosePluralForm, formatDate, formatTime} from "../../data/formatters";
import {Surface, SurfaceFooter} from "../kit/Surface";
import {Cell, Column, PhotosCell, Row, Table, TableBody, TableHeader} from "../kit/Table";
import {PageContainer} from "../kit/PageContainer";
import {PagePanel} from "../kit/PagePanel";
import {Field} from "../kit/Field";
import {TextInput} from "../kit/TextInput";
import styles from "./NegotiationApproval.module.scss";
import {Icon} from "../kit/Icon";
import * as uuid from "uuid";
import {Button} from "../kit/Button";
import {PhotoViewer} from "../kit/PhotoViewer";

interface NegotiationApprovalProps {
    id: string,
    onCompleteNegotiation: (id: string) => void;
    onDeclareNegotiationIssues: (id: string) => void;
    onCancel: () => void;
}


interface IssueForm {
    issue: Issue | null
}


interface NegotiationApprovalState {
    negotiation: NegotiationModel | null,
    transportation: TransportationForm | null,
    issueForm: IssueForm,
    issues: Issue[],
    containerTypeOptions: Option[]
    truckOptions: Option[],
    generationPointOptions: Option[]
}

export class NegotiationApproval extends Component<NegotiationApprovalProps, NegotiationApprovalState> {

    photoViewer = React.createRef<PhotoViewer>();

    constructor(props: NegotiationApprovalProps) {
        super(props);
        this.state = {
            negotiation: null,
            transportation: null,
            issueForm: {
                issue: null
            },
            issues: [],
            containerTypeOptions: [],
            truckOptions: [],
            generationPointOptions: []
        }
    }

    async componentDidMount() {
        const {id} = this.props;
        let negotiation = await getNegotiation(id);
        this.setState({negotiation})
        let transportation = await getTransportation(negotiation.transportation);
        this.setState({transportation})
        this.defineTransportationIssues(transportation);
        let containerTypeOptions = await getContainerTypeOptions()
        this.setState({containerTypeOptions})
        let truckOptions = await getTransporterTruckOptions()
        this.setState({truckOptions})
        let generationPointOptions = await getGenerationPointOptions()
        this.setState({generationPointOptions})
    }

    defineTransportationIssues(transportation: TransportationForm) {
        let issues = [];
        for (let destination of transportation.destinations) {
            issues.push(new CollectionStartTimeIssue({
                id: uuid.v4(),
                destination: destination.id,
                comment: '',
                declared: false
            }))
            for (let collection of destination.collections) {
                issues.push(new WasteVolumeIssue({
                    id: uuid.v4(),
                    destination: destination.id,
                    collection: collection.id,
                    comment: '',
                    declared: false
                }));
                issues.push(new CollectionPhotoIssue({
                    id: uuid.v4(),
                    destination: destination.id,
                    collection: collection.id,
                    comment: '',
                    declared: false
                }))
            }
        }
        this.setState({issues})
    }

    toggleIssue(issue: Issue) {
        let {issueForm} = this.state;
        if (issueForm.issue?.id !== issue.id) {
            issueForm.issue = issue;
        } else {
            issueForm.issue = null;
        }
        this.setState({issueForm})
    }

    declareIssue(id: string) {
        const {issues} = this.state;
        for (let issue of issues) {
            if (issue.id === id) {
                issue.declared = true;
                this.setState({issues})
                return;
            }
        }
    }

    changeIssueComment(id: string, comment: string) {
        const {issues} = this.state;
        for (let issue of issues) {
            if (issue.id === id) {
                issue.comment = comment;
                this.setState({issues})
                return;
            }
        }
    }

    removeIssue(id: string) {
        const {issues} = this.state;
        for (let issue of issues) {
            if (issue.id === id) {
                issue.declared = false;
                issue.comment = '';
                this.setState({issues})
                return;
            }
        }
    }

    closeIssueForm() {
        const {issueForm} = this.state;
        issueForm.issue = null;
        this.setState({issueForm})
    }

    countIssues(): number {
        const {issues} = this.state;
        let count = 0;
        for (let issue of issues) {
            if (issue.declared) {
                count += 1;
            }
        }
        return count;
    }

    async completeNegotiation() {
        const {onCompleteNegotiation, id} = this.props;
        await completeNegotiation(id);
        onCompleteNegotiation(id);
    }

    async declareNegotiationIssues() {
        const {id, onDeclareNegotiationIssues} = this.props;
        const {issues} = this.state;
        await declareNegotiationIssues(id, {issues: issues.filter(issue => issue.declared)});
        onDeclareNegotiationIssues(id);
    }

    render() {
        const {
            transportation,
            negotiation,
            issueForm,
            issues,
            containerTypeOptions,
            truckOptions,
            generationPointOptions
        } = this.state;
        const {onCancel} = this.props;

        if (!transportation || !negotiation) {
            return <div>Загрузка...</div>
        }

        let issuesCount = this.countIssues();

        const nameOf = (value: string, options: Option[]) => {
            for (let option of options) {
                if (option.id === value) {
                    return option.name;
                }
            }
            return value;
        }

        const rows = [];

        let destinationNumber = 0;

        const renderIssueClaim = (issue: Issue) => {
            let className = styles.issueClaim;
            if (issueForm.issue?.id === issue.id) {
                className += ' ' + styles.active;
            } else if (issue.declared) {
                className += ' ' + styles.declared;
            }
            return (
                <div className={className} onClick={() => this.toggleIssue(issue)}>
                    <i><Icon name={"claim"}/></i>
                </div>
            )
        }

        const wasteVolumeIssues: WasteVolumeIssue[] = issues.filter(issue => issue instanceof WasteVolumeIssue) as any;
        const renderWasteVolumeIssue = (destination: string, collection: string) => {
            for (let issue of wasteVolumeIssues) {
                if (issue.destination === destination && issue.collection === collection) {
                    return renderIssueClaim(issue);
                }
            }
            return undefined
        }

        const collection_photo_issues: CollectionPhotoIssue[] = issues.filter(issue => issue instanceof CollectionPhotoIssue) as any;
        const renderCollectionPhotoIssue = (destination: string, collection: string) => {
            for (let issue of collection_photo_issues) {
                if (issue.destination === destination && issue.collection === collection) {
                    return renderIssueClaim(issue);
                }
            }
            return undefined
        }

        const collection_start_time_issues: CollectionStartTimeIssue[] = issues.filter(issue => issue instanceof CollectionStartTimeIssue) as any;
        const renderCollectionStartTimeIssue = (destination: string) => {
            for (let issue of collection_start_time_issues) {
                if (issue.destination === destination) {
                    return renderIssueClaim(issue);
                }
            }
            return undefined
        }

        for (let destination of transportation.destinations) {
            destinationNumber += 1;
            let row = (
                <Row key={destination.id}>
                    <Cell>
                        {destinationNumber}
                    </Cell>
                    <Cell>
                        {formatTime(destination.collectionStartTime)}
                        {renderCollectionStartTimeIssue(destination.id)}
                    </Cell>
                    <Cell>
                        {nameOf(destination.wasteGenerationPoint, generationPointOptions)}
                    </Cell>
                    <Cell merge columns={3}>
                        <Table>
                            <TableBody>
                                {destination.collections.map(collection => (
                                    <Row key={collection.id}>
                                        <Cell width={160}>
                                            {nameOf(collection.containerType, containerTypeOptions)}
                                        </Cell>
                                        <Cell width={120}>
                                            {collection.wasteVolumeM3}
                                            {renderWasteVolumeIssue(destination.id, collection.id)}
                                        </Cell>
                                        <PhotosCell width={308} photos={collection.photos} onPreview={
                                            (photo) => {
                                                this.photoViewer.current?.showPhotos(photo, collection.photos)
                                            }
                                        }>
                                            {renderCollectionPhotoIssue(destination.id, collection.id)}
                                        </PhotosCell>
                                    </Row>
                                ))}
                            </TableBody>
                        </Table>
                    </Cell>
                </Row>
            )
            rows.push(row);

            if (issueForm.issue && issueForm.issue.destination === destination.id) {
                let issue = issueForm.issue;
                let title = "Замечание";
                if (issue instanceof WasteVolumeIssue) {
                    title = "Замечание к вывезенному объему"
                }
                if (issue instanceof CollectionStartTimeIssue) {
                    title = "Замечание ко времени вывоза"
                }
                if (issue instanceof CollectionPhotoIssue) {
                    title = "Замечание к фотографиям"
                }
                let issueFormRow = (
                    <Row key={issue.id}>
                        <Cell merge columns={6}>
                            <section className={styles.issueForm}>
                                <Field width={520}>
                                    <label>{title}</label>
                                    <TextInput
                                        rows={3}
                                        type="multiline"
                                        value={issue.comment}
                                        onChange={comment => this.changeIssueComment(issue.id, comment)}
                                    />
                                </Field>
                                <div className={styles.issueButtons}>
                                    <button className={styles.issueButton} onClick={() => {
                                        this.declareIssue(issue.id);
                                        this.closeIssueForm();
                                    }}>
                                        <i><Icon name="check"/></i>
                                        Сохранить
                                    </button>
                                    {issue.declared ?
                                        <button className={styles.issueButton} onClick={() => {
                                            this.removeIssue(issue.id);
                                            this.closeIssueForm();
                                        }}>
                                            <i><Icon name="erase"/></i>
                                            Удалить
                                        </button>
                                        :
                                        <button className={styles.issueButton} onClick={() => this.closeIssueForm()}>
                                            <i><Icon name="erase"/></i>
                                            Отменить
                                        </button>
                                    }
                                </div>
                            </section>
                        </Cell>
                    </Row>
                )
                rows.push(issueFormRow)
            }
        }

        return (
            <PageContainer>
                <PagePanel>
                    <h1>
                        Согласование маршрутного
                        журнала {nameOf(transportation.truck, truckOptions)} на {formatDate(transportation.executionDate)}
                    </h1>
                </PagePanel>
                <Surface>
                    <div className={styles.note}>
                        Выберите ячейку журнала, к которой хотите оставить замечание
                        <i><Icon name="claim"/></i>
                    </div>
                    <Table>
                        <TableHeader>
                            <Column>#</Column>
                            <Column width={80}>Время</Column>
                            <Column>Адрес КП</Column>
                            <Column width={160}>Тип контейнера</Column>
                            <Column width={120}>Объем, м3</Column>
                            <Column width={308}>Фотографии</Column>
                        </TableHeader>
                        <TableBody>
                            {rows}
                        </TableBody>
                    </Table>
                    <div style={{paddingTop: '32px'}}/>
                    <SurfaceFooter>
                        {issuesCount > 0 ?
                            <Button type="primary" onClick={() => this.declareNegotiationIssues()}>
                                Отправить {issuesCount} {choosePluralForm(['замечение', 'замечания', 'замечаний'], issuesCount)}
                            </Button>
                            :
                            <Button type="primary" onClick={() => this.completeNegotiation()}>
                                Утвердить
                            </Button>
                        }
                        <Button type="outline" onClick={onCancel}>Закрыть</Button>
                    </SurfaceFooter>
                </Surface>
                <div style={{paddingTop: '72px'}}/>
                <PhotoViewer ref={this.photoViewer}/>
            </PageContainer>
        );
    }

}