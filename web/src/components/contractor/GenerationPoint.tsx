import React, {Component} from "react";
import {PagePanel} from "../kit/PagePanel";
import {PageContainer} from "../kit/PageContainer";
import styles from "./GenerationPoint.module.scss";
import {Surface} from "../kit/Surface";
import {Property, PropertySection} from "../kit/PropertySection";
import {Cell, Column, Row, Table, TableBody, TableHeader} from "../kit/Table";
import {
    getAccrualMethodOptions,
    getContainerTypeOptions,
    getCoveringTypeOptions,
    getFenceMaterialOptions,
    getGenerationPoint,
    getGeneratorOptions,
    getMeasurementUnitOptions,
    getOperationalStatusOptions,
    getOrganizationLegalTypeOptions,
    getTransporterOptions,
    getWasteTypeOptions
} from "../../service/handlers";
import {GenerationPointModel, Option} from "../../service/model";

interface GenerationPointProps {
    id: string
}

interface GenerationPointState {
    generationPoint: GenerationPointModel | null
    containerTypeOptions: Option[]
    accrualMethodOptions: Option[]
    measurementUnitOptions: Option[]
    coveringTypeOptions: Option[]
    fenceMaterialOptions: Option[]
    operationalStatusOptions: Option[]
    organizationLegalTypeOptions: Option[]
    wasteTypeOptions: Option[]
    transporterOptions: Option[]
    generatorOptions: Option[]
}

export class GenerationPoint extends Component<GenerationPointProps, GenerationPointState> {

    constructor(props: GenerationPointProps) {
        super(props);
        this.state = {
            containerTypeOptions: [],
            accrualMethodOptions: [],
            measurementUnitOptions: [],
            coveringTypeOptions: [],
            fenceMaterialOptions: [],
            operationalStatusOptions: [],
            organizationLegalTypeOptions: [],
            wasteTypeOptions: [],
            generatorOptions: [],
            transporterOptions: [],
            generationPoint: null,
        }
    }

    async componentDidMount() {
        const {id} = this.props;
        let generationPoint = await getGenerationPoint(id);
        this.setState({generationPoint})
        let accrualMethodOptions = await getAccrualMethodOptions()
        this.setState({accrualMethodOptions})
        let containerTypeOptions = await getContainerTypeOptions()
        this.setState({containerTypeOptions})
        let coveringTypeOptions = await getCoveringTypeOptions()
        this.setState({coveringTypeOptions})
        let fenceMaterialOptions = await getFenceMaterialOptions()
        this.setState({fenceMaterialOptions})
        let measurementUnitOptions = await getMeasurementUnitOptions()
        this.setState({measurementUnitOptions})
        let operationalStatusOptions = await getOperationalStatusOptions()
        this.setState({operationalStatusOptions})
        let organizationLegalTypeOptions = await getOrganizationLegalTypeOptions()
        this.setState({organizationLegalTypeOptions})
        let wasteTypeOptions = await getWasteTypeOptions()
        this.setState({wasteTypeOptions})
        let transporterOptions = await getTransporterOptions()
        this.setState({transporterOptions})
        let generatorOptions = await getGeneratorOptions()
        this.setState({generatorOptions})
    }

    render() {
        const {
            generationPoint,
            containerTypeOptions,
            accrualMethodOptions,
            measurementUnitOptions,
            coveringTypeOptions,
            fenceMaterialOptions,
            operationalStatusOptions,
            organizationLegalTypeOptions,
            wasteTypeOptions,
            generatorOptions,
            transporterOptions
        } = this.state;

        if (!generationPoint) {
            return <div>Загрузка...</div>
        }

        const nameOf = (value: string, options: Option[]) => {
            for (let option of options) {
                if (option.id === value) {
                    return option.name;
                }
            }
            return value;
        }

        const containers: any = {};
        for (let container of generationPoint.containers) {
            if (container.containerType in containers) {
                containers[container.containerType] += 1;
            } else {
                containers[container.containerType] = 1;
            }
        }

        return (
            <PageContainer>
                <PagePanel>
                    <h1>{generationPoint.internalSystemNumber}</h1>
                </PagePanel>
                <p className={styles.parent}>
                    Мусорообразователь — {nameOf(generationPoint.generator, generatorOptions)}
                </p>
                <Surface>
                    <PropertySection>
                        <header>Характеристики площадки</header>
                        <Property>
                            <label>Номер в реестре мест</label>
                            {generationPoint.externalRegistryNumber}
                        </Property>
                        <Property>
                            <label>Площадь, м2</label>
                            {generationPoint.areaM2}
                        </Property>
                        <Property>
                            <label>Вид площадки</label>
                            {nameOf(generationPoint.organizationLegalType, organizationLegalTypeOptions)}
                        </Property>
                        <Property>
                            <label>Тип покрытия</label>
                            {nameOf(generationPoint.coveringType, coveringTypeOptions)}
                        </Property>
                        <Property>
                            <label>Материал ограждения</label>
                            {nameOf(generationPoint.fenceMaterial, fenceMaterialOptions)}
                        </Property>
                        <Property>
                            <label>Описание</label>
                            {generationPoint.description}
                        </Property>
                    </PropertySection>
                    <PropertySection>
                        <header>Расположение</header>
                        <Property>
                            <label>Координаты</label>
                            {generationPoint.coordinates}
                        </Property>
                        <Property>
                            <label>Адрес</label>
                            {generationPoint.fullAddress}
                        </Property>
                    </PropertySection>
                    <PropertySection>
                        <header>Дополнительно</header>
                        <Property>
                            <label>Способ начисления</label>
                            {nameOf(generationPoint.accrualMethod, accrualMethodOptions)}
                        </Property>
                        <Property>
                            <label>Расчетная единица измерения</label>
                            {nameOf(generationPoint.measurementUnit, measurementUnitOptions)}
                        </Property>
                    </PropertySection>
                    <PropertySection>
                        <header>Контейнеры</header>
                        <Table width={500}>
                            <TableHeader>
                                <Column>Тип контейнера</Column>
                                <Column>Количество</Column>
                            </TableHeader>
                            <TableBody>
                                {Object.keys(containers).map(containerType => (
                                    <Row key={containerType}>
                                        <Cell>{nameOf(containerType, containerTypeOptions)}</Cell>
                                        <Cell>{containers[containerType]}</Cell>
                                    </Row>
                                ))}
                            </TableBody>
                        </Table>
                    </PropertySection>
                </Surface>
                <div style={{paddingTop: '72px'}}/>
            </PageContainer>
        )
    }
}