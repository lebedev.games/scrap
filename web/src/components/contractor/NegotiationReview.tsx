import React, {Component} from "react";
import {NegotiationModel, Option, TransportationForm} from "../../service/model";
import {
    getContainerTypeOptions,
    getGenerationPointOptions,
    getNegotiation,
    getTransportationByVersion,
    getTransporterTruckOptions
} from "../../service/handlers";
import {formatDate, formatTime} from "../../data/formatters";
import {Surface} from "../kit/Surface";
import {Cell, Column, PhotosCell, Row, Table, TableBody, TableHeader} from "../kit/Table";
import {PageContainer} from "../kit/PageContainer";
import {PagePanel} from "../kit/PagePanel";
import styles from "./NegotiationReview.module.scss";
import {PhotoViewer} from "../kit/PhotoViewer";

interface NegotiationReviewProps {
    id: string,
    onCancel: () => void;
}

interface NegotiationReviewState {
    negotiation: NegotiationModel | null,
    transportation: TransportationForm | null,
    containerTypeOptions: Option[]
    truckOptions: Option[],
    generationPointOptions: Option[],
    currentPoint: number
}

export class NegotiationReview extends Component<NegotiationReviewProps, NegotiationReviewState> {
    photoViewer = React.createRef<PhotoViewer>();
    cache: any

    constructor(props: NegotiationReviewProps) {
        super(props);
        this.state = {
            negotiation: null,
            transportation: null,
            containerTypeOptions: [],
            truckOptions: [],
            generationPointOptions: [],
            currentPoint: 0
        }
        this.cache = {}
    }

    async componentDidMount() {
        const {id} = this.props;
        let negotiation = await getNegotiation(id);
        this.setState({negotiation})

        await this.changePoint(negotiation.timeline.length - 1, negotiation);

        let containerTypeOptions = await getContainerTypeOptions()
        this.setState({containerTypeOptions})
        let truckOptions = await getTransporterTruckOptions()
        this.setState({truckOptions})
        let generationPointOptions = await getGenerationPointOptions()
        this.setState({generationPointOptions})
    }

    async changePoint(currentPoint: number, negotiation: NegotiationModel) {
        let transportationId = negotiation.transportation;
        let transportationVersion = negotiation.timeline[currentPoint].transportationVersion;

        let key = transportationId + transportationVersion;

        let transportation;
        if (key in this.cache) {
            transportation = this.cache[key];
        } else {
            transportation = await getTransportationByVersion(
                transportationId,
                transportationVersion
            );
            this.cache[key] = transportation;
        }
        
        this.setState({transportation, currentPoint})
    }

    render() {
        const {
            transportation,
            negotiation,
            containerTypeOptions,
            truckOptions,
            generationPointOptions,
            currentPoint
        } = this.state;
        const {onCancel} = this.props;

        if (!transportation || !negotiation) {
            return <div>Загрузка...</div>
        }


        const nameOf = (value: string, options: Option[]) => {
            for (let option of options) {
                if (option.id === value) {
                    return option.name;
                }
            }
            return value;
        }

        const rows = [];

        let destinationNumber = 0;


        for (let destination of transportation.destinations) {
            destinationNumber += 1;
            let row = (
                <Row key={destination.id}>
                    <Cell>
                        {destinationNumber}
                    </Cell>
                    <Cell>
                        {formatTime(destination.collectionStartTime)}
                    </Cell>
                    <Cell>
                        {nameOf(destination.wasteGenerationPoint, generationPointOptions)}
                    </Cell>
                    <Cell merge columns={3}>
                        <Table>
                            <TableBody>
                                {destination.collections.map(collection => (
                                    <Row key={collection.id}>
                                        <Cell width={160}>
                                            {nameOf(collection.containerType, containerTypeOptions)}
                                        </Cell>
                                        <Cell width={120}>
                                            {collection.wasteVolumeM3}
                                        </Cell>
                                        <PhotosCell width={280} photos={collection.photos} onPreview={
                                            (photo) => {
                                                this.photoViewer.current?.showPhotos(photo, collection.photos)
                                            }
                                        }>
                                        </PhotosCell>
                                    </Row>
                                ))}
                            </TableBody>
                        </Table>
                    </Cell>
                </Row>
            )
            rows.push(row);


        }

        const getStatusName = (status: string) => {
            if (status === 'pending_approval') {
                return 'На согласовании'
            }
            if (status === 'pending_resolution') {
                return 'Требует доработки'
            }
            if (status === 'completed') {
                return 'Согласован'
            }
        }

        const getStatusColor = (active: boolean, status: string) => {
            if (active) {
                if (status === 'pending_approval') {
                    return styles.pendingApproval;
                }
                if (status === 'pending_resolution') {
                    return styles.pendingResolution;
                }
                if (status === 'completed') {
                    return styles.completed;
                }
            }
            return '';
        }

        return (
            <PageContainer>
                <PagePanel>
                    <h1>
                        Обзор маршрутного
                        журнала {nameOf(transportation.truck, truckOptions)} на {formatDate(transportation.executionDate)}
                    </h1>
                </PagePanel>
                <Surface>
                    <h2 className={styles.timelineHeader}>История изменений</h2>
                    <section className={styles.timeline}>
                        <div className={styles.line}/>
                        <div className={styles.points}>
                            {negotiation.timeline.map((point, index) =>
                                <div
                                    key={point.stageId}
                                    onClick={() => this.changePoint(index, negotiation)}
                                    className={styles.timelinePoint + ' ' + getStatusColor(currentPoint === index, point.status)}
                                >
                                    <div className={styles.tick}/>
                                    <label>{getStatusName(point.status)}</label>
                                    <label>{formatDate(point.timestamp) + ', ' + formatTime(point.timestamp)}</label>
                                </div>
                            )}
                        </div>
                    </section>
                </Surface>
                <div style={{paddingTop: '16px'}}/>
                <Surface>
                    <Table>
                        <TableHeader>
                            <Column>#</Column>
                            <Column width={80}>Время</Column>
                            <Column>Адрес КП</Column>
                            <Column width={160}>Тип контейнера</Column>
                            <Column width={120}>Объем, м3</Column>
                            <Column width={280}>Фотографии</Column>
                        </TableHeader>
                        <TableBody>
                            {rows}
                        </TableBody>
                    </Table>
                </Surface>
                <div style={{paddingTop: '72px'}}/>
                <PhotoViewer ref={this.photoViewer}/>
            </PageContainer>
        );
    }

}