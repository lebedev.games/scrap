import React, {Component} from "react";
import {Surface, SurfaceFooter} from "../kit/Surface";
import {Button, TextButton} from "../kit/Button";
import {TextInput} from "../kit/TextInput";
import {Compound, Field, FieldsSeparator, FormSection} from "../kit/Field";
import {Control, Form} from "../kit/forms";
import {getParentalTransporterOptions, registerTransporter} from "../../service/handlers";
import {LegalEntityType, Option, ResponsiblePerson} from "../../service/model";
import {notEmptyCollection, required, splitted} from "../../data/validators";
import {OptionInput} from "../kit/OptionInput";
import {formatPhone} from "../../data/formatters";
import {parsePhone} from "../../data/parsers";
import {PageContainer} from "../kit/PageContainer";
import {PagePanel} from "../kit/PagePanel";
import {CheckInput} from "../kit/CheckInput";

interface BusinessEntityGrProps {
    onRegistrationComplete: (id: string) => void
    onCancel: () => void
}

interface BusinessEntityGrState {
    parentalOptions: Option[],
    responsiblePersonForm: {
        phone: Control,
        fullName: Control,
    },
    truckFleetTypeOptions: Option[],
    responsiblePersons: ResponsiblePerson[],
    form: {
        parent: Control,
        fkko: Control,
        truckFleetTypes: Control,
        note: Control,
        fullName: Control,
        shortName: Control,
        inn: Control,
        kpp: Control,
        ogrn: Control,
        phone: Control,
        email: Control,
        address: Control,
        postalAddress: Control,
        actualAddress: Control,
        account: Control
        correspondentAccount: Control
        bankName: Control
        rcbic: Control
    },
}


export class BusinessEntityTransporterRegistration extends Component<BusinessEntityGrProps, BusinessEntityGrState> {

    constructor(props: BusinessEntityGrProps) {
        super(props);
        const control = Form.getControlConstructor(this);
        this.state = {
            parentalOptions: [],
            responsiblePersons: [],
            truckFleetTypeOptions: [
                {id: '1964b722-2ce3-4a67-a948-26ebd2d0171b', name: 'Собственный'},
                {id: 'd11a6833-cb25-438e-8eab-05f15329491f', name: 'Арендованный'}
            ],
            responsiblePersonForm: {
                phone: control({
                    value: '+79811648000',
                    getter: formatPhone,
                    setter: parsePhone,
                    validators: [required()]
                }),
                fullName: control({
                    value: 'Тестовый Отвественный',
                    validators: [required()]
                })
            },
            form: {
                parent: control({
                    value: null
                }),
                fkko: control({
                    value: '11111002235',
                    validators: [required()]
                }),
                note: control({
                    value: ''
                }),
                truckFleetTypes: control({
                    value: [],
                    validators: [
                        notEmptyCollection()
                    ]
                }),
                fullName: control({
                    value: 'Открытое акционерное общество «Перевозчикофф»',
                    validators: [required()]
                }),
                shortName: control({
                    value: 'Перевозчикофф',
                    validators: [required()]
                }),
                inn: control({
                    value: '100485636000',
                    validators: [required()]
                }),
                kpp: control({
                    value: '783901000',
                    validators: [required()]
                }),
                ogrn: control({
                    value: '1177847045000',
                    validators: [required()]
                }),
                phone: control({
                    value: '+79811648000',
                    getter: formatPhone,
                    setter: parsePhone,
                    validators: [required()]
                }),
                email: control({
                    value: 'test@mail'
                }),
                address: control({
                    value: '197664, Российская Федерация, г. Барнаул, Александровская ул., дом 7, квартира 192',
                    validators: [required()]
                }),
                postalAddress: control({
                    value: '197664, Российская Федерация, г. Барнаул, Александровская ул., дом 7, квартира 192',
                    validators: [required()]
                }),
                actualAddress: control({
                    value: '197664, Российская Федерация, г. Барнаул, Александровская ул., дом 7, квартира 192',
                    validators: [required()]
                }),
                account: control({
                    value: '40802810832130000000',
                    validators: [required()]
                }),
                correspondentAccount: control({
                    value: '30101810600000000000',
                    validators: [required()]
                }),
                bankName: control({
                    value: 'Публичное акционерное общество Сбербанк России',
                    validators: [required()]
                }),
                rcbic: control({
                    value: '044030000',
                    validators: [required()]
                })
            }
        }
    }

    async componentDidMount() {
        let parentalOptions = await getParentalTransporterOptions();
        this.setState({parentalOptions});
    }

    addResponsiblePerson() {
        const {responsiblePersonForm, responsiblePersons} = this.state;

        Form.validate(responsiblePersonForm);
        if (!Form.isValid(responsiblePersonForm)) {
            return;
        }

        const responsiblePerson = {
            fullName: responsiblePersonForm.fullName.value,
            phone: responsiblePersonForm.phone.value
        };
        responsiblePersons.push(responsiblePerson);
        this.setState({responsiblePersons})

        responsiblePersonForm.phone.setValue('');
        responsiblePersonForm.fullName.setValue('');
    }

    removeResponsiblePerson(person: ResponsiblePerson) {
        const {responsiblePersons} = this.state;
        let index = responsiblePersons.indexOf(person);
        if (index > -1) {
            responsiblePersons.splice(index, 1);
        }
        this.setState({responsiblePersons});
    }

    async registerTransporter() {
        const {form, responsiblePersons} = this.state;
        const {onRegistrationComplete} = this.props;

        Form.validate(form);
        if (!Form.isValid(form)) {
            return;
        }

        const registration = {
            parent: form.parent.value,
            fkko: form.fkko.value,
            truckFleetTypes: form.truckFleetTypes.value,
            note: form.note.value,
            legalEntity: {
                fullName: form.fullName.value,
                shortName: form.shortName.value,
                inn: form.inn.value,
                kpp: form.kpp.value,
                ogrn: form.ogrn.value,
                phone: form.phone.value,
                email: form.email.value,
                address: form.address.value,
                postalAddress: form.postalAddress.value,
                actualAddress: form.actualAddress.value,
                responsiblePersons: responsiblePersons,
                bankDetails: {
                    account: form.account.value,
                    correspondentAccount: form.correspondentAccount.value,
                    bankName: form.bankName.value,
                    rcbic: form.rcbic.value
                },
                type: 'BusinessEntity' as LegalEntityType
            }
        }
        let generator = await registerTransporter(registration);
        onRegistrationComplete(generator.id);
    }

    render() {
        const {onCancel} = this.props;
        const {
            form,
            parentalOptions,
            responsiblePersonForm,
            responsiblePersons,
            truckFleetTypeOptions
        } = this.state;

        return (
            <PageContainer>
                <PagePanel>
                    <h1>Добавление перевозчика (юр. лицо)</h1>
                </PagePanel>
                <Surface>
                    <FormSection title="Основная информация">
                        <Compound>
                            <Field width={160} error={form.shortName.getError()}>
                                <label>Краткое наименование</label>
                                <TextInput control={form.shortName}/>
                            </Field>
                            <Field error={form.fullName.getError()}>
                                <label>Полное наименование</label>
                                <TextInput control={form.fullName}/>
                            </Field>
                        </Compound>
                        <Compound>
                            <Field width={160} error={form.inn.getError()}>
                                <label>ИНН</label>
                                <TextInput control={form.inn}/>
                            </Field>
                            <Field width={160} error={form.kpp.getError()}>
                                <label>КПП</label>
                                <TextInput control={form.kpp}/>
                            </Field>
                            <Field width={160} error={form.ogrn.getError()}>
                                <label>ОГРН</label>
                                <TextInput control={form.ogrn}/>
                            </Field>
                        </Compound>
                    </FormSection>
                    <FormSection title="Банковские реквизиты">
                        <Compound>
                            <Field error={form.account.getError()}>
                                <label>Номер расчетного счета</label>
                                <TextInput control={form.account}/>
                            </Field>
                            <Field error={form.correspondentAccount.getError()}>
                                <label>Корреспондентский счет</label>
                                <TextInput control={form.correspondentAccount}/>
                            </Field>
                        </Compound>
                        <Compound>
                            <Field width={160} error={form.rcbic.getError()}>
                                <label>БИК</label>
                                <TextInput control={form.rcbic}/>
                            </Field>
                            <Field error={form.bankName.getError()}>
                                <label>Полное название банка</label>
                                <TextInput control={form.bankName}/>
                            </Field>
                        </Compound>
                    </FormSection>
                    <FormSection title="Головная организация">
                        <Field error={form.parent.getError()}>
                            <label>Наименование</label>
                            <OptionInput nullable control={form.parent} options={parentalOptions}/>
                        </Field>
                    </FormSection>
                    <FormSection title="Ответственные лица">
                        <Compound>
                            <Field width={332} error={responsiblePersonForm.fullName.getError()}>
                                <label>ФИО</label>
                                <TextInput control={responsiblePersonForm.fullName}/>
                            </Field>
                            <Field width={160} error={responsiblePersonForm.phone.getError()}>
                                <label>Телефон</label>
                                <TextInput control={responsiblePersonForm.phone}/>
                            </Field>
                            <TextButton onClick={() => this.addResponsiblePerson()}>
                                Добавить
                            </TextButton>
                        </Compound>
                        {responsiblePersons.length > 0 && (
                            <React.Fragment>
                                <FieldsSeparator/>
                                {responsiblePersons.map(person => (
                                    <Compound key={person.fullName}>
                                        <Field width={332}>
                                            <TextInput readonly value={person.fullName}/>
                                        </Field>
                                        <Field width={160}>
                                            <TextInput readonly value={person.phone}/>
                                        </Field>
                                        <TextButton type="secondary" onClick={() => this.removeResponsiblePerson(person)}>
                                            Удалить
                                        </TextButton>
                                    </Compound>
                                ))}
                            </React.Fragment>
                        )}
                    </FormSection>
                    <FormSection title="Контакты">
                        <Compound>
                            <Field width={160} error={form.phone.getError()}>
                                <label>Телефон</label>
                                <TextInput placeholder="+7 999 999 99 99" control={form.phone}/>
                            </Field>
                            <Field width={332} error={form.email.getError()}>
                                <label>Электронный адрес</label>
                                <TextInput placeholder="some@mail.com" control={form.email}/>
                            </Field>
                        </Compound>
                        <Field error={form.address.getError()}>
                            <label>Адрес регистрации</label>
                            <TextInput control={form.address}/>
                        </Field>
                        <Field error={form.postalAddress.getError()}>
                            <label>Адрес почтовый</label>
                            <TextInput control={form.postalAddress}/>
                        </Field>
                        <Field error={form.actualAddress.getError()}>
                            <label>Адрес фактический</label>
                            <TextInput control={form.actualAddress}/>
                        </Field>
                    </FormSection>
                    <FormSection title="Парки">
                        <Field error={form.truckFleetTypes.getError()}>
                            <CheckInput control={form.truckFleetTypes} options={truckFleetTypeOptions} />
                        </Field>
                    </FormSection>
                    <FormSection title="Дополнительно">
                        <Field error={form.note.getError()}>
                            <label>Примечание</label>
                            <TextInput type="multiline" control={form.note}/>
                        </Field>
                    </FormSection>
                    <SurfaceFooter>
                        <Button type="primary" onClick={() => this.registerTransporter()}>
                            Добавить перевозчика
                        </Button>
                        <Button type="outline" onClick={onCancel}>Отмена</Button>
                    </SurfaceFooter>
                </Surface>
                <div style={{paddingTop: '72px'}}/>
            </PageContainer>
        )
    }
}

interface EntrepreneurGrProps {
    onRegistrationComplete: (id: string) => void
    onCancel: () => void
}

interface EntrepreneurGrState {
    form: {
        parent: Control,
        fkko: Control,
        truckFleetTypes: Control,
        note: Control,
        fullName: Control
        inn: Control
        ogrnip: Control
        phone: Control
        email: Control
        address: Control
        actualAddress: Control
        account: Control
        correspondentAccount: Control
        bankName: Control
        rcbic: Control
    },
    parentalOptions: Option[],
    truckFleetTypeOptions: Option[]
}


export class EntrepreneurTransporterRegistration extends Component<EntrepreneurGrProps, EntrepreneurGrState> {

    constructor(props: EntrepreneurGrProps) {
        super(props);
        const control = Form.getControlConstructor(this);
        this.state = {
            parentalOptions: [],
            truckFleetTypeOptions: [
                {id: '1964b722-2ce3-4a67-a948-26ebd2d0171b', name: 'Собственный'},
                {id: 'd11a6833-cb25-438e-8eab-05f15329491f', name: 'Арендованный'}
            ],
            form: {
                parent: control({
                    value: null
                }),
                fkko: control({
                    value: '11111002235',
                    validators: [required()]
                }),
                note: control({
                    value: ''
                }),
                truckFleetTypes: control({
                    value: [],
                    validators: [
                        notEmptyCollection()
                    ]
                }),
                fullName: control({
                    value: 'Перевоз Перевозчик Перевозчиков',
                    validators: [splitted(3, 'Должно быть в формате ФИО')]
                }),
                inn: control({
                    value: '100485636000',
                    validators: [required()]
                }),
                ogrnip: control({
                    value: '316100100060000',
                    validators: [required()]
                }),
                phone: control({
                    value: '+79811648000',
                    getter: formatPhone,
                    setter: parsePhone,
                    validators: [required()]
                }),
                email: control({
                    value: 'test@mail'
                }),
                address: control({
                    value: '197664, Российская Федерация, г. Барнаул, Александровская ул., дом 7, квартира 192',
                    validators: [required()]
                }),
                actualAddress: control({
                    value: '197664, Российская Федерация, г. Барнаул, Александровская ул., дом 7, квартира 192',
                    validators: [required()]
                }),
                account: control({
                    value: '40802810832130000000',
                    validators: [required()]
                }),
                correspondentAccount: control({
                    value: '30101810600000000000',
                    validators: [required()]
                }),
                bankName: control({
                    value: 'Публичное акционерное общество Сбербанк России',
                    validators: [required()]
                }),
                rcbic: control({
                    value: '044030000',
                    validators: [required()]
                }),
            }
        }
    }

    async componentDidMount() {
        let parentalOptions = await getParentalTransporterOptions();
        this.setState({parentalOptions});
    }

    async registerTransporter() {
        const {form} = this.state;
        const {onRegistrationComplete} = this.props;

        Form.validate(form);
        if (!Form.isValid(form)) {
            return;
        }

        const fio = form.fullName.value.split(' ');
        const [lastName, firstName, patronymic] = fio;
        const registration = {
            parent: form.parent.value,
            fkko: form.fkko.value,
            truckFleetTypes: form.truckFleetTypes.value,
            note: form.note.value,
            legalEntity: {
                firstName,
                lastName,
                patronymic,
                inn: form.inn.value,
                ogrnip: form.ogrnip.value,
                phone: form.phone.value,
                email: form.email.value,
                address: form.address.value,
                actualAddress: form.actualAddress.value,
                bankDetails: {
                    account: form.account.value,
                    correspondentAccount: form.correspondentAccount.value,
                    bankName: form.bankName.value,
                    rcbic: form.rcbic.value
                },
                type: 'Entrepreneur' as LegalEntityType
            }
        }
        let generator = await registerTransporter(registration);
        onRegistrationComplete(generator.id);
    }

    render() {
        const {form, parentalOptions, truckFleetTypeOptions} = this.state;
        const {onCancel} = this.props;

        return (
            <PageContainer>
                <PagePanel>
                    <h1>Добавление перевозчика (ИП)</h1>
                </PagePanel>
                <Surface>
                    <FormSection title="Основная информация">
                        <Compound>
                            <Field width={316} error={form.fullName.getError()}>
                                <label>ФИО</label>
                                <TextInput control={form.fullName}/>
                            </Field>
                            <Field width={160} error={form.inn.getError()}>
                                <label>ИНН</label>
                                <TextInput control={form.inn}/>
                            </Field>
                            <Field error={form.ogrnip.getError()}>
                                <label>ОГРНИП</label>
                                <TextInput control={form.ogrnip}/>
                            </Field>
                        </Compound>
                    </FormSection>
                    <FormSection title="Паспортные данные">
                        <Compound>
                            <Field width={160}>
                                <label>Серия</label>
                                <TextInput/>
                            </Field>
                            <Field width={160}>
                                <label>Номер</label>
                                <TextInput/>
                            </Field>
                            <Field width={160}>
                                <label>Дата выдачи</label>
                                <TextInput/>
                            </Field>
                        </Compound>
                        <Field>
                            <label>Кем выдан</label>
                            <TextInput/>
                        </Field>
                    </FormSection>
                    <FormSection title="Банковские реквизиты">
                        <Compound>
                            <Field error={form.account.getError()}>
                                <label>Номер расчетного счета</label>
                                <TextInput control={form.account}/>
                            </Field>
                            <Field error={form.correspondentAccount.getError()}>
                                <label>Корреспондентский счет</label>
                                <TextInput control={form.correspondentAccount}/>
                            </Field>
                        </Compound>
                        <Compound>
                            <Field width={160} error={form.rcbic.getError()}>
                                <label>БИК</label>
                                <TextInput control={form.rcbic}/>
                            </Field>
                            <Field error={form.bankName.getError()}>
                                <label>Полное название банка</label>
                                <TextInput control={form.bankName}/>
                            </Field>
                        </Compound>
                    </FormSection>
                    <FormSection title="Головная организация">
                        <Field error={form.parent.getError()}>
                            <label>Наименование</label>
                            <OptionInput nullable control={form.parent} options={parentalOptions}/>
                        </Field>
                    </FormSection>
                    <FormSection title="Контакты">
                        <Compound>
                            <Field width={160} error={form.phone.getError()}>
                                <label>Телефон</label>
                                <TextInput placeholder="+7 999 999 99 99" control={form.phone}/>
                            </Field>
                            <Field width={332} error={form.email.getError()}>
                                <label>Электронный адрес</label>
                                <TextInput placeholder="some@mail.com" control={form.email}/>
                            </Field>
                        </Compound>
                        <Field error={form.address.getError()}>
                            <label>Адрес регистрации</label>
                            <TextInput control={form.address}/>
                        </Field>
                        <Field error={form.actualAddress.getError()}>
                            <label>Адрес фактический</label>
                            <TextInput control={form.actualAddress}/>
                        </Field>
                    </FormSection>
                    <FormSection title="Парки">
                        <Field error={form.truckFleetTypes.getError()}>
                            <CheckInput control={form.truckFleetTypes} options={truckFleetTypeOptions} />
                        </Field>
                    </FormSection>
                    <FormSection title="Дополнительно">
                        <Field error={form.note.getError()}>
                            <label>Примечание</label>
                            <TextInput type="multiline" control={form.note}/>
                        </Field>
                    </FormSection>
                    <SurfaceFooter>
                        <Button type="primary" onClick={() => this.registerTransporter()}>
                            Добавить перевозчика
                        </Button>
                        <Button type="outline" onClick={onCancel}>Отмена</Button>
                    </SurfaceFooter>
                </Surface>
                <div style={{paddingTop: '72px'}}/>
            </PageContainer>
        )
    }
}
