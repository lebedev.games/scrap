import React, {Component} from "react";
import {emptyList, List, ReportListItem, ReportSearchCriteria} from "../../service/model";
import {backend, getReports} from "../../service/handlers";
import {Surface} from "../kit/Surface";
import {Cell, Column, Row, Table, TableBody, TableHeader, TablePaginator} from "../kit/Table";
import {formatDate, formatPeriod} from "../../data/formatters";
import {PageContainer} from "../kit/PageContainer";
import {PagePanel} from "../kit/PagePanel";
import {TextInput} from "../kit/TextInput";
import {Subject, Subscription} from "rxjs";
import {debounceTime, switchMap} from "rxjs/operators";
import {Control, Form} from "../kit/forms";
import {DateInput} from "../kit/DateInput";
import {Button} from "../kit/Button";
import {Icon} from "../kit/Icon";
import styles from "./Reports.module.scss";

interface ReportsProps {
    readonly: boolean
    onCreateReport: () => void;
}

interface ReportsState {
    reportList: List<ReportListItem>,
    search: {
        reportingDate: Control
        targetMonth: Control
        targetTransporter: Control
        limit: Control
        offset: Control
    }
}

export class Reports extends Component<ReportsProps, ReportsState> {
    subscription = new Subscription();
    searchRequest: Subject<ReportSearchCriteria>;

    constructor(props: ReportsProps) {
        super(props);
        let control = Form.getControlConstructor(this, () => this.searchRequest.next(this.getSearchCriteria()));
        this.state = {
            reportList: emptyList<ReportListItem>(),
            search: {
                reportingDate: control({value: null}),
                targetMonth: control({value: null}),
                targetTransporter: control({value: ''}),
                limit: control({value: 10}),
                offset: control({value: 0})
            }
        };
        this.searchRequest = new Subject<ReportSearchCriteria>();
    }

    async componentDidMount() {
        this.subscription.add(
            this.searchRequest.pipe(
                debounceTime(350),
                switchMap(search => getReports(search))
            ).subscribe(reportList => {
                this.setState({reportList})
            })
        );

        const reportList = await getReports(this.getSearchCriteria())
        this.setState({reportList})
    }

    componentWillUnmount() {
        this.subscription.unsubscribe()
    }

    getSearchCriteria(): ReportSearchCriteria {
        const {search} = this.state;
        return {
            targetTransporter: search.targetTransporter.value,
            targetMonth: search.targetMonth.value,
            reportingDate: search.reportingDate.value,
            limit: search.limit.value,
            offset: search.offset.value
        };
    }

    render() {
        const {onCreateReport, readonly} = this.props;
        const {reportList, search} = this.state;

        return (
            <PageContainer>
                <PagePanel>
                    <h1>Отчеты</h1>
                    {!readonly &&
                    <Button onClick={onCreateReport}>
                        Сформировать отчет
                    </Button>
                    }
                </PagePanel>
                <Surface>
                    <Table>
                        <TableHeader>
                            <Column width={160}>Отчетный период</Column>
                            <Column>Перевозчик</Column>
                            <Column width={160}>Дата отправки</Column>
                            <Column>Файлы</Column>
                        </TableHeader>
                        <TableHeader>
                            <Column>
                                <DateInput control={search.targetMonth} onlyMonth format="MMMM, yyyy"/>
                            </Column>
                            <Column>
                                <TextInput control={search.targetTransporter} icon="search"/>
                            </Column>
                            <Column>
                                <DateInput control={search.reportingDate}/>
                            </Column>
                            <Column>
                                <TextInput readonly icon="search"/>
                            </Column>
                        </TableHeader>
                        <TableBody>
                            {reportList.items.map(report => (
                                <Row key={report.id}>
                                    <Cell>{formatPeriod(report.targetMonth)}</Cell>
                                    <Cell>{report.targetTransporter}</Cell>
                                    <Cell>{formatDate(report.reportingDate)}</Cell>
                                    <Cell>
                                        <a className={styles.downloadLink} href={backend('/reports/' + report.id)}>
                                            <Icon name="xlsx"/>
                                            {report.targetTransporter} {formatPeriod(report.targetMonth)}.xlsx
                                        </a>
                                    </Cell>
                                </Row>
                            ))}
                        </TableBody>
                    </Table>
                    <TablePaginator
                        count={reportList.items.length}
                        total={reportList.total}
                        forms={['отчет', 'отчета', 'отчетов']}
                        limit={search.limit}
                        offset={search.offset}
                    />
                </Surface>
            </PageContainer>
        )
    }
}