import React, {Component} from "react";
import {Affiliate, emptyList, GeneratorListItem, GeneratorSearchCriteria, List, Option} from "../../service/model";
import {getGenerators} from "../../service/handlers";
import {ComboButton} from "../kit/ComboButton";
import {Surface} from "../kit/Surface";
import {Cell, Column, Row, Table, TableBody, TableHeader, TablePaginator} from "../kit/Table";
import {formatLegalEntityType} from "../../data/formatters";
import {PageContainer} from "../kit/PageContainer";
import {PagePanel} from "../kit/PagePanel";
import {Control, Form} from "../kit/forms";
import {Subject, Subscription} from "rxjs";
import {debounceTime, switchMap} from "rxjs/operators";
import {OptionInput} from "../kit/OptionInput";
import {TextInput} from "../kit/TextInput";

interface GeneratorsProps {
    readonly: boolean
    onRegisterBusinessEntityGenerator: () => void;
    onRegisterEntrepreneurGenerator: () => void;
    onOpenGenerator: (generator: GeneratorListItem) => void;
}


interface GeneratorsState {
    generatorsList: List<GeneratorListItem>,
    legalEntityTypeOptions: Option[],
    search: {
        inn: Control,
        name: Control,
        legalEntityType: Control,
        limit: Control,
        offset: Control
    }
}

export class Generators extends Component<GeneratorsProps, GeneratorsState> {
    subscription = new Subscription();
    searchRequest: Subject<GeneratorSearchCriteria>;

    constructor(props: GeneratorsProps) {
        super(props);
        let control = Form.getControlConstructor(this, () => this.searchRequest.next(this.getSearchCriteria()));
        this.state = {
            generatorsList: emptyList<GeneratorListItem>(),
            legalEntityTypeOptions: [
                {id: 'BusinessEntity', name: 'Юр. лицо'},
                {id: 'Entrepreneur', name: 'ИП'},
            ],
            search: {
                inn: control({value: ''}),
                name: control({value: ''}),
                legalEntityType: control({value: null}),
                limit: control({value: 10}),
                offset: control({value: 0})
            }
        };
        this.searchRequest = new Subject<GeneratorSearchCriteria>();
    }

    async componentDidMount() {
        this.subscription.add(
            this.searchRequest.pipe(
                debounceTime(350),
                switchMap(search => getGenerators(search))
            ).subscribe(generatorsList => {
                this.setState({generatorsList})
            })
        );

        const generatorsList = await getGenerators(this.getSearchCriteria())
        this.setState({generatorsList})
    }

    componentWillUnmount() {
        this.subscription.unsubscribe()
    }

    getSearchCriteria(): GeneratorSearchCriteria {
        const {search} = this.state;
        return {
            inn: search.inn.value,
            name: search.name.value,
            legalEntityType: search.legalEntityType.value,
            limit: search.limit.value,
            offset: search.offset.value
        };
    }

    render() {
        const {onOpenGenerator, onRegisterBusinessEntityGenerator, onRegisterEntrepreneurGenerator, readonly} = this.props;
        const {generatorsList, legalEntityTypeOptions, search} = this.state;
        const options = [
            {name: 'Юридическое лицо', action: onRegisterBusinessEntityGenerator},
            {name: 'Индивидуальный предприниматель', action: onRegisterEntrepreneurGenerator}
        ]

        const renderAffiliates = (affiliates: Affiliate[]) => {
            if (affiliates.length > 0) {
                return (
                    <ul>
                        {affiliates.map(affiliate => <li key={affiliate.name}>{affiliate.name}</li>)}
                    </ul>
                )
            } else {
                return '—';
            }
        }

        return (
            <PageContainer>
                <PagePanel>
                    <h1>Мусорообразователи</h1>
                    {!readonly &&
                    <ComboButton options={options}>
                        Добавить мусорообразователь
                    </ComboButton>
                    }
                </PagePanel>
                <Surface>
                    <Table>
                        <TableHeader>
                            <Column>Тип</Column>
                            <Column>ИНН</Column>
                            <Column>Краткое наименование / ФИО</Column>
                            <Column>Дочерние организации</Column>
                        </TableHeader>
                        <TableHeader>
                            <Column>
                                <OptionInput
                                    nullable
                                    options={legalEntityTypeOptions}
                                    control={search.legalEntityType}
                                />
                            </Column>
                            <Column>
                                <TextInput control={search.inn} icon="search"/>
                            </Column>
                            <Column>
                                <TextInput control={search.name} icon="search"/>
                            </Column>
                            <Column>
                                <TextInput readonly icon="search"/>
                            </Column>
                        </TableHeader>
                        <TableBody>
                            {generatorsList.items.map(generator => (
                                <Row key={generator.id} onClick={() => onOpenGenerator(generator)}>
                                    <Cell>{formatLegalEntityType(generator.legalEntityType)}</Cell>
                                    <Cell>{generator.inn}</Cell>
                                    <Cell>{generator.name}</Cell>
                                    <Cell>{renderAffiliates(generator.affiliates)}</Cell>
                                </Row>
                            ))}
                        </TableBody>
                    </Table>
                    <TablePaginator
                        count={generatorsList.items.length}
                        total={generatorsList.total}
                        forms={['мусорообразователь', 'мусорообразователя', 'мусорообразователей']}
                        limit={search.limit}
                        offset={search.offset}
                    />
                </Surface>
            </PageContainer>
        )
    }
}