import {Option, TruckModel} from "../../service/model";
import React, {Component} from "react";
import {
    getAxleConfigurationOptions,
    getCargoBodyTypeOptions,
    getFleetTypeOptions,
    getRequiredDriverLicenseCategoryOptions,
    getTransporterOptions,
    getTruck,
} from "../../service/handlers";
import {PageContainer} from "../kit/PageContainer";
import {PagePanel} from "../kit/PagePanel";
import {Surface} from "../kit/Surface";
import {Property, PropertySection} from "../kit/PropertySection";

interface TruckProps {
    id: string
}

interface TruckState {
    truck: TruckModel | null
    fleetTypeOptions: Option[]
    cargoBodyTypeOptions: Option[]
    requiredDriverLicenseCategoryOptions: Option[]
    axleConfigurationOptions: Option[]
    fenceMaterialOptions: Option[]
    operationalStatusOptions: Option[]
    organizationLegalTypeOptions: Option[]
    wasteTypeOptions: Option[]
    transporterOptions: Option[]
    generatorOptions: Option[]
}

export class Truck extends Component<TruckProps, TruckState> {

    constructor(props: TruckProps) {
        super(props);
        this.state = {
            truck: null,
            fleetTypeOptions: [],
            cargoBodyTypeOptions: [],
            requiredDriverLicenseCategoryOptions: [],
            axleConfigurationOptions: [],
            fenceMaterialOptions: [],
            operationalStatusOptions: [],
            organizationLegalTypeOptions: [],
            wasteTypeOptions: [],
            generatorOptions: [],
            transporterOptions: [],
        }
    }

    async componentDidMount() {
        const {id} = this.props;
        let truck = await getTruck(id)
        this.setState({truck})
        let fleetTypeOptions = await getFleetTypeOptions()
        this.setState({fleetTypeOptions})
        let cargoBodyTypeOptions = await getCargoBodyTypeOptions()
        this.setState({cargoBodyTypeOptions: cargoBodyTypeOptions})
        let requiredDriverLicenseCategoryOptions = await getRequiredDriverLicenseCategoryOptions()
        this.setState({requiredDriverLicenseCategoryOptions: requiredDriverLicenseCategoryOptions})
        let axleConfigurationOptions = await getAxleConfigurationOptions()
        this.setState({axleConfigurationOptions})
        let transporterOptions = await getTransporterOptions();
        this.setState({transporterOptions})
    }

    render() {
        const {
            truck,
            transporterOptions,
            fleetTypeOptions,
            cargoBodyTypeOptions,
            requiredDriverLicenseCategoryOptions,
            axleConfigurationOptions,
        } = this.state;

        if (!truck) {
            return <div>Загрузка...</div>
        }

        const nameOf = (value: string, options: Option[]) => {
            for (let option of options) {
                if (option.id === value) {
                    return option.name;
                }
            }
            return value;
        }

        return (
            <PageContainer>
                <PagePanel>
                    <h1>{truck.registrationNumber}</h1>
                </PagePanel>
                <Surface>
                    <PropertySection>
                        <header>Основная информация</header>
                        <Property>
                            <label>Владелец</label>
                            {truck.owner}
                        </Property>
                        <Property>
                            <label>Перевозчик</label>
                            {nameOf(truck.transporter, transporterOptions)}
                        </Property>
                        <Property>
                            <label>Парк</label>
                            {nameOf(truck.fleetType, fleetTypeOptions)}
                        </Property>
                    </PropertySection>
                    <PropertySection>
                        <header>Идентификационные номера</header>
                        <Property>
                            <label>VIN</label>
                            {truck.vin}
                        </Property>
                        <Property>
                            <label>Государственный регистрационный номер</label>
                            {truck.registrationNumber}
                        </Property>
                        <Property>
                            <label>Гаражный номер</label>
                            {truck.garagingNumber}
                        </Property>
                    </PropertySection>
                    <PropertySection>
                        <header>Характеристики</header>
                        <Property>
                            <label>Марка и модель ТС</label>
                            {truck.vehicleModel}
                        </Property>
                        <Property>
                            <label>Марка установки</label>
                            {truck.wasteEquipmentModel}
                        </Property>
                        <Property>
                            <label>Тип ТС</label>
                            {nameOf(truck.cargoBodyType, cargoBodyTypeOptions)}
                        </Property>
                        <Property>
                            <label>Категория ТС</label>
                            {nameOf(truck.requiredDriverLicenseCategory, requiredDriverLicenseCategoryOptions)}
                        </Property>
                        <Property>
                            <label>Колесная формула</label>
                            {nameOf(truck.axleConfiguration, axleConfigurationOptions)}
                        </Property>
                        <Property>
                            <label>Год выпуска ТС</label>
                            {truck.productionYear}
                        </Property>
                        <Property>
                            <label>БНСО</label>
                            {truck.navigationEquipmentName}
                        </Property>
                        <Property>
                            <label>Вес пустого ТС</label>
                            {truck.emptyWeightKg} кг
                        </Property>
                        <Property>
                            <label>Вес допустимого груза</label>
                            {truck.cargoCapacityKg} кг
                        </Property>
                    </PropertySection>
                    <PropertySection>
                        <header>Перечень полигонов <br/>с разрешенным въездом</header>
                    </PropertySection>
                </Surface>
                <div style={{paddingTop: '72px'}}/>
            </PageContainer>
        )
    }
}
