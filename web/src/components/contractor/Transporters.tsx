import React, {Component} from "react";
import {emptyList, List, Option, TransporterListItemModel, TransporterSearchCriteria} from "../../service/model";
import {getTransporters} from "../../service/handlers";
import {ComboButton} from "../kit/ComboButton";
import {Surface} from "../kit/Surface";
import {Cell, Column, Row, Table, TableBody, TableHeader, TablePaginator} from "../kit/Table";
import {formatLegalEntityType} from "../../data/formatters";
import {PageContainer} from "../kit/PageContainer";
import {PagePanel} from "../kit/PagePanel";
import {TextInput} from "../kit/TextInput";
import {Subject, Subscription} from "rxjs";
import {debounceTime, switchMap} from "rxjs/operators";
import {Control, Form} from "../kit/forms";
import {OptionInput} from "../kit/OptionInput";

interface TransportersProps {
    onRegisterBusinessEntityTransporter: () => void;
    onRegisterEntrepreneurTransporter: () => void;
    onOpenTransporter: (transporter: TransporterListItemModel) => void;
}


interface TransportersState {
    transporterList: List<TransporterListItemModel>
    truckFleetTypeOptions: Option[],
    legalEntityTypeOptions: Option[],
    search: {
        inn: Control,
        name: Control,
        truckFleetType: Control,
        legalEntityType: Control,
        limit: Control,
        offset: Control
    }
}

export class Transporters extends Component<TransportersProps, TransportersState> {
    subscription = new Subscription();
    searchRequest: Subject<TransporterSearchCriteria>;

    constructor(props: TransportersProps) {
        super(props);
        let control = Form.getControlConstructor(this, () => this.searchRequest.next(this.getSearchCriteria()));
        this.state = {
            transporterList: emptyList<TransporterListItemModel>(),
            truckFleetTypeOptions: [
                {id: '1964b722-2ce3-4a67-a948-26ebd2d0171b', name: 'Собственный'},
                {id: 'd11a6833-cb25-438e-8eab-05f15329491f', name: 'Арендованный'}
            ],
            legalEntityTypeOptions: [
                {id: 'BusinessEntity', name: 'Юр. лицо'},
                {id: 'Entrepreneur', name: 'ИП'},
            ],
            search: {
                inn: control({value: ''}),
                name: control({value: ''}),
                truckFleetType: control({value: null}),
                legalEntityType: control({value: null}),
                limit: control({value: 10}),
                offset: control({value: 0})
            }
        };
        this.searchRequest = new Subject<TransporterSearchCriteria>();
    }

    async componentDidMount() {
        this.subscription.add(
            this.searchRequest.pipe(
                debounceTime(350),
                switchMap(search => getTransporters(search))
            ).subscribe(transporterList => {
                this.setState({transporterList})
            })
        );

        const transporterList = await getTransporters(this.getSearchCriteria())
        this.setState({transporterList})
    }

    componentWillUnmount() {
        this.subscription.unsubscribe()
    }

    getSearchCriteria(): TransporterSearchCriteria {
        const {search} = this.state;
        return {
            inn: search.inn.value,
            name: search.name.value,
            truckFleetType: search.truckFleetType.value,
            legalEntityType: search.legalEntityType.value,
            limit: search.limit.value,
            offset: search.offset.value
        };
    }

    render() {
        const {onOpenTransporter, onRegisterBusinessEntityTransporter, onRegisterEntrepreneurTransporter} = this.props;
        const {transporterList, search, truckFleetTypeOptions, legalEntityTypeOptions} = this.state;
        const options = [
            {name: 'Юридическое лицо', action: onRegisterBusinessEntityTransporter},
            {name: 'Индивидуальный предприниматель', action: onRegisterEntrepreneurTransporter}
        ]

        let rows = [];
        for (let i = 0; i < transporterList.limit; i++) {
            if (i < transporterList.items.length) {
                let transporter = transporterList.items[i];
                let row = (
                    <Row key={transporter.id} onClick={() => onOpenTransporter(transporter)}>
                        <Cell>{formatLegalEntityType(transporter.legalEntityType)}</Cell>
                        <Cell>{transporter.inn}</Cell>
                        <Cell>{transporter.name}</Cell>
                        <Cell>{transporter.truckFleetTypes.join(', ')}</Cell>
                        <Cell>{transporter.districts.length}</Cell>
                    </Row>
                );
                rows.push(row);
            } else {
                let row = (
                    <Row key={i}>
                        <Cell>
                            <div style={{color: 'transparent'}}>a</div>
                        </Cell>
                        <Cell/>
                        <Cell/>
                        <Cell/>
                        <Cell/>
                    </Row>
                )
                rows.push(row)
            }
        }

        return (
            <PageContainer>
                <PagePanel>
                    <h1>Перевозчики</h1>
                    <ComboButton options={options}>
                        Добавить перевозчика
                    </ComboButton>
                </PagePanel>
                <Surface>
                    <Table>
                        <TableHeader>
                            <Column>Тип</Column>
                            <Column>ИНН</Column>
                            <Column>Краткое наименование / ФИО</Column>
                            <Column>Парк</Column>
                            <Column width={80}>Площадки</Column>
                        </TableHeader>
                        <TableHeader>
                            <Column>
                                <OptionInput
                                    nullable
                                    options={legalEntityTypeOptions}
                                    control={search.legalEntityType}
                                />
                            </Column>
                            <Column>
                                <TextInput control={search.inn} icon="search"/>
                            </Column>
                            <Column>
                                <TextInput control={search.name} icon="search"/>
                            </Column>
                            <Column>
                                <OptionInput nullable options={truckFleetTypeOptions} control={search.truckFleetType}/>
                            </Column>
                            <Column>
                                <TextInput readonly icon="search"/>
                            </Column>
                        </TableHeader>
                        <TableBody>
                            {rows}
                        </TableBody>
                    </Table>
                    <TablePaginator
                        count={transporterList.items.length}
                        total={transporterList.total}
                        forms={['перевозчик', 'перевозчика', 'перевозчиков']}
                        limit={search.limit}
                        offset={search.offset}
                    />
                </Surface>
            </PageContainer>
        )
    }
}