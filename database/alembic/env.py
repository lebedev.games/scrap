from __future__ import with_statement

import os
import sys
from logging.config import fileConfig
from time import sleep

import psycopg2
import sqlalchemy
from alembic import context
from sqlalchemy import create_engine
from sqlalchemy.exc import OperationalError

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + "/..")

# this is the Alembic Config object, which provides
# access to the values within the .ini file in use.
config = context.config

# Interpret the config file for Python logging.
# This line sets up loggers basically.
fileConfig(config.config_file_name)

# add your model's MetaData object here
# for 'autogenerate' support
# from myapp import mymodel
# target_metadata = mymodel.Base.metadata
target_metadata = None


# other values from the config, defined by the needs of env.py,
# can be acquired:
# my_important_option = config.get_main_option("my_important_option")
# ... etc.

def get_database_url() -> str:
    return os.environ.get('DATABASE_URL')


def run_migrations_offline():
    """Run migrations in 'offline' mode.

    This configures the context with just a URL
    and not an Engine, though an Engine is acceptable
    here as well.  By skipping the Engine creation
    we don't even need a DBAPI to be available.

    Calls to context.execute() here emit the given string to the
    script output.

    """
    context.configure(
        url=get_database_url(),
        target_metadata=target_metadata,
        literal_binds=True,
        compare_type=True,
    )

    with context.begin_transaction():
        context.run_migrations()


def run_migrations_online():
    """Run migrations in 'online' mode.

    In this scenario we need to create an Engine
    and associate a connection with the context.

    """
    url = get_database_url()
    connectable = create_engine(url, connect_args={'connect_timeout': 1})

    delay = 10
    attempts = 5

    connection = None
    for attempt in range(attempts):
        try:
            connection = connectable.connect()
        except OperationalError as error:
            print(
                f'Unable to connect "{url}", '
                f'reconnect after {delay} seconds '
                f'({attempts - attempt} attempts left)'
            )
            if attempt < attempts - 1:
                sleep(delay)
            else:
                raise error

    with connection:
        context.configure(
            connection=connection,
            target_metadata=target_metadata,
            compare_type=True,
            version_table='migrations',
        )

        with context.begin_transaction():
            context.run_migrations()

    database_host, database_name = url.rsplit('/', 1)
    database_url = f'{database_host}/postgres'
    database_temp = 'scrap_template'
    connectable = create_engine(
        database_url,
        connect_args={
            'connect_timeout': 1
        },
        execution_options={
            "isolation_level": "AUTOCOMMIT"
        })
    with connectable.connect() as connection:
        connection.execute(f"SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE datname='{database_name}';")
        connection.execute(f"SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE datname='{database_temp}';")
        connection.execute(f'DROP DATABASE IF EXISTS {database_temp};')
        connection.execute(f"CREATE DATABASE {database_temp} TEMPLATE {database_name};")


if context.is_offline_mode():
    run_migrations_offline()
else:
    run_migrations_online()
