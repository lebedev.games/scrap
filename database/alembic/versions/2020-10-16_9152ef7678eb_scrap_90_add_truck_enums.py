"""scrap 90 add truck enums

Revision ID: 9152ef7678eb
Revises: 888128195394
Create Date: 2020-10-16 00:00:02.464267+00:00

"""
from alembic import op


# revision identifiers, used by Alembic.
revision = '9152ef7678eb'
down_revision = '888128195394'
branch_labels = None
depends_on = None


def upgrade():
    op.execute("""
    create table dictionaries.cargo_body_types
    (
        entry_id        bigserial primary key,
        id              uuid not null unique,
        type_name       varchar(255)
    );
    
    insert into dictionaries.cargo_body_types (id, type_name) values
    ('999dd103-83e8-417e-bd7a-b31f39344fd1', 'Мусоровоз контейнерного типа'),
    ('deac883d-57af-47e3-9a0d-465a0499151f', 'Мусоровоз кузовного типа'),
    ('557361cb-e574-46f0-80cc-ccaebee3d1d1', 'Ломовоз'),
    ('b07ff2f9-eff5-4d88-b79e-9966f4164c0e', 'Мусоровоз-самосвал');
    
    create table dictionaries.driver_license_categories
    (
        entry_id        bigserial primary key,
        id              uuid not null unique,
        category_name       varchar(255)
    );
    
    insert into dictionaries.driver_license_categories (id, category_name) values
    ('d6196de7-2123-49dd-b9cf-d9bac1df0ecd', 'B'),
    ('1a957e14-1658-452d-8071-ce41386e3a24', 'C'),
    ('170d6127-679c-484a-872f-58b19f11b2d8', 'E');
    
    create table dictionaries.axle_configurations
    (
        entry_id        bigserial primary key,
        id              uuid not null unique,
        arrangement     varchar(255)
    );
    
    insert into dictionaries.axle_configurations (id, arrangement) values
    ('e1f68a50-0292-48ae-8838-77a7d6694a63', '4'),
    ('5dc3f25d-7cf6-4631-a9ba-8139aa805308', '6'),
    ('1207d627-e764-467a-94a0-34d1122841fb', '8');
    """)


def downgrade():
    op.execute("""
    drop table dictionaries.cargo_body_type;
    drop table dictionaries.driver_license_categories;
    drop table dictionaries.axle_configurations;
    """)
