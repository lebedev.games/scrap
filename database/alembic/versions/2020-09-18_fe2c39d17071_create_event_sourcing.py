"""create event sourcing

Revision ID: fe2c39d17071
Revises: 468b569df431
Create Date: 2020-09-18 08:52:43.710802+00:00

"""
from alembic import op

# revision identifiers, used by Alembic.
revision = 'fe2c39d17071'
down_revision = '468b569df431'
branch_labels = None
depends_on = None


def upgrade():
    op.execute("""
    create extension if not exists "uuid-ossp";

    create sequence event_timestamp;
    
    create table entities
    (
        entry_id  bigserial primary key,
        id        uuid                     not null,
        source    varchar(32)              not null,
        version   integer                  not null,
        published timestamp with time zone not null default now(),
        publisher uuid                     not null,
        unique (id, version)
    );
    
    create table user_registered
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),
    
        registrar       uuid                     not null,
        date            timestamp with time zone not null
    );
    
    create table user_deleted
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),
    
        registrar       uuid                     not null,
        date            timestamp with time zone not null
    );
    
    create table user_permission_granted
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),
    
        grantor         uuid                     not null,
        permission      varchar(64)              not null,
        date            timestamp with time zone not null
    );
    
    create table user_role_specified
    (
        entry_id        bigserial primary key,
        event_id        uuid         not null default uuid_generate_v4(),
        timestamp       bigint       not null default nextval('event_timestamp'),
        entity_entry_id bigint       not null references entities (entry_id),
    
        manager         uuid         not null,
        role            varchar(255) not null
    );
    
    create table user_role_entity_assigned
    (
        entry_id        bigserial primary key,
        event_id        uuid         not null default uuid_generate_v4(),
        timestamp       bigint       not null default nextval('event_timestamp'),
        entity_entry_id bigint       not null references entities (entry_id),
    
        manager         uuid         not null,
        entity_id       uuid         not null
    );
    
    create table user_name_specified
    (
        entry_id        bigserial primary key,
        event_id        uuid         not null default uuid_generate_v4(),
        timestamp       bigint       not null default nextval('event_timestamp'),
        entity_entry_id bigint       not null references entities (entry_id),
    
        registrar       uuid         not null,
        name            varchar(255) not null
    );
    
    create table user_password_credentials_specified
    (
        entry_id        bigserial primary key,
        event_id        uuid         not null default uuid_generate_v4(),
        timestamp       bigint       not null default nextval('event_timestamp'),
        entity_entry_id bigint       not null references entities (entry_id),
    
        grantor         uuid         not null,
        login           varchar(255) not null,
        password        varchar(255) not null
    );
    
    create procedure register_system_user()
    language plpgsql as
    $$
    declare
        entity_entry_id bigint;
        system_user_id  uuid := '7036ba92-3c56-4405-8fdc-be3863f3cc70';
    begin
        insert into entities (id, source, version, publisher)
        values (system_user_id, 'service::domain::User', 0, system_user_id)
        returning entry_id into entity_entry_id;
    
        insert into user_registered (entity_entry_id, registrar, date)
        values (entity_entry_id, system_user_id, now());
    
        insert into user_role_specified (entity_entry_id, manager, role)
        values (entity_entry_id, system_user_id, 'Contractor');
        
        insert into user_password_credentials_specified (entity_entry_id, grantor, login, password)
        values (entity_entry_id, system_user_id, 'system', 'system');
    
        insert into user_permission_granted (entity_entry_id, grantor, permission, date)
        values (entity_entry_id, system_user_id, 'file/upload', now()),
                (entity_entry_id, system_user_id, 'user/register', now()),
                (entity_entry_id, system_user_id, 'user/grant_permission', now()),
                (entity_entry_id, system_user_id, 'user/delete', now()),
                (entity_entry_id, system_user_id, 'user/list', now()),
                (entity_entry_id, system_user_id, 'user/get', now()),
                (entity_entry_id, system_user_id, 'generator/register', now()),
                (entity_entry_id, system_user_id, 'generator/list', now()),
                (entity_entry_id, system_user_id, 'generator/get', now()),
                (entity_entry_id, system_user_id, 'generator/get_parental', now()),
                (entity_entry_id, system_user_id, 'transportation_draft/register', now()),
                (entity_entry_id, system_user_id, 'transportation_draft/change', now()),
                (entity_entry_id, system_user_id, 'transportation_draft/list', now()),
                (entity_entry_id, system_user_id, 'transportation_draft/get', now()),
                (entity_entry_id, system_user_id, 'target_generation_point/list', now()),
                (entity_entry_id, system_user_id, 'generation_point/get', now()),
                (entity_entry_id, system_user_id, 'generation_point/register', now()),
                (entity_entry_id, system_user_id, 'generation_point/list', now()),
                (entity_entry_id, system_user_id, 'transporter/register', now()),
                (entity_entry_id, system_user_id, 'transporter/get', now()),
                (entity_entry_id, system_user_id, 'transporter/list', now()),
                (entity_entry_id, system_user_id, 'truck/register', now()),
                (entity_entry_id, system_user_id, 'truck/get', now()),
                (entity_entry_id, system_user_id, 'truck/list', now()),
                (entity_entry_id, system_user_id, 'transporter/list', now()),
                (entity_entry_id, system_user_id, 'negotiation/complete', now()),
                (entity_entry_id, system_user_id, 'negotiation/issues/declare', now()),
                (entity_entry_id, system_user_id, 'negotiation/issues/solve', now()),
                (entity_entry_id, system_user_id, 'negotiation/get', now()),
                (entity_entry_id, system_user_id, 'negotiation/list', now()),
                (entity_entry_id, system_user_id, 'reports/*', now())
                ;
    end;
    $$;
    
    call register_system_user();
    
    drop procedure register_system_user();
    """)


def downgrade():
    pass
