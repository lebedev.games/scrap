"""scrap_44_create_generator_events

Revision ID: a5ce8684a4ca
Revises: fe2c39d17071
Create Date: 2020-10-05 15:02:17.122187+00:00

"""
from alembic import op


# revision identifiers, used by Alembic.
revision = 'a5ce8684a4ca'
down_revision = 'fe2c39d17071'
branch_labels = None
depends_on = None


def upgrade():
    op.execute("""
    create table business_entity_registered
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),
    
        registrar       uuid                     not null,
        date            timestamp with time zone not null
    );
    
    create table business_entity_administrative_name_updated
    (
        entry_id        bigserial primary key,
        event_id        uuid         not null default uuid_generate_v4(),
        timestamp       bigint       not null default nextval('event_timestamp'),
        entity_entry_id bigint       not null references entities (entry_id),
    
        registrar       uuid         not null,
        full_name       varchar(255) not null,
        short_name      varchar(255) not null
    );
    
    create table business_entity_tax_identification_updated
    (
        entry_id        bigserial primary key,
        event_id        uuid         not null default uuid_generate_v4(),
        timestamp       bigint       not null default nextval('event_timestamp'),
        entity_entry_id bigint       not null references entities (entry_id),
    
        registrar       uuid         not null,
        inn             varchar(255) not null
    );
    
    create table business_entity_tax_registration_updated
    (
        entry_id        bigserial primary key,
        event_id        uuid         not null default uuid_generate_v4(),
        timestamp       bigint       not null default nextval('event_timestamp'),
        entity_entry_id bigint       not null references entities (entry_id),
    
        registrar       uuid         not null,
        kpp             varchar(255) not null
    );
    
    create table business_entity_primary_registration_updated
    (
        entry_id        bigserial primary key,
        event_id        uuid         not null default uuid_generate_v4(),
        timestamp       bigint       not null default nextval('event_timestamp'),
        entity_entry_id bigint       not null references entities (entry_id),
    
        registrar       uuid         not null,
        ogrn            varchar(255) not null
    );
    
    create table business_entity_phone_contact_updated
    (
        entry_id        bigserial primary key,
        event_id        uuid        not null default uuid_generate_v4(),
        timestamp       bigint      not null default nextval('event_timestamp'),
        entity_entry_id bigint      not null references entities (entry_id),
    
        registrar       uuid        not null,
        phone           varchar(31) not null
    );
    
    create table business_entity_email_contact_updated
    (
        entry_id        bigserial primary key,
        event_id        uuid         not null default uuid_generate_v4(),
        timestamp       bigint       not null default nextval('event_timestamp'),
        entity_entry_id bigint       not null references entities (entry_id),
    
        registrar       uuid         not null,
        email           varchar(127) not null
    );
    
    create table business_entity_address_updated
    (
        entry_id        bigserial primary key,
        event_id        uuid         not null default uuid_generate_v4(),
        timestamp       bigint       not null default nextval('event_timestamp'),
        entity_entry_id bigint       not null references entities (entry_id),
    
        registrar       uuid         not null,
        full_address    varchar(255) not null
    );
    
    create table business_entity_postal_address_updated
    (
        entry_id        bigserial primary key,
        event_id        uuid         not null default uuid_generate_v4(),
        timestamp       bigint       not null default nextval('event_timestamp'),
        entity_entry_id bigint       not null references entities (entry_id),
    
        registrar       uuid         not null,
        full_address    varchar(255) not null
    );
    
    create table business_entity_actual_address_updated
    (
        entry_id        bigserial primary key,
        event_id        uuid         not null default uuid_generate_v4(),
        timestamp       bigint       not null default nextval('event_timestamp'),
        entity_entry_id bigint       not null references entities (entry_id),
    
        registrar       uuid         not null,
        full_address    varchar(255) not null
    );
    
    create table business_entity_responsible_person_added
    (
        entry_id        bigserial primary key,
        event_id        uuid         not null default uuid_generate_v4(),
        timestamp       bigint       not null default nextval('event_timestamp'),
        entity_entry_id bigint       not null references entities (entry_id),
    
        registrar       uuid         not null,
        full_name       varchar(255) not null,
        phone           varchar(31)  not null
    );
    
    create table business_entity_bank_details_updated
    (
        entry_id              bigserial primary key,
        event_id              uuid         not null default uuid_generate_v4(),
        timestamp             bigint       not null default nextval('event_timestamp'),
        entity_entry_id       bigint       not null references entities (entry_id),
    
        registrar             uuid         not null,
        account               varchar(255) not null,
        correspondent_account varchar(255) not null,
        bank_name             varchar(255) not null,
        rcbic                 varchar(255) not null
    );

    create table entrepreneur_registered
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),
    
        registrar       uuid                     not null,
        date            timestamp with time zone not null
    );
    
    create table entrepreneur_personal_name_updated
    (
        entry_id        bigserial primary key,
        event_id        uuid         not null default uuid_generate_v4(),
        timestamp       bigint       not null default nextval('event_timestamp'),
        entity_entry_id bigint       not null references entities (entry_id),
    
        registrar       uuid         not null,
        first_name      varchar(255) not null,
        last_name       varchar(255) not null,
        patronymic      varchar(255) not null
    );
    
    create table entrepreneur_tax_identification_updated
    (
        entry_id        bigserial primary key,
        event_id        uuid         not null default uuid_generate_v4(),
        timestamp       bigint       not null default nextval('event_timestamp'),
        entity_entry_id bigint       not null references entities (entry_id),
    
        registrar       uuid         not null,
        inn             varchar(255) not null
    );
    
    create table entrepreneur_primary_registration_updated
    (
        entry_id        bigserial primary key,
        event_id        uuid         not null default uuid_generate_v4(),
        timestamp       bigint       not null default nextval('event_timestamp'),
        entity_entry_id bigint       not null references entities (entry_id),
    
        registrar       uuid         not null,
        ogrnip          varchar(255) not null
    );
    
    create table entrepreneur_phone_contact_updated
    (
        entry_id        bigserial primary key,
        event_id        uuid         not null default uuid_generate_v4(),
        timestamp       bigint       not null default nextval('event_timestamp'),
        entity_entry_id bigint       not null references entities (entry_id),
    
        registrar       uuid         not null,
        phone           varchar(255) not null
    );
    
    create table entrepreneur_email_contact_updated
    (
        entry_id        bigserial primary key,
        event_id        uuid         not null default uuid_generate_v4(),
        timestamp       bigint       not null default nextval('event_timestamp'),
        entity_entry_id bigint       not null references entities (entry_id),
    
        registrar       uuid         not null,
        email           varchar(255) not null
    );
    
    create table entrepreneur_address_updated
    (
        entry_id        bigserial primary key,
        event_id        uuid         not null default uuid_generate_v4(),
        timestamp       bigint       not null default nextval('event_timestamp'),
        entity_entry_id bigint       not null references entities (entry_id),
    
        registrar       uuid         not null,
        full_address    varchar(255) not null
    );
    
    create table entrepreneur_actual_address_updated
    (
        entry_id        bigserial primary key,
        event_id        uuid         not null default uuid_generate_v4(),
        timestamp       bigint       not null default nextval('event_timestamp'),
        entity_entry_id bigint       not null references entities (entry_id),
    
        registrar       uuid         not null,
        full_address    varchar(255) not null
    );
    
    create table entrepreneur_bank_details_updated
    (
        entry_id              bigserial primary key,
        event_id              uuid         not null default uuid_generate_v4(),
        timestamp             bigint       not null default nextval('event_timestamp'),
        entity_entry_id       bigint       not null references entities (entry_id),
    
        registrar             uuid         not null,
        account               varchar(255) not null,
        correspondent_account varchar(255) not null,
        bank_name             varchar(255) not null,
        rcbic                 varchar(255) not null
    );

    create table generator_registered
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),
    
        registrar       uuid                     not null,
        date            timestamp with time zone not null
    );
    
    
    create table generator_entrepreneur_assigned
    (
        entry_id        bigserial primary key,
        event_id        uuid   not null default uuid_generate_v4(),
        timestamp       bigint not null default nextval('event_timestamp'),
        entity_entry_id bigint not null references entities (entry_id),
    
        registrar       uuid   not null,
        entrepreneur    uuid   not null
    );
    
    create table generator_business_entity_assigned
    (
        entry_id        bigserial primary key,
        event_id        uuid   not null default uuid_generate_v4(),
        timestamp       bigint not null default nextval('event_timestamp'),
        entity_entry_id bigint not null references entities (entry_id),
    
        registrar       uuid   not null,
        business_entity uuid   not null
    );
    
    create table generator_parent_updated
    (
        entry_id        bigserial primary key,
        event_id        uuid   not null default uuid_generate_v4(),
        timestamp       bigint not null default nextval('event_timestamp'),
        entity_entry_id bigint not null references entities (entry_id),
    
        registrar       uuid   not null,
        parent          uuid   not null
    );

    """)


def downgrade():
    pass
