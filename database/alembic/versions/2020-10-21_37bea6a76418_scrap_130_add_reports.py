"""scrap_130_add_reports

Revision ID: 37bea6a76418
Revises: 9e9089cb8f90
Create Date: 2020-10-21 12:18:59.503768+00:00

"""
from alembic import op

# revision identifiers, used by Alembic.
revision = '37bea6a76418'
down_revision = '9e9089cb8f90'
branch_labels = None
depends_on = None


def upgrade():
    op.execute("""
    create table report_created
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),
    
        contractor      uuid                     not null,
        date            timestamp with time zone not null
    );
    
    create table report_transportation_covered
    (
        entry_id        bigserial primary key,
        event_id        uuid   not null default uuid_generate_v4(),
        timestamp       bigint not null default nextval('event_timestamp'),
        entity_entry_id bigint not null references entities (entry_id),
    
        transportations uuid[] not null
    );
    
    create table report_target_specified
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),
    
        transporter     uuid                     not null,
        month           timestamp with time zone not null
    );
    
    create table reports
    (
        entry_id           bigserial primary key,
        id                 uuid                     not null,
        target_transporter varchar(255)             not null,
        target_month       timestamp with time zone not null,
        transportations    uuid[]                   not null,
        reporting_date     timestamp with time zone not null
    );
    """)


def downgrade():
    pass
