"""scrap_92_add_trucks_projection

Revision ID: 0557f31eb4de
Revises: 6006c4999acd
Create Date: 2020-10-16 21:11:25.224178+00:00

"""
from alembic import op


# revision identifiers, used by Alembic.
revision = '0557f31eb4de'
down_revision = '6006c4999acd'
branch_labels = None
depends_on = None


def upgrade():
    op.execute("""
    create table trucks
    (
        entry_id                bigserial       primary key,
        truck_id                uuid            not null unique,
        registration_number     varchar(255)    not null,
        cargo_body_type         uuid            not null,
        transporter_id          uuid            not null,
        truck_fleet_type        uuid            not null
    );
    
    create table trucks_transporters (
        entry_id                            bigserial       primary key,
        transporter_id                      uuid            not null unique,
        legal_entity_id                     uuid            not null
    );
    
    create table trucks_legal_entities (
        entry_id                            bigserial       primary key,
        legal_entity_id                     uuid            not null unique,
        name                                varchar(255)    not null
    );
    """)


def downgrade():
    op.execute("""
    drop table trucks;
    drop table trucks_transporters;
    drop table trucks_legal_entities;
    """)
