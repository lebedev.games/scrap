"""scrap_57_generation_points_projections

Revision ID: 811534581880
Revises: 637fc1e5422a
Create Date: 2020-10-13 14:41:48.943308+00:00

"""
from alembic import op


# revision identifiers, used by Alembic.
revision = '811534581880'
down_revision = '637fc1e5422a'
branch_labels = None
depends_on = None


def upgrade():
    op.execute("""
    
    create table generation_points
    (
        entry_id                bigserial       primary key,
        generation_point_id     uuid            not null unique,
        internal_system_number  varchar(255)    not null,
        full_address            varchar(255)    not null,
        transporter_id          uuid            not null,
        area_m2                 numeric(10, 2)  not null
    );
    
    create table generation_point_transporters (
        entry_id                            bigserial       primary key,
        transporter_id                      uuid            not null unique,
        legal_entity_id                     uuid            not null
    );
    
    create table generation_point_legal_entities (
        entry_id                            bigserial       primary key,
        legal_entity_id                     uuid            not null unique,
        name                                varchar(255)    not null
    );
    
    create table target_generation_points
    (
        entry_id                bigserial       primary key,
        generation_point_id     uuid            not null unique,
        address                 varchar(255)    not null,
        transporter             uuid            not null
    );
    
    create table target_generation_points_container_types
    (
        entry_id                bigserial       primary key,
        generation_point_id     uuid            not null references target_generation_points (generation_point_id),
        container_type_id       uuid            not null references dictionaries.container_types (id)
    );
    """)


def downgrade():
    op.execute("""
    drop table generation_points;
    drop table target_generation_points_container_types;
    drop table target_generation_points;
    drop table generation_point_transporters;
    drop table generation_point_legal_entities;
    """)
