"""scrap_49_add_transporter_events

Revision ID: 637fc1e5422a
Revises: 028c0dcd6a30
Create Date: 2020-10-08 13:50:32.423700+00:00

"""
from alembic import op


# revision identifiers, used by Alembic.
revision = '637fc1e5422a'
down_revision = '028c0dcd6a30'
branch_labels = None
depends_on = None


def upgrade():
    op.execute("""
        create table transporter_registered
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),
    
        registrar       uuid                     not null,
        date            timestamp with time zone not null
    );
    
    create table transporter_entrepreneur_assigned
    (
        entry_id        bigserial primary key,
        event_id        uuid   not null default uuid_generate_v4(),
        timestamp       bigint not null default nextval('event_timestamp'),
        entity_entry_id bigint not null references entities (entry_id),
    
        registrar       uuid   not null,
        entrepreneur    uuid   not null
    );
    
    create table transporter_business_entity_assigned
    (
        entry_id        bigserial primary key,
        event_id        uuid   not null default uuid_generate_v4(),
        timestamp       bigint not null default nextval('event_timestamp'),
        entity_entry_id bigint not null references entities (entry_id),
    
        registrar       uuid   not null,
        business_entity uuid   not null
    );
    
    create table transporter_federal_classification_updated
    (
        entry_id        bigserial primary key,
        event_id        uuid         not null default uuid_generate_v4(),
        timestamp       bigint       not null default nextval('event_timestamp'),
        entity_entry_id bigint       not null references entities (entry_id),
    
        registrar       uuid         not null,
        fkko            varchar(255) not null
    );
    
    create table transporter_parent_updated
    (
        entry_id        bigserial primary key,
        event_id        uuid   not null default uuid_generate_v4(),
        timestamp       bigint not null default nextval('event_timestamp'),
        entity_entry_id bigint not null references entities (entry_id),
    
        registrar       uuid   not null,
        parent          uuid   not null
    );
    
    create table transporter_truck_fleet_type_specified
    (
        entry_id         bigserial primary key,
        event_id         uuid   not null default uuid_generate_v4(),
        timestamp        bigint not null default nextval('event_timestamp'),
        entity_entry_id  bigint not null references entities (entry_id),
    
        registrar        uuid   not null,
        truck_fleet_type uuid   not null references dictionaries.truck_fleet_types (id)
    );
    
    create table transporter_truck_added
    (
        entry_id        bigserial primary key,
        event_id        uuid   not null default uuid_generate_v4(),
        timestamp       bigint not null default nextval('event_timestamp'),
        entity_entry_id bigint not null references entities (entry_id),
    
        registrar       uuid   not null,
        truck           uuid   not null
    );
    
    create table transporter_target_generation_point_added
    (
        entry_id         bigserial primary key,
        event_id         uuid   not null default uuid_generate_v4(),
        timestamp        bigint not null default nextval('event_timestamp'),
        entity_entry_id  bigint not null references entities (entry_id),
    
        registrar        uuid   not null,
        assignment_date  timestamp with time zone not null,
        generation_point uuid   not null
    );
    
    create table transporter_note_updated
    (
        entry_id        bigserial primary key,
        event_id        uuid         not null default uuid_generate_v4(),
        timestamp       bigint       not null default nextval('event_timestamp'),
        entity_entry_id bigint       not null references entities (entry_id),
    
        registrar       uuid         not null,
        note            varchar(255) not null
    );
    
    """)


def downgrade():
    pass
