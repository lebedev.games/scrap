"""scrap_55_add_transportation_projection

Revision ID: 3ac7f26ead21
Revises: fdfd2abd9f54
Create Date: 2020-10-08 13:48:28.523786+00:00

"""
from alembic import op


# revision identifiers, used by Alembic.
revision = '3ac7f26ead21'
down_revision = 'fdfd2abd9f54'
branch_labels = None
depends_on = None


def upgrade():
    op.execute("""
    create table transportations
    (
        entry_id       bigserial primary key,
        id             uuid                     not null,
        transporter    uuid                     not null,
        execution_date timestamp with time zone not null,
        truck          uuid                     not null,
        status         varchar(64)              not null
    );
    
    create table transportations_trucks
    (
        entry_id            bigserial primary key,
        id                  uuid        not null,
        registration_number varchar(64) not null
    );
    """)


def downgrade():
    pass
