"""scrap_51_add_enum_tables

Revision ID: 384173cddfea
Revises: 3ac7f26ead21
Create Date: 2020-10-09 08:20:04.526241+00:00

"""
from alembic import op


# revision identifiers, used by Alembic.
revision = '384173cddfea'
down_revision = '3ac7f26ead21'
branch_labels = None
depends_on = None


def upgrade():
    op.execute("""
    create schema dictionaries;
    
    create table dictionaries.waste_types
    (
        entry_id        bigserial primary key,
        id              uuid not null unique,
        type_name       varchar(255)
    );
    
    insert into dictionaries.waste_types (id, type_name) values
    ('529a07b6-fa73-4536-ac94-fabf95b9ff63', 'ТКО'),
    ('a05ed0f4-dca0-4e25-a374-047d2870eddc', 'КГО');
    
    create table dictionaries.organization_legal_types
    (
        entry_id        bigserial primary key,
        id              uuid not null unique,
        type_name       varchar(255)
    );
    
    insert into dictionaries.organization_legal_types (id, type_name) values
    ('2d20f39e-e332-49e3-b45f-a49fcf0006d6', 'ТСЖ'),
    ('c3cd0010-3610-4263-bafa-8f6ba1e9bb5f', 'ЮЛ'),
    ('0b86ec05-ecc4-496f-a88a-67fb42a98b07', 'СНТ'),
    ('148dcc15-13e3-4677-a267-07ddceb15b30', 'СТН'),
    ('f21f4bf1-acbb-408d-9efb-94e67084e864', 'ТСН'),
    ('abb76d46-3120-43fc-b3d0-89b80d39ab5b', 'ИЖС'),
    ('9f94f519-1911-4a7e-815c-9ed40f75f217', 'МКД/ТСН'),
    ('87af06a0-5b5b-4ae9-b3b4-b1545c85e0cf', 'ИП'),
    ('1ebe2a79-b674-4336-a21b-40af600a1e2a', 'ДНП'),
    ('dbd024d7-87dd-443e-8b94-fdd487423330', 'ЖКС'),
    ('dc2c73e0-58bf-43cb-b75c-e25741eb4a57', 'МКД/ТСЖ'),
    ('7b7024b7-3e22-4383-97c0-752eefd7a95b', 'ИЖС/ТСН');
    
    create table dictionaries.operational_statuses
    (
        entry_id        bigserial primary key,
        id              uuid not null unique,
        status_name       varchar(255)
    );
    
    insert into dictionaries.operational_statuses (id, status_name) values
    ('d6e6e2a6-989c-4446-9c64-da534fbda03b', 'Активная'),
    ('1efb4e58-89c7-498c-8087-95126bbe7a51', 'Неактивная'),
    ('d3ee93a3-d37e-49ac-ad51-195101c4c516', 'Проектируемая');
    
    create table dictionaries.covering_types
    (
        entry_id        bigserial primary key,
        id              uuid not null unique,
        type_name       varchar(255)
    );
    
    insert into dictionaries.covering_types (id, type_name) values
    ('2f9e0dea-50e4-40b1-a182-216594d76024', 'Грунт'),
    ('33668d6c-ddf1-41cd-9bf8-de3907a8d728', 'Бетон');
    
    create table dictionaries.fence_materials
    (
        entry_id        bigserial primary key,
        id              uuid not null unique,
        material_name       varchar(255)
    );
    
    insert into dictionaries.fence_materials (id, material_name) values
    ('2c42e6b8-cdef-4b10-90ee-1d1fe3a6f222', 'Проф. лист'),
    ('d12f57ea-e687-4323-b6b1-2d3b0a54fc3a', 'Металл'),
    ('737ff861-8e77-46df-b42a-ca8206f5e7f7', 'Бетон');
    
    create table dictionaries.container_types
    (
        entry_id        bigserial primary key,
        id              uuid not null unique,
        type_name       varchar(255) not null,
        volume_limit_m3 numeric(10, 2)
    );
    
    insert into dictionaries.container_types (id, type_name, volume_limit_m3) values
    ('5f701da0-afc9-4d6d-809f-db5531cef21e', '0,75 м3 пластик', 1.5),
    ('b4ab2b45-e647-471b-90e4-7768ff0f4831', '6,00 м3 металл', 1.5),
    ('aba815af-dc2e-4265-b42c-6ecc87dd89c8', 'КГО', null);
    
    create table dictionaries.accrual_methods
    (
        entry_id        bigserial primary key,
        id              uuid not null unique,
        method_name       varchar(255)
    );
    
    insert into dictionaries.accrual_methods (id, method_name) values
    ('12ff4c0f-4897-4f3f-af55-37edcbf60803', 'Норматив');
    
    create table dictionaries.measurement_units
    (
        entry_id        bigserial primary key,
        id              uuid not null unique,
        unit_name       varchar(255)
    );
    
    insert into dictionaries.measurement_units (id, unit_name) values
    ('167d3dba-91a7-4a85-94ad-0aa9443e0732', 'м3');
    
    
    create table dictionaries.truck_fleet_types
    (
        entry_id        bigserial primary key,
        id              uuid not null unique,
        type_name       varchar(255)
    );
    
    insert into dictionaries.truck_fleet_types (id, type_name) values
    ('1964b722-2ce3-4a67-a948-26ebd2d0171b', 'Собственный'),
    ('d11a6833-cb25-438e-8eab-05f15329491f', 'Арендованный');
    
    """)


def downgrade():
    op.execute("""
    drop schema dictionaries cascade;
    """)
