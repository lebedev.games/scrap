"""scrap_90_add_truck_events

Revision ID: e2fb7b248346
Revises: 9152ef7678eb
Create Date: 2020-10-16 16:52:16.571403+00:00

"""
from alembic import op


# revision identifiers, used by Alembic.
revision = 'e2fb7b248346'
down_revision = '9152ef7678eb'
branch_labels = None
depends_on = None


def upgrade():
    op.execute("""
    create table truck_registered
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),

        registrar       uuid                     not null,
        date            timestamp with time zone not null
    );
    
    create table truck_owner_changed
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),

        registrar       uuid                     not null,
        date            timestamp with time zone not null,
        owner           varchar(255)             not null
    );
    
    create table truck_transporter_assigned
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),

        registrar       uuid                     not null,
        date            timestamp with time zone not null,
        transporter     uuid                     not null
    );
    
    create table truck_fleet_type_changed
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),

        registrar       uuid                     not null,
        date            timestamp with time zone not null,
        fleet_type      uuid                     not null
    );
    
    create table truck_vin_changed
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),

        registrar       uuid                     not null,
        date            timestamp with time zone not null,
        vin             varchar(255)             not null
    );
    
    create table truck_registration_number_changed
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),

        registrar           uuid                     not null,
        date                timestamp with time zone not null,
        registration_number varchar(255)             not null
    );
    
    create table truck_garaging_number_changed
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),

        registrar       uuid                     not null,
        date            timestamp with time zone not null,
        garaging_number varchar(255)             not null
    );
    
    create table truck_vehicle_model_changed
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),

        registrar       uuid                     not null,
        date            timestamp with time zone not null,
        vehicle_model   varchar(255)             not null
    );
    
    create table truck_waste_equipment_model_changed
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),

        registrar             uuid                     not null,
        date                  timestamp with time zone not null,
        waste_equipment_model varchar(255)             not null
    );
    
    create table truck_cargo_body_type_changed
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),

        registrar       uuid                     not null,
        date            timestamp with time zone not null,
        cargo_body_type uuid                     not null
    );
    
    create table truck_required_driver_license_changed
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),

        registrar       uuid                     not null,
        date            timestamp with time zone not null,
        category        uuid                     not null
    );
    
    create table truck_axle_configuration_changed
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),

        registrar           uuid                     not null,
        date                timestamp with time zone not null,
        axle_configuration  uuid                     not null
    );
    
        create table truck_production_year_changed
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),

        registrar       uuid                     not null,
        date            timestamp with time zone not null,
        production_year int                      not null
    );
    
    create table truck_navigation_equipment_name_changed
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),

        registrar                   uuid                     not null,
        date                        timestamp with time zone not null,
        navigation_equipment_name   varchar(255)             not null
    );
    
    create table truck_empty_weight_changed
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),

        registrar       uuid                     not null,
        date            timestamp with time zone not null,
        empty_weight_kg numeric(10, 2)           not null
    );
    
    create table truck_cargo_capacity_changed
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),

        registrar           uuid                     not null,
        date                timestamp with time zone not null,
        cargo_capacity_kg   numeric(10, 2)           not null
    );
    """)


def downgrade():
    op.execute("""
    drop table truck_registered;
    drop table truck_owner_changed;
    drop table truck_transporter_assigned;
    drop table truck_fleet_type_changed;
    drop table truck_vin_changed;
    drop table truck_registration_number_changed;
    drop table truck_garaging_number_changed;
    drop table truck_vehicle_model_changed;
    drop table truck_waste_equipment_model_changed;
    drop table truck_cargo_body_type_changed;
    drop table truck_required_driver_license_changed;
    drop table truck_axle_configuration_changed;
    drop table truck_production_year_changed;
    drop table truck_navigation_equipment_name_changed;
    drop table truck_empty_weight_changed;
    drop table truck_cargo_capacity_changed;
    """)
