"""scrap_47_add_generators_projection

Revision ID: b55380b5b91f
Revises: a5ce8684a4ca
Create Date: 2020-10-06 06:00:53.534916+00:00

"""
from alembic import op


# revision identifiers, used by Alembic.
revision = 'b55380b5b91f'
down_revision = 'a5ce8684a4ca'
branch_labels = None
depends_on = None


def upgrade():
    op.execute("""
    create table generators
    (
        entry_id          bigserial primary key,
        id                uuid         not null,
        legal_entity_type varchar(64)  not null,
        legal_entity_id   uuid         not null,
        name              varchar(255) not null,
        inn               varchar(255) not null,
        parent_id         uuid
    );
    """)


def downgrade():
    pass
