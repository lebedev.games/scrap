"""scrap_113_add_negotiation_events

Revision ID: 6006c4999acd
Revises: e2fb7b248346
Create Date: 2020-10-16 10:47:59.314045+00:00

"""
from alembic import op


# revision identifiers, used by Alembic.
revision = '6006c4999acd'
down_revision = 'e2fb7b248346'
branch_labels = None
depends_on = None


def upgrade():
    op.execute("""
    create table negotiation_started
    (
        entry_id               bigserial primary key,
        event_id               uuid                     not null default uuid_generate_v4(),
        timestamp              bigint                   not null default nextval('event_timestamp'),
        entity_entry_id        bigint                   not null references entities (entry_id),
    
        transporter            uuid                     not null,
        transportation         uuid                     not null,
        transportation_version int                      not null,
        date                   timestamp with time zone not null
    );
    
    create table negotiation_issues_declaration_stage_passed
    (
        entry_id               bigserial primary key,
        event_id               uuid                     not null default uuid_generate_v4(),
        timestamp              bigint                   not null default nextval('event_timestamp'),
        entity_entry_id        bigint                   not null references entities (entry_id),
    
        id                     uuid                     not null,
        contractor             uuid                     not null,
        transportation_version int                      not null,
        date                   timestamp with time zone not null
    );
    
    create table negotiation_transportation_issue_opened
    (
        entry_id        bigserial primary key,
        event_id        uuid   not null default uuid_generate_v4(),
        timestamp       bigint not null default nextval('event_timestamp'),
        entity_entry_id bigint not null references entities (entry_id),
    
        id              uuid   not null,
        declaration     uuid   not null,
        comment         text   not null
    );
    
    create table negotiation_transportation_issue_reopened
    (
        entry_id        bigserial primary key,
        event_id        uuid   not null default uuid_generate_v4(),
        timestamp       bigint not null default nextval('event_timestamp'),
        entity_entry_id bigint not null references entities (entry_id),
    
        id              uuid   not null,
        declaration     uuid   not null,
        comment         text   not null
    );
    
    create table negotiation_waste_volume_issue_opened
    (
        entry_id         bigserial primary key,
        event_id         uuid   not null default uuid_generate_v4(),
        timestamp        bigint not null default nextval('event_timestamp'),
        entity_entry_id  bigint not null references entities (entry_id),
    
        id               uuid   not null,
        declaration      uuid   not null,
        comment          text   not null,
        waste_collection uuid   not null,
        destination      uuid   not null
    );
    
    create table negotiation_waste_volume_issue_reopened
    (
        entry_id        bigserial primary key,
        event_id        uuid   not null default uuid_generate_v4(),
        timestamp       bigint not null default nextval('event_timestamp'),
        entity_entry_id bigint not null references entities (entry_id),
    
        id              uuid   not null,
        declaration     uuid   not null,
        comment         text   not null
    );
    
    create table negotiation_collection_start_time_issue_opened
    (
        entry_id        bigserial primary key,
        event_id        uuid   not null default uuid_generate_v4(),
        timestamp       bigint not null default nextval('event_timestamp'),
        entity_entry_id bigint not null references entities (entry_id),
    
        id              uuid   not null,
        declaration     uuid   not null,
        comment         text   not null,
        destination     uuid   not null
    );
    
    create table negotiation_collection_start_time_issue_reopened
    (
        entry_id        bigserial primary key,
        event_id        uuid   not null default uuid_generate_v4(),
        timestamp       bigint not null default nextval('event_timestamp'),
        entity_entry_id bigint not null references entities (entry_id),
    
        id              uuid   not null,
        declaration     uuid   not null,
        comment         text   not null
    );
    
    create table negotiation_collection_photo_issue_opened
    (
        entry_id         bigserial primary key,
        event_id         uuid   not null default uuid_generate_v4(),
        timestamp        bigint not null default nextval('event_timestamp'),
        entity_entry_id  bigint not null references entities (entry_id),
    
        id               uuid   not null,
        declaration      uuid   not null,
        comment          text   not null,
        waste_collection uuid   not null,
        destination      uuid   not null
    );
    
    create table negotiation_collection_photo_issue_reopened
    (
        entry_id        bigserial primary key,
        event_id        uuid   not null default uuid_generate_v4(),
        timestamp       bigint not null default nextval('event_timestamp'),
        entity_entry_id bigint not null references entities (entry_id),
    
        id              uuid   not null,
        declaration     uuid   not null,
        comment         text   not null
    );
    
    create table negotiation_issues_resolution_stage_passed
    (
        entry_id               bigserial primary key,
        event_id               uuid                     not null default uuid_generate_v4(),
        timestamp              bigint                   not null default nextval('event_timestamp'),
        entity_entry_id        bigint                   not null references entities (entry_id),
    
        id                     uuid                     not null,
        transporter            uuid                     not null,
        transportation_version int                      not null,
        date                   timestamp with time zone not null
    );
    
    create table negotiation_transportation_issue_resolved
    (
        entry_id        bigserial primary key,
        event_id        uuid   not null default uuid_generate_v4(),
        timestamp       bigint not null default nextval('event_timestamp'),
        entity_entry_id bigint not null references entities (entry_id),
    
        id              uuid   not null,
        resolution      uuid   not null,
        comment         text   not null
    );
    
    create table negotiation_transportation_issue_rejected
    (
        entry_id        bigserial primary key,
        event_id        uuid   not null default uuid_generate_v4(),
        timestamp       bigint not null default nextval('event_timestamp'),
        entity_entry_id bigint not null references entities (entry_id),
    
        id              uuid   not null,
        resolution      uuid   not null,
        comment         text   not null
    );
    
    create table negotiation_waste_volume_issue_resolved
    (
        entry_id        bigserial primary key,
        event_id        uuid           not null default uuid_generate_v4(),
        timestamp       bigint         not null default nextval('event_timestamp'),
        entity_entry_id bigint         not null references entities (entry_id),
    
        id              uuid           not null,
        resolution      uuid   not null,
        waste_volume_m3 numeric(10, 2) not null
    );
    
    create table negotiation_waste_volume_issue_rejected
    (
        entry_id        bigserial primary key,
        event_id        uuid   not null default uuid_generate_v4(),
        timestamp       bigint not null default nextval('event_timestamp'),
        entity_entry_id bigint not null references entities (entry_id),
    
        id              uuid   not null,
        resolution      uuid   not null,
        comment         text   not null
    );
    
    create table negotiation_collection_start_time_issue_resolved
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),
    
        id              uuid                     not null,
        resolution      uuid   not null,
        start_time      timestamp with time zone not null
    );
    
    create table negotiation_collection_start_time_issue_rejected
    (
        entry_id        bigserial primary key,
        event_id        uuid   not null default uuid_generate_v4(),
        timestamp       bigint not null default nextval('event_timestamp'),
        entity_entry_id bigint not null references entities (entry_id),
    
        id              uuid   not null,
        resolution      uuid   not null,
        comment         text   not null
    );
    
    
    create table negotiation_collection_photo_issue_resolved
    (
        entry_id        bigserial primary key,
        event_id        uuid   not null default uuid_generate_v4(),
        timestamp       bigint not null default nextval('event_timestamp'),
        entity_entry_id bigint not null references entities (entry_id),
    
        id              uuid   not null,
        resolution      uuid   not null,
        photos          uuid[] not null
    );
    
    create table negotiation_collection_photo_issue_rejected
    (
        entry_id        bigserial primary key,
        event_id        uuid   not null default uuid_generate_v4(),
        timestamp       bigint not null default nextval('event_timestamp'),
        entity_entry_id bigint not null references entities (entry_id),
    
        id              uuid   not null,
        resolution      uuid   not null,
        comment         text   not null
    );
    
    create table negotiation_completed
    (
        entry_id               bigserial primary key,
        event_id               uuid                     not null default uuid_generate_v4(),
        timestamp              bigint                   not null default nextval('event_timestamp'),
        entity_entry_id        bigint                   not null references entities (entry_id),
    
        contractor             uuid                     not null,
        transportation_version int                      not null,
        date                   timestamp with time zone not null
    );
    """)


def downgrade():
    pass
