"""scrap_119_add_negotiation_projection

Revision ID: 9e9089cb8f90
Revises: 0557f31eb4de
Create Date: 2020-10-18 07:20:15.917920+00:00

"""
from alembic import op


# revision identifiers, used by Alembic.
revision = '9e9089cb8f90'
down_revision = '0557f31eb4de'
branch_labels = None
depends_on = None


def upgrade():
    op.execute("""
    create table negotiations
    (
        entry_id               bigserial primary key,
        id                     uuid                     not null,
        transportation         uuid                     not null,
        transportation_version int                      not null,
        transportation_date    timestamp with time zone not null,
        truck                  varchar(64)              not null,
        started                timestamp with time zone not null,
        transporter            uuid                     not null,
        transporter_name       varchar(255)             not null,
        status                 varchar(64)              not null,
        district               varchar(64)              not null,
        consolidated           boolean                  not null default false
    );
    
    create table negotiations_timeline
    (
        entry_id               bigserial primary key,
        stage_id               uuid                     not null,
        negotiation_id         uuid                     not null,
        transportation_version int                      not null,
        timestamp              timestamp with time zone not null,
        status                 varchar(64)              not null
    )
    """)


def downgrade():
    pass
