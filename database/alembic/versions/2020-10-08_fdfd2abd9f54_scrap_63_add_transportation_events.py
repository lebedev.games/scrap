"""scrap_63_add_transportation_events

Revision ID: fdfd2abd9f54
Revises: b55380b5b91f
Create Date: 2020-10-08 08:16:00.580787+00:00

"""
from alembic import op


# revision identifiers, used by Alembic.
revision = 'fdfd2abd9f54'
down_revision = 'b55380b5b91f'
branch_labels = None
depends_on = None


def upgrade():
    op.execute("""
    create table transportation_registered
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),
    
        transporter     uuid                     not null,
        date            timestamp with time zone not null
    );
    
    create table transportation_submitted
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),
    
        transporter     uuid                     not null,
        date            timestamp with time zone not null
    );
    
    create table transportation_deleted
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),
    
        transporter     uuid                     not null,
        date            timestamp with time zone not null
    );
    
    create table transportation_truck_assigned
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),
    
        transporter     uuid                     not null,
        date            timestamp with time zone not null,
        truck           uuid                     not null,
        execution_date  timestamp with time zone not null
    );
    
    create table transportation_destination_added
    (
        entry_id               bigserial primary key,
        event_id               uuid                     not null default uuid_generate_v4(),
        timestamp              bigint                   not null default nextval('event_timestamp'),
        entity_entry_id        bigint                   not null references entities (entry_id),
    
        id                     uuid                     not null,
        transporter            uuid                     not null,
        date                   timestamp with time zone not null,
        collection_start_time  timestamp with time zone not null,
        waste_generation_point uuid                     not null
    );
    
    create table transportation_collection_start_time_changed
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),
    
        destination     uuid                     not null,
        transporter     uuid                     not null,
        date            timestamp with time zone not null,
        start_time      timestamp with time zone not null
    );
    
    create table transportation_waste_generation_point_changed
    (
        entry_id         bigserial primary key,
        event_id         uuid                     not null default uuid_generate_v4(),
        timestamp        bigint                   not null default nextval('event_timestamp'),
        entity_entry_id  bigint                   not null references entities (entry_id),
    
        destination      uuid                     not null,
        transporter      uuid                     not null,
        date             timestamp with time zone not null,
        generation_point uuid                     not null
    );
    
    create table transportation_destination_removed
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),
    
        id              uuid                     not null,
        transporter     uuid                     not null,
        date            timestamp with time zone not null
    );
    
    create table transportation_waste_collected
    (
        entry_id              bigserial primary key,
        event_id              uuid                     not null default uuid_generate_v4(),
        timestamp             bigint                   not null default nextval('event_timestamp'),
        entity_entry_id       bigint                   not null references entities (entry_id),
    
        id                    uuid                     not null,
        transporter           uuid                     not null,
        date                  timestamp with time zone not null,
        destination           uuid                     not null,
        container_type        uuid                     not null,
        container_volume      numeric(10, 2)           not null,
        container_utilization numeric(10, 2)           not null,
        waste_volume_m3       numeric(10, 2)           not null
    );
    
    create table transportation_waste_volume_changed
    (
        entry_id         bigserial primary key,
        event_id         uuid                     not null default uuid_generate_v4(),
        timestamp        bigint                   not null default nextval('event_timestamp'),
        entity_entry_id  bigint                   not null references entities (entry_id),
    
        waste_collection uuid                     not null,
        transporter      uuid                     not null,
        date             timestamp with time zone not null,
        waste_volume_m3  numeric(10, 2)           not null
    );
    
    create table transportation_container_changed
    (
        entry_id         bigserial primary key,
        event_id         uuid                     not null default uuid_generate_v4(),
        timestamp        bigint                   not null default nextval('event_timestamp'),
        entity_entry_id  bigint                   not null references entities (entry_id),
    
        waste_collection uuid                     not null,
        transporter      uuid                     not null,
        date             timestamp with time zone not null,
        container_type   uuid                     not null,
        container_volume numeric(10, 2)           not null
    );
    
    create table transportation_container_utilization_changed
    (
        entry_id              bigserial primary key,
        event_id              uuid                     not null default uuid_generate_v4(),
        timestamp             bigint                   not null default nextval('event_timestamp'),
        entity_entry_id       bigint                   not null references entities (entry_id),
    
        waste_collection      uuid                     not null,
        transporter           uuid                     not null,
        date                  timestamp with time zone not null,
        container_utilization numeric(10, 2)           not null
    );
    
    create table transportation_waste_collection_photographed
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),
    
        id              uuid                     not null,
        transporter     uuid                     not null,
        date            timestamp with time zone not null,
        photos          uuid[]                   not null
    );
    
    create table transportation_waste_collection_revoked
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),
    
        id              uuid                     not null,
        transporter     uuid                     not null,
        date            timestamp with time zone not null
    );
    """)


def downgrade():
    pass
