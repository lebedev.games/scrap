"""scrap_59_add_transporter_projection

Revision ID: 888128195394
Revises: 637fc1e5422a
Create Date: 2020-10-13 11:58:13.794091+00:00

"""
from alembic import op

# revision identifiers, used by Alembic.
revision = '888128195394'
down_revision = '811534581880'
branch_labels = None
depends_on = None


def upgrade():
    op.execute("""
    create table transporters
    (
        entry_id          bigserial primary key,
        id                uuid         not null unique,
        legal_entity_type varchar(64)  not null,
        legal_entity_id   uuid         not null
    );

    create table transporters_truck_fleet_types
    (
        entry_id            bigserial primary key,
        transporter_id      uuid        not null references transporters (id),
        truck_fleet_type_id uuid        not null references dictionaries.truck_fleet_types (id)
    );
    
    create table transporters_legal_entities (
        entry_id            bigserial       primary key,
        legal_entity_id     uuid            not null unique,
        inn                 varchar(64)     not null,
        name                varchar(255)    not null
    );
    """)


def downgrade():
    pass
