"""scrap_51_add_generation_point_events

Revision ID: 028c0dcd6a30
Revises: 384173cddfea
Create Date: 2020-10-08 20:53:02.834199+00:00

"""
from alembic import op

# revision identifiers, used by Alembic.
revision = '028c0dcd6a30'
down_revision = '384173cddfea'
branch_labels = None
depends_on = None


def upgrade():
    op.execute("""
    create table generation_point_registered
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),

        registrar       uuid                     not null,
        date            timestamp with time zone not null
    );

    create table generation_point_internal_identification_changed
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),

        registrar       uuid                     not null,
        date            timestamp with time zone not null,
        system_number   varchar                  not null
    );

    create table generation_point_external_registration_changed
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),

        registrar             uuid                     not null,
        date                  timestamp with time zone not null,
        registry_number   varchar                  not null
    );

    create table generation_point_waste_type_changed
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),

        registrar       uuid                     not null,
        date            timestamp with time zone not null,
        waste_type      uuid                     not null references dictionaries.waste_types(id)
    );

    create table generation_point_organization_legal_type_changed
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),

        registrar                    uuid                     not null,
        date                         timestamp with time zone not null,
        organization_legal_type      uuid                     not null references dictionaries.organization_legal_types(id)
    );

    create table generation_point_operational_status_changed
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),

        registrar                    uuid                     not null,
        date                         timestamp with time zone not null,
        status           uuid                     not null references dictionaries.operational_statuses(id)
    );

    create table generation_point_address_changed
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),

        registrar              uuid                        not null,
        date                   timestamp with time zone    not null,
        full_address           varchar                     not null
    );

    create table generation_point_coordinates_changed
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),

        registrar             uuid                     not null,
        date                  timestamp with time zone not null,
        coordinates           varchar                  not null
    );

    create table generation_point_area_changed
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),

        registrar        uuid                       not null,
        date             timestamp with time zone   not null,
        area_m2          numeric(10, 2)             not null
    );

    create table generation_point_covering_type_changed
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),

        registrar               uuid                     not null,
        date                    timestamp with time zone not null,
        covering_type           uuid                     not null references dictionaries.covering_types(id)
    );

    create table generation_point_fence_material_changed
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),

        registrar               uuid                     not null,
        date                    timestamp with time zone not null,
        fence_material          uuid                     not null references dictionaries.fence_materials(id)
    );

    create table generation_point_service_company_changed
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),

        registrar        uuid                       not null,
        date             timestamp with time zone   not null,
        service_company_name  varchar                    not null
    );

    create table generation_point_generator_assigned
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),

        registrar        uuid                       not null,
        date             timestamp with time zone   not null,
        generator        uuid                    not null
    );

    create table generation_point_description_changed
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),

        registrar        uuid                       not null,
        date             timestamp with time zone   not null,
        description      varchar                    not null
    );

    create table generation_point_container_added
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),

        registrar        uuid                       not null,
        date             timestamp with time zone   not null,
        container_type   uuid                       not null references dictionaries.container_types(id)
    );

    create table generation_point_container_removed
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),

        registrar        uuid                       not null,
        date             timestamp with time zone   not null,
        container_type   uuid                       not null references dictionaries.container_types(id)
    );

    create table generation_point_transporter_assigned
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),

        registrar                       uuid                       not null,
        date                            timestamp with time zone   not null,
        transporter                     uuid                       not null,
        start_transportation_date       timestamp with time zone   not null,
        end_transportation_date         timestamp with time zone   not null
    );

    create table generation_point_transportation_frequency_changed
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),

        registrar        uuid                       not null,
        date             timestamp with time zone   not null,
        frequency        varchar                    not null
    );

    create table generation_point_photos_changed
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),

        registrar        uuid                       not null,
        date             timestamp with time zone   not null,
        photos           uuid[]                     not null
    );

    create table generation_point_accrual_method_changed
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),

        registrar        uuid                       not null,
        date             timestamp with time zone   not null,
        method           uuid                       not null references dictionaries.accrual_methods(id)
    );

    create table generation_point_measurement_unit_changed
    (
        entry_id        bigserial primary key,
        event_id        uuid                     not null default uuid_generate_v4(),
        timestamp       bigint                   not null default nextval('event_timestamp'),
        entity_entry_id bigint                   not null references entities (entry_id),

        registrar        uuid                       not null,
        date             timestamp with time zone   not null,
        unit           uuid                         not null references dictionaries.measurement_units(id)
    );
    """)



def downgrade():
    op.execute("""
    drop table generation_point_registered;
    drop table generation_point_internal_identification_changed;
    drop table generation_point_external_registration_changed;
    drop table generation_point_waste_type_changed;
    drop table generation_point_organization_legal_type_changed;
    drop table generation_point_operational_status_changed;
    drop table generation_point_address_changed;
    drop table generation_point_coordinates_changed;
    drop table generation_point_area_changed;
    drop table generation_point_covering_type_changed;
    drop table generation_point_fence_material_changed;
    drop table generation_point_service_company_changed;
    drop table generation_point_generator_assigned;
    drop table generation_point_description_changed;
    drop table generation_point_container_added;
    drop table generation_point_container_removed;
    drop table generation_point_transporter_assigned;
    drop table generation_point_transportation_frequency_changed;
    drop table generation_point_photos_changed;
    drop table generation_point_accrual_method_changed;
    drop table generation_point_measurement_unit_changed;
    """)
