"""set up database

Revision ID: 468b569df431
Revises: 
Create Date: 2020-09-17 09:15:25.329610+00:00

"""
from alembic import op

# revision identifiers, used by Alembic.
revision = '468b569df431'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.execute("""
    CREATE TABLE users
    (
        entry_id       bigserial primary key,
        id             uuid          not null,
        permissions    varchar(64)[] not null default '{}',
        role           varchar(255)  not null,
        role_entity_id uuid,
        login          varchar(255),
        password       varchar(255),
        name           varchar(255)  not null
    );
    """)


def downgrade():
    pass
